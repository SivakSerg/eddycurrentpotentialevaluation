/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "Operator_B_num.h"

complex<double> Operator_B_num::Det(complex<double> M[][2], uint size)
{
	return M[0][0] * M[1][1] - M[0][1] * M[1][0];
}
VectorXD<complex<double>,3> Operator_B_num::vector_mult(VectorXD<complex<double>,3> &a, VectorXD<double,3> &b)
{
	VectorXD<complex<double>,3> Mult[2];
	Mult[0] = a;
	for(uint i=0; i<3; i++)
		Mult[1].coords[i] = b.coords[i];
	return VectorXD<complex<double>,3>::vector_mult(Mult,Det);
}
bool Operator_B_num::operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy)
{
	if(!fx || !fy )
		return false;
	this->fx = fx;
	this->fy = fy;
	return true;
}
complex<double> Operator_B_num::GelmSolv(VectorXD<double,3> x, VectorXD<double,3> y)
{
	complex<double> ret;
	double r;
	r = (x - y).norma();
	ret = exp( -vave_num_k * r ) / ( 4 * PI * r );
	return ret;
}
VectorXD<complex<double>,3> Operator_B_num::GradXGelmSolv(VectorXD<double,3> x, VectorXD<double,3> y)
{
	VectorXD<complex<double>,3> grad;	
	VectorXD<double,3> acc;
	double r;
	acc = x - y;
	r = acc.norma();
	for(uint i=0; i<3; i++)
		grad.coords[ i ] = acc.coords[ i ];
	grad = grad * exp(-vave_num_k * r) * (-1.) * ( vave_num_k / (4 * PI * r * r) + 1. / (4 * PI * pow(r,3) ) );
	return grad;
}
VectorXD<complex<double>,3> Operator_B_num::evalf_y_int(VectorXD<double,3> x)
{
	VectorXD<complex<double>,3> res, acc;
	VectorXD<double,3> y, loc_coord;
	VectorXD<double,2> gauss_points;
	complex<double> area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
	vectCache_elem fyCache;
	res.common_init(0.);
	while(tgr_y.next())
	{
		fyCache = fy->get_cache_value( gauss_points );
		// точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr_y.e.e1;
		gauss_points.coords[ 1 ] = tgr_y.e.e2;
		//
		y = fyCache.glob_arg;	//fy->get_Global_point(gauss_points);
		//
		// в этих точках вычисляем интегрируемую функцию
		acc = GradXGelmSolv(x, y) * (area * tgr_y.w);
		// векторное умножение градиента на векторную функцию		
		//acc = vector_mult(acc,fy->current_parameter.S * fyCache.lam_val/*fy->lambda(gauss_points)*/);		
		VectorXD<double,3> s_mult;
		s_mult = fy->current_parameter.S * fyCache.lam_val;
		fast_vector_mutl(acc,s_mult,acc);
		// добавляем новое слагаемое к результату
		res = res + acc;
	}
	return res;
}
complex<double> Operator_B_num::evalf_operator(std::complex<double> vave_num_k)
{
	complex<double> ret, area;
	vectCache_elem fxCache;
	ret = 0.;
	if(!fx || !fy)
		ret = 0.;
	// считаем площадь треугольника
	area = step = fx->current_parameter.s_t * fx->current_parameter.t_k * 0.5;
	step *= 1e-8;
	this->vave_num_k = vave_num_k;
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double,2> gauss_points;
	VectorXD<double,3> local_u_val, global_u_val, n;
	double normal_proj;
	complex<double> acc;
	VectorXD<complex<double>,3> vect_acc;
	n = fx->current_parameter.n;
	while( tgr_x.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr_x.e.e1;
		gauss_points.coords[ 1 ] = tgr_x.e.e2;
		// в этих точках вычисляем функцию lambda
		fxCache = fx->get_cache_value( gauss_points );
		//
		local_u_val = fxCache.u_val;	// fx->u_tangen( gauss_points ); // в локальных координатах треугольника Ty
		//
		global_u_val = fx->current_parameter.S * local_u_val; // в глобальных координатах (в которых задавались точки сетки)
		// вычисляем внутренний интеграл
		vect_acc = evalf_y_int( fxCache.glob_arg/*fx->get_Global_point(gauss_points)*/ );
		vect_acc = vector_mult( vect_acc, n);
		// вычисляем скалярное произведение базисной функции и векторного произведения
		acc = 0.;
		for(uint i=0; i<3; i++)
			acc += vect_acc.coords[ i ] * global_u_val.coords[ i ];
		ret += acc * tgr_x.w * area;
	}
	return ret;
}
