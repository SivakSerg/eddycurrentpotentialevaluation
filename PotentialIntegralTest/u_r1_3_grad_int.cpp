/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "u_r1_3_grad_int.h"

double u_r1_3_grad_int::get_numeric_integral_r1_3(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp)
{
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    uint coord_nums[2];
    init_params = true;
    mdlp.set_trg_params(Tx);
    Ty_params Ty_points;
    build_Ty_params(Tx,Ty,Ty_points);
    get_integr_coord_num(Ty_points, coord_nums);
    lines_Ty_calc_params(Ty_points, coord_nums[0], coord_nums[1]);
    this->zero = sqrt(Ty.s_t * Ty.t_k * 0.5) * 1e-8;
    // вначале вычисляем полностью численный интеграл (с вычитанием особенности)
    double numeric_part = pure_numeric_integral(gr, mdlp, Ty_points);
    double analytical_part;
    analytical_part = 0.;
    double interval_1[2], interval_2[2];
    interval_1[0] = Ty_points.vertexes_Tx[0].coords[ coord_nums[0] ];
    interval_1[1] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];  
    interval_2[0] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];
    interval_2[1] = Ty_points.vertexes_Tx[2].coords[ coord_nums[0] ];    
    Integral integr;
    if(abs(interval_1[1] - interval_1[0]) > zero)
        analytical_part = analytical_part + integr.get_value(interval_1[0],interval_1[1],2,*this);
    if(abs(interval_2[1] - interval_2[0]) > zero)
        analytical_part = analytical_part + integr.get_value(interval_2[0],interval_2[1],2,*this); 
    double ret = analytical_part + numeric_part;
    return ret;
}

double u_r1_3_grad_int::operator()(double a)
{    
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    double limits[2];       
    if(init_params)
    {
        uint coord_nums[2];
        init_params = false;
        build_Ty_params(Tx,Ty,Ty_points);
        get_integr_coord_num(Ty_points, coord_nums);
        integr_num_queue[0] = Ty_points.non_integr_coord_num;
        integr_num_queue[1] = coord_nums[0];
        integr_num_queue[2] = coord_nums[1];
        get_surface_coefs(Tx,Ty_points,surface_coefs);   
        lines_Ty_calc_params(Ty_points, integr_num_queue[1], integr_num_queue[2]);
    }
    double function_value; // значение функции в точке с особенностью (не важно, какой предел интегрирования выбран, верхний или нижний, функция заменит его на аргумент, с соответствующей особенностью)       
    get_analytical_limits(Ty_points, a, integr_num_queue[1], limits);    
    VectorXD<double,3> start_point, end_point;
    double t_star, alpha2, alpha1, t_vave, st;
    double res;
    t_star = Tx.t_star;
    t_vave = Tx.t_k;
    alpha2 = Tx.tg_a2;
    alpha1 = Tx.tg_a1;
    st = Tx.s_t;
    res = 0;
    start_point = get_x_local_coord(integr_num_queue, surface_coefs, a, limits[0]);
    end_point = get_x_local_coord(integr_num_queue, surface_coefs, a, limits[1]);
    double sy_start, ty_start, uy_start, sy_end, ty_end, uy_end;
    sy_start = start_point.coords[0];
    ty_start = start_point.coords[1];
    uy_start = start_point.coords[2];
    sy_end = end_point.coords[0];
    ty_end = end_point.coords[1];
    uy_end = end_point.coords[2];
    function_value = basis_function_combination(start_point, Ty_points.non_integr_coord_num, surface_coefs, true);
    switch(Ty_points.non_integr_coord_num)
    {
        case 2:
            sy_start = max( start_point.coords[0], end_point.coords[0] );
            sy_end = min( start_point.coords[0], end_point.coords[0] );
            if(abs(alpha1) > 1e-8)
            res += (r1_3_func_case_A(alpha1, 0., st, ty_start, sy_start, surface_coefs) -
                    r1_3_func_case_A(alpha1, -t_star, st, ty_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_A(alpha1, 0., st, ty_end, sy_end, surface_coefs) -
                    r1_3_func_case_A(alpha1, -t_star, st, ty_end, sy_end, surface_coefs));
            if(abs(alpha2) > 1e-8)
            res += (r1_3_func_case_A(alpha2, t_vave-t_star, st, ty_start, sy_start, surface_coefs) -
                    r1_3_func_case_A(alpha2, 0., st, ty_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_A(alpha2, t_vave-t_star, st, ty_end, sy_end, surface_coefs) -
                    r1_3_func_case_A(alpha2, 0., st, ty_end, sy_end, surface_coefs));           	
            break;
        case 1:
			uy_start = max( start_point.coords[2], end_point.coords[2] );
            uy_end = min( start_point.coords[2], end_point.coords[2] );
            if(abs(alpha1) > 1e-8)
            res += (r1_3_func_case_C(alpha1, 0., st, uy_start, sy_start, surface_coefs) -
                    r1_3_func_case_C(alpha1, -t_star, st, uy_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_C(alpha1, 0., st, uy_end, sy_end, surface_coefs) -
                    r1_3_func_case_C(alpha1, -t_star, st, uy_end, sy_end, surface_coefs));
            if(abs(alpha2) > 1e-8)
            res += (r1_3_func_case_C(alpha2, t_vave-t_star, st, uy_start, sy_start, surface_coefs) -
                    r1_3_func_case_C(alpha2, 0., st, uy_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_C(alpha2, t_vave-t_star, st, uy_end, sy_end, surface_coefs) -
                    r1_3_func_case_C(alpha2, 0., st, uy_end, sy_end, surface_coefs));           	
            break;
        case 0:
            uy_start = max( start_point.coords[2], end_point.coords[2] );
            uy_end = min( start_point.coords[2], end_point.coords[2] );
            if(abs(alpha1) > 1e-8)
            res += (r1_3_func_case_B(alpha1, 0., st, ty_start, uy_start, surface_coefs) -
                    r1_3_func_case_B(alpha1, -t_star, st, ty_start, uy_start, surface_coefs)) -
                    
                    (r1_3_func_case_B(alpha1, 0., st, ty_end, uy_end, surface_coefs) -
                    r1_3_func_case_B(alpha1, -t_star, st, ty_end, uy_end, surface_coefs));
            if(abs(alpha2) > 1e-8)
            res += (r1_3_func_case_B(alpha2, t_vave-t_star, st, ty_start, uy_start, surface_coefs) -
                    r1_3_func_case_B(alpha2, 0., st, ty_start, uy_start, surface_coefs)) -
                    
                    (r1_3_func_case_B(alpha2, t_vave-t_star, st, ty_end, uy_end, surface_coefs) -
                    r1_3_func_case_B(alpha2, 0., st, ty_end, uy_end, surface_coefs));           	
            break;
    }    
    res /= fabs(Ty_points.ny.coords[ Ty_points.non_integr_coord_num ]);
    double ret = function_value*res;    
    return ret;
}

VectorXD<double,3> u_r1_3_grad_int::get_x_local_coord(uint integr_num_queue[], double coefs[], double a, double limit)
{
    double rest_coord = coefs[0] * a + // координата, по которой проводится численное интегрирование
                        coefs[1] * limit + // координата, по которой проводилось аналитическое интегрирование
                        coefs[2]; // свободный член линейной функции недостающей координаты для получения точки
    VectorXD<double,3> point;
    point.coords[integr_num_queue[0]] = rest_coord; // исключённая координата
    point.coords[integr_num_queue[1]] = a;
    point.coords[integr_num_queue[2]] = limit;
    return point;
}

void u_r1_3_grad_int::lines_Ty_calc_params(Ty_params &Ty, uint num_integr_coord, uint analytic_integr_coord)
{
    Vector_predicate pred(num_integr_coord);
    sort(Ty.vertexes_Tx, Ty.vertexes_Tx+3, pred); // сортируем точки по параметру интегрирования
    uint s_num = analytic_integr_coord, t_num = num_integr_coord;
    // теперь выражаем линейную зависимость координаты, по которой производится аналитическое интегрирование от той координаты,
    // по которой интегрирование проводится численно.
    Ty.line_coefs[0] = (Ty.vertexes_Tx[2].coords[s_num] - Ty.vertexes_Tx[0].coords[s_num]) / (Ty.vertexes_Tx[2].coords[t_num] - Ty.vertexes_Tx[0].coords[t_num]);
    Ty.line_coefs[1] = (Ty.vertexes_Tx[1].coords[s_num] - Ty.vertexes_Tx[0].coords[s_num]) / (Ty.vertexes_Tx[1].coords[t_num] - Ty.vertexes_Tx[0].coords[t_num]);
    Ty.line_coefs[2] = (Ty.vertexes_Tx[2].coords[s_num] - Ty.vertexes_Tx[1].coords[s_num]) / (Ty.vertexes_Tx[2].coords[t_num] - Ty.vertexes_Tx[1].coords[t_num]);
    Ty.t0[0] = Ty.vertexes_Tx[0].coords[t_num];
    Ty.t0[1] = Ty.vertexes_Tx[0].coords[t_num];
    Ty.t0[2] = Ty.vertexes_Tx[1].coords[t_num];
    Ty.s0[0] = Ty.vertexes_Tx[0].coords[s_num];
    Ty.s0[1] = Ty.vertexes_Tx[0].coords[s_num];
    Ty.s0[2] = Ty.vertexes_Tx[1].coords[s_num];
}

// по каждому значению численного интеграла a определяем пределы интегрирования
void u_r1_3_grad_int::get_analytical_limits(Ty_params &Ty, double a, uint num_integr_coord, double limits[])
{
    uint int_case;
    uint t_num = num_integr_coord;
    if( (a > Ty.vertexes_Tx[0].coords[t_num]) && (a < Ty.vertexes_Tx[1].coords[t_num]) )
        int_case = 1;
    else
        int_case = 2;
    // предполагаем, что параметр a всегда находится в пределах между
    // Ty.vertexes_Tx[0].coords[t_num] и Ty.vertexes_Tx[2].coords[t_num]
    switch(int_case)
    {
        case 1:
            limits[0] = Ty.line_coefs[0] * (a - Ty.t0[0]) + Ty.s0[0];
            limits[1] = Ty.line_coefs[1] * (a - Ty.t0[1]) + Ty.s0[1];
            break;
        case 2:
            limits[0] = Ty.line_coefs[0] * (a - Ty.t0[0]) + Ty.s0[0];
            limits[1] = Ty.line_coefs[2] * (a - Ty.t0[2]) + Ty.s0[2];
            break;
        default:
            break;
    }
    sort(limits,limits+2); // эти пределы интегрирования так же упорядочены по ворзрастанию (интегрируем по площади).
}

double u_r1_3_grad_int::pure_numeric_integral(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty)
{
    TrigGaussRuleInfo tgri(gr);
    VectorXD<double,2> gauss_points;
    double acc, ret;
    ret = 0.;
    double area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    mdlp.set_trg_params(fx->current_parameter);
    while( tgri.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgri.e.e1;
        gauss_points.coords[ 1 ] = tgri.e.e2;
        // в этих точках вычисляем интегрируемую функцию
        acc = numeric_integrand(gauss_points, mdlp, Ty) * area;
        // в отличие от area, параметр весов w может менятся на итерациях
        acc = acc * tgri.w;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}
double u_r1_3_grad_int::numeric_integrand(VectorXD<double,2> &Gauss_point, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty_points)
{
    triangle_parameters &Tx = fx->current_parameter;    
    triangle_parameters &Ty = fy->current_parameter;
    VectorXD<double,3> glob_arg, x_arg, x_arg_special;
    // вычисляем аргумент в локальных координатах треугольника Tx
    glob_arg = Ty.get_Global_point(Gauss_point); // интегрирование ведётся по треугольнику Ty, точки выбираются на нём!
    x_arg = Tx.get_local_coordinate_point(glob_arg); // затем точка переводится в координаты треугольника Tx
    x_arg_special = x_arg; // x_arg_special - дублирует функционал basis_function_combination с той разницей, что здесь надо определить расстояние до точки, в которой вычисляется аргумент
    double surface_coefs[3];
    get_surface_coefs(Tx, Ty_points, surface_coefs);
    double sy, ty, uy, A, B, C;
    A = surface_coefs[0];
    B = surface_coefs[1];
    C = surface_coefs[2];
    switch(Ty_points.non_integr_coord_num)
    {
        case 0:
            x_arg_special.coords[2] = 0.; // интегрирование по ty, uy, uy = 0.            
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[0] = A * ty + B * uy + C;
            break;
        case 1:
            x_arg_special.coords[2] = 0.; // интегрирование по uy, sy, sy = st    
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[1] = A * sy + B * uy + C;
            break;
        case 2:
            x_arg_special.coords[0] = Tx.s_t; // интегрирование по ty, sy, sy = st
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[2] = A * ty + B * sy + C;
            break;
        default:
            break;
    }
    // вычисляем необходимую нам комбинацию
    double a_value = basis_function_combination(x_arg,Ty_points.non_integr_coord_num, surface_coefs,false);
    double b_value = basis_function_combination(x_arg,Ty_points.non_integr_coord_num, surface_coefs,true); // выделяем особенность
    double delta = a_value - b_value;
    double distance;
    distance = (x_arg - x_arg_special).norma();
    if(abs(distance) < zero)
        return 0.;
    double u_r1_3_grad_int_mdlp = mdlp.r1_3_component(x_arg)*x_arg.coords[2];
    return delta*u_r1_3_grad_int_mdlp;
}
// функция, используемая в методе вычитания особенности
double u_r1_3_grad_int::basis_function_combination(VectorXD<double,3> loc_x_arg, uint non_integr_num, double surface_coefs[], bool singularity_point)
{
    triangle_parameters &Tx = fx->current_parameter;
    VectorXD<double,3> arg;
    VectorXD<double,3> ret;
    arg = loc_x_arg;   
    // arg - критическая точка с особенностью, в нашем случае это множество с особенностью при sy = st и uy = 0
    // поэтому внешний интеграл оставляем по произвольной переменной (ty или uy), а особенность определяем для координаты st = sy
    // можно было определить особенность для координаты uy.
    double A, B, C;
    double sy, ty, uy;
    A = surface_coefs[0];
    B = surface_coefs[1];
    C = surface_coefs[2];
    if(singularity_point) // в этом случае интегрирование проводится с одной степенью свободы и один из параметров связан значением в точке сингулярности
    switch(non_integr_num)
    {
        case 0:
            arg.coords[2] = 0.; // интегрирование по ty, uy, uy = 0.            
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[0] = A * ty + B * uy + C;
            break;
        case 1:
            arg.coords[2] = 0.; // интегрирование по uy, sy, sy = st    
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[1] = A * sy + B * uy + C;
            break;
        case 2:
            arg.coords[0] = Tx.s_t; // интегрирование по ty, sy, sy = st
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[2] = A * ty + B * sy + C;
            break;
        default:
            break;
    }
    Block<double,2> grad;
    VectorXD<double,3> local_lambda_val, global_lambda_val, loc_y_arg, glob_y_arg;
    // градиент вычисляется в локальных координатах треугольника Tx.
    grad = fx->gradient_u_tang(arg); // градиент возвращается в локальных координатах треугольника, на котором определена функция
    ret.coords[0] = grad.block[0][0];
    ret.coords[1] = grad.block[1][0];   
    ret.coords[2] = 0.;
    glob_y_arg = fx->current_parameter.get_global_coordinate_point(arg);
    loc_y_arg = fy->current_parameter.get_local_coordinate_point(glob_y_arg);
    local_lambda_val = fy->lambda(loc_y_arg); // fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
    global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
    local_lambda_val = fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
    // теперь, когда и градиент и lambda переведены в одни координаты вычислим скалярное произведение
    double dot_product = local_lambda_val.dotprod(ret);
    return dot_product;
}

double u_r1_3_grad_int::F_arctan_2_mult(double A1,double A2,double arg)
{
	double disc = A2 * A2 - 4 * A1; // функция в любом случае действительная, она либо гиперболическая, либо триганометрическая
	double res=0., x;
        if(sqrt(fabs(disc)) < zero)
        {
            A1 += A1 / 1e+6 + 10*zero;
            A2 += A2 / 1e+6 + 10*zero;
            disc = A2 * A2 - 4 * A1;
        }
	if(disc < 0)
	{
		res = -atan( (2 * arg + A2) / sqrt( -disc ) ) * sqrt(-disc);
	}
	if(disc > 0)
	{
		x = 2 * arg + A2;
		x /= sqrt(disc);
		res = -0.5 * log( abs((1 + x)/(1 - x)) ) * sqrt(disc);                                
	}
	return res;
}

double u_r1_3_grad_int::F_arctan(double A1,double A2,double arg)
{
    	double disc = A2 * A2 - 4 * A1; // функция в любом случае действительная, она либо гиперболическая, либо триганометрическая
	double res, x;
	if(disc < 0)
	{
		res = -atan( (2 * arg + A2) / sqrt( -disc ) ) / sqrt(-disc);
	}
	if(disc > 0)
	{
		x = 2 * arg + A2;
		x /= sqrt(disc);
		res = 0.5 * log( abs((1 + x)/(1 - x)) )/sqrt(disc);                                
	}
	return res;
}
// четыре аргумента вместо трёх, но схема вычисления та же самая
double u_r1_3_grad_int::F_arctan(double V, double L, double D, double x)
{
	double disc = V*V*D - D + L*L;
	double arg;
	double ret = 0.;
        if(fabs(sqrt(disc)) < zero)
        {
            V += V / 1e+6 + 100 * zero;
            L += L / 1e+6 + 100 * zero;
            D += D / 1e+6 + 100 * zero;
        }
        disc = V*V*D - D + L*L;
        arg = (V + 1) * x + L;
	if(disc < 0.)
	{
		ret = atan( arg / sqrt(-disc) ) * sqrt(-disc);
	}
	if(disc > 0.)
	{
		double lg_arg = arg / sqrt(disc);
                if(fabs(1 - lg_arg) < zero)
                    return 0.;
		ret = 0.5 * log( fabs((1 + lg_arg) / (1 - lg_arg)) ) * sqrt(disc);
	}
	return ret;
}

double u_r1_3_grad_int::log_doub_area_int(double V, double L, double D, double C, double x)
{
    double res2;
    // определяем вспомогательные константы
    double A1, A2, u, t, disc;
    A1 = D * (1 - V) / (1 + V);
    A2 = 2 * L / (1 + V);
    u = x;
    t = u + sqrt(fabs(u * u + D));
    // непосредственно вычисляем интегральное выражение
    disc = A2 * A2 - 4 * A1;
    double res_ex;
    // Вначале разберём частные случаи
    if(fabs(D) > zero)
    {
        if(fabs(V - 1) < 1e-8) // случай V == 1 возможен
        {
            if(fabs(L) > zero)
                return 0.5 * C * ( (t + L) * log(t + L) - (t + L) ) - D * C * 0.5 * (log(t + L) / t - log(t / (t + L)) / L);
            else
                return 0.5 * C * ( (t + L) * log(t + L) - (t + L) ) - D * C * 0.5 * (log(t + L) / t + 1/t);
        }
        if(fabs(V + 1) < 1e-8)
        {
            // V == -1
            t = -u + sqrt( fabs(u*u + D) );
            if(fabs(L) > zero)
            {
                double I = -t + L * log(fabs(t + L)) + (D / L) * log(fabs(t)) - (D / L) * log(fabs(t + L));
                double ret = (log(t + L)*u - 0.5 * I)*C;
                return ret;
            }
            else
            {
                double ret = u*log(t) + D / (2 * t) + t / 2.;
                return ret*C;
            }
            
        }
        // в противном случае рассматривается более общий случай
    }
    else
    {
        if(fabs(L) < zero)
        {
            double arg = fabs(V * u + L + sqrt(fabs(u*u + D)));
            if(fabs(arg) < zero)
            {   
                if(fabs(u) < zero)
                    return (- 0.5 * t)*C; // Если L = 0, То и аргумент логарифма тоже может быть равен нулю (поэтому случай рассмотрен отдельно)
                else
                    return 0.;
            }
            else
                return (u*log(arg) - 0.5 * t) * C;
        }
        else
        {
            double arg = (V + 1)*t + 2.*L;
            if(fabs(arg) < zero)
                std::runtime_error("Impossible case!");
            else
                return (u*log(arg) - 0.5 * t + L * log( arg ) / (V + 1))*C;
        }
    }
    // более общий случай
    if(abs(disc)<zero)
    {
        //res2 = t / 2 - D / (2 *t) + log(abs(2 * t + A2)) * ( 2 * D / A2 - A2 / 2) - 2 * D * log(abs(t))/A2; // в этом случае A1 = A2 * A2 / 4 <- корень квардратного выражения 
        res2 = 0.;
        res2 -= t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) +
                F_arctan_2_mult(A1,A2,t)*(0.5 + D / (2.*A1));
        double arg1, arg2, mult_log;
        mult_log = A2 / 4 - D * A2 / (4 * A1);
        if(fabs(mult_log + u) < zero) // НЕ СМОГ ВЫЧИСЛИТЬ ПРЕДЕЛ АНАЛИТИЧЕСКИ!!! ВЫРАЖАЮ ЕГО ЧИСЛЕННО ОТСТУПОМ ОТ НУЛЯ!
        {
            u += fabs(u / 1e+6) + zero*10;
            // так же возмущаем константу L, т. к. другая может быть равна нулю.
            L += fabs(L / 1e+6) + zero*10;
            D += fabs(D / 1e+6) + zero*10;
            t = u + sqrt(u*u + D);
        }
        arg1 = abs(V*u + L + sqrt(u*u+D));
        arg2 = abs(t*t + A2*t + A1);
        if( arg1 < zero && arg2 < zero && fabs(mult_log + u) < zero)
        {
            //res2 += ( log(fabs(arg1 / arg2)) * u );            
            return res2 * C;
        }
        else
        {
            double res3 = log( arg1 ) * u + log( arg2 ) * mult_log;
            res2 += res3;            
            res2 *= C;
            return res2;
        } 
    }
    else
    {	// если нулю равен D, то нулю равен коэффициент A1
            if(abs(D)>zero)
            {
                res2 = 0.;
                res2 -= t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) - 
                       log( abs(t*t + A2*t + A1) ) * (A2 / 4 - D * A2 / (4 * A1));
                res2 -= F_arctan_2_mult(A1,A2,t)*(0.5 + D / (2.*A1));
            }
            else
            {
                    res2 = t - A2 * log(abs(t + A2)); // странный момент! разберись, здесь ошибка!
            }
    }
    res2 *= C;
    double res1 = log( abs(V*u + L + sqrt(u*u+D)) )*u*C;    
    return res2 + res1;
}

double u_r1_3_grad_int::x_log_int(double V, double L, double D, double C, double u)
{
    double glob_res = 0.;
    // Вначале разберём частные случаи
    //if(fabs(D) > zero) // не уверен, что надо выделять случаи V = +/-1, поскольку на D ни разу не делим.
    {
        double t;
        bool is_v_eq_1 = false;
        if(fabs(V - 1) < 1e-8) // случай V == 1 возможен
        {
            t = u + sqrt(fabs(u*u + D));
            is_v_eq_1 = true;
        }
        if(fabs(V + 1) < 1e-8) // случай V == -1 так же возможен
        {
            t = -u + sqrt(fabs(u*u + D));
            is_v_eq_1 = true;
        }
        if(is_v_eq_1)
        {
            // для этих замен переменной итоговый интеграл выглядит одинаково
            if(fabs(L) < zero)
            {
                double ret;
                if(fabs(t) < zero)
                    ret = 0.;
                else
                {
                    ret = t*t*log(t) + D*D*log(t)/(t*t) - 0.5*t*t + D*D / (2*t*t);
                    ret /= 8.;
                }
                return ret*C;
            }
            else
            {
                double ret1, ret2;
                if(fabs(t) < zero)
                    ret1 = ret2 = 0.;
                else
                {
                    ret1 = t*t*log(t+L) + D*D*log(t+L)/(t*t);
                    ret1 /= 8.;
                    ret2 = t*t/2. - L*t + L*L*log(t+L) - (D*D)*(L/t + log(t) - log(t+L))/(L*L);
                    ret2 /= 8.;
                }
                return (ret1 - ret2)*C;
            }
        }
    }
    //else
    {
    	if(fabs(L) < zero && fabs(D) < zero)
        {
            double arg = fabs(V * u + L + sqrt(fabs(u*u + D)));
            if(fabs(arg) < zero)               
                return 0.; // не регламентированный случай, интеграл не определён 
        }
    }
    // теперь разберём особенный случай и наиболее общий.	
    double t = u + sqrt(fabs(u*u + D)); // замена переменной для дальнейших преобразований.
    if( fabs(t) < zero )
    {
        D += D / 1e+6 + 100*zero;  
        t = u + sqrt(u*u + D); // замена переменной для дальнейших преобразований.
        if(fabs(L) < zero)
            L += L / 1e+6 + 100*zero;
    }
    double mult1, res2;
    mult1 = -2.*L*V;//D*L*(V+1) - L*L*L*V*(V+1) - L*L*L*(V+1) + L*V*D*(V+1) + L*L*L*V*V + L*L*L + L*V*V*D - L*D - 2.*V*V*D*L*(V+1);
    mult1 /= (V - 1)*(V - 1)*(V + 1)*(V + 1);
    res2 = F_arctan(V, L, D, t)*mult1;
    res2 += t*t/16. - 0.25*L*t/(V+1) + D*D/(t*t*16.) - ( 0.5*log(t)/pow(V-1,2.) )*( V*D + L*L - D ) + 0.25*D*L/(t * (V - 1)); 
    double arg1, arg2, mult_log;
    mult_log = L*L + V*V*D - D + L*L*V*V;
    mult_log /= 2.*(V-1)*(V-1)*(V+1)*(V+1);
    if(fabs(mult_log - u*u/2.) < zero) // НЕ СМОГ ВЫЧИСЛИТЬ ПРЕДЕЛ АНАЛИТИЧЕСКИ!!! ВЫРАЖАЮ ЕГО ЧИСЛЕННО ОТСТУПОМ ОТ НУЛЯ!
    {
        u += fabs(u / 1e+6) + zero*10;
        // так же возмущаем константу L, т. к. другая может быть равна нулю.
        L += fabs(L / 1e+6) + zero*10;
        D += fabs(D / 1e+6) + zero*10;
        t = u + sqrt(u*u + D);
    }
    arg1 = fabs(V*u + L + sqrt(u*u+D));
    arg2 = fabs(V*t*t - V*D + 2.*L*t + t*t + D);
    if( arg1 < zero && arg2 < zero && fabs(mult_log - u*u / 2.) < zero)
    {            
        glob_res = -res2 * C;
    }
    else
    {
        double res3 = log( arg1 )*u*u/2. - log( arg2 )*mult_log;
        res3 -= res2;            
        res3 *= C;
        glob_res = res3;
    } 
    return glob_res;
}

double u_r1_3_grad_int::r1_3_func_case_A(double alpha, double t, double st, double ty, double sy, double surface_coefs[])
{
    double res1, res2;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = alpha*alpha*(1 + B*B);
    double b = -2.*alpha*t + 2*alpha*alpha*A*B*ty + 2*alpha*alpha*B*C;
    double c = t*t + alpha*alpha*pow(t - ty, 2) + alpha*alpha*A*A*ty*ty + alpha*alpha*C*C + 2.*alpha*alpha*A*C*ty;
    double V_wave = -alpha / sqrt(1. + alpha*alpha);
    double L_wave = (t + alpha*alpha*(t - ty)) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double s_wave = sqrt(a) * sy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res1 = (fabs(alpha) / sqrt(1 + alpha*alpha)) * (
			( A*ty + C - b * B / (2*a) )*log_doub_area_int(V, L, D, 1./sqrt(a), s_wave) +
			x_log_int(V,L,D,1.,s_wave) * B / a
												   );
    // аналогично, нижний предел по t
    a = B*B + 1;
    b = 2*A*B*ty + 2.*B*C - 2*st;
    c = pow(t - ty,2) + A*A*ty*ty + C*C + 2.*A*ty*C + st*st;
    s_wave = sqrt(a) * sy + b / (2.*sqrt(a));
    D = c - b*b / (4. * a);
    L = t - ty;
    V = 0.;
    res2 = ( A*ty + C - b * B / (2*a) ) * log_doub_area_int(V, L, D, 1./sqrt(a), s_wave) +
			x_log_int(V,L,D,1.,s_wave) * B / a;
    return (res1 - res2) / (4*PI);
}

double u_r1_3_grad_int::r1_3_func_case_B(double alpha, double t, double st, double ty, double uy, double surface_coefs[])
{
    double res1, res2;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = alpha*alpha*(1 + B*B);
    double b = 2*alpha*alpha*A*B*ty - 2*alpha*t*B + alpha*alpha*2.*B*C;
    double c = alpha*alpha*pow(t - ty, 2.) + alpha*alpha*A*A*ty*ty + 
               t*t - 2.*alpha*t*A*ty + alpha*alpha*C*C + 2.*A*ty*C*alpha*alpha - 2.*alpha*t*C;
    double V_wave = -alpha * B / sqrt(1. + alpha*alpha);
    double L_wave = (t + alpha*alpha*(t - ty) - alpha*A*ty - alpha*C) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res1 = (fabs(alpha) / sqrt(1 + alpha*alpha)) * (
			x_log_int(V,L,D,1.,u_wave)/a
			- b*log_doub_area_int(V, L, D, 1./sqrt(a), u_wave)/(2.*a)
			);
    // аналогично, нижний предел по t
    a = B*B + 1;
    b = -2.*B*st + 2.*A*B*ty + 2.*B*C;
    c = pow(t-ty,2) + st*st + A*A*ty*ty + C*C - 2.*A*st*ty - 2.*C*st + 2.*A*C*ty;
    u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    D = c - b*b / (4. * a);
    L = t - ty;
    V = 0.;
    res2 = x_log_int(V,L,D,1.,u_wave)/a
		-b*log_doub_area_int(V, L, D, 1./sqrt(a), u_wave)/(2.*a);
    return (res1 - res2) / (4*PI);
}

double u_r1_3_grad_int::r1_3_func_case_C(double alpha, double t, double st, double uy, double sy, double surface_coefs[])
{
    double res1, res2;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = alpha*alpha*(1 + B*B);
    double b = -2.*alpha*alpha*t*B + 2.*alpha*alpha*B*C + 2.*alpha*alpha*A*B*sy;
    double c = pow(t - alpha*sy, 2) + alpha*alpha*t*t - 2*alpha*alpha*t*A*sy + 
				2.*alpha*alpha*A*sy*C - 2.*alpha*alpha*t*C + alpha*alpha*A*A*sy*sy + alpha*alpha*C*C;
    double V_wave = -alpha*alpha*B / sqrt(1. + alpha*alpha);
    double L_wave = (alpha*alpha*t - alpha*alpha*A*sy - alpha*alpha*C + t - alpha*sy) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res1 = (fabs(alpha) / sqrt(1 + alpha*alpha)) * (
			x_log_int(V,L,D,1.,u_wave)/a
			-b*log_doub_area_int(V, L, D, 1./sqrt(a), u_wave)/(2.*a)
			);
    // аналогично, нижний предел по t
    a = 1+B*B;
    b = -2.*B*t + 2.*B*C+2.*A*B*sy;
    c = t*t - 2*t*A*sy + 2.*A*sy*C - 2*t*C + A*A*sy*sy + C*C + st*st - 2.*st*sy + sy*sy;
	V_wave = -B;
	L_wave = t - A*sy - C;
    u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    V = V_wave / sqrt(a);
    L = L_wave - V_wave * b / (2. * a);
    D = c - b*b / (4.*a);
    res2 = x_log_int(V,L,D,1.,u_wave)/a
			-b*log_doub_area_int(V, L, D, 1./sqrt(a), u_wave)/(2.*a);
    return (res1 - res2) / (4*PI);
}

void u_r1_3_grad_int::get_integr_coord_num(Ty_params& Ty, uint coord_nums[])
{
    switch(Ty.non_integr_coord_num)
    {
        case 0: // координата sy, случай Б
            coord_nums[0] = 1; // ty
            coord_nums[1] = 2; // uy
            break;
        case 1: // координата ty, случай В
            coord_nums[0] = 0; // sy
            coord_nums[1] = 2; // uy
            break;
        case 2: // uy, случай A
            coord_nums[0] = 1; // ty
            coord_nums[1] = 0; // sy
            break;
        default: // если эти координаты равны, значит исходые данные не верны.
            coord_nums[0] = 0;
            coord_nums[1] = 0;
            break;
    }    
}

void u_r1_3_grad_int::build_Ty_params(triangle_parameters &Tx, Ty_params &Ty, triangle *area, VectorXD<double,3> *points, uint trig_num)
{
    // находим локальные координаты точек
    for(uint p=0; p<3; ++p)
    {
        Ty.vertexes_Tx[p] = Tx.get_local_coordinate_point( points[ area[trig_num].pnums(p) ] );
    }
    // находим локальные координаты нормали
    VectorXD<double,3> A, B;
    A = Ty.vertexes_Tx[2] - Ty.vertexes_Tx[0];
    B = Ty.vertexes_Tx[1] - Ty.vertexes_Tx[0];
    fast_vector_mutl(A,B,Ty.ny); // направление нормали не важно, служит для определения коэффициентов уравнения плоскости.
    Ty.ny.normalize();
    // находим плоскость интегрирования (оптимальная плоскость для вычисления интеграла)
    VectorXD<double,3> AA[3], BB[3], acc;
    double mes[3];
    for(uint i=0; i<3; ++i)
    {
        AA[i] = A; BB[i] = B;
    }
    for(uint coord=0; coord<3; ++coord)    
    {   // нули соответствующих координат дают проекции на плоскости локальных координат sx, tx, nx
        AA[coord].coords[coord] = 0.;
        BB[coord].coords[coord] = 0.;
        fast_vector_mutl(AA[coord],BB[coord],acc);
        mes[coord] = acc.norma();
    }
    double *max_elem = max_element(mes,mes+3);
    Ty.non_integr_coord_num = max_elem - mes;
}

void u_r1_3_grad_int::build_Ty_params(triangle_parameters &Tx, triangle_parameters &TY, Ty_params &Ty)
{
    // находим локальные координаты точек
    Ty.vertexes_Tx[0] = Tx.get_local_coordinate_point( TY.x0 );  
    Ty.vertexes_Tx[1] = Tx.get_local_coordinate_point( TY.x1 );
    Ty.vertexes_Tx[2] = Tx.get_local_coordinate_point( TY.x2 );
    // находим локальные координаты нормали
    VectorXD<double,3> A, B;
    A = Ty.vertexes_Tx[2] - Ty.vertexes_Tx[0];
    B = Ty.vertexes_Tx[1] - Ty.vertexes_Tx[0];
    fast_vector_mutl(A,B,Ty.ny); // направление нормали не важно, служит для определения коэффициентов уравнения плоскости.
    Ty.ny.normalize();
    // находим плоскость интегрирования (оптимальная плоскость для вычисления интеграла)
    VectorXD<double,3> AA[3], BB[3], acc;
    double mes[3];
    for(uint i=0; i<3; ++i)
    {
        AA[i] = A; BB[i] = B;
    }
    for(uint coord=0; coord<3; ++coord)    
    {   // нули соответствующих координат дают проекции на плоскости локальных координат sx, tx, nx
        AA[coord].coords[coord] = 0.;
        BB[coord].coords[coord] = 0.;
        fast_vector_mutl(AA[coord],BB[coord],acc);
        mes[coord] = acc.norma();
    }
    double *max_elem = max_element(mes,mes+3);
    Ty.non_integr_coord_num = max_elem - mes;
}

void u_r1_3_grad_int::get_surface_coefs(triangle_parameters &Tx, Ty_params &Ty,  double surface_coefs[] )
{
    uint coord_num[2];
    get_integr_coord_num(Ty, coord_num);
    double C_wave = Ty.vertexes_Tx[0].dotprod(Ty.ny);
    surface_coefs[0] = -Ty.ny.coords[ coord_num[0] ] / Ty.ny.coords[ Ty.non_integr_coord_num ];
    surface_coefs[1] = -Ty.ny.coords[ coord_num[1] ] / Ty.ny.coords[ Ty.non_integr_coord_num ];
    surface_coefs[2] = C_wave / Ty.ny.coords[ Ty.non_integr_coord_num ];
}