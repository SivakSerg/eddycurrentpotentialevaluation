/* 
 * File:   u_r2_3_grad_int.h
 * Author: root
 *
 * Created on June 27, 2015, 8:08 PM
 */

#ifndef U_R2_3_GRAD_INT_H
#define	U_R2_3_GRAD_INT_H

#include "u_r1_3_grad_int.h"
#pragma once

class u_r2_3_grad_int : public u_r1_3_grad_int
{
public:
    ~u_r2_3_grad_int(){};
    u_r2_3_grad_int(){};
    virtual double pure_numeric_integral(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // данная функция вычисляет интеграл от выражения для s-составляющей
    virtual double numeric_integrand(VectorXD<double,2> &Gauss_point, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // интегрируемая функция в полностью численном интеграле.
    virtual double basis_function_combination(VectorXD<double,3> loc_x_arg, uint non_integr_num, double surface_coefs[], bool singularity_point);
    double r2_3_func_case_A(double alpha, double s, double ty, double sy, double surface_coefs[]); // ty, sy
    double r2_3_func_case_B(double alpha, double s, double ty, double uy, double surface_coefs[]); // ty, uy
    double r2_3_func_case_C(double alpha, double s, double sy, double uy, double surface_coefs[]); // sy, uy
    virtual double operator()(double a);
    double get_numeric_integral_r2_3(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp);
};

#endif	/* U_R2_3_GRAD_INT_H */

