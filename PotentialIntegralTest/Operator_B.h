#pragma once
#include "Modyfied_Doub_Layer_Potential.hpp"
#include "Scalar_Doub_Layer_Potential.hpp"
#include "Vect_Doub_Layer_Potential.hpp"
#include "Operator_B_num.h"
#include "limits.h"
#include "Mod_Doub_Pot_by_parts.hpp"
/*
Не путать с блок-матрицей B. Данный оператор вычисляет
двойственный оператор билинейной формы (т. е. элемент
блока).
*/
class Operator_B 
{
public:        
	TrigGaussRuleInfo tgr_Ytest;
	Scalar_Doub_Layer_Potential sdlp;
	Modyfied_Doub_Layer_Potential mdlp;
	Vect_Doub_Layer_Potential vdlp;
	VectorBasisDecomposerInterface *decomposer_y; // получаем разложение проекций векторных базисных функций
	VectorBasisInterface *fx, *fy;				  // интерфейс векторных базисных функций на паре треугольников
	basis_storage combination;					  // хранилище функций и весов разложения
	TrigGaussRuleInfo tgr;						  // класс, реализующий численное интегрирование
	uint fx_last_el;	// последний заненсенный элемент носителя функции fx
	bool use_cache;
	// полностью численный потенциал для вычисления оператора, в не сингулярном случае
	Operator_B_num B_num;
public:
	Operator_B():tgr(12),fx(0),fy(0),tgr_Ytest(0),B_num(12,12),use_cache(false),fx_last_el(UINT_MAX){}
	Operator_B(GaussRule gr):tgr(gr),fx(0),fy(0),tgr_Ytest(gr),B_num(12,12),use_cache(false),fx_last_el(UINT_MAX){}
	Operator_B(GaussRule grX, GaussRule grY):tgr(grX),tgr_Ytest(grY),sdlp(grY),mdlp(grY),vdlp(grY),fx(0),fy(0),B_num(grX,grY),use_cache(false),fx_last_el(UINT_MAX){}
	void set_gauss_rule(GaussRule grX, GaussRule grY){tgr = grX; tgr_Ytest = grY; sdlp.tgr = grY; vdlp.tgr = grY; mdlp.tgr = grY; B_num.set_gauss_rule(grX,grY);}
	bool operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy, VectorBasisDecomposerInterface *dec_y); // передаем необходимые функции для вычисления элемента оператора B
	complex<double> evalf_operator(bool is_singular, complex<double> vave_num_k); // по переданным значениям параметров функций и декомпозиций вычисляет компоненту матрицы, критерий сингулярности вычисляется отдельно
        complex<double> evalf_singular_operator(complex<double> vave_num_k); // вычисляет только сингулярный случай
        complex<double> evalf_singular_muly_trig_operator(complex<double> vave_num_k); // использует подразбиение треугольника
        complex<double> evalf_multy_trig_operator(bool is_singular, std::complex<double> vave_num_k);
        complex<double> evalf_by_parts(bool is_singular, std::complex<double> vave_num_k);
	double u_test(VectorXD<double,3> n);
	complex<double> get_value_mod_pot(bool is_singular, std::complex<double> vave_num_k, VectorXD<double, 3> &glob_coord, VectorBasisInterface *fx); // значение модифицированного потенциала двойного слоя
	VectorXD<complex<double>, 3> get_value_vect_doub_pot(bool is_singular, std::complex<double> vave_num_k, VectorXD<double, 3> &glob_coord, VectorBasisInterface *fx); // значение векторного потенциала двойного слоя
        complex<double> evalf_by_parts_addapted(bool is_singular, complex<double> vave_num_k, int N, double epsilon, GaussRule gr_rude, GaussRule gr_refine);
        complex<double> evalf_by_parts(triangle_parameters &Ty, bool is_singular, complex<double> vave_num_k, GaussRule gr);
};