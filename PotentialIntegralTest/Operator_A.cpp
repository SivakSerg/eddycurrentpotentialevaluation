#include "Operator_A.h"
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// вычисляет оператор A адаптивно, дробления происходят лишь у тех треугольников, у которых относительная погрешность максимальна.
complex<double> Operator_A::evalf_operator_vave_addapted(bool is_singular, complex<double> vave_num_k, int N, double epsilon, GaussRule gr_rude, GaussRule gr_refine)
{
    double discrepancy = 1., zero(0.), cur_disc;
    complex<double> ret(0.), ret_refined(0.), saved_values(0.);
//    vector<triangle_parameters> before_sectioning;
//    vector<triangle_parameters> after_sectioning;
    triangle_parameters current_parameter;
    complex<double> rude_value, refined_value;
    if(!is_singular)
        return evalf_operator_roof(is_singular, vave_num_k);    
    before_sectioning.push_back(fy->current_parameter);
    zero = before_sectioning[0].s_t * before_sectioning[0].t_k * 0.5 * epsilon;
    for(int iter=0; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();
            rude_value = evalf_operator_vave(current_parameter, is_singular, vave_num_k, gr_rude);
            refined_value = evalf_operator_vave(current_parameter, is_singular, vave_num_k, gr_refine);
            complex<double> testRet = saved_values + rude_value;
            complex<double> testRetRefined = saved_values + refined_value;
            ret += rude_value;
            ret_refined += refined_value;            
            cur_disc = abs(testRetRefined - testRet)/abs(testRetRefined);
            if(cur_disc >= epsilon && abs(rude_value) >= zero && abs(refined_value) >= zero)  // если для данного фрагмента разбиения погрешность слишком высока, то необходимо внести разбиения в этот элемент
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values += refined_value;
        }
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();
    return ret_refined;
}
complex<double> Operator_A::evalf_operator_vave(triangle_parameters &Ty, bool is_singular, complex<double> vave_num_k, GaussRule gr)
{
    TrigGaussRuleInfo tgr(gr);
    
    complex<double> ret, area;
    ret = 0.;
    if(!fx || !fy)
        ret = 0.;
    // инициализация волновых чисел для различных подобластей
    slp.set_vave_num( vave_num_k );
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val, global_arg, local_arg;
    VectorXD<complex<double>,3> acc;
    double div_lam_y;
    
    area = Ty.s_t * Ty.t_k * 0.5;
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        // в этих точках вычисляем функцию lambda
        global_arg = Ty.get_Global_point(gauss_points);
        local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
        local_lambda_val = fy->lambda( local_arg ); // в локальных координатах треугольника Ty
        global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
        local_lambda_val = 
                fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
        // вычисляем потенциал простого слоя
        acc = slp.EvalfSingleLayerVectorPot2(is_singular,fx,gauss_points,Ty);
        for(uint p=0; p<3; p++)
                ret += acc.coords[ p ] * (local_lambda_val.coords[ p ] * tgr.w * area);
        // вычисляем стабилизирующую часть
//        div_lam_y = fy->div_F_lam( local_arg );
//        ret	+= (slp.EvalfSingleLayerPot(is_singular,fx,gauss_points,Ty) * div_lam_y * tgr.w * area);
    }    
    return ret;
}
// вычисляет оператор A адаптивно, дробления происходят лишь у тех треугольников, у которых относительная погрешность максимальна.
complex<double> Operator_A::evalf_operator_roof_addapted(bool is_singular, complex<double> vave_num_k, int N, double epsilon, GaussRule gr_rude, GaussRule gr_refine)
{
    double discrepancy = 1., zero(0.), cur_disc;
    complex<double> ret(0.), ret_refined(0.), saved_values(0.);
//    vector<triangle_parameters> before_sectioning;
//    vector<triangle_parameters> after_sectioning;
    triangle_parameters current_parameter;
    complex<double> rude_value, refined_value;
    if(!is_singular)
        return evalf_operator_roof(is_singular, vave_num_k);    
    before_sectioning.push_back(fy->current_parameter);
    zero = before_sectioning[0].s_t * before_sectioning[0].t_k * 0.5 * epsilon;
    for(int iter=0; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();
            rude_value = evalf_operator_roof(current_parameter, is_singular, vave_num_k, gr_rude);
            refined_value = evalf_operator_roof(current_parameter, is_singular, vave_num_k, gr_refine);
            complex<double> testRet = saved_values + rude_value;
            complex<double> testRetRefined = saved_values + refined_value;
            ret += rude_value;
            ret_refined += refined_value;            
            cur_disc = abs(testRetRefined - testRet)/abs(testRetRefined);
            if(cur_disc >= epsilon && abs(rude_value) >= zero && abs(refined_value) >= zero)  // если для данного фрагмента разбиения погрешность слишком высока, то необходимо внести разбиения в этот элемент
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values += refined_value;
        }
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();    
    return ret_refined;
}

complex<double> Operator_A::evalf_operator_roof(triangle_parameters &Ty, bool is_singular, complex<double> vave_num_k, GaussRule gr)
{
    TrigGaussRuleInfo tgr(gr);
// НЕ МЕНЯЙ ПОРЯДОК ГАУССА, РАБОТАЕТ КЭШ!!!        
//    Single_Layer_Potential slp(gr);
    
    complex<double> ret, area;
    ret = 0.;
    if(!fx || !fy)
        ret = 0.;
    // инициализация волновых чисел для различных подобластей
    slp.set_vave_num(vave_num_k);    
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val, global_arg, local_arg;
    VectorXD<complex<double>,3> acc;
    double div_lam_y;
    
    area = Ty.s_t * Ty.t_k * 0.5;
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        // в этих точках вычисляем функцию lambda
        global_arg = Ty.get_Global_point(gauss_points);
        local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
        local_lambda_val = fy->lambda( local_arg ); // в локальных координатах треугольника Ty
        global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
        local_lambda_val = 
            fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
        // вычисляем потенциал простого слоя
        // acc = slp.EvalfSingleLayerVectorPot2(is_singular,fx,gauss_points,Ty);
        VectorXD<complex<double>,3> acc = slp.EvalfSingleLayerVectorPotSections(1e-4, fx, gauss_points, Ty, 1, 12, 21);
        for(uint p=0; p<3; p++)
            ret += acc.coords[ p ] * (local_lambda_val.coords[ p ] * tgr.w * area);
        // вычисляем стабилизирующую часть
        if (abs(vave_num_k) >= zeroMaterial)
        {
            div_lam_y = fy->div_F_lam( local_arg );
            ret	+= (slp.EvalfSingleLayerPot(is_singular,fx,gauss_points,Ty) * div_lam_y * tgr.w * area) / (vave_num_k * vave_num_k);
        }
    }    
    return ret;
}

bool Operator_A::operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy)
{
	if(!fx || !fy)
		return false;
	this->fx = fx; this->fy = fy;
	return true;
}
bool Operator_A::operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy, int custom_rule)
{
    if(!operator_init(fx, fy))
        return false;
    this->custom_rule = custom_rule;
    return true;
}
// Обыкновенный оператор А - без стабилизирующих модификаций
complex<double> Operator_A::evalf_operator(bool is_singular, std::complex<double> vave_num_k)
{
    int gauss_rule;
    if(custom_rule > 0)
        gauss_rule = custom_rule;
    else
        gauss_rule = this->tgr.GetRule();
    GaussRule gr(gauss_rule);
    TrigGaussRuleInfo tgr(gr);
    Single_Layer_Potential slp(gr);
    
	complex<double> ret, area;
	ret = 0.;
	if(!fx || !fy)
		ret = 0.;
	// считаем площадь треугольника
	area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
	// инициализация волновых чисел для различных подобластей
	slp.set_vave_num( vave_num_k );
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double,2> gauss_points;
	VectorXD<double,3> local_lambda_val, global_lambda_val;
	VectorXD<complex<double>,3> acc;
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем функцию lambda
		local_lambda_val = fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
		global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
		local_lambda_val = 
			fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
		// вычисляем модифицированный потенциал простого слоя
		acc = slp.EvalfSingleLayerVectorPot(is_singular,fx,gauss_points,fy->current_parameter);
		for(uint i=0; i<3; i++)
			ret += acc.coords[ i ] * (local_lambda_val.coords[ i ] * tgr.w * area);
	}
	return ret;
}
complex<double> Operator_A::evalf_operator_roof(bool is_singular, std::complex<double> vave_num_k)
{
    int gauss_rule;
    if(custom_rule > 0)
        gauss_rule = custom_rule;
    else
        gauss_rule = this->tgr.GetRule();
    GaussRule gr(gauss_rule);
    TrigGaussRuleInfo tgr(gr);
    Single_Layer_Potential slp(gr);
    
    complex<double> ret, area;
    vectCache_elem fyCache;
    ret = 0.;
    if(!fx || !fy)
            ret = 0.;
    // считаем площадь треугольника
    area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    // инициализация волновых чисел для различных подобластей
    slp.set_vave_num( vave_num_k );	
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val;
    VectorXD<complex<double>,3> acc;
    double div_lam_y;
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        // в этих точках вычисляем функцию lambda
        fyCache = fy->get_cache_value( gauss_points );
        local_lambda_val = fyCache.lam_val;		//fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
        global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
        local_lambda_val = 
                fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
        // вычисляем потенциал простого слоя
        acc = slp.EvalfSingleLayerVectorPot2(is_singular,fx,gauss_points,fy->current_parameter);               
        for(uint i=0; i<3; i++)
                ret += acc.coords[ i ] * (local_lambda_val.coords[ i ] * tgr.w * area);
        // вычисляем стабилизирующую часть
        if (abs(vave_num_k) >= zeroMaterial)
        {
            div_lam_y = fy->div_F_lam( gauss_points );
            ret	+= (slp.EvalfSingleLayerPot(is_singular,fx,gauss_points,fy->current_parameter) * div_lam_y * tgr.w * area) / (vave_num_k * vave_num_k);
        }
    }
    return ret;
}

complex<double> Operator_A::evalf_multy_trig_operator_roof(bool is_singular, complex<double> vave_num_k)
{
    int gauss_rule;
    if(custom_rule > 0)
        gauss_rule = custom_rule;
    else
        gauss_rule = this->tgr.GetRule();
    GaussRule gr(gauss_rule);
    TrigGaussRuleInfo tgr(gr);
    Single_Layer_Potential slp(gr);
    
    complex<double> ret, area;
    vectCache_elem fyCache;
    ret = 0.;
    if(!fx || !fy)
            ret = 0.;
    // считаем площадь треугольника
    area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    // инициализация волновых чисел для различных подобластей
    slp.set_vave_num( vave_num_k );    
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val, global_arg, local_arg;
    VectorXD<complex<double>,3> acc;
    double div_lam_y;
    std::vector< triangle_parameters > params_x1, params;
    fy->current_parameter.Build_Sections(params_x1);
    for(int k=0; k<params_x1.size(); ++k)
        params_x1[k].Build_Sections(params);
//    for(int k=0; k<params_x2.size(); ++k)
//        params_x2[k].Build_Sections(params);
    for(int i=0; i<params.size(); ++i)
    {
        area = params[i].s_t * params[i].t_k * 0.5;
        while( tgr.next() ) // цикл по числу точек Гаусса
        {   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            // в этих точках вычисляем функцию lambda
            global_arg = params[i].get_Global_point(gauss_points);
            local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
            local_lambda_val = fy->lambda( local_arg ); // в локальных координатах треугольника Ty
            global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
            local_lambda_val = 
                    fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
            // вычисляем потенциал простого слоя
            acc = slp.EvalfSingleLayerVectorPot2(is_singular,fx,gauss_points,params[i]);
            for(uint p=0; p<3; p++)
                    ret += acc.coords[ p ] * (local_lambda_val.coords[ p ] * tgr.w * area);
            // вычисляем стабилизирующую часть
            if (abs(vave_num_k) >= zeroMaterial)
            {
                    div_lam_y = fy->div_F_lam( local_arg );
                    ret	+= (slp.EvalfSingleLayerPot(is_singular,fx,gauss_points,params[i]) * div_lam_y * tgr.w * area) / (vave_num_k * vave_num_k);
            }
        }
    }
    return ret;
}

// Следующий код реализует получение оператора А для внешней области
complex<double> Operator_A::evalf_operator_vave(bool is_singular, std::complex<double> vave_num_k)
{
    int gauss_rule;
    if(custom_rule > 0)
        gauss_rule = custom_rule;
    else
        gauss_rule = this->tgr.GetRule();
    GaussRule gr(gauss_rule);
    TrigGaussRuleInfo tgr(gr);
    Single_Layer_Potential slp(gr);
    
	complex<double> ret, area;
	vectCache_elem fyCache;
	ret = 0.;
	if(!fx || !fy)
		ret = 0.;
	// считаем площадь треугольника
	area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
	// инициализация волновых чисел для различных подобластей
	slp.set_vave_num( vave_num_k );
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double,2> gauss_points;
	VectorXD<double,3> local_lambda_val, global_lambda_val;
	VectorXD<complex<double>,3> acc;
	double div_lam_y;
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем функцию lambda
		fyCache = fy->get_cache_value( gauss_points );
		local_lambda_val = fyCache.lam_val; // fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
		global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
		local_lambda_val = 
			fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
		// вычисляем модифицированный потенциал двойного слоя
		acc = slp.EvalfSingleLayerVectorPot2(is_singular,fx,gauss_points,fy->current_parameter);
		for(uint i=0; i<3; i++)
			ret += acc.coords[ i ] * (local_lambda_val.coords[ i ] * tgr.w * area);
		// вычисляем стабилизирующую часть
		div_lam_y = fy->div_F_lam( gauss_points );
		ret	+= slp.EvalfSingleLayerPot(is_singular,fx,gauss_points,fy->current_parameter) * div_lam_y * tgr.w * area;
	}
	return ret;
}

complex<double> Operator_A::operator_V(bool is_singular, complex<double> vave_num_k, ScalarBasisInterface *fsa, VectorBasisInterface *fvb)
{
    int gauss_rule;
    if(custom_rule > 0)
        gauss_rule = custom_rule;
    else
        gauss_rule = this->tgr.GetRule();
    GaussRule gr(gauss_rule);
    TrigGaussRuleInfo tgr(gr);
    Single_Layer_Potential slp(gr);
    
    	complex<double> ret, area;
	scalCache_elem fsaCache;
	ret = 0.;
	if(!fsa || !fvb)
		ret = 0.;
	// считаем площадь треугольника
	area = fsa->current_parameter.s_t * fsa->current_parameter.t_k * 0.5;
	// инициализация волновых чисел для различных подобластей
	slp.set_vave_num( vave_num_k );
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double,2> gauss_points;	
	complex<double> acc;	
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            fsaCache = fsa->get_cache_value(gauss_points);
            acc = slp.EvalfSingleLayerPot(is_singular,fvb,gauss_points,fsa->current_parameter) * tgr.w * area;
            ret += acc * fsaCache.value;                    
	}
	return ret;
}

complex<double> Operator_A::operator_V(bool is_singular, complex<double> vave_num_k, triangle_parameters &trig_a, VectorBasisInterface *fvb)
{
    int gauss_rule;
    if(custom_rule > 0)
        gauss_rule = custom_rule;
    else
        gauss_rule = this->tgr.GetRule();
    GaussRule gr(gauss_rule);
    TrigGaussRuleInfo tgr(gr);
    Single_Layer_Potential slp(gr);
    
        complex<double> ret, area;	
	ret = 0.;
	if(!fvb)
		ret = 0.;
	// считаем площадь треугольника
	area = trig_a.s_t * trig_a.t_k * 0.5;
	// инициализация волновых чисел для различных подобластей
	slp.set_vave_num( vave_num_k );
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double,2> gauss_points;	
	complex<double> acc;	
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;            
            acc = slp.EvalfSingleLayerPot(is_singular,fvb,gauss_points,trig_a) * tgr.w * area;
            ret += acc;                    
	}
	return ret;
}

complex<double> Operator_A::operator_V_multy_trig(bool is_singular, complex<double> vave_num_k, ScalarBasisInterface *fsa, VectorBasisInterface *fvb)
{
    int gauss_rule;
    if(custom_rule > 0)
        gauss_rule = custom_rule;
    else
        gauss_rule = this->tgr.GetRule();
    GaussRule gr(gauss_rule);
    TrigGaussRuleInfo tgr(gr);
    Single_Layer_Potential slp(gr);
    
    complex<double> ret, area;
    scalCache_elem fsaCache;
    ret = 0.;
    if(!fsa || !fvb)
        ret = 0.;
    // считаем площадь треугольника
    area = fsa->current_parameter.s_t * fsa->current_parameter.t_k * 0.5;
    // инициализация волновых чисел для различных подобластей
    slp.set_vave_num( vave_num_k );
    std::vector< triangle_parameters > params_x1, params;
    fsa->current_parameter.Build_Sections(params_x1);
    for(int k=0; k<params_x1.size(); ++k)
        params_x1[k].Build_Sections(params);
    VectorXD<double,2> gauss_points;	
    complex<double> acc;
    complex<double> value;
    VectorXD<double,3> local_arg, global_arg;
    for(int i=0; i<params.size(); ++i)
    {
	// численное интегрирование по множеству треугольника y (lambda - функция)
	area = params[i].s_t * params[i].t_k * 0.5;
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            global_arg = params[i].get_Global_point(gauss_points);
            local_arg = fsa->current_parameter.get_local_coordinate_point(global_arg);
            //fsaCache = fsa->get_cache_value(gauss_points);
            value = fsa->f_value( local_arg );
            acc = slp.EvalfSingleLayerPot(is_singular,fvb,gauss_points,params[i]) * tgr.w * area;
            ret += acc * value/*fsaCache.value*/;                    
	}
    }
    return ret;        
}