/* 
 * File:   u_r1_3_grad_int.h
 * Author: root
 *
 * Created on June 27, 2015, 8:07 PM
 */

#ifndef U_R1_3_GRAD_INT_H
#define	U_R1_3_GRAD_INT_H
#pragma once

#include "r1_3_integral.h"

class u_r1_3_grad_int : public Functor
{   
protected:
    double zero;
    VectorBasisInterface *fx, *fy; // указатели на базисные функции, параметры интегирования.    
    /*
     * Параметры каждого из треугольников хранятся в экземплярах интерфейсных классов VectorBasisInterface.
    */
    /*
     * Параметры нужны для численного интегрирования  
    */
    Ty_params Ty_points;
    uint integr_num_queue[3];
    double surface_coefs[3];
    bool init_params;
    double F_arctan_2_mult(double A1,double A2,double arg);
public:
    ~u_r1_3_grad_int(){};
    u_r1_3_grad_int():zero(1e-8){};
    void set_basis_functions(VectorBasisInterface *fx, VectorBasisInterface *fy){this->fx = fx; this->fy = fy;}
    void build_Ty_params(triangle_parameters &Tx, Ty_params &Ty, triangle *area, VectorXD<double,3> *points, uint trig_num); // строит параметры треугольника Ty в координатах треугольинка Tx;
    void build_Ty_params(triangle_parameters &Tx, triangle_parameters &TY, Ty_params &Ty); // строит параметры треугольника Ty в координатах треугольинка Tx;
    void get_integr_coord_num(Ty_params &Ty, uint coord_nums[]); // определяет номера координа, по которым ведётся интегирования, coord_nums[0] - внешний интеграл, coord_nums[1] - внутренний
    void get_surface_coefs(triangle_parameters &Tx, Ty_params &Ty,  double surface_coefs[] ); // определяет коэффициенты поверхности треугольника Ty заданной зависимости одной исключённой координаты от двух других
    double x_log_int(double V, double L, double D, double C, double x); // общий интеграл для всех интегралов от произведения логарифмической фукции на x.
    double log_doub_area_int(double V, double L, double D, double C, double x); // общий интеграл для всех интегралов от логарифмической функции, подобной гиперболическому арксинусу.    
    virtual double pure_numeric_integral(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // данная функция вычисляет интеграл от выражения для s-составляющей
    virtual double numeric_integrand(VectorXD<double,2> &Gauss_point, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // интегрируемая функция в полностью численном интеграле.
    double r1_3_func_case_A(double alpha, double t, double st, double ty, double sy, double surface_coefs[]); // ty, sy
    double r1_3_func_case_B(double alpha, double t, double st, double ty, double uy, double surface_coefs[]); // ty, uy
    double r1_3_func_case_C(double alpha, double t, double st, double uy, double sy, double surface_coefs[]); // uy, sy
    virtual double basis_function_combination(VectorXD<double,3> loc_x_arg, uint non_integr_num, double surface_coefs[], bool singularity_point=false); // используется в методе вычитания особенности    
    virtual double operator()(double a); // возвращает значение интегрируемой функции, являющейся произведением выражения, связывающего базисные функции на искомый интеграл
    // для вычисления этого оператора нужны следующие функции:
    VectorXD<double,3> get_x_local_coord(uint integr_num_queue[], double coefs[], double a, double limit); // возрващает точку, соответствующую параметру интегрирования a и одному из пределов интегрирования limit (верхнему или нижнему) в локальных координатах треугольника Tx, координатный номер этой точки равен integr_num_queue[1], integr_num_queue[0] - исключённая координата в уравнении поверхности треугольника Ty, integr_num_queue[2] - параметр, по которому интеграл взят аналитически (соответствует limit)    
    void lines_Ty_calc_params(Ty_params &Ty, uint num_integr_coord, uint analytic_integr_coord); // передаётся класс параметров треугольника Ty и номер координаты, по которой интегрирование осуществляется численно + номер координаты, по которой интегрирование осуществляется аналитически.
    void get_analytical_limits(Ty_params &Ty, double a, uint num_integr_coord, double limits[]); // получаем пределы для аналитического интегрирования вдоль одного из направлений    
    double set_zero(double zero){this->zero = zero;}
    double F_arctan(double A1,double A2,double arg);
    double F_arctan(double V, double L, double D, double x);
    double get_numeric_integral_r1_3(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp); // функция вычисляет искомый интеграл
};

#endif	/* U_R1_3_GRAD_INT_H */

