#pragma once
#include "Single_Layer_Potential.hpp"
#include "Modyfied_Doub_Layer_Potential.hpp"
#include <iostream>
#include <fstream>
#include <vector>
/*
Не путать с блок-матрицей A. Данный оператор вычисляет
двойственный оператор билинейной формы (т. е. элемент
блока). Содержит функции с модификациями этого оператора,
которые были введены для улучшения сходимости. A_vave, A_roof -
это А - с волной и А - крышка. Оператор A используется так же
и при вычислении оператора N, однако с функциями уже совсем
другого пространства.
 * 
 * 10 марта 2015
 * Решил добавить функцию вычисления оператора V в матрицу A для следующих целей
 * а) скалярное произведение оператора V на дивергенцию пробной функции можно внести в правую часть,
 * это нужно для определения следа Неймана при нулевом волновом числе с матрицей А в качестве матрицы СЛАУ.
 * Ранее определить след Неймана при нулевом волновом числе можно было определить только при помощи второго
 * уравнения следов Nk[gamma_D(U)] = (0.5 I - Bk)[gamma_N(U)], где gamma_N(U) след Неймана функции U.
 * Фактически, это тестовая функция.
 * б) Для того, чтобы вычислять поле в окружающей среде (где волновое число равно нулю) необходимо определить
 * чему равен нормальный след. Это невозможно сделать, не введя дополнительных скалярных функций. Для простоты
 * можно выбрать кусочно постоянные базисные функции, но реализация предусматривает функции любого порядка.
*/
class Operator_A
{
public:
	Single_Layer_Potential slp;	  // потенциал простого слоя (скалярный и векторный)
	TrigGaussRuleInfo tgr;		  // класс, реализующий численное интегрирование
	VectorBasisInterface *fx, *fy;// интерфейс векторных базисных функций на паре треугольников
        int custom_rule;
        vector<triangle_parameters> before_sectioning;
        vector<triangle_parameters> after_sectioning;
        double zeroMaterial=1e-10;
public:
	Operator_A():tgr(12),fx(0),fy(0),custom_rule(0){}
	Operator_A(GaussRule gr):tgr(gr),fx(0),fy(0),custom_rule(0){}
	Operator_A(GaussRule grX, GaussRule grY):tgr(grX),slp(grY),custom_rule(0){}
        void set_zero_material(double zeroMaterial){this->zeroMaterial = zeroMaterial;}
	void set_gauss_rule(GaussRule grX, GaussRule grY){tgr = grX; slp.tgr = grY;}
	bool operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy); // передаем необходимые функции для вычисления элемента оператора B
        bool operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy, int custom_rule); // передаем необходимые функции для вычисления элемента оператора B
	complex<double> evalf_operator(bool is_singular, complex<double> vave_num_k); // оператор А по переданным значениям параметров функций и декомпозиций вычисляет компоненту матрицы, критерий сингулярности вычисляется отдельно
	complex<double> evalf_operator_roof(bool is_singular, complex<double> vave_num_k); // A - крышка, вычисляется со стабилизирующей добавкой, деленной на волновое число
        complex<double> evalf_multy_trig_operator_roof(bool is_singular, complex<double> vave_num_k); // A - крышка, вычисляется со стабилизирующей добавкой, деленной на волновое число
	complex<double> evalf_operator_vave(bool is_singular, complex<double> vave_num_k); // A - крышка, вычисляется со стабилизирующей добавкой для внешнего пространства (где волновое число предполагается равным нулю)
        complex<double> operator_V(bool is_singular, complex<double> vave_num_k, ScalarBasisInterface *fsa, VectorBasisInterface *fvb); // вычисление оператора V для нормального следа (скалярные функции раскладывают нормальный след)
        complex<double> operator_V(bool is_singular, complex<double> vave_num_k, triangle_parameters &trig_a, VectorBasisInterface *fvb); // вычисление оператора V для нормального следа (кусочно постоянные функции на элементе раскладывают нормальный след)
        complex<double> operator_V_multy_trig(bool is_singular, complex<double> vave_num_k, ScalarBasisInterface *fsa, VectorBasisInterface *fvb); // 
        complex<double> evalf_operator_roof_addapted(bool is_singular, complex<double> vave_num_k, int N, double epsilon, GaussRule gr_rude = 6, GaussRule gr_refine = 12); // is_singular = false, вызывается обычный оператор без наворотов, vave_num_k - волновое число, N - счётчик числа адаптивных дроблений, eps - максимальное относительное дробление
        complex<double> evalf_operator_roof(triangle_parameters &Ty, bool is_singular, complex<double> vave_num_k, GaussRule gr); // вычисляет значение оператора на треугольном фрагменте
        complex<double> evalf_operator_vave_addapted(bool is_singular, complex<double> vave_num_k, int N, double epsilon, GaussRule gr_rude = 6, GaussRule gr_refine = 12); // is_singular = false, вызывается обычный оператор без наворотов, vave_num_k - волновое число, N - счётчик числа адаптивных дроблений, eps - максимальное относительное дробление
        complex<double> evalf_operator_vave(triangle_parameters &Ty, bool is_singular, complex<double> vave_num_k, GaussRule gr); // вычисляет значение оператора на треугольном фрагменте
};
