/* 
 * File:   r1_3_integral.h
 * Author: root
 *
 * Created on May 5, 2015, 11:19 PM
 */
#ifndef R1_3_INTEGRAL_H
#define	R1_3_INTEGRAL_H
#pragma once

#include "Modyfied_Doub_Layer_Potential.hpp"
#include <algorithm>
#include "Integral.h"
#include <fstream>

/*
 * Данный интеграл вычисляется полуаналитическим способом путём интегрирования сразу двух треугольников,
 * поскольку эксперементально проверено, что он (интеграл по областям точек x и y) является особенным.
 * Особенность возникает при sy = st и uy = 0.
 * 
 * В отличие от исходного варианта в файле Modyfied_Doub_Layer_Potential.h здесь имеем полуаналитическое
 * интегрирования лишь по одному направлению. Участвуют в полуаналитическом интегрировании сразу обе базисные
 * функции, поскольку их значения вычисляются в локальных координатах своих треугольников (с которыми каждая из
 * функций ассоциированна), то появляется некоторая дополнительная сложность с преобразованием координат.
 * Функционал имеет вид (в локальных координатах Tx) (lambda_Ty(y), nx) * u_Tx(y).s или u_Tx.coords[0].
 */

// параметры треугольника Ty в координатах треугольника Tx
struct Ty_params
{
    VectorXD<double,3> vertexes_Tx[3]; // вершины треугольника в локальных координатах треугольника Tx (расположены в любом порядке)    
    VectorXD<double,3> ny;             // нормаль к треугольнику. в локальных координатах.
    uint non_integr_coord_num;         // номер координаты, по которой не ведётся интегрирование (она является функцией двух других);  
    double line_coefs[3], t0[3], s0[3];// параметры, которые нужны для вычисления пределов интегрирования, t0 - начало отрезка прямой для координаты, по которой производится численное интегрирование, s0 - начало отрезка прямой для координаты, по которой проводится аналитическое интегрирование.
};

struct Vector_predicate {
    uint coord_num;
    bool operator()(VectorXD<double,3> a, VectorXD<double,3> b){return a.coords[coord_num] < b.coords[coord_num];}
    Vector_predicate(uint coord_num){ this->coord_num = coord_num;}
};

class r1_3_integral : public Functor
{   
protected:
    double zero;
    VectorBasisInterface *fx, *fy; // указатели на базисные функции, параметры интегирования.    
    /*
     * Параметры каждого из треугольников хранятся в экземплярах интерфейсных классов VectorBasisInterface.
    */
    /*
     * Параметры нужны для численного интегрирования  
    */
    Ty_params Ty_points;
    uint integr_num_queue[3];
    double surface_coefs[3];
    bool init_params;
    double F_arctan_2_mult(double A1,double A2,double arg);
public:
    ~r1_3_integral(){};
    r1_3_integral():zero(1e-8){};
    void set_basis_functions(VectorBasisInterface *fx, VectorBasisInterface *fy){this->fx = fx; this->fy = fy;}
    double numeric_integrand(VectorXD<double,3> global_arg, Modyfied_Doub_Layer_Potential &mdlp); // переводит глобальные координаты треугольника в точки Гаусса
    double pure_numeric_integral_x16(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty);
    void build_Ty_params(triangle_parameters &Tx, Ty_params &Ty, triangle *area, VectorXD<double,3> *points, uint trig_num); // строит параметры треугольника Ty в координатах треугольинка Tx;
    void build_Ty_params(triangle_parameters &Tx, triangle_parameters &TY, Ty_params &Ty); // строит параметры треугольника Ty в координатах треугольинка Tx;
    void get_integr_coord_num(Ty_params &Ty, uint coord_nums[]); // определяет номера координа, по которым ведётся интегирования, coord_nums[0] - внешний интеграл, coord_nums[1] - внутренний
    void get_surface_coefs(triangle_parameters &Tx, Ty_params &Ty,  double surface_coefs[] ); // определяет коэффициенты поверхности треугольника Ty заданной зависимости одной исключённой координаты от двух других
    double log_doub_area_int(double V, double L, double D, double C, double x); // общий интеграл для всех интегралов от логарифмической функции, подобной гиперболическому арксинусу.    
    virtual double pure_numeric_integral(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // данная функция вычисляет интеграл от выражения для s-составляющей
    virtual double numeric_integrand(VectorXD<double,2> &Gauss_point, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // интегрируемая функция в полностью численном интеграле.
    double r1_3_func_case_A(double alpha, double t, double st, double ty, double sy, double surface_coefs[]); // ty, sy
    double r1_3_func_case_B(double alpha, double t, double st, double ty, double uy, double surface_coefs[]); // ty, uy
    double r1_3_func_case_C(double alpha, double t, double st, double uy, double sy, double surface_coefs[]); // uy, sy
    virtual double basis_function_combination(VectorXD<double,3> loc_x_arg, uint non_integr_num, double surface_coefs[], bool singularity_point=false); // используется в методе вычитания особенности    
    virtual double operator()(double a); // возвращает значение интегрируемой функции, являющейся произведением выражения, связывающего базисные функции на искомый интеграл
    // для вычисления этого оператора нужны следующие функции:
    VectorXD<double,3> get_x_local_coord(uint integr_num_queue[], double coefs[], double a, double limit); // возрващает точку, соответствующую параметру интегрирования a и одному из пределов интегрирования limit (верхнему или нижнему) в локальных координатах треугольника Tx, координатный номер этой точки равен integr_num_queue[1], integr_num_queue[0] - исключённая координата в уравнении поверхности треугольника Ty, integr_num_queue[2] - параметр, по которому интеграл взят аналитически (соответствует limit)    
    void lines_Ty_calc_params(Ty_params &Ty, uint num_integr_coord, uint analytic_integr_coord); // передаётся класс параметров треугольника Ty и номер координаты, по которой интегрирование осуществляется численно + номер координаты, по которой интегрирование осуществляется аналитически.
    void get_analytical_limits(Ty_params &Ty, double a, uint num_integr_coord, double limits[]); // получаем пределы для аналитического интегрирования вдоль одного из направлений    
    double set_zero(double zero){this->zero = zero;}
    double F_arctan(double A1,double A2,double arg);
    double get_numeric_integral_r1_3(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp); // функция вычисляет искомый интеграл    
    double get_numeric_integral_r1_3_x16(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp);
    void out_values_along_the_interval(STR Points, STR Values, int n); //тестовая функция для выдачи значений вдоль интервала для интегрирования (проверка на непрерывность)
};

#endif	/* R1_3_INTEGRAL_H */

