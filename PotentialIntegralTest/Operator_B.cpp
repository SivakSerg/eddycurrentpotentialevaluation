/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "Operator_B.h"
#include "r1_3_integral.h"
#include "r2_3_integral.h"
#include "u_r1_3_grad_int.h"
#include "u_r2_3_grad_int.h"

complex<double> Operator_B::evalf_by_parts_addapted(bool is_singular, complex<double> vave_num_k, int N, double epsilon, GaussRule gr_rude, GaussRule gr_refine)
{
    double discrepancy = 1., zero, cur_disc;
    complex<double> ret(0.), ret_refined(0.), saved_values(0.);
    vector<triangle_parameters> before_sectioning;
    vector<triangle_parameters> after_sectioning;
    triangle_parameters current_parameter;
    complex<double> rude_value, refined_value;
    if(!is_singular)
        return evalf_operator(is_singular, vave_num_k);    
    before_sectioning.push_back(fy->current_parameter);
    zero = before_sectioning[0].s_t * before_sectioning[0].t_k * 0.5 * epsilon;
    for(int iter=0; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();
            rude_value = evalf_by_parts(current_parameter, is_singular, vave_num_k, gr_rude);
            refined_value = evalf_by_parts(current_parameter, is_singular, vave_num_k, gr_refine);
            complex<double> testRet = saved_values + rude_value;
            complex<double> testRetRefined = saved_values + refined_value;
            ret += rude_value;
            ret_refined += refined_value;            
            cur_disc = abs(testRetRefined - testRet)/abs(testRetRefined);
            if(cur_disc >= epsilon && abs(rude_value) >= zero && abs(refined_value) >= zero) // если для данного фрагмента разбиения погрешность слишком высока, то необходимо внести разбиения в этот элемент
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values += refined_value;
        }
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    Mod_Doub_Pot_by_parts mdlp_partial;
    complex<double> mod_doub_pot(0.,0.);
    mdlp_partial.init_potential(this->fx,this->fy,gr_refine,vave_num_k,is_singular);
    mod_doub_pot = mdlp_partial.EvalfIntegral(N,epsilon,gr_rude,gr_refine);
    ret_refined = ret_refined - mod_doub_pot;
    return ret_refined;
}

complex<double> Operator_B::evalf_by_parts(triangle_parameters &Ty, bool is_singular, complex<double> vave_num_k, GaussRule gr)
{
    complex<double> ret, area;
    ret = 0.;	
    if(!fx || !fy)
            ret = 0.;
    // инициализация волновых чисел для различных подобластей
// НЕ МЕНЯЙ ПОРЯДОК ГАУССА, РАБОТАЕТ КЭШ!!!        
//    Scalar_Doub_Layer_Potential sdlp(gr);
//    Modyfied_Doub_Layer_Potential mdlp(gr);
//    Vect_Doub_Layer_Potential vdlp(gr);
    sdlp.set_vave_num( vave_num_k );
    mdlp.set_vave_num( vave_num_k );
    vdlp.set_vave_num( vave_num_k );
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val;
    double normal_proj;
    complex<double> acc, vect_doub_pot(0.,0.);    
    VectorXD<complex<double>,3> vect_val;
    VectorXD<double,3> global_arg, local_arg;
    
    area = Ty.s_t * Ty.t_k * 0.5;
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        // в этих точках вычисляем функцию lambda
        global_arg = Ty.get_Global_point(gauss_points);
        local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
        local_lambda_val = fy->lambda( local_arg ); // в локальных координатах треугольника Ty
        global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
        local_lambda_val = 
                fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
        normal_proj = local_lambda_val.coords[2]; // проекция этого значения на нормаль
        // вычисляем модифицированный потенциал двойного слоя
        acc = 0.;              
        vect_val = vdlp.EvalfVectDoubPotential_no_r_int(fx,gauss_points,Ty);
        for(int p=0; p<3; ++p)
                acc += vect_val.coords[p] * local_lambda_val.coords[p];
        // умножение на веса разложения по формуле гаусса Summ(f(gauss_point[i]) * w[i], i = 1..psize);
        vect_doub_pot += acc * tgr.w * area;		
    } 
    return vect_doub_pot;
}

double Operator_B::u_test(VectorXD<double,3> n)
{
	VectorXD<double,2> gaussY;
	VectorXD<double,3> glob_arg;
	double test_sum = 0.;
	while(tgr_Ytest.next())
	{
		gaussY.coords[ 0 ] = tgr_Ytest.e.e1;
		gaussY.coords[ 1 ] = tgr_Ytest.e.e2;
		glob_arg = fy->get_Global_point(gaussY);
		test_sum += pow(decomposer_y->u_test(fx, n, combination, glob_arg),2);
	}
	test_sum = sqrt(test_sum);
	return test_sum;
}

bool Operator_B::operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy, VectorBasisDecomposerInterface *dec_y)
{
	if(!fx || !fy)
		return false;
	this->fx = fx;
	this->fy = fy;
	if(fx->current_parameter.elnum == UINT_MAX)
		printf("Warning: seriously large number of elements!\n");
	if(fx->current_parameter.elnum == fx_last_el && fx_last_el != UINT_MAX)
		use_cache = true;
	else
	{
		fx_last_el = fx->current_parameter.elnum;
		use_cache = false;
	}
	this->decomposer_y = dec_y;	
	B_num.operator_init(fx,fy);
	return true;
}
complex<double> Operator_B::evalf_operator(bool is_singular, std::complex<double> vave_num_k)
{
	complex<double> ret, area;
	vectCache_elem fyCache;
	ret = 0.;	
	if(!fx || !fy)
		ret = 0.;
	// считаем площадь треугольника
	area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
	// инициализация волновых чисел для различных подобластей
	sdlp.set_vave_num( vave_num_k );
	mdlp.set_vave_num( vave_num_k );
	vdlp.set_vave_num( vave_num_k );
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double,2> gauss_points;
	VectorXD<double,3> local_lambda_val, global_lambda_val;
	double normal_proj;
	complex<double> acc, vect_doub_pot(0.,0.);
	complex<double> test_var, mod_doub_pot(0.,0.);
	VectorXD<complex<double>,3> vect_val;        
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем функцию lambda
		fyCache = fy->get_cache_value( gauss_points );
		local_lambda_val = fyCache.lam_val; // fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
		global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
		local_lambda_val = 
			fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
		normal_proj = local_lambda_val.coords[2]; // проекция этого значения на нормаль
		// вычисляем модифицированный потенциал двойного слоя
		test_var = acc = 0.;
		if(abs(normal_proj / sqrt(fx->current_parameter.s_t * fx->current_parameter.t_k) ) > 1e-9)
		    test_var = normal_proj * mdlp.EvalfPotential(is_singular,fx,gauss_points,fy->current_parameter);                
                acc = 0.;
		vect_val = vdlp.EvalfVectDoubPotential(is_singular,fx,gauss_points,fy->current_parameter);
		for(int p=0; p<3; ++p)
			acc += vect_val.coords[p] * local_lambda_val.coords[p];
		// умножение на веса разложения по формуле гаусса Summ(f(gauss_point[i]) * w[i], i = 1..psize);
                vect_doub_pot += acc * tgr.w * area;
                mod_doub_pot += test_var * tgr.w * area;		
	}
        ret = vect_doub_pot + mod_doub_pot; // складываем потенциалы после интегрирования, чтобы не накапливать погрешность, складывая потенциалы в каждой точке Гаусса.
	return ret;
}

complex<double> Operator_B::evalf_by_parts(bool is_singular, std::complex<double> vave_num_k)
{
    complex<double> ret, area;
    vectCache_elem fyCache;
    ret = 0.;	
    if(!fx || !fy)
            ret = 0.;
    // считаем площадь треугольника
    area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    // инициализация волновых чисел для различных подобластей
    sdlp.set_vave_num( vave_num_k );
    mdlp.set_vave_num( vave_num_k );
    vdlp.set_vave_num( vave_num_k );
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val;
    double normal_proj;
    complex<double> acc, vect_doub_pot(0.,0.);
    complex<double> test_var, mod_doub_pot(0.,0.);
    VectorXD<complex<double>,3> vect_val;
    Mod_Doub_Pot_by_parts mdlp_partial;
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            // в этих точках вычисляем функцию lambda
            fyCache = fy->get_cache_value( gauss_points );
            local_lambda_val = fyCache.lam_val; // fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
            global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
            local_lambda_val = 
                    fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
            normal_proj = local_lambda_val.coords[2]; // проекция этого значения на нормаль
            // вычисляем модифицированный потенциал двойного слоя
            acc = 0.;
            vect_val = vdlp.EvalfVectDoubPotential(is_singular,fx,gauss_points,fy->current_parameter);
            for(int p=0; p<3; ++p)
                    acc += vect_val.coords[p] * local_lambda_val.coords[p];
            // умножение на веса разложения по формуле гаусса Summ(f(gauss_point[i]) * w[i], i = 1..psize);
            vect_doub_pot += acc * tgr.w * area;	
    }
    GaussRule GR(this->vdlp.tgr.GetRule());
    mdlp_partial.init_potential(this->fx,this->fy,GR,vave_num_k,is_singular);
    mod_doub_pot = mdlp_partial.EvalfIntegral();
    ret = vect_doub_pot - mod_doub_pot; // складываем потенциалы после интегрирования, чтобы не накапливать погрешность, складывая потенциалы в каждой точке Гаусса.
    return ret;
}

// имеет смысл вычислять эту функцию вместо "evalf_operator" в случае смежных треугольников по ребру.
complex<double> Operator_B::evalf_singular_operator(complex<double> vave_num_k)
{
	complex<double> ret, area;
	vectCache_elem fyCache;
	ret = 0.;	
	if(!fx || !fy)
		ret = 0.;
	// считаем площадь треугольника
	area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
	// инициализация волновых чисел для различных подобластей
	sdlp.set_vave_num( vave_num_k );
	mdlp.set_vave_num( vave_num_k );
	vdlp.set_vave_num( vave_num_k );
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double,2> gauss_points;
	VectorXD<double,3> local_lambda_val, global_lambda_val;
	double normal_proj;
	complex<double> acc, vect_doub_pot(0.,0.);
	complex<double> test_var, mod_doub_pot(0.,0.);
	VectorXD<complex<double>,3> vect_val;        
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем функцию lambda
		fyCache = fy->get_cache_value( gauss_points );
		local_lambda_val = fyCache.lam_val; // fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
		global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
		local_lambda_val = 
			fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
		normal_proj = local_lambda_val.coords[2]; // проекция этого значения на нормаль
		// вычисляем модифицированный потенциал двойного слоя
		test_var = acc = 0.;
		if(abs(normal_proj / sqrt(fx->current_parameter.s_t * fx->current_parameter.t_k) ) > 1e-9)
		    test_var = normal_proj * mdlp.EvalfPotential_no_r_int(fx,gauss_points,fy->current_parameter);                
                acc = 0.;
		vect_val = vdlp.EvalfVectDoubPotential_no_r_int(fx,gauss_points,fy->current_parameter);
		for(int p=0; p<3; ++p)
			acc += vect_val.coords[p] * local_lambda_val.coords[p];
		// умножение на веса разложения по формуле гаусса Summ(f(gauss_point[i]) * w[i], i = 1..psize);
                vect_doub_pot += acc * tgr.w * area;
                mod_doub_pot += test_var * tgr.w * area;		
	}
        // теперь к test_var добавим выражение интеграла с особенностью третьего порядка (итеграл взят по двум треугольникам)
        r1_3_integral r1_3_int_; // класс для вычисления интеграла от особенной функции (третьего порядка)
        r2_3_integral r2_3_int_;
        u_r1_3_grad_int u_r1_3_int_;
        u_r2_3_grad_int u_r2_3_int_;
        r1_3_int_.set_basis_functions(fx,fy);
        r2_3_int_.set_basis_functions(fx,fy);
        u_r1_3_int_.set_basis_functions(fx,fy);
        u_r2_3_int_.set_basis_functions(fx,fy);
        GaussRule gr(this->tgr.GetRule()), GR(3);
        test_var = 0.;
        test_var+= r1_3_int_.get_numeric_integral_r1_3(gr, mdlp); // добавка интеграла с особенностью
        test_var+= r2_3_int_.get_numeric_integral_r2_3(gr, mdlp);        
        ret = vect_doub_pot + mod_doub_pot + test_var; // складываем потенциалы после интегрирования, чтобы не накапливать погрешность, складывая потенциалы в каждой точке Гаусса.
	return ret;
}

complex<double> Operator_B::get_value_mod_pot(bool is_singular, std::complex<double> vave_num_k, VectorXD<double, 3> &glob_coord, VectorBasisInterface *fx)
{
	complex<double> ret, area;
	vectCache_elem fyCache;
	ret = 0.;	
	if (!fx)
		ret = 0.;	
	triangle_parameters current_parameter;
	// треугольник вырожденный, любая его гауссовская координата должна отображаться в x0
	current_parameter.x0 = glob_coord;
	current_parameter.x1 = glob_coord;
	current_parameter.x2 = glob_coord;
	// инициализация волновых чисел для различных подобластей
	mdlp.set_vave_num(vave_num_k);
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double, 2> gauss_points;	
	complex<double> acc;	
	// точки Гаусса (L - координаты)
	gauss_points.coords[0] = 0.;
	gauss_points.coords[1] = 0.;
        current_parameter.s_t = fx->current_parameter.s_t;
        current_parameter.t_k = fx->current_parameter.t_k;
	// вычисляем модифицированный потенциал двойного слоя
	acc = mdlp.EvalfPotential(is_singular, fx, gauss_points, current_parameter);
        //acc = mdlp.LinearPotential(gauss_points, current_parameter);
	return acc;
}
// возвращаем значение в глобальных координатах
VectorXD<complex<double>, 3> Operator_B::get_value_vect_doub_pot(bool is_singular, std::complex<double> vave_num_k, VectorXD<double, 3> &glob_coord, VectorBasisInterface *fx)
{
	complex<double> ret, area;
	vectCache_elem fyCache;
	ret = 0.;
	if (!fx)
		ret = 0.;
	triangle_parameters current_parameter;
	// треугольник вырожденный, любая его гауссовская координата должна отображаться в x0
	current_parameter.x0 = glob_coord;
	current_parameter.x1 = glob_coord;
	current_parameter.x2 = glob_coord;
	// инициализация волновых чисел для различных подобластей
	vdlp.set_vave_num(vave_num_k);
	// численное интегрирование по множеству треугольника y (lambda - функция)
	VectorXD<double, 2> gauss_points;
	VectorXD<complex<double>,3> acc, glob_res;
	// точки Гаусса (L - координаты)
	gauss_points.coords[0] = 0.;
	gauss_points.coords[1] = 0.;
	// вычисляем модифицированный потенциал двойного слоя
	acc = vdlp.EvalfVectDoubPotential(is_singular, fx, gauss_points, current_parameter);
	for (uint p = 0; p < 3; ++p) // переводим в систему глобальных координат
		glob_res.coords[p] =   acc.coords[0] * fx->current_parameter.r1.coords[p] +
							   acc.coords[1] * fx->current_parameter.r2.coords[p] + 
							   acc.coords[2] * fx->current_parameter.n.coords[p];
	return glob_res;
}

complex<double> Operator_B::evalf_singular_muly_trig_operator(complex<double> vave_num_k)
{
    complex<double> ret, area;
    vectCache_elem fyCache;
    ret = 0.;	
    if(!fx || !fy)
            ret = 0.;
    // считаем площадь треугольника
    area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    // инициализация волновых чисел для различных подобластей
    sdlp.set_vave_num( vave_num_k );
    mdlp.set_vave_num( vave_num_k );
    vdlp.set_vave_num( vave_num_k );
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val;
    double normal_proj;
    complex<double> acc, vect_doub_pot(0.,0.);
    complex<double> test_var, mod_doub_pot(0.,0.);
    VectorXD<complex<double>,3> vect_val;
    VectorXD<double,3> global_arg, local_arg;
    Mod_Doub_Pot_by_parts mdlp_partial;
    //    
    std::vector< triangle_parameters > params_x1, params;
    fy->current_parameter.Build_Sections(params_x1);
    for(int k=0; k<params_x1.size(); ++k)
        params_x1[k].Build_Sections(params);
    //
    for(int i=0; i<params.size(); ++i)
    {    
        area = params[i].s_t * params[i].t_k * 0.5;
        while( tgr.next() ) // цикл по числу точек Гаусса
        {   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            // в этих точках вычисляем функцию lambda
            global_arg = params[i].get_Global_point(gauss_points);
            local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
            local_lambda_val = fy->lambda( local_arg ); // в локальных координатах треугольника Ty
            global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
            local_lambda_val = 
                    fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
            normal_proj = local_lambda_val.coords[2]; // проекция этого значения на нормаль
            // вычисляем модифицированный потенциал двойного слоя
            acc = 0.;              
            vect_val = vdlp.EvalfVectDoubPotential_no_r_int(fx,gauss_points,params[i]);
            for(int p=0; p<3; ++p)
                    acc += vect_val.coords[p] * local_lambda_val.coords[p];
            // умножение на веса разложения по формуле гаусса Summ(f(gauss_point[i]) * w[i], i = 1..psize);
            vect_doub_pot += acc * tgr.w * area;		
        }
    }
    // теперь к test_var добавим выражение интеграла с особенностью третьего порядка (итеграл взят по двум треугольникам)
    GaussRule gr(this->tgr.GetRule()), GR(3);
    mdlp_partial.init_potential(this->fx,this->fy,gr,vave_num_k,true);
    mod_doub_pot = mdlp_partial.EvalfIntegral();
    ret = vect_doub_pot - mod_doub_pot; 
    return ret;
}

complex<double> Operator_B::evalf_multy_trig_operator(bool is_singular, std::complex<double> vave_num_k)
{
    complex<double> ret, area;
    vectCache_elem fyCache;
    ret = 0.;	
    if(!fx || !fy)
        ret = 0.;
    // считаем площадь треугольника
    area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    // инициализация волновых чисел для различных подобластей
    sdlp.set_vave_num( vave_num_k );
    mdlp.set_vave_num( vave_num_k );
    vdlp.set_vave_num( vave_num_k );
    // численное интегрирование по множеству треугольника y (lambda - функция)
    VectorXD<double,2> gauss_points;
    VectorXD<double,3> local_lambda_val, global_lambda_val;
    double normal_proj;
    complex<double> acc, vect_doub_pot(0.,0.);
    complex<double> test_var, mod_doub_pot(0.,0.);
    VectorXD<complex<double>,3> vect_val;         
    VectorXD<double,3> global_arg, local_arg;
    //    
    std::vector< triangle_parameters > params_x1, params;
    fy->current_parameter.Build_Sections(params_x1);
    for(int k=0; k<params_x1.size(); ++k)
        params_x1[k].Build_Sections(params);
    //
    for(int i=0; i<params.size(); ++i)
    {    
        area = params[i].s_t * params[i].t_k * 0.5;
	while( tgr.next() ) // цикл по числу точек Гаусса
	{  
            // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            // в этих точках вычисляем функцию lambda
            global_arg = params[i].get_Global_point(gauss_points);
            local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);		
            local_lambda_val = fy->lambda( local_arg ); // в локальных координатах треугольника Ty
            global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
            local_lambda_val = 
                    fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
            normal_proj = local_lambda_val.coords[2]; // проекция этого значения на нормаль
            // вычисляем модифицированный потенциал двойного слоя
            test_var = acc = 0.;
            if(abs(normal_proj / sqrt(fx->current_parameter.s_t * fx->current_parameter.t_k) ) > 1e-9)
                test_var = normal_proj * mdlp.EvalfPotential(is_singular,fx,gauss_points,params[i]);                
            acc = 0.;
            vect_val = vdlp.EvalfVectDoubPotential(is_singular,fx,gauss_points,params[i]);
            for(int p=0; p<3; ++p)
                    acc += vect_val.coords[p] * local_lambda_val.coords[p];
            // умножение на веса разложения по формуле гаусса Summ(f(gauss_point[i]) * w[i], i = 1..psize);
            vect_doub_pot += acc * tgr.w * area;
            mod_doub_pot += test_var * tgr.w * area;		
	}
    }
    ret = vect_doub_pot + mod_doub_pot; // складываем потенциалы после интегрирования, чтобы не накапливать погрешность, складывая потенциалы в каждой точке Гаусса.
    return ret;
}