/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include "r1_3_integral.h"


double r1_3_integral::pure_numeric_integral_x16(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty)
{
    TrigGaussRuleInfo tgri(gr);
    VectorXD<double,2> gauss_points;
    double acc, ret(0.);
    double area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    mdlp.set_trg_params(fx->current_parameter);
//    Silo_writer writer;
//    std::vector< VectorXD<double,3> > points;
//    std::vector< double > values;
    
    std::vector< triangle_parameters > params_x1, params;
    fy->current_parameter.Build_Sections(params_x1);
    VectorXD<double,3> global_arg, local_arg;     
    for(int k=0; k<params_x1.size(); ++k)
        params_x1[k].Build_Sections(params);
    //
    for(int i=0; i<params.size(); ++i)
    {    
        area = params[i].s_t * params[i].t_k * 0.5;
//        mdlp.set_trg_params(params[i]);
        while( tgri.next() ) // цикл по числу точек Гаусса
        {   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgri.e.e1;
            gauss_points.coords[ 1 ] = tgri.e.e2;
            //
            global_arg = params[i].get_Global_point(gauss_points);
            local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
            gauss_points = fy->current_parameter.find_projection_point(local_arg);
            // в этих точках вычисляем интегрируемую функцию
            acc = numeric_integrand(gauss_points, mdlp, Ty);
//            points.push_back(global_arg);
//            values.push_back(acc);
            // в отличие от area, параметр весов w может менятся на итерациях
            acc *= (tgri.w * area);
            // добавляем новое слагаемое к результату
            ret += acc;
        }
    }
//    int N = 400;   
//    double koef = 0.95;
//    double step = 1. / N;
//    points.clear();
//    values.clear();
//    for(int i=N; i>=0; --i)
//    {
//        global_arg.coords[0] = 0.5;
//        global_arg.coords[1] = 0.5 + i * step;
//        global_arg.coords[2] = 0.5 + i * step * koef;
//        local_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
//        gauss_points = fy->current_parameter.find_projection_point(local_arg);
//        acc = numeric_integrand(gauss_points, mdlp, Ty);
//        points.push_back(global_arg);
//        values.push_back(acc);
//    }
//    writer.write_values_and_points(points,values,"VisMesh/Numeric_r1.silo");
//    writer.write_txt(points, values, "VisMesh/Numeric_r1.txt");
    return ret;
}

double r1_3_integral::numeric_integrand(VectorXD<double,3> global_arg, Modyfied_Doub_Layer_Potential &mdlp)
{
    Ty_params Ty_points;
    VectorXD<double,3> loc_arg;
    VectorXD<double,2> gauss_point;
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    build_Ty_params(Tx,Ty,Ty_points);
    loc_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
    gauss_point = fy->current_parameter.find_projection_point( loc_arg );
    return numeric_integrand(gauss_point, mdlp, Ty_points);
}

// выдача значений происходит вдоль двух интервалов
// строки в файлах соответствуют номеру интервала
void r1_3_integral::out_values_along_the_interval(STR POINTS, STR VALUES, int n)
{
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    uint coord_nums[2];
    init_params = true;    
    Ty_params Ty_points;
    build_Ty_params(Tx,Ty,Ty_points);
    get_integr_coord_num(Ty_points, coord_nums);
    lines_Ty_calc_params(Ty_points, coord_nums[0], coord_nums[1]);
    this->zero = sqrt(Ty.s_t * Ty.t_k * 0.5) * 1e-8;
    double interval_1[2], interval_2[2];
    interval_1[0] = Ty_points.vertexes_Tx[0].coords[ coord_nums[0] ];
    interval_1[1] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];  
    interval_2[0] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];
    interval_2[1] = Ty_points.vertexes_Tx[2].coords[ coord_nums[0] ];
    double start_value[2], step[2];
    std::ofstream Points, Values;
    Points.open(POINTS);
    Values.open(VALUES);
    step[0] = (interval_1[1] - interval_1[0]) / n;
    step[1] = (interval_2[1] - interval_2[0]) / n;
    start_value[0] = interval_1[0];
    start_value[1] = interval_2[0];
    int m = n;
    double arg;
    for(int i=0; i<2; ++i)
    {
        if( abs(n*step[i]) > zero)
            for(int k=0; k<m; ++k)
            {
                arg = start_value[i] + step[i] * k;
                Points << arg << "\t";
                Values << (*this)(arg) << "\t";
            }
        Points << "\n";
        Values << "\n";
    }
    Points.close();
    Values.close();
}

double r1_3_integral::get_numeric_integral_r1_3(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp)
{
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    uint coord_nums[2];
    init_params = true;
    mdlp.set_trg_params(Tx);
    Ty_params Ty_points;
    build_Ty_params(Tx,Ty,Ty_points);
    get_integr_coord_num(Ty_points, coord_nums);
    lines_Ty_calc_params(Ty_points, coord_nums[0], coord_nums[1]);
    this->zero = sqrt(Ty.s_t * Ty.t_k * 0.5) * 1e-8;
    GaussRule GR(3);
    // вначале вычисляем полностью численный интеграл (с вычитанием особенности)
    double numeric_part;
    switch(Ty_points.non_integr_coord_num)
    {
        case 0:
            numeric_part = pure_numeric_integral(gr, mdlp, Ty_points);
            break;
        case 1:
            numeric_part = pure_numeric_integral(gr, mdlp, Ty_points);
            break;
        case 2:
            numeric_part = pure_numeric_integral(gr, mdlp, Ty_points);
            break;
    }
    double analytical_part = 0.;
    double interval_1[2], interval_2[2];
    interval_1[0] = Ty_points.vertexes_Tx[0].coords[ coord_nums[0] ];
    interval_1[1] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];  
    interval_2[0] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];
    interval_2[1] = Ty_points.vertexes_Tx[2].coords[ coord_nums[0] ];    
    Integral integr;
    if(abs(interval_1[1] - interval_1[0]) > zero)
        analytical_part += integr.get_value(interval_1[0],interval_1[1],2,*this);
    if(abs(interval_2[1] - interval_2[0]) > zero)
        analytical_part += integr.get_value(interval_2[0],interval_2[1],2,*this); 
    double ret = analytical_part + numeric_part;
    return ret;
}

double r1_3_integral::get_numeric_integral_r1_3_x16(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp)
{
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    uint coord_nums[2];
    init_params = true;
    mdlp.set_trg_params(Tx);
    Ty_params Ty_points;
    build_Ty_params(Tx,Ty,Ty_points);
    get_integr_coord_num(Ty_points, coord_nums);
    lines_Ty_calc_params(Ty_points, coord_nums[0], coord_nums[1]);
    this->zero = sqrt(Ty.s_t * Ty.t_k * 0.5) * 1e-8;
    GaussRule GR(3);
    // вначале вычисляем полностью численный интеграл (с вычитанием особенности)
    double numeric_part;
    switch(Ty_points.non_integr_coord_num)
    {
        case 0:
            numeric_part = pure_numeric_integral_x16(gr, mdlp, Ty_points);            
            break;
        case 1:
            numeric_part = pure_numeric_integral_x16(gr, mdlp, Ty_points);
            break;
        case 2:
            numeric_part = pure_numeric_integral_x16(gr, mdlp, Ty_points);
            break;
    }
    double analytical_part = 0.;
    double interval_1[2], interval_2[2];
    interval_1[0] = Ty_points.vertexes_Tx[0].coords[ coord_nums[0] ];
    interval_1[1] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];  
    interval_2[0] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];
    interval_2[1] = Ty_points.vertexes_Tx[2].coords[ coord_nums[0] ];    
    Integral integr;
    if(abs(interval_1[1] - interval_1[0]) > zero)
        analytical_part += integr.get_value(interval_1[0],interval_1[1],2,*this);
    if(abs(interval_2[1] - interval_2[0]) > zero)
        analytical_part += integr.get_value(interval_2[0],interval_2[1],2,*this); 
    double ret = analytical_part + numeric_part;
    return ret;
}

double r1_3_integral::operator()(double a)
{    
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    double limits[2];       
    if(init_params)
    {
        uint coord_nums[2];
        init_params = false;
        build_Ty_params(Tx,Ty,Ty_points);
        get_integr_coord_num(Ty_points, coord_nums);
        integr_num_queue[0] = Ty_points.non_integr_coord_num;
        integr_num_queue[1] = coord_nums[0];
        integr_num_queue[2] = coord_nums[1];
        get_surface_coefs(Tx,Ty_points,surface_coefs);   
        lines_Ty_calc_params(Ty_points, integr_num_queue[1], integr_num_queue[2]);
    }
    double function_value; // значение функции в точке с особенностью (не важно, какой предел интегрирования выбран, верхний или нижний, функция заменит его на аргумент, с соответствующей особенностью)       
    get_analytical_limits(Ty_points, a, integr_num_queue[1], limits);    
    VectorXD<double,3> start_point, end_point;
    double t_star, alpha2, alpha1, t_vave, st;
    double res;
    t_star = Tx.t_star;
    t_vave = Tx.t_k;
    alpha2 = Tx.tg_a2;
    alpha1 = Tx.tg_a1;
    st = Tx.s_t;
    res = 0;
    start_point = get_x_local_coord(integr_num_queue, surface_coefs, a, limits[0]);
    end_point = get_x_local_coord(integr_num_queue, surface_coefs, a, limits[1]);
    double sy_start, ty_start, uy_start, sy_end, ty_end, uy_end;
    sy_start = start_point.coords[0];
    ty_start = start_point.coords[1];
    uy_start = start_point.coords[2];
    sy_end = end_point.coords[0];
    ty_end = end_point.coords[1];
    uy_end = end_point.coords[2];
    function_value = basis_function_combination(start_point, Ty_points.non_integr_coord_num, surface_coefs, true);
    if(fabs(function_value) < zero)
        return 0.;
    switch(Ty_points.non_integr_coord_num)
    {
        case 2:
            sy_start = max( start_point.coords[0], end_point.coords[0] );
            sy_end = min( start_point.coords[0], end_point.coords[0] );
            if(abs(alpha1) > 1e-8)
//                res += (r1_3_func_case_A(alpha1, 0., st, ty_start, sy_start, surface_coefs) + 
//                        r1_3_func_case_A(alpha1, -t_star, st, ty_end, sy_end, surface_coefs)) -
//                        (r1_3_func_case_A(alpha1, -t_star, st, ty_start, sy_start, surface_coefs) 
//                         + r1_3_func_case_A(alpha1, 0., st, ty_end, sy_end, surface_coefs));
            res += (r1_3_func_case_A(alpha1, 0., st, ty_start, sy_start, surface_coefs) -
                    r1_3_func_case_A(alpha1, -t_star, st, ty_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_A(alpha1, 0., st, ty_end, sy_end, surface_coefs) -
                    r1_3_func_case_A(alpha1, -t_star, st, ty_end, sy_end, surface_coefs));
            if(abs(alpha2) > 1e-8)
//                res += (r1_3_func_case_A(alpha2, t_vave-t_star, st, ty_start, sy_start, surface_coefs) +
//                        r1_3_func_case_A(alpha2, 0., st, ty_end, sy_end, surface_coefs))-
//                        (r1_3_func_case_A(alpha2, 0., st, ty_start, sy_start, surface_coefs) +
//                        r1_3_func_case_A(alpha2, t_vave-t_star, st, ty_end, sy_end, surface_coefs) );
            res += (r1_3_func_case_A(alpha2, t_vave-t_star, st, ty_start, sy_start, surface_coefs) -
                    r1_3_func_case_A(alpha2, 0., st, ty_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_A(alpha2, t_vave-t_star, st, ty_end, sy_end, surface_coefs) -
                    r1_3_func_case_A(alpha2, 0., st, ty_end, sy_end, surface_coefs));           	
            break;
        case 1:
            uy_start = max( start_point.coords[2], end_point.coords[2] );
            uy_end = min( start_point.coords[2], end_point.coords[2] );
            if(abs(alpha1) > 1e-8)
//                res += (r1_3_func_case_C(alpha1, 0., st, uy_start, sy_start, surface_coefs)
//                        + r1_3_func_case_C(alpha1, -t_star, st, uy_end, sy_end, surface_coefs)) -
//                        (r1_3_func_case_C(alpha1, -t_star, st, uy_start, sy_start, surface_coefs)
//                        +r1_3_func_case_C(alpha1, 0., st, uy_end, sy_end, surface_coefs));
            res += (r1_3_func_case_C(alpha1, 0., st, uy_start, sy_start, surface_coefs) -
                    r1_3_func_case_C(alpha1, -t_star, st, uy_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_C(alpha1, 0., st, uy_end, sy_end, surface_coefs) -
                    r1_3_func_case_C(alpha1, -t_star, st, uy_end, sy_end, surface_coefs));
            if(abs(alpha2) > 1e-8)
//                res += (r1_3_func_case_C(alpha2, t_vave-t_star, st, uy_start, sy_start, surface_coefs)
//                        +r1_3_func_case_C(alpha2, 0., st, uy_end, sy_end, surface_coefs))-
//                        (r1_3_func_case_C(alpha2, 0., st, uy_start, sy_start, surface_coefs)
//                        +r1_3_func_case_C(alpha2, t_vave-t_star, st, uy_end, sy_end, surface_coefs));
            res += (r1_3_func_case_C(alpha2, t_vave-t_star, st, uy_start, sy_start, surface_coefs) -
                    r1_3_func_case_C(alpha2, 0., st, uy_start, sy_start, surface_coefs)) -
                    
                    (r1_3_func_case_C(alpha2, t_vave-t_star, st, uy_end, sy_end, surface_coefs) -
                    r1_3_func_case_C(alpha2, 0., st, uy_end, sy_end, surface_coefs));           	
            break;
        case 0:
            uy_start = max( start_point.coords[2], end_point.coords[2] );
            uy_end = min( start_point.coords[2], end_point.coords[2] );
            if(abs(alpha1) > 1e-8)
//                res += (r1_3_func_case_B(alpha1, 0., st, ty_start, uy_start, surface_coefs)
//                        +r1_3_func_case_B(alpha1, -t_star, st, ty_end, uy_end, surface_coefs))-
//                        (r1_3_func_case_B(alpha1, -t_star, st, ty_start, uy_start, surface_coefs)
//                        +r1_3_func_case_B(alpha1, 0., st, ty_end, uy_end, surface_coefs));
            res += (r1_3_func_case_B(alpha1, 0., st, ty_start, uy_start, surface_coefs) -
                    r1_3_func_case_B(alpha1, -t_star, st, ty_start, uy_start, surface_coefs)) -
                    
                    (r1_3_func_case_B(alpha1, 0., st, ty_end, uy_end, surface_coefs) -
                    r1_3_func_case_B(alpha1, -t_star, st, ty_end, uy_end, surface_coefs));
            if(abs(alpha2) > 1e-8)
//                res += (r1_3_func_case_B(alpha2, t_vave-t_star, st, ty_start, uy_start, surface_coefs)
//                        +r1_3_func_case_B(alpha2, 0., st, ty_end, uy_end, surface_coefs))
//                        -(r1_3_func_case_B(alpha2, 0., st, ty_start, uy_start, surface_coefs)
//                        +r1_3_func_case_B(alpha2, t_vave-t_star, st, ty_end, uy_end, surface_coefs));
            res += (r1_3_func_case_B(alpha2, t_vave-t_star, st, ty_start, uy_start, surface_coefs) -
                    r1_3_func_case_B(alpha2, 0., st, ty_start, uy_start, surface_coefs)) -
                    
                    (r1_3_func_case_B(alpha2, t_vave-t_star, st, ty_end, uy_end, surface_coefs) -
                    r1_3_func_case_B(alpha2, 0., st, ty_end, uy_end, surface_coefs));           	
            break;
    }    
    res /= fabs(Ty_points.ny.coords[ Ty_points.non_integr_coord_num ]);
    double ret = res*function_value;    
    return ret;
}

VectorXD<double,3> r1_3_integral::get_x_local_coord(uint integr_num_queue[], double coefs[], double a, double limit)
{
    double rest_coord = coefs[0] * a + // координата, по которой проводится численное интегрирование
                        coefs[1] * limit + // координата, по которой проводилось аналитическое интегрирование
                        coefs[2]; // свободный член линейной функции недостающей координаты для получения точки
    VectorXD<double,3> point;
    point.coords[integr_num_queue[0]] = rest_coord; // исключённая координата
    point.coords[integr_num_queue[1]] = a;
    point.coords[integr_num_queue[2]] = limit;
    return point;
}

void r1_3_integral::lines_Ty_calc_params(Ty_params &Ty, uint num_integr_coord, uint analytic_integr_coord)
{
    Vector_predicate pred(num_integr_coord);
    sort(Ty.vertexes_Tx, Ty.vertexes_Tx+3, pred); // сортируем точки по параметру интегрирования
    uint s_num = analytic_integr_coord, t_num = num_integr_coord;
    // теперь выражаем линейную зависимость координаты, по которой производится аналитическое интегрирование от той координаты,
    // по которой интегрирование проводится численно.
    Ty.line_coefs[0] = (Ty.vertexes_Tx[2].coords[s_num] - Ty.vertexes_Tx[0].coords[s_num]) / (Ty.vertexes_Tx[2].coords[t_num] - Ty.vertexes_Tx[0].coords[t_num]);
    Ty.line_coefs[1] = (Ty.vertexes_Tx[1].coords[s_num] - Ty.vertexes_Tx[0].coords[s_num]) / (Ty.vertexes_Tx[1].coords[t_num] - Ty.vertexes_Tx[0].coords[t_num]);
    Ty.line_coefs[2] = (Ty.vertexes_Tx[2].coords[s_num] - Ty.vertexes_Tx[1].coords[s_num]) / (Ty.vertexes_Tx[2].coords[t_num] - Ty.vertexes_Tx[1].coords[t_num]);
    Ty.t0[0] = Ty.vertexes_Tx[0].coords[t_num];
    Ty.t0[1] = Ty.vertexes_Tx[0].coords[t_num];
    Ty.t0[2] = Ty.vertexes_Tx[1].coords[t_num];
    Ty.s0[0] = Ty.vertexes_Tx[0].coords[s_num];
    Ty.s0[1] = Ty.vertexes_Tx[0].coords[s_num];
    Ty.s0[2] = Ty.vertexes_Tx[1].coords[s_num];
}

// по каждому значению численного интеграла a определяем пределы интегрирования
void r1_3_integral::get_analytical_limits(Ty_params &Ty, double a, uint num_integr_coord, double limits[])
{
    uint int_case;
    uint t_num = num_integr_coord;
    if( (a > Ty.vertexes_Tx[0].coords[t_num]) && (a < Ty.vertexes_Tx[1].coords[t_num]) )
        int_case = 1;
    else
        int_case = 2;
    // предполагаем, что параметр a всегда находится в пределах между
    // Ty.vertexes_Tx[0].coords[t_num] и Ty.vertexes_Tx[2].coords[t_num]
    switch(int_case)
    {
        case 1:
            limits[0] = Ty.line_coefs[0] * (a - Ty.t0[0]) + Ty.s0[0];
            limits[1] = Ty.line_coefs[1] * (a - Ty.t0[1]) + Ty.s0[1];
            break;
        case 2:
            limits[0] = Ty.line_coefs[0] * (a - Ty.t0[0]) + Ty.s0[0];
            limits[1] = Ty.line_coefs[2] * (a - Ty.t0[2]) + Ty.s0[2];
            break;
        default:
            break;
    }
    sort(limits,limits+2); // эти пределы интегрирования так же упорядочены по ворзрастанию (интегрируем по площади).
}

double r1_3_integral::pure_numeric_integral(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty)
{
    TrigGaussRuleInfo tgri(gr);
    VectorXD<double,2> gauss_points;
    double acc, ret(0.);
    double area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    mdlp.set_trg_params(fx->current_parameter);
//    Silo_writer writer;
//    std::vector<VectorXD<double,3> > points;
//    std::vector< double > values;
    
    while( tgri.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgri.e.e1;
        gauss_points.coords[ 1 ] = tgri.e.e2;
        // в этих точках вычисляем интегрируемую функцию
        acc = numeric_integrand(gauss_points, mdlp, Ty); 
//        VectorXD<double,3> glob_arg = fy->current_parameter.get_Global_point(gauss_points);
//        points.push_back(glob_arg);
//        values.push_back(acc);
        // в отличие от area, параметр весов w может менятся на итерациях
        acc *= (tgri.w * area);
        // добавляем новое слагаемое к результату
        ret += acc;
    }
//    writer.write_values_and_points(points,values,"VisMesh/Numeric_r1.silo");
    return ret;
}
double r1_3_integral::numeric_integrand(VectorXD<double,2> &Gauss_point, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty_points)
{
    triangle_parameters &Tx = fx->current_parameter;    
    triangle_parameters &Ty = fy->current_parameter;
    VectorXD<double,3> glob_arg, x_arg, x_arg_special;
    // вычисляем аргумент в локальных координатах треугольника Tx
    glob_arg = Ty.get_Global_point(Gauss_point); // интегрирование ведётся по треугольнику Ty, точки выбираются на нём!
    x_arg = Tx.get_local_coordinate_point(glob_arg); // затем точка переводится в координаты треугольника Tx
    x_arg_special = x_arg; // x_arg_special - дублирует функционал basis_function_combination с той разницей, что здесь надо определить расстояние до точки, в которой вычисляется аргумент
    double surface_coefs[3];
    get_surface_coefs(Tx, Ty_points, surface_coefs);
    double sy, ty, uy, A, B, C;
    A = surface_coefs[0];
    B = surface_coefs[1];
    C = surface_coefs[2];
    switch(Ty_points.non_integr_coord_num)
    {
        case 0:
            x_arg_special.coords[2] = 0.; // интегрирование по ty, uy, uy = 0.            
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[0] = A * ty + B * uy + C;
            break;
        case 1:
            x_arg_special.coords[2] = 0.; // интегрирование по uy, sy, sy = st    
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[1] = A * sy + B * uy + C;
            break;
        case 2:
            x_arg_special.coords[0] = Tx.s_t; // интегрирование по ty, sy, sy = st
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[2] = A * ty + B * sy + C;
            break;
        default:
            break;
    }
    // вычисляем необходимую нам комбинацию
    double a_value = basis_function_combination(x_arg,Ty_points.non_integr_coord_num, surface_coefs,false);
    double b_value = basis_function_combination(x_arg,Ty_points.non_integr_coord_num, surface_coefs,true); // выделяем особенность
    double delta = a_value - b_value, distance;
    distance = (x_arg - x_arg_special).norma();
    if(abs(delta) < zero)
        return 0.;
    double r1_3_integral_mdlp = mdlp.r1_3_component(x_arg);
    return delta * r1_3_integral_mdlp;
}
// функция, используемая в методе вычитания особенности
double r1_3_integral::basis_function_combination(VectorXD<double,3> loc_x_arg, uint non_integr_num, double surface_coefs[], bool singularity_point)
{
    triangle_parameters &Tx = fx->current_parameter;
    VectorXD<double,3> arg;
    double ret;
    arg = loc_x_arg;   
    // arg - критическая точка с особенностью, в нашем случае это множество с особенностью при sy = st и uy = 0
    // поэтому внешний интеграл оставляем по произвольной переменной (ty или uy), а особенность определяем для координаты st = sy
    // можно было определить особенность для координаты uy.
    double A, B, C;
    double sy, ty, uy;
    A = surface_coefs[0];
    B = surface_coefs[1];
    C = surface_coefs[2];
    if(singularity_point) // в этом случае интегрирование проводится с одной степенью свободы и один из параметров связан значением в точке сингулярности
    switch(non_integr_num)
    {
        case 0:
            arg.coords[2] = 0.; // интегрирование по ty, uy, uy = 0.            
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[0] = A * ty + B * uy + C;
            break;
        case 1:
            arg.coords[2] = 0.; // интегрирование по uy, sy, sy = st    
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[1] = A * sy + B * uy + C;
            break;
        case 2:
            arg.coords[0] = Tx.s_t; // интегрирование по ty, sy, sy = st
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[2] = A * ty + B * sy + C;
            break;
        default:
            break;
    }
    double u_s_val; // особенность возникает при интегрировании только для этой компоненты
    u_s_val = fx->u_tangen(arg).coords[0];
    VectorXD<double,3> global_arg, local_y_arg, lambda_y_val;
    // вычисляем аргументы для функции lambda
    global_arg = fx->current_parameter.get_global_coordinate_point(arg);
    local_y_arg = fy->current_parameter.get_local_coordinate_point(global_arg);
    // теперь получим значение функции в координатах x-треугольника
    lambda_y_val = fy->lambda(local_y_arg);
    global_arg = fy->current_parameter.S * lambda_y_val;
    // (lambda, nx) * u_s
    ret = global_arg.dotprod(fx->current_parameter.n) * u_s_val;
    return ret;
}

double r1_3_integral::F_arctan_2_mult(double A1,double A2,double arg)
{
    	double disc = A2 * A2 - 4 * A1; // функция в любом случае действительная, она либо гиперболическая, либо триганометрическая
	double res, x;
        if(sqrt(fabs(disc)) < zero)
        {
            return 0.; // взял предел, atan - ограничен на бесконечности, sqrt(-disc) - убывает
        }
	if(disc < 0)
	{
		res = -atan( (2 * arg + A2) / sqrt( -disc ) ) * sqrt(-disc);
	}
	if(disc > 0)
	{
		x = 2 * arg + A2;
		x /= sqrt(disc);
		res = -0.5 * log( abs((1 + x)/(1 - x)) ) * sqrt(disc);                                
	}
	return res;
}

double r1_3_integral::F_arctan(double A1,double A2,double arg)
{
    	double disc = A2 * A2 - 4 * A1; // функция в любом случае действительная, она либо гиперболическая, либо триганометрическая
	double res, x;
	if(disc < 0)
	{
		res = -atan( (2 * arg + A2) / sqrt( -disc ) ) / sqrt(-disc);
	}
	if(disc > 0)
	{
		x = 2 * arg + A2;
		x /= sqrt(disc);
		res = 0.5 * log( abs((1 + x)/(1 - x)) )/sqrt(disc);                                
	}
	return res;
}

double r1_3_integral::log_doub_area_int(double V, double L, double D, double C, double x)
{
    double res2;
    // определяем вспомогательные константы
    double A1, A2, u, t, disc;
    A1 = D * (1 - V) / (1 + V);
    A2 = 2 * L / (1 + V);
    u = x;
    t = u + sqrt(fabs(u * u + D));
    // непосредственно вычисляем интегральное выражение
    disc = A2 * A2 - 4 * A1;
    double res_ex;
    // Вначале разберём частные случаи
    if(fabs(D) > zero)
    {
        if(fabs(V - 1) < 1e-8) // случай V == 1 возможен
        {
            if(fabs(L) > zero)
                return 0.5 * C * ( (t + L) * log(t + L) - (t + L) ) - D * C * 0.5 * (log(t + L) / t - log(t / (t + L)) / L);
            else
                return 0.5 * C * ( (t + L) * log(t + L) - (t + L) ) - D * C * 0.5 * (log(t + L) / t + 1/t);
        }
        if(fabs(V + 1) < 1e-8)
        {
            // V == -1
            t = -u + sqrt( fabs(u*u + D) );
            if(fabs(L) > zero)
            {
                double I = -t + L * log(fabs(t + L)) + (D / L) * log(fabs(t)) - (D / L) * log(fabs(t + L));
                double ret = (log(t + L)*u - 0.5 * I)*C;
                return ret;
            }
            else
            {
                double ret = u*log(t) + D / (2 * t) + t / 2.;
                return ret*C;
            }
            
        }
        // в противном случае рассматривается более общий случай
    }
    else
    {
        if(fabs(L) < zero)
        {
            double arg = fabs(V * u + L + sqrt(fabs(u*u + D)));
            if(fabs(arg) < zero)
            {   
                if(fabs(u) < zero)
                    return (- 0.5 * t)*C; // Если L = 0, То и аргумент логарифма тоже может быть равен нулю (поэтому случай рассмотрен отдельно)
                else
                    return 0.;
            }
            else
                return (u*log(arg) - 0.5 * t) * C;
        }
        else
        {
            double arg = (V + 1)*t + 2.*L;
            if(fabs(arg) < zero)
                std::runtime_error("Impossible case!");
            else
                return (u*log(arg) - 0.5 * t + L * log( arg ) / (V + 1))*C;
        }
    }
//    if(fabs(L) < zero && fabs(V) < 1e-8)
//    {
//        if(fabs(D) < zero)
//            res_ex = u * log(u) - u;
//        else
//        {
//            res_ex = 0.5 * u * log(fabs(u*u + D)) - u + sqrt(D)*atan(u / sqrt(D));           
//        }
//        return res_ex*C;
//    }
    // более общий случай
    if(abs(disc)<zero)
    {
        //res2 = t / 2 - D / (2 *t) + log(abs(2 * t + A2)) * ( 2 * D / A2 - A2 / 2) - 2 * D * log(abs(t))/A2; // в этом случае A1 = A2 * A2 / 4 <- корень квардратного выражения 
        res2 = 0.;
        res2 -= t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) +
                F_arctan_2_mult(A1,A2,t)*(0.5 + D / (2.*A1));
        double arg1, arg2, mult_log;
        mult_log = A2 / 4 - D * A2 / (4 * A1);
        if(fabs(mult_log + u) < zero) // НЕ СМОГ ВЫЧИСЛИТЬ ПРЕДЕЛ АНАЛИТИЧЕСКИ!!! ВЫРАЖАЮ ЕГО ЧИСЛЕННО ОТСТУПОМ ОТ НУЛЯ!
        {
            u += fabs(u / 1e+6) + zero*10;
            // так же возмущаем константу L, т. к. другая может быть равна нулю.
            L += fabs(L / 1e+6) + zero*10;
            D += fabs(D / 1e+6) + zero*10;
            t = u + sqrt(u*u + D);
        }
        arg1 = abs(V*u + L + sqrt(u*u+D));
        arg2 = abs(t*t + A2*t + A1);
        if( arg1 < zero && arg2 < zero && fabs(mult_log + u) < zero)
        {
            //res2 += ( log(fabs(arg1 / arg2)) * u );            
            return res2 * C;
        }
        else
        {
            double res3 = log( arg1 ) * u + log( arg2 ) * mult_log;
            res2 += res3;            
            res2 *= C;
            return res2;
        } 
    }
    else
    {	// если нулю равен D, то нулю равен коэффициент A1
            if(abs(D)>zero)
            {
                res2 = 0.;
                res2 -= t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) - 
                       log( abs(t*t + A2*t + A1) ) * (A2 / 4 - D * A2 / (4 * A1));
                res2 -= F_arctan_2_mult(A1,A2,t)*(0.5 + D / (2.*A1));
            }
            else
            {
                    res2 = t - A2 * log(abs(t + A2)); // странный момент! разберись, здесь ошибка!
            }
    }
    res2 *= C;
    double res1 = log( abs(V*u + L + sqrt(u*u+D)) )*u*C;    
    return res2 + res1;
}

double r1_3_integral::r1_3_func_case_A(double alpha, double t, double st, double ty, double sy, double surface_coefs[])
{
    double res1, res2;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = alpha*alpha*(1 + B*B);
    double b = -2.*alpha*t + 2*alpha*alpha*A*B*ty + 2*alpha*alpha*B*C;
    double c = t*t + alpha*alpha*pow(t - ty, 2) + alpha*alpha*A*A*ty*ty + alpha*alpha*C*C + 2.*alpha*alpha*A*C*ty;
    double V_wave = -alpha / sqrt(1. + alpha*alpha);
    double L_wave = (t + alpha*alpha*(t - ty)) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double s_wave = sqrt(a) * sy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res1 = log_doub_area_int(V, L, D, 1./sqrt(a), s_wave) * fabs(alpha) / sqrt(1 + alpha*alpha);
    // аналогично, нижний предел по t
    a = B*B + 1;
    b = 2*A*B*ty + 2.*B*C - 2*st;
    c = pow(t - ty,2) + A*A*ty*ty + C*C + 2.*A*ty*C + st*st;
    s_wave = sqrt(a) * sy + b / (2.*sqrt(a));
    D = c - b*b / (4. * a);
    L = t - ty;
    V = 0.;
    res2 = log_doub_area_int(V, L, D, 1./sqrt(a), s_wave);
    return (res1 - res2) / (4*PI);
}

double r1_3_integral::r1_3_func_case_B(double alpha, double t, double st, double ty, double uy, double surface_coefs[])
{
    double res1, res2;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = alpha*alpha*(1 + B*B);
    double b = 2*alpha*alpha*A*B*ty - 2*alpha*t*B + alpha*alpha*2.*B*C;
    double c = alpha*alpha*pow(t - ty, 2.) + alpha*alpha*A*A*ty*ty + 
               t*t - 2.*alpha*t*A*ty + alpha*alpha*C*C + 2.*A*ty*C*alpha*alpha - 2.*alpha*t*C;
    double V_wave = -alpha * B / sqrt(1. + alpha*alpha);
    double L_wave = (t + alpha*alpha*(t - ty) - alpha*A*ty - alpha*C) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res1 = log_doub_area_int(V, L, D, 1./sqrt(a), u_wave) * fabs(alpha) / sqrt(1 + alpha*alpha);
    // аналогично, нижний предел по t
    a = B*B + 1;
    b = -2.*B*st + 2.*A*B*ty + 2.*B*C;
    c = pow(t-ty,2) + st*st + A*A*ty*ty + C*C - 2.*A*st*ty - 2.*C*st + 2.*A*C*ty;
    u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    D = c - b*b / (4. * a);
    L = t - ty;
    V = 0.;
    res2 = log_doub_area_int(V, L, D, 1./sqrt(a), u_wave);
    return (res1 - res2) / (4*PI);
}

double r1_3_integral::r1_3_func_case_C(double alpha, double t, double st, double uy, double sy, double surface_coefs[])
{
    double res1, res2;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = alpha*alpha*(1 + B*B);
    double b = -2.*alpha*alpha*t*B + 2.*alpha*alpha*B*C + 2.*alpha*alpha*A*B*sy;
    double c = pow(t - alpha*sy, 2) + alpha*alpha*t*t - 2*alpha*alpha*t*A*sy + 
				2.*alpha*alpha*A*sy*C - 2.*alpha*alpha*t*C + alpha*alpha*A*A*sy*sy + alpha*alpha*C*C;
    double V_wave = -alpha*alpha*B / sqrt(1. + alpha*alpha);
    double L_wave = (alpha*alpha*t - alpha*alpha*A*sy - alpha*alpha*C + t - alpha*sy) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res1 = log_doub_area_int(V, L, D, 1./sqrt(a), u_wave) * fabs(alpha) / sqrt(1 + alpha*alpha);
    // аналогично, нижний предел по t
    a = 1+B*B;
    b = -2.*B*t + 2.*B*C+2.*A*B*sy;
    c = t*t - 2*t*A*sy + 2.*A*sy*C - 2*t*C + A*A*sy*sy + C*C + st*st - 2.*st*sy + sy*sy;
	V_wave = -B;
	L_wave = t - A*sy - C;
    u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    V = V_wave / sqrt(a);
    L = L_wave - V_wave * b / (2. * a);
    D = c - b*b / (4.*a);
    res2 = log_doub_area_int(V, L, D, 1./sqrt(a), u_wave);
    return (res1 - res2) / (4*PI);
}

void r1_3_integral::get_integr_coord_num(Ty_params& Ty, uint coord_nums[])
{
    switch(Ty.non_integr_coord_num)
    {
        case 0: // координата sy, случай Б
            coord_nums[0] = 1; // ty
            coord_nums[1] = 2; // uy
            break;
        case 1: // координата ty, случай В
            coord_nums[0] = 0; // sy
            coord_nums[1] = 2; // uy
            break;
        case 2: // uy, случай A
            coord_nums[0] = 1; // ty
            coord_nums[1] = 0; // sy
            break;
        default: // если эти координаты равны, значит исходые данные не верны.
            coord_nums[0] = 0;
            coord_nums[1] = 0;
            break;
    }    
}

void r1_3_integral::build_Ty_params(triangle_parameters &Tx, Ty_params &Ty, triangle *area, VectorXD<double,3> *points, uint trig_num)
{
    // находим локальные координаты точек
    for(uint p=0; p<3; ++p)
    {
        Ty.vertexes_Tx[p] = Tx.get_local_coordinate_point( points[ area[trig_num].pnums(p) ] );
    }
    // находим локальные координаты нормали
    VectorXD<double,3> A, B;
    A = Ty.vertexes_Tx[2] - Ty.vertexes_Tx[0];
    B = Ty.vertexes_Tx[1] - Ty.vertexes_Tx[0];
    fast_vector_mutl(A,B,Ty.ny); // направление нормали не важно, служит для определения коэффициентов уравнения плоскости.
    Ty.ny.normalize();
    // находим плоскость интегрирования (оптимальная плоскость для вычисления интеграла)
    VectorXD<double,3> AA[3], BB[3], acc;
    double mes[3];
    for(uint i=0; i<3; ++i)
    {
        AA[i] = A; BB[i] = B;
    }
    for(uint coord=0; coord<3; ++coord)    
    {   // нули соответствующих координат дают проекции на плоскости локальных координат sx, tx, nx
        AA[coord].coords[coord] = 0.;
        BB[coord].coords[coord] = 0.;
        fast_vector_mutl(AA[coord],BB[coord],acc);
        mes[coord] = acc.norma();
    }
    double *max_elem = max_element(mes,mes+3);
    Ty.non_integr_coord_num = max_elem - mes;
}

void r1_3_integral::build_Ty_params(triangle_parameters &Tx, triangle_parameters &TY, Ty_params &Ty)
{
    // находим локальные координаты точек
    Ty.vertexes_Tx[0] = Tx.get_local_coordinate_point( TY.x0 );  
    Ty.vertexes_Tx[1] = Tx.get_local_coordinate_point( TY.x1 );
    Ty.vertexes_Tx[2] = Tx.get_local_coordinate_point( TY.x2 );
    // находим локальные координаты нормали
    VectorXD<double,3> A, B;
    A = Ty.vertexes_Tx[2] - Ty.vertexes_Tx[0];
    B = Ty.vertexes_Tx[1] - Ty.vertexes_Tx[0];
    fast_vector_mutl(A,B,Ty.ny); // направление нормали не важно, служит для определения коэффициентов уравнения плоскости.
    Ty.ny.normalize();
    // находим плоскость интегрирования (оптимальная плоскость для вычисления интеграла)
    VectorXD<double,3> AA[3], BB[3], acc;
    double mes[3];
    for(uint i=0; i<3; ++i)
    {
        AA[i] = A; BB[i] = B;
    }
    for(uint coord=0; coord<3; ++coord)    
    {   // нули соответствующих координат дают проекции на плоскости локальных координат sx, tx, nx
        AA[coord].coords[coord] = 0.;
        BB[coord].coords[coord] = 0.;
        fast_vector_mutl(AA[coord],BB[coord],acc);
        mes[coord] = acc.norma();
    }
    double *max_elem = max_element(mes,mes+3);
    Ty.non_integr_coord_num = max_elem - mes;
}

void r1_3_integral::get_surface_coefs(triangle_parameters &Tx, Ty_params &Ty,  double surface_coefs[] )
{
    uint coord_num[2];
    get_integr_coord_num(Ty, coord_num);
    double C_wave = Ty.vertexes_Tx[0].dotprod(Ty.ny);
    surface_coefs[0] = -Ty.ny.coords[ coord_num[0] ] / Ty.ny.coords[ Ty.non_integr_coord_num ];
    surface_coefs[1] = -Ty.ny.coords[ coord_num[1] ] / Ty.ny.coords[ Ty.non_integr_coord_num ];
    surface_coefs[2] = C_wave / Ty.ny.coords[ Ty.non_integr_coord_num ];
}