#include "GaussRuleInfo.h"
#include <complex>
#include <math.h>
#include "base.hpp"
#include "VectorBasisInterface.hpp"
#include "FastVectorMult.hpp"
using namespace std;
/*
Данный код предназначен для полностью численного способа вычисления
оператора B. Данный способ избавлен от вычисления потенциала двойного
слоя, оператор B вычисляется напрямую, независимо от всех других процедур.
Это нужно для ускорения работы решателя, а так же для тестирования
исходного оператора.

Вычисление производится на прямую. Т. е. ротор от произведения фундаментального 
решение на вектор-функцию раскрывается как векторное произведение градиента
фундаментального решения на вектор-функцию.
*/
class Operator_B_num
{
	TrigGaussRuleInfo tgr_x, tgr_y;
	complex<double> vave_num_k;
	VectorBasisInterface *fx, *fy; // интерфейс векторных базисных функций на паре треугольников
	double step;
	/* реализация векторного произведения */
	static complex<double> Det(complex<double> M[][2], uint size);
	VectorXD<complex<double>,3> vector_mult(VectorXD<complex<double>,3> &a, VectorXD<double,3> &b);
	/* вспомогательные процедуры и функции */
	complex<double> GelmSolv(VectorXD<double,3> x, VectorXD<double,3> y); // фундаментальное решение уравнения Гельмгольца
	VectorXD<complex<double>,3> GradXGelmSolv(VectorXD<double,3> x, VectorXD<double,3> y); // градиент этого решения
	VectorXD<complex<double>,3> evalf_y_int(VectorXD<double,3> x); // вычисление интеграла по y для фиксированной точки x
public:
	Operator_B_num(GaussRule grX, GaussRule grY):tgr_x(grX),tgr_y(grY),step(1e-4){}
	void set_num_step(double step){this->step = (step < 0.1) ? step : 1e-6; }
	void set_gauss_rule(GaussRule grX, GaussRule grY){tgr_x = grX; tgr_y = grY;}
	bool operator_init(VectorBasisInterface *fx, VectorBasisInterface *fy); // передаем необходимые функции для вычисления элемента оператора B
	complex<double> evalf_operator(complex<double> vave_num_k); // по переданным значениям параметров функций и декомпозиций вычисляет компоненту матрицы, критерий сингулярности вычисляется отдельно
};
