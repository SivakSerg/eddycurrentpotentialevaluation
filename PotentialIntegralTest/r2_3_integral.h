/* 
 * File:   r2_3_integral.h
 * Author: root
 *
 * Created on June 8, 2015, 1:39 AM
 */

#ifndef R2_3_INTEGRAL_H
#define	R2_3_INTEGRAL_H
#pragma once

#include "r1_3_integral.h"
#include <fstream>

class r2_3_integral : public r1_3_integral
{
public:
    ~r2_3_integral(){};
    r2_3_integral(){};
    virtual double pure_numeric_integral(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // данная функция вычисляет интеграл от выражения для s-составляющей
    double pure_numeric_integral_x16(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // данная функция используется для реализации дробления множества интегрирования на 16 частей.
    virtual double numeric_integrand(VectorXD<double,2> &Gauss_point, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty); // интегрируемая функция в полностью численном интеграле.
    double numeric_integrand(VectorXD<double,3> global_arg, Modyfied_Doub_Layer_Potential &mdlp); // переводит глобальные координаты треугольника в точки Гаусса
    virtual double basis_function_combination(VectorXD<double,3> loc_x_arg, uint non_integr_num, double surface_coefs[], bool singularity_point);
    double r2_3_func_case_A(double alpha, double s, double ty, double sy, double surface_coefs[]); // ty, sy
    double r2_3_func_case_B(double alpha, double s, double ty, double uy, double surface_coefs[]); // ty, uy
    double r2_3_func_case_C(double alpha, double s, double sy, double uy, double surface_coefs[]); // sy, uy
    virtual double operator()(double a);
    double get_numeric_integral_r2_3(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp);
    double get_numeric_integral_r2_3_x16(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp);
    void out_values_along_the_interval(STR Points, STR Values, int n); //тестовая функция для выдачи значений вдоль интервала для интегрирования (проверка на непрерывность)
};

#endif	/* R2_3_INTEGRAL_H */

