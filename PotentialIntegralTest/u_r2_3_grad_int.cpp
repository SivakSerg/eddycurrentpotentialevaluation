/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "u_r2_3_grad_int.h"


double u_r2_3_grad_int::get_numeric_integral_r2_3(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp)
{
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    uint coord_nums[2];
    init_params = true;
    mdlp.set_trg_params(Tx);
    Ty_params Ty_points;
    build_Ty_params(Tx,Ty,Ty_points);
    get_integr_coord_num(Ty_points, coord_nums);
    lines_Ty_calc_params(Ty_points, coord_nums[0], coord_nums[1]);
    this->zero = sqrt(Ty.s_t * Ty.t_k * 0.5) * 1e-8;
    // вначале вычисляем полностью численный интеграл (с вычитанием особенности)
    double numeric_part = pure_numeric_integral(gr, mdlp, Ty_points);
    double analytical_part = 0.;
    double interval_1[2], interval_2[2];
    interval_1[0] = Ty_points.vertexes_Tx[0].coords[ coord_nums[0] ];
    interval_1[1] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];  
    interval_2[0] = Ty_points.vertexes_Tx[1].coords[ coord_nums[0] ];
    interval_2[1] = Ty_points.vertexes_Tx[2].coords[ coord_nums[0] ];    
    Integral integr;    
    analytical_part += integr.get_value(interval_1[0],interval_1[1],2,*this);
    analytical_part += integr.get_value(interval_2[0],interval_2[1],2,*this); 
    double ret = analytical_part + numeric_part;
    return ret;
}

double u_r2_3_grad_int::operator()(double a)
{
    triangle_parameters &Tx = fx->current_parameter;
    triangle_parameters &Ty = fy->current_parameter;
    double limits[2];       
    if(init_params)
    {
        uint coord_nums[2];
        init_params = false;
        build_Ty_params(Tx,Ty,Ty_points);
        get_integr_coord_num(Ty_points, coord_nums);
        integr_num_queue[0] = Ty_points.non_integr_coord_num;
        integr_num_queue[1] = coord_nums[0];
        integr_num_queue[2] = coord_nums[1];
        get_surface_coefs(Tx,Ty_points,surface_coefs);   
        lines_Ty_calc_params(Ty_points, integr_num_queue[1], integr_num_queue[2]);
    }
    double function_value; // значение функции в точке с особенностью (не важно, какой предел интегрирования выбран, верхний или нижний, функция заменит его на аргумент, с соответствующей особенностью)       
    get_analytical_limits(Ty_points, a, integr_num_queue[1], limits);    
    VectorXD<double,3> start_point, end_point;
    double t_star, alpha2, alpha1, t_vave, st;
    double res;
    t_star = Tx.t_star;
    t_vave = Tx.t_k;
    alpha2 = Tx.tg_a2;
    alpha1 = Tx.tg_a1;
    st = Tx.s_t;
    res = 0;
    start_point = get_x_local_coord(integr_num_queue, surface_coefs, a, limits[0]);
    end_point = get_x_local_coord(integr_num_queue, surface_coefs, a, limits[1]);
    double sy_start, ty_start, uy_start, sy_end, ty_end, uy_end;
    sy_start = start_point.coords[0];
    ty_start = start_point.coords[1];
    uy_start = start_point.coords[2];
    sy_end = end_point.coords[0];
    ty_end = end_point.coords[1];
    uy_end = end_point.coords[2];
    function_value = basis_function_combination(start_point, Ty_points.non_integr_coord_num, surface_coefs, true);
    switch(Ty_points.non_integr_coord_num)
    {
        case 2:// ty, sy
            sy_start = max( start_point.coords[0], end_point.coords[0] );
            sy_end = min( start_point.coords[0], end_point.coords[0] );
            res +=( (r2_3_func_case_A(alpha2, st, ty_start, sy_start, surface_coefs) -
					 r2_3_func_case_A(alpha2, 0, ty_start, sy_start, surface_coefs)) -
					(r2_3_func_case_A(alpha1, st, ty_start, sy_start, surface_coefs) -
					 r2_3_func_case_A(alpha1, 0., ty_start, sy_start, surface_coefs)) ) -

				  ( (r2_3_func_case_A(alpha2, st, ty_end, sy_end, surface_coefs) -
					 r2_3_func_case_A(alpha2, 0, ty_end, sy_end, surface_coefs)) -
					(r2_3_func_case_A(alpha1, st, ty_end, sy_end, surface_coefs) -
					 r2_3_func_case_A(alpha1, 0., ty_end, sy_end, surface_coefs)) );              	
            break;
        case 1:// sy, uy
            uy_start = max( start_point.coords[2], end_point.coords[2] );
            uy_end = min( start_point.coords[2], end_point.coords[2] );
            res +=(	(r2_3_func_case_C(alpha2, st, sy_start, uy_start, surface_coefs) -
					 r2_3_func_case_C(alpha2, 0, sy_start, uy_start, surface_coefs)) -
					(r2_3_func_case_C(alpha1, st, sy_start, uy_start, surface_coefs) -
					 r2_3_func_case_C(alpha1, 0., sy_start, uy_start, surface_coefs)) ) -

				(	(r2_3_func_case_C(alpha2, st, sy_end, uy_end, surface_coefs) -
					 r2_3_func_case_C(alpha2, 0, sy_end, uy_end, surface_coefs)) -
					(r2_3_func_case_C(alpha1, st, sy_end, uy_end, surface_coefs) -
					 r2_3_func_case_C(alpha1, 0., sy_end, uy_end, surface_coefs)) );          	
            break;
        case 0:// ty, uy
            uy_start = max( start_point.coords[2], end_point.coords[2] );
            uy_end = min( start_point.coords[2], end_point.coords[2] );
            res += (	(r2_3_func_case_B(alpha2, st, ty_start, uy_start, surface_coefs) -
						 r2_3_func_case_B(alpha2, 0, ty_start, uy_start, surface_coefs)) -

						(r2_3_func_case_B(alpha1, st, ty_start, uy_start, surface_coefs) -
						 r2_3_func_case_B(alpha1, 0., ty_start, uy_start, surface_coefs)) ) -

					(	(r2_3_func_case_B(alpha2, st, ty_end, uy_end, surface_coefs) -
						 r2_3_func_case_B(alpha2, 0, ty_end, uy_end, surface_coefs)) -

						(r2_3_func_case_B(alpha1, st, ty_end, uy_end, surface_coefs) -
						 r2_3_func_case_B(alpha1, 0., ty_end, uy_end, surface_coefs)) );            	
            break;
    }
    res /= fabs(Ty_points.ny.coords[ Ty_points.non_integr_coord_num ]);    
    double ret = res*function_value;    
    return ret;
}
// uy = A*ty + B*sy + C
double u_r2_3_grad_int::r2_3_func_case_A(double alpha, double s, double ty, double sy, double surface_coefs[])
{
    double res;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = B*B + 1;
    double b = -2.*s + 2.*B*C + 2.*A*B*ty;
    double c = alpha*alpha*s*s - 2.*alpha*s*ty + ty*ty + s*s + 2.*A*C*ty + A*A*ty*ty + C*C;
    double V_wave = -1/sqrt(1 + alpha*alpha);
    double L_wave = (s + s*alpha*alpha - alpha*ty) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double u_wave = sqrt(a) * sy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res = ( -1./sqrt(1 + alpha*alpha) )*( ( A*ty + C - b * B / (2*a) ) * log_doub_area_int(V, L, D, 1./sqrt(a), u_wave) +
			x_log_int(V,L,D,1.,u_wave) * B / a );
    //res = -1. * log_doub_area_int(V, L, D, 1./sqrt(a), u_wave) * 1. / sqrt(1 + alpha*alpha);
    return res / (4*PI);
}
// sy = A*ty +B*uy + C
double u_r2_3_grad_int::r2_3_func_case_B(double alpha, double s, double ty, double uy, double surface_coefs[])
{
    double res;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = (1 + B*B);
    double b = -2.*s*B + 2.*B*C + 2.*A*ty*B;
    double c = alpha*alpha*s*s - 2.*alpha*s*ty + ty*ty + s*s - 2.*s*A*ty + 2.*A*ty*C - 2.*s*C + A*A*ty*ty + C*C;
    double V_wave = -B / sqrt(1 + alpha*alpha);
    double L_wave = (s + s*alpha*alpha - alpha*ty - A*ty - C) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res = ( -1./sqrt(1 + alpha*alpha) )*( x_log_int(V,L,D,1.,u_wave)/a
			- b*log_doub_area_int(V, L, D, 1./sqrt(a), u_wave)/(2.*a) );
    //res = -1. * log_doub_area_int(V, L, D, 1./sqrt(a), u_wave) * 1. / sqrt(1 + alpha*alpha);
    return res / (4*PI);
}
// ty = A*sy + B*uy + C
double u_r2_3_grad_int::r2_3_func_case_C(double alpha, double s, double sy, double uy, double surface_coefs[])
{
    double res;    
    double A = surface_coefs[0], B = surface_coefs[1], C = surface_coefs[2];
    double a = (1 + B*B);
    double b = 2.*A*sy*B - 2*alpha*s*B + 2.*B*C;
    double c = alpha*alpha*s*s - 2.*alpha*s*A*sy + 2.*A*sy*C - 2.*alpha*s*C + A*A*sy*sy + C*C + s*s - 2.*s*sy + sy*sy;
    double V_wave = -alpha * B / sqrt(1. + alpha*alpha);
    double L_wave = (s + s*alpha*alpha - alpha*A*sy - alpha*C - sy) / sqrt(1 + alpha*alpha);
    // последние параметры подставляются в сложный интеграл от логарифмической функции (верхний предел по t)
    double u_wave = sqrt(a) * uy + b / (2.*sqrt(a));
    double V = V_wave / sqrt(a);
    double L = L_wave - V_wave * b / (2. * a);
    double D = c - b*b / (4. * a);
    res = ( -1./sqrt(1 + alpha*alpha) )*( x_log_int(V,L,D,1.,u_wave)/a
                    - b*log_doub_area_int(V, L, D, 1./sqrt(a), u_wave)/(2.*a) );
    //res = -1. * log_doub_area_int(V, L, D, 1./sqrt(a), u_wave) * 1. / sqrt(1 + alpha*alpha);
    return res / (4*PI);
}

// функции вычисления численной добавки к аналитической части
double u_r2_3_grad_int::pure_numeric_integral(GaussRule &gr, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty)
{
    TrigGaussRuleInfo tgri(gr);
    VectorXD<double,2> gauss_points;
    double acc, ret(0.);
    double area = fy->current_parameter.s_t * fy->current_parameter.t_k * 0.5;
    mdlp.set_trg_params(fx->current_parameter);
    while( tgri.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgri.e.e1;
        gauss_points.coords[ 1 ] = tgri.e.e2;
        // в этих точках вычисляем интегрируемую функцию
        acc = numeric_integrand(gauss_points, mdlp, Ty) * area;
        // в отличие от area, параметр весов w может менятся на итерациях
        acc *= tgri.w;
        // добавляем новое слагаемое к результату
        ret += acc;
    }
    return ret;
}
double u_r2_3_grad_int::numeric_integrand(VectorXD<double,2> &Gauss_point, Modyfied_Doub_Layer_Potential &mdlp, Ty_params &Ty_points)
{
    triangle_parameters &Tx = fx->current_parameter;    
    triangle_parameters &Ty = fy->current_parameter;
    VectorXD<double,3> glob_arg, x_arg, x_arg_special;
    // вычисляем аргумент в локальных координатах треугольника Tx
    glob_arg = Ty.get_Global_point(Gauss_point); // интегрирование ведётся по треугольнику Ty, точки выбираются на нём!
    x_arg = Tx.get_local_coordinate_point(glob_arg); // затем точка переводится в координаты треугольника Tx
    x_arg_special = x_arg; // x_arg_special - дублирует функционал basis_function_combination с той разницей, что здесь надо определить расстояние до точки, в которой вычисляется аргумент
    double surface_coefs[3];
    get_surface_coefs(Tx, Ty_points, surface_coefs);
    double sy, ty, uy, A, B, C;
    A = surface_coefs[0];
    B = surface_coefs[1];
    C = surface_coefs[2];
    switch(Ty_points.non_integr_coord_num)
    {
        case 0:
            x_arg_special.coords[2] = 0.; // интегрирование по ty, uy, uy = 0.            
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[0] = A * ty + B * uy + C;
            break;
        case 1:
            x_arg_special.coords[2] = 0.; // интегрирование по uy, sy, sy = st    
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[1] = A * sy + B * uy + C;
            break;
        case 2:
            x_arg_special.coords[0] = Tx.s_t; // интегрирование по ty, sy, sy = st
            sy = x_arg_special.coords[0];
            ty = x_arg_special.coords[1];
            uy = x_arg_special.coords[2];
            x_arg_special.coords[2] = A * ty + B * sy + C;
            break;
        default:
            break;
    }
    // вычисляем необходимую нам комбинацию
    double a_value = basis_function_combination(x_arg,Ty_points.non_integr_coord_num, surface_coefs,false);
    double b_value = basis_function_combination(x_arg,Ty_points.non_integr_coord_num, surface_coefs,true); // выделяем особенность
    double delta = a_value - b_value, distance;
    distance = (x_arg - x_arg_special).norma();
    if(abs(distance) < zero)
        return 0.;
    double u_r2_3_grad_int_mdlp = mdlp.r2_3_component(x_arg);
    return delta*u_r2_3_grad_int_mdlp*x_arg.coords[2];
}
// функция, используемая в методе вычитания особенности
double u_r2_3_grad_int::basis_function_combination(VectorXD<double,3> loc_x_arg, uint non_integr_num, double surface_coefs[], bool singularity_point)
{
    triangle_parameters &Tx = fx->current_parameter;
    VectorXD<double,3> arg;
    VectorXD<double,3> ret;
    arg = loc_x_arg;   
    // arg - критическая точка с особенностью, в нашем случае это множество с особенностью при sy = st и uy = 0
    // поэтому внешний интеграл оставляем по произвольной переменной (ty или uy), а особенность определяем для координаты st = sy
    // можно было определить особенность для координаты uy.
    double A, B, C;
    double sy, ty, uy;
    A = surface_coefs[0];
    B = surface_coefs[1];
    C = surface_coefs[2];
    if(singularity_point) // в этом случае интегрирование проводится с одной степенью свободы и один из параметров связан значением в точке сингулярности
    switch(non_integr_num)
    {
        case 0:
            arg.coords[2] = 0.; // интегрирование по ty, uy, uy = 0.            
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[0] = A * ty + B * uy + C;
            break;
        case 1:
            arg.coords[2] = 0.; // интегрирование по uy, sy, sy = st    
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[1] = A * sy + B * uy + C;
            break;
        case 2:
            arg.coords[0] = Tx.s_t; // интегрирование по ty, sy, sy = st
            sy = arg.coords[0];
            ty = arg.coords[1];
            uy = arg.coords[2];
            arg.coords[2] = A * ty + B * sy + C;
            break;
        default:
            break;
    }
    Block<double,2> grad;
    VectorXD<double,3> local_lambda_val, global_lambda_val, loc_y_arg, glob_y_arg;
    // градиент вычисляется в локальных координатах треугольника Tx.
    grad = fx->gradient_u_tang(arg); // градиент возвращается в локальных координатах треугольника, на котором определена функция
    ret.coords[0] = grad.block[0][1];
    ret.coords[1] = grad.block[1][1];   
    ret.coords[2] = 0.;
    glob_y_arg = fx->current_parameter.get_global_coordinate_point(arg);
    loc_y_arg = fy->current_parameter.get_local_coordinate_point(glob_y_arg);
    local_lambda_val = fy->lambda(loc_y_arg); // fy->lambda( gauss_points ); // в локальных координатах треугольника Ty
    global_lambda_val = fy->current_parameter.S * local_lambda_val; // в глобальных координатах (в которых задавались точки сетки)
    local_lambda_val = fx->current_parameter.get_local_coordinate_vector(global_lambda_val); // в локальных координатах теперь уже треугольника Tx
    // теперь, когда и градиент и lambda переведены в одни координаты вычислим скалярное произведение
    double dot_product = local_lambda_val.dotprod(ret);
    return dot_product;
}