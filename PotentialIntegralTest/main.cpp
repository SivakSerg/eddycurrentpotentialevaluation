/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: sergey
 *
 * Created on March 7, 2020, 4:20 PM
 */
#include <iostream>
#include <fstream>
#include <UnitTest++/UnitTest++.h>
#include "UVdecomposer.h"
#include "VectorXD.hpp"

#include <cstdlib>

using namespace std;

/*
 * 
 */

int main(void)
{
    VectorXD<double,2>::set_scalar_zero(0.0);
    VectorXD<double,3>::set_scalar_zero(0.0);
    VectorXD<complex<double>,3>::set_scalar_zero(0.0);

    Block<double,2>::set_one(1.0);
    Block<double,2>::set_zero(0.0);    
    Block<double,3>::set_one(1.0);
    Block<double,3>::set_zero(0.0);
    Block<complex<double>,3>::set_one(1.0);
    Block<complex<double>,3>::set_zero(0.0);

    return UnitTest::RunAllTests();
}

