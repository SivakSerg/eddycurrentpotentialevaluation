/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <UnitTest++/UnitTest++.h>
#include <iostream>
#include <math.h>

#include "LinearBasisFunctions.hpp"
#include "OperatorA.h"  // new operator
#include "OperatorV.h"

#include "Operator_A.h" // old operator

namespace
{
    TEST(operatorARoofTest)
    {
        VectorXD<double,3> p[3];
        // first point
        p[0].coords[0] = 0.0;
        p[0].coords[1] = 0.;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.0;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = 0.0;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;

        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;

        std::complex<double> waveNumber = 1.0;
        GaussRule grRuff(12), grFine(21);
        for (int xFunctionInd = 0; xFunctionInd < 3; ++xFunctionInd)
        {
            LinearVectorBasisFunctions xBasis;
            xBasis.set_current_trig_parameter(xFunctionInd, &trig, p, 0);
            
            auto xGlobBasisFunction = [&](const VectorXD<double,3> & arg)->VectorXD<double,3>
            {
                VectorXD<double,3> loc_arg;
                loc_arg = xBasis.current_parameter.get_local_coordinate_point(arg);
                auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&xBasis);
                auto lambdaVal = pBasisFunction->lambda(loc_arg);
                return xBasis.current_parameter.get_global_coordinate_vector(lambdaVal);
            };
            
            auto xDivGlobBasisFunciton = [&](const VectorXD<double,3> & arg)->double
            {
                VectorXD<double,3> loc_arg;
                loc_arg = xBasis.current_parameter.get_local_coordinate_point(arg);
                auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&xBasis);
                return pBasisFunction->div_F_lam(loc_arg);
            };
            
            for (int yFunctionInd = 0; yFunctionInd < 3; ++yFunctionInd)
            {
                LinearVectorBasisFunctions yBasis;
                yBasis.set_current_trig_parameter(yFunctionInd, &trig, p, 0);
                
                auto yGlobBasisFunction = [&](const VectorXD<double,3> & arg)->VectorXD<double,3>
                {
                    VectorXD<double,3> loc_arg;
                    loc_arg = yBasis.current_parameter.get_local_coordinate_point(arg);
                    auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&yBasis);
                    auto lambdaVal = pBasisFunction->lambda(loc_arg);
                    return yBasis.current_parameter.get_global_coordinate_vector(lambdaVal);
                };                 
                
                auto yDivGlobBasisFunciton = [&](const VectorXD<double,3> & arg)->double
                {
                    VectorXD<double,3> loc_arg;
                    loc_arg = yBasis.current_parameter.get_local_coordinate_point(arg);
                    auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&yBasis);
                    return pBasisFunction->div_F_lam(loc_arg);
                };                 
                
                Operator_A oldOperator;
                oldOperator.operator_init(&xBasis, &yBasis);
                complex<double> oldRes = oldOperator.evalf_operator_roof_addapted(true, waveNumber, 4, 1e-6, grRuff, grFine);
                
                // first we check the calculations based on the recursive method of integral computation.
                OperatorA newOperator;
                OperatorV scalarOperator;
                DinamicBlock<complex<double>> aLocalMatrix(1);
                std::vector<BasisFunction> axBasisFunctions = {xGlobBasisFunction};
                std::vector<std::pair<BasisFunction, BasisOrder>> ayBasisFunctionsWithOrder = 
                    {make_pair(yGlobBasisFunction, BasisOrder(1))};
                complex<double> newRes = 0.0;
                newOperator.buildOperatorLocalMatrix(axBasisFunctions, xBasis.current_parameter,
                    ayBasisFunctionsWithOrder, yBasis.current_parameter, waveNumber, 1e-6, 5, grRuff, grFine, grFine, aLocalMatrix);
                newRes += aLocalMatrix.block(0, 0);
                
                if (abs(waveNumber) > 1e-6)
                {
                    std::vector<ScalarBasisFunction> axDivFunction = {xDivGlobBasisFunciton};
                    std::vector<std::pair<ScalarBasisFunction, BasisOrder>> ayDivFunctionsWithOrder = 
                        {std::make_pair(yDivGlobBasisFunciton, BasisOrder(0))};
                    scalarOperator.buildOperatorLocalMatrix(axDivFunction, xBasis.current_parameter,
                        ayDivFunctionsWithOrder, yBasis.current_parameter, waveNumber, 1e-6, 5, grRuff, grFine, grFine, aLocalMatrix);
                    newRes += aLocalMatrix.block(0, 0) / (waveNumber * waveNumber);
                }
             
                double discrepancy = abs(newRes - oldRes) / abs(oldRes);
                bool checkIf = (discrepancy < 3e-5);
                if (!checkIf)
                {
                    std::cout << discrepancy << "\n";
                }
                
                // now we check the second method of refinement which is based on the subdivisions of a triangular support
                newOperator.setCalculationType(OperatorA::eCalculationType::WITHSUBDIVISIONS);
                
                std::complex<double> resWithSubdivs = 0.0;
                newOperator.buildOperatorLocalMatrix(axBasisFunctions, xBasis.current_parameter,
                    ayBasisFunctionsWithOrder, yBasis.current_parameter, waveNumber, 1e-6, 5, grRuff, grFine, grFine, aLocalMatrix);
                resWithSubdivs += aLocalMatrix.block(0, 0);
                
                if (abs(waveNumber) > 1e-6)
                {
                    std::vector<ScalarBasisFunction> axDivFunction = {xDivGlobBasisFunciton};
                    std::vector<std::pair<ScalarBasisFunction, BasisOrder>> ayDivFunctionsWithOrder = 
                        {std::make_pair(yDivGlobBasisFunciton, BasisOrder(0))};
                    scalarOperator.buildOperatorLocalMatrix(axDivFunction, xBasis.current_parameter,
                        ayDivFunctionsWithOrder, yBasis.current_parameter, waveNumber, 1e-6, 5, grRuff, grFine, grFine, aLocalMatrix);
                    resWithSubdivs += aLocalMatrix.block(0, 0) / (waveNumber * waveNumber);
                }
             
                discrepancy = abs(resWithSubdivs - oldRes) / abs(oldRes);
                checkIf = (discrepancy < 3e-5);
                if (!checkIf)
                {
                    std::cout << discrepancy << "\n";
                }                

                CHECK( checkIf );
                
                discrepancy = abs(newRes - resWithSubdivs) / abs(newRes);
                checkIf = (discrepancy < 1e-6);
                if (!checkIf)
                {
                    std::cout << discrepancy << "\n";
                }
                
                CHECK( checkIf );
                
                auto globBasisYGradient = [&](const VectorXD<double,3> & arg, int basisIndex)->Block<double,3>
                {
                    VectorXD<double,3> loc_arg;
                    loc_arg = yBasis.current_parameter.get_local_coordinate_point(arg);
                    auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&yBasis);
                    auto lambdaGrad = pBasisFunction->gradient_lambda(loc_arg);
                    Block<double,3> expandedGrad;
                    expandedGrad.Zerofied();
                    for (int i=0; i<2; ++i)
                        for (int j=0; j<2; ++j)
                            expandedGrad.block[i][j] = lambdaGrad.block[i][j];

                    auto St = transpose(yBasis.current_parameter.S);
                    auto & S = yBasis.current_parameter.S;
                    return (S * expandedGrad) * St;
                };
                
                // now for the method of subdivisions the integral is taken not for the gradient value gathered approximately,
                // but rather it's taken precise.
                newOperator.setGradientYFunction(globBasisYGradient);
                resWithSubdivs = 0.0;
                newOperator.buildOperatorLocalMatrix(axBasisFunctions, xBasis.current_parameter,
                    ayBasisFunctionsWithOrder, yBasis.current_parameter, waveNumber, 1e-6, 5, grRuff, grFine, grFine, aLocalMatrix);
                resWithSubdivs += aLocalMatrix.block(0, 0);
                
                if (abs(waveNumber) > 1e-6)
                {
                    std::vector<ScalarBasisFunction> axDivFunction = {xDivGlobBasisFunciton};
                    std::vector<std::pair<ScalarBasisFunction, BasisOrder>> ayDivFunctionsWithOrder = 
                        {std::make_pair(yDivGlobBasisFunciton, BasisOrder(0))};
                    scalarOperator.buildOperatorLocalMatrix(axDivFunction, xBasis.current_parameter,
                        ayDivFunctionsWithOrder, yBasis.current_parameter, waveNumber, 1e-6, 5, grRuff, grFine, grFine, aLocalMatrix);
                    resWithSubdivs += aLocalMatrix.block(0, 0) / (waveNumber * waveNumber);
                }
                
                discrepancy = abs(newRes - resWithSubdivs) / abs(newRes);
                checkIf = (discrepancy < 1e-6);
                if (!checkIf)
                {
                    std::cout << discrepancy << "\n";
                }
                
                CHECK( checkIf );                
            }
        }
    }
}