#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/OperatorATest.o \
	${OBJECTDIR}/Operator_A.o \
	${OBJECTDIR}/Operator_B.o \
	${OBJECTDIR}/Operator_B_num.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/r1_3_integral.o \
	${OBJECTDIR}/r2_3_integral.o \
	${OBJECTDIR}/u_r1_3_grad_int.o \
	${OBJECTDIR}/u_r2_3_grad_int.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Wl,-rpath,'../HelmholtzPotentialIntegrals/dist/Release/GNU-Linux' -L../HelmholtzPotentialIntegrals/dist/Release/GNU-Linux -lHelmholtzPotentialIntegrals -Wl,-rpath,'../PotentialEvaluation/dist/Release/GNU-Linux' -L../PotentialEvaluation/dist/Release/GNU-Linux -lPotentialEvaluation -lUnitTest++

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialintegraltest
	${CP} ../HelmholtzPotentialIntegrals/dist/Release/GNU-Linux/libHelmholtzPotentialIntegrals.so ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${CP} ../PotentialEvaluation/dist/Release/GNU-Linux/libPotentialEvaluation.so ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialintegraltest: ../HelmholtzPotentialIntegrals/dist/Release/GNU-Linux/libHelmholtzPotentialIntegrals.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialintegraltest: ../PotentialEvaluation/dist/Release/GNU-Linux/libPotentialEvaluation.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialintegraltest: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialintegraltest ${OBJECTFILES} ${LDLIBSOPTIONS} -L/usr/local/ -llapack -L/usr/local/ -lblas -lrefblas -lgfortran -lquadmath

${OBJECTDIR}/OperatorATest.o: OperatorATest.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OperatorATest.o OperatorATest.cpp

${OBJECTDIR}/Operator_A.o: Operator_A.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Operator_A.o Operator_A.cpp

${OBJECTDIR}/Operator_B.o: Operator_B.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Operator_B.o Operator_B.cpp

${OBJECTDIR}/Operator_B_num.o: Operator_B_num.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Operator_B_num.o Operator_B_num.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/r1_3_integral.o: r1_3_integral.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/r1_3_integral.o r1_3_integral.cpp

${OBJECTDIR}/r2_3_integral.o: r2_3_integral.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/r2_3_integral.o r2_3_integral.cpp

${OBJECTDIR}/u_r1_3_grad_int.o: u_r1_3_grad_int.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/u_r1_3_grad_int.o u_r1_3_grad_int.cpp

${OBJECTDIR}/u_r2_3_grad_int.o: u_r2_3_grad_int.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -I../PotentialEvaluation/include -I../PotentialEvaluation -I../HelmholtzPotentialIntegrals -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/u_r2_3_grad_int.o u_r2_3_grad_int.cpp

# Subprojects
.build-subprojects:
	cd ../HelmholtzPotentialIntegrals && ${MAKE}  -f Makefile CONF=Release
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Release

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} -r ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libHelmholtzPotentialIntegrals.so ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libPotentialEvaluation.so
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialintegraltest

# Subprojects
.clean-subprojects:
	cd ../HelmholtzPotentialIntegrals && ${MAKE}  -f Makefile CONF=Release clean
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Release clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
