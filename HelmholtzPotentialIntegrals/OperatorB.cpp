/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "OperatorB.h"

#include "DoubleLayerPotential.h"
#include "FastVectorMult.hpp"
#include "Mod_Doub_Pot_by_parts.hpp"
#include "Scalar_Doub_Layer_Potential_With_Subdivisions.hpp"
#include "UVdecomposer.h"

std::complex<double> OperatorB::calcPotentialCached(const triangle_parameters & Tx, 
            BasisFunction basisX, GaussRule grX,
            const triangle_parameters & Ty, BasisFunction basisY, BasisOrder orderY, int indexOfCachedYFunction, 
            GaussRule grY, std::complex<double> waveNumber, double epsilon) const
{
    // scalar components of basis functions
    auto yBasisYCoordX = [&](const VectorXD<double,3> & arg)
    {
        return basisY(arg).coords[0];
    };

    auto yBasisYCoordY = [&](const VectorXD<double,3> & arg)
    {
        return basisY(arg).coords[1];
    };        

    auto yBasisYCoordZ = [&](const VectorXD<double,3> & arg)
    {
        return basisY(arg).coords[2];
    };
    
    // gradients taken for the scalar components numerically
    auto yGradBasisYCoordX = [&](const VectorXD<double,3> & arg)
    {
      return basisGradient(yBasisYCoordX, arg, epsilon);
    };
    
    auto yGradBasisYCoordY = [&](const VectorXD<double,3> & arg)
    {
      return basisGradient(yBasisYCoordY, arg, epsilon);
    };
    
    auto yGradBasisYCoordZ = [&](const VectorXD<double,3> & arg)
    {
      return basisGradient(yBasisYCoordZ, arg, epsilon);
    };

    // gradients passed to the operator in order to deal with the analytic gradient values
    auto hardSetGrad = [&](const VectorXD<double,3> & arg)
    {
      return m_hardSetGradientY(arg, indexOfCachedYFunction);  
    };    
    
    auto yGradAnalyticCoordX = [&](const VectorXD<double,3> & arg)
    {
      Block<double,3> gradMtr = hardSetGrad(arg);
      VectorXD<double,3> retValue;
      for (int coordIndex = 0; coordIndex < 3; ++coordIndex)
      {
          retValue.coords[coordIndex] = gradMtr.block[0][coordIndex];
      }
      return retValue;
    };
    
    auto yGradAnalyticCoordY = [&](const VectorXD<double,3> & arg)
    {
      Block<double,3> gradMtr = hardSetGrad(arg);
      VectorXD<double,3> retValue;
      for (int coordIndex = 0; coordIndex < 3; ++coordIndex)
      {
          retValue.coords[coordIndex] = gradMtr.block[1][coordIndex];
      }
      return retValue;
    };
    
    auto yGradAnalyticCoordZ = [&](const VectorXD<double,3> & arg)
    {
      Block<double,3> gradMtr = hardSetGrad(arg);
      VectorXD<double,3> retValue;
      for (int coordIndex = 0; coordIndex < 3; ++coordIndex)
      {
          retValue.coords[coordIndex] = gradMtr.block[2][coordIndex];
      }
      return retValue;
    };
    
    TrigGaussRuleInfo tgrX(grX);
    DoubleLayerPotential dlsp;
    Scalar_Doub_Layer_Potential_With_Subdivisions sdlp(grY);
    double area = Tx.s_t * Tx.t_k * 0.5;
    VectorXD<double,3> pY[3];
    pY[0] = Ty.x0; pY[1] = Ty.x1; pY[2] = Ty.x2;
    complex<double> res = 0.0;
    while(tgrX.next())
    {
        VectorXD<double,2> gaussPoints;
        
        // точки Гаусса (L - координаты)
        gaussPoints.coords[ 0 ] = tgrX.e.e1;
        gaussPoints.coords[ 1 ] = tgrX.e.e2;
        
        // в этих точках вычисляем функцию lambda
        auto r = Tx.get_Global_point(gaussPoints);
        auto xBasisValue = basisX(r) * area * tgrX.w;

        VectorXD<complex<double>,3> result;
        result.zerofied();
        
        if (eCalculationType::RECURSIVE == m_calcType)
        {
            if (!m_useCache)
            {
                result.coords[0] = dlsp.getScalarDoubleLayerHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordX);
                result.coords[1] = dlsp.getScalarDoubleLayerHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordY);
                result.coords[2] = dlsp.getScalarDoubleLayerHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordZ);
            }
            else
            {
                result.coords[0] = dlsp.getScalarDoubleLayerHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordX, m_aYTrigDecVectorCoeffs[indexOfCachedYFunction][0]);
                result.coords[1] = dlsp.getScalarDoubleLayerHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordY, m_aYTrigDecVectorCoeffs[indexOfCachedYFunction][1]);
                result.coords[2] = dlsp.getScalarDoubleLayerHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordZ, m_aYTrigDecVectorCoeffs[indexOfCachedYFunction][2]);
            }
        }
        else if (eCalculationType::WITHSUBDIVISIONS == m_calcType)
        {
            if (m_useNumericGradY)
            {
                sdlp.set_vave_num(waveNumber);
                result.coords[0] = sdlp.EvalfScalarPotWithSubdivisions(epsilon, 
                    yBasisYCoordX, yGradBasisYCoordX, Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);
                
                result.coords[1] = sdlp.EvalfScalarPotWithSubdivisions(epsilon, 
                    yBasisYCoordY, yGradBasisYCoordY, Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);
                
                result.coords[2] = sdlp.EvalfScalarPotWithSubdivisions(epsilon, 
                    yBasisYCoordZ, yGradBasisYCoordZ, Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);                
            }
            else
            {
                sdlp.set_vave_num(waveNumber);
                result.coords[0] = sdlp.EvalfScalarPotWithSubdivisions(epsilon, 
                    yBasisYCoordX, yGradAnalyticCoordX, Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);
                
                result.coords[1] = sdlp.EvalfScalarPotWithSubdivisions(epsilon, 
                    yBasisYCoordY, yGradAnalyticCoordY, Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);
                
                result.coords[2] = sdlp.EvalfScalarPotWithSubdivisions(epsilon, 
                    yBasisYCoordZ, yGradAnalyticCoordZ, Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);                 
            }
        }
        
        res += fast_dot_prod(result, xBasisValue);          
    }
    
    return 0.0;
}

bool OperatorB::buildOperatorLocalMatrix(const std::vector<BasisFunction> & aBasisFunctionsX, 
    const triangle_parameters & Tx, 
    const std::vector<std::pair<BasisFunction,BasisOrder>> & aBasisFunctionY, 
    const triangle_parameters & Ty, 
    std::complex<double> waveNumber, double eps, int maxDepth,
    GaussRule xRuff, GaussRule xFine, GaussRule yGauss,
    DinamicBlock<complex<double>> & aLocalMatrix) const
{
    bool doubleLayerRes = OperatorA::buildOperatorLocalMatrix(aBasisFunctionsX, Tx, aBasisFunctionY, Ty, waveNumber, eps, 
            maxDepth, xRuff, xFine, yGauss, aLocalMatrix);
    if (!doubleLayerRes)
        return false;
    
    if (m_onlyScalarDoubleLayer)
        return true;
    
    Mod_Doub_Pot_by_parts mdpp;
    for (std::size_t rowIndex = 0; rowIndex < aBasisFunctionsX.size(); ++rowIndex)
    {
        for (std::size_t colIndex = 0; colIndex < aBasisFunctionY.size(); ++colIndex)
        {
            auto hardSetGrad = [&](const VectorXD<double,3> & arg)
            {
              return m_hardSetGradientY(arg, colIndex);  
            };
            
            auto & basisY = aBasisFunctionY[colIndex].first;
            // scalar components of basis functions
            auto yBasisYCoordX = [&](const VectorXD<double,3> & arg)
            {
                return basisY(arg).coords[0];
            };

            auto yBasisYCoordY = [&](const VectorXD<double,3> & arg)
            {
                return basisY(arg).coords[1];
            };        

            auto yBasisYCoordZ = [&](const VectorXD<double,3> & arg)
            {
                return basisY(arg).coords[2];
            };

            // gradients taken for the scalar components numerically
            auto yGradBasisYCoordX = [&](const VectorXD<double,3> & arg)
            {
              return basisGradient(yBasisYCoordX, arg, eps);
            };

            auto yGradBasisYCoordY = [&](const VectorXD<double,3> & arg)
            {
              return basisGradient(yBasisYCoordY, arg, eps);
            };

            auto yGradBasisYCoordZ = [&](const VectorXD<double,3> & arg)
            {
              return basisGradient(yBasisYCoordZ, arg, eps);
            };            
            
            auto globFirstBasisDiv = [&](const VectorXD<double,3> & arg)
            {
                if (!m_useNumericGradY)
                {            
                    Block<double,3> gradBlock = hardSetGrad(arg);
                    double div = 0.0;
                    for (int coordIndex = 0; coordIndex < 3; ++coordIndex)
                    {
                        div += gradBlock.block[coordIndex][coordIndex];
                    }
                    return div;
                }

                VectorXD<double,3> xGrad = yGradBasisYCoordX(arg);
                VectorXD<double,3> yGrad = yGradBasisYCoordY(arg);
                VectorXD<double,3> zGrad = yGradBasisYCoordZ(arg);

                double div = xGrad.coords[0] + yGrad.coords[1] + zGrad.coords[2];

                return div;
            };
            
            aLocalMatrix.block(rowIndex, colIndex) -= mdpp.EvalfIntegral(aBasisFunctionsX[rowIndex],
                aBasisFunctionY[colIndex].first, globFirstBasisDiv,
                XBASIS_MAX_ORDER, aBasisFunctionY[colIndex].second, Tx, Ty, waveNumber,
                true, m_numberOfSubdivision, eps,
                xRuff, xFine, m_zeroDivergence);            
        }
    }
    return true;
}