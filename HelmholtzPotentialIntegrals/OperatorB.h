/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OperatorB.h
 * Author: sergey
 *
 * Created on March 12, 2020, 10:20 PM
 */

#ifndef OPERATORB_H
#define OPERATORB_H

#include "OperatorA.h"

class OperatorB : public OperatorA
{
    protected:
        const int XBASIS_MAX_ORDER = 1; // the order of the first argument-basis function,
        // normally, it should not have any influence what so ever.
        bool m_zeroDivergence = false, m_onlyScalarDoubleLayer = false;
        
        std::complex<double> calcPotentialCached(const triangle_parameters & Tx, 
            BasisFunction basisX, GaussRule grX,
            const triangle_parameters & Ty, BasisFunction basisY, BasisOrder orderY, int indexOfCachedYFunction, 
            GaussRule grY, std::complex<double> waveNumber, double epsilon) const override;
    public:
        // for this function we presuppose that the divergence of the basis function is zero - this makes the computational
        // costs lower
        void setZeroDivergenceFlag(bool zeroDivergence) {m_zeroDivergence = zeroDivergence;}
        
        // this function sets the flag that signalizes, if true, that only the double layer potential integrals should be
        // calculated. Also, this may mitigate the computational costs if the cached function values are to be used.
        void setOnlyScalarDoubleLayer(bool flagOnlyScalarIntegral) {m_onlyScalarDoubleLayer = flagOnlyScalarIntegral;}
        
        // here we suppose that the first function is the u-basis function and the second one associated with the Ty
        // zone is the lambda-basis function
        bool buildOperatorLocalMatrix(const std::vector<BasisFunction> & aBasisFunctionsX, 
            const triangle_parameters & Tx, 
            const std::vector<std::pair<BasisFunction,BasisOrder>> & aBasisFunctionY, 
            const triangle_parameters & Ty, 
            std::complex<double> waveNumber, double eps, int maxDepth,
            GaussRule xRuff, GaussRule xFine, GaussRule yGauss,
            DinamicBlock<complex<double>> & aLocalMatrix) const override;
};

#endif /* OPERATORB_H */

