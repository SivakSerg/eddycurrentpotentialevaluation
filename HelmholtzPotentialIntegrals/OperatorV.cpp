/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "OperatorV.h"

#include "Single_Layer_Potential.hpp"
#include "SingleLayerSurfacePotential.h"
#include "FastVectorMult.hpp"
#include "UVdecomposer.h"

#include <complex>
#include <functional>
#include <vector>

void OperatorV::setGradientYFunction(OperatorScalarBasisGradient gradY)
{
    m_hardSetGradientY = gradY;
    m_useNumericGradY = false;
}

void OperatorV::switchToNumericGradient()
{
    m_useNumericGradY = true;
}

void OperatorV::setCalculationType(eCaclulationType type)
{
    m_calcType = type;
}
    
void OperatorV::setCacheUsage(bool isCacheUsed)
{
    m_useCache = isCacheUsed;
}

const std::vector<std::vector<double>> & OperatorV::getCache() const
{
    return m_aYTrigDecVectorCoeffs;
}

std::complex<double> OperatorV::calcPotentialCached(const triangle_parameters & Tx, 
    ScalarBasisFunction basisX, GaussRule grX,
    const triangle_parameters & Ty, ScalarBasisFunction basisY, BasisOrder orderY, int indexOfCachedYFunction, 
    GaussRule grY, std::complex<double> waveNumber, double epsilon) const
{
    auto gradY = [&](const VectorXD<double,3> & arg)
    {
      return basisGradient(basisY, arg, epsilon);
    };
    
    TrigGaussRuleInfo tgrX(grX);
    SingleLayerSurfacePotential slsp;
    Single_Layer_Potential slp;
    double area = Tx.s_t * Tx.t_k * 0.5;
    VectorXD<double,3> pY[3];
    pY[0] = Ty.x0; pY[1] = Ty.x1; pY[2] = Ty.x2;
    complex<double> res = 0.0;
    while(tgrX.next())
    {
        VectorXD<double,2> gaussPoints;
        
        // точки Гаусса (L - координаты)
        gaussPoints.coords[ 0 ] = tgrX.e.e1;
        gaussPoints.coords[ 1 ] = tgrX.e.e2;
        
        // в этих точках вычисляем функцию lambda
        auto r = Tx.get_Global_point(gaussPoints);
        auto xBasisValue = basisX(r) * area * tgrX.w;

        complex<double> result = 0.0;        
        if (eCaclulationType::RECURSIVE == m_calcType)
        {
            if (!m_useCache)
            {
                result = slsp.getScalarHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    basisY);
            }
            else
            {
                result = slsp.getScalarHelmholtzPotentialCached(waveNumber, epsilon, grY, pY, r, orderY,
                    basisY, m_aYTrigDecVectorCoeffs[indexOfCachedYFunction]);
            }
        }
        else if (eCaclulationType::WITHSUBDIVISIONS == m_calcType)
        {
            if (m_useNumericGradY)
            {
                slp.set_vave_num(waveNumber);
                result = slp.EvalfSingleLayerScalarPotSections(epsilon, basisY, gradY,
                    Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);
            }
            else
            {
                auto hardSetGrad = [&](const VectorXD<double,3> & arg)
                {
                  return m_hardSetGradientY(arg, indexOfCachedYFunction);
                };
                
                slp.set_vave_num(waveNumber);
                result = slp.EvalfSingleLayerScalarPotSections(epsilon, basisY, hardSetGrad,
                    Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);                
            }//*/
        }
        
        res += result * xBasisValue;        
    }
    return res;
}

bool OperatorV::buildOperatorLocalMatrix(const std::vector<ScalarBasisFunction> & aScalarBasisFunctionsX, 
    const triangle_parameters & Tx, 
    const std::vector<std::pair<ScalarBasisFunction,BasisOrder>> & aScalarBasisFunctionsY, 
    const triangle_parameters & Ty, 
    std::complex<double> waveNumber, double epsilon, int numberOfSubdivision,
    GaussRule xRuff, GaussRule xFine, GaussRule yGauss,
    DinamicBlock<complex<double>> & aLocalMatrix) const
{    
    m_numberOfSubdivision = numberOfSubdivision;
    m_grRuff = xRuff, m_grFine = xFine;
    
    VectorXD<double,3> yPoints[] = {Ty.x0, Ty.x1, Ty.x2};
    SingleLayerSurfacePotential slsp;
    slsp.create_parameters_for_trinagle_only(yPoints);
 
    if (m_useCache)
    {
        UVdecomposer decomposer;
        // first we decompose the Y-basis function in a component-wise manner.
        for (auto basisPair : aScalarBasisFunctionsY)
        {
            auto basisFunction = basisPair.first;
            BasisOrder order = basisPair.second;

            m_aYTrigDecVectorCoeffs.push_back(std::vector<double>());
            decomposer.createDecomposeList(slsp, basisFunction, yGauss, order, m_aYTrigDecVectorCoeffs.back());
        }
    }
    
    std::vector<triangle_parameters> before_sectioning, after_sectioning;
    double zero_flag = sqrt(Tx.t_k * Tx.s_t / 2.) * epsilon / numberOfSubdivision;
    double discrepancy = 1.0;
    VectorXD<complex<double>,3>::set_scalar_zero(0.0);
    
    aLocalMatrix.resize(aScalarBasisFunctionsX.size() * aScalarBasisFunctionsY.size(), false);
    
    for (std::size_t rowIndex = 0; rowIndex < aScalarBasisFunctionsX.size(); ++rowIndex)
    {
        for (std::size_t colIndex = 0; colIndex < aScalarBasisFunctionsY.size(); ++colIndex)
        {    
            std::complex<double> savedValues = 0.0, ret = 0.0, retRefined = 0.0;
            std::complex<double> fullSum = 0.0;            
            before_sectioning.push_back(Tx);
            
            int iter = 0;
            for(; iter<numberOfSubdivision && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
            {                
                ret = savedValues;
                retRefined = savedValues;
                while(!before_sectioning.empty())
                {
                    triangle_parameters current_parameter = before_sectioning.back();
                    before_sectioning.pop_back();

                    complex<double> rude_value = calcPotentialCached(current_parameter, aScalarBasisFunctionsX[rowIndex],
                            xRuff, Ty, aScalarBasisFunctionsY[colIndex].first, aScalarBasisFunctionsY[colIndex].second,
                            colIndex, yGauss, waveNumber, epsilon);
                    complex<double> refined_value = calcPotentialCached(current_parameter, aScalarBasisFunctionsX[rowIndex],
                            xFine, Ty, aScalarBasisFunctionsY[colIndex].first, aScalarBasisFunctionsY[colIndex].second,
                            colIndex, yGauss, waveNumber, epsilon);

                    complex<double> testRet = savedValues + rude_value;
                    complex<double> testRetRefined = savedValues + refined_value;
                    ret = ret + rude_value;
                    retRefined = retRefined + refined_value;
                    if (!iter)
                    {
                        fullSum = testRetRefined;
                    }
                    double cur_disc = abs(testRetRefined - testRet)/abs(fullSum);
                    if(cur_disc >= epsilon && abs(rude_value) >= zero_flag
                            && abs(refined_value) >= zero_flag)
                        current_parameter.Build_Sections(after_sectioning);
                    else
                        savedValues = savedValues + refined_value;
                }
                discrepancy = abs(ret - retRefined) / abs(retRefined);
                fullSum = retRefined;
                if(discrepancy >= epsilon)
                {
                    std::swap(before_sectioning, after_sectioning);
                }
            }
            before_sectioning.clear();
            after_sectioning.clear();
            aLocalMatrix[rowIndex][colIndex] = retRefined;
        }
    }
    return true;
}
