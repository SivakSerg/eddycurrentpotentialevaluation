/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OperatorV.h
 * Author: sergey
 *
 * Created on March 7, 2020, 10:14 PM
 */

#ifndef OPERATORV_H
#define OPERATORV_H


#include "base.hpp"
#include "DinamicXD.hpp"
#include "VectorBasisInterface.hpp"

#include <array>
#include <complex>
#include <vector>

using OperatorScalarBasisGradient = std::function<VectorXD<double,3>(const VectorXD<double,3> &arg, int basisIndex)>;

class OperatorV
{
public:
    enum class eCaclulationType
    {
        RECURSIVE = 0,
        WITHSUBDIVISIONS = 1
    };
private:
    // the decomposition coefficients are stored to optimize the calculation process
    mutable std::vector<std::vector<double>> m_aYTrigDecVectorCoeffs;     
    
    mutable int m_numberOfSubdivision = 5;
    
    mutable GaussRule m_grRuff = 6, m_grFine = 12;
    
    bool m_useCache = false;
    
    eCaclulationType m_calcType = eCaclulationType::RECURSIVE;
    
    OperatorScalarBasisGradient m_hardSetGradientY = [&](const VectorXD<double,3> &arg, int basisIndex)
    {
        VectorXD<double,3> block;
        block.zerofied();
        return block;
    };
    
    bool m_useNumericGradY = true;
    
    std::complex<double> calcPotentialCached(const triangle_parameters & Tx, 
        ScalarBasisFunction basisX, GaussRule grX,
        const triangle_parameters & Ty, ScalarBasisFunction basisY, BasisOrder orderY, int indexOfCachedYFunction, 
        GaussRule grY, std::complex<double> waveNumber, double epsilon) const;
public:
    void setCalculationType(eCaclulationType type);
    
    void setCacheUsage(bool isCacheUsed);
    
    void setGradientYFunction(OperatorScalarBasisGradient gradY);
    
    void switchToNumericGradient();
    
    const std::vector<std::vector<double>> & getCache() const;
    /** this function constructs the local matrix for the single layer potential using the recursive approach.
        each function
        the cached function uses the UVdecomposition directly to optimize the computation    
     * 
     * @param aScalarBasisFunctionsX
     *  basis function which are non-zero on the first X triangle. 
     * @param Tx
     *  the support of the first basis function array. The X triangle to be subdivided.
     * @param aScalarBasisFunctionY
     *  basis function which are non-zero on the second Y triangle. The second argument in the pair structure is the
     *  order of basis functions.
     * @param Ty
     *  the support of the second basis function array. The X triangle to be subdivided.
     * @param waveNumber
     *  The Helmholtz potential's wave number, it defines the frequency of possible oscillations.
     * @param eps
     *  The threshold defining how accurate computations are
     * @param maxDepth
     *  The maximum level of subdivisions specified for the supporting triangles, it defines how many subdivisions are allowed
     *  for the sake of computational costs.
     * @param xRuff
     *  the ruff Gauss rule specified for the integration with subdivisions for the first X triangle
     * @param xFine
     *  the fine Gauss rule specified for the integration with subdivisions for the first X triangle
     * @param yGauss
     *  the integration by the Y-triangle is not conducted via subdivisions that's why one rule is needed
     * @param aLocalMatrix
     *  The collection of pairwise integrals taken to be then stored as a local matrix which is to be added to the global one
     * @return 
     */
    bool buildOperatorLocalMatrix(const std::vector<ScalarBasisFunction> & aScalarBasisFunctionsX, 
        const triangle_parameters & Tx, 
        const std::vector<std::pair<ScalarBasisFunction,BasisOrder>> & aScalarBasisFunctionY, 
        const triangle_parameters & Ty, 
        std::complex<double> waveNumber, double eps, int maxDepth,
        GaussRule xRuff, GaussRule xFine, GaussRule yGauss,
        DinamicBlock<complex<double>> & aLocalMatrix) const;
};

#endif /* OPERATORV_H */

