#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/OperatorA.o \
	${OBJECTDIR}/OperatorB.o \
	${OBJECTDIR}/OperatorV.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Wl,-rpath,'../PotentialEvaluation/dist/Release/GNU-Linux' -L../PotentialEvaluation/dist/Release/GNU-Linux -lPotentialEvaluation -Wl,-rpath,'../PotentialEvaluation/dist/Debug/GNU-Linux' -L../PotentialEvaluation/dist/Debug/GNU-Linux -lPotentialEvaluation

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libHelmholtzPotentialIntegrals.${CND_DLIB_EXT}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libHelmholtzPotentialIntegrals.${CND_DLIB_EXT}: ../PotentialEvaluation/dist/Release/GNU-Linux/libPotentialEvaluation.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libHelmholtzPotentialIntegrals.${CND_DLIB_EXT}: ../PotentialEvaluation/dist/Debug/GNU-Linux/libPotentialEvaluation.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libHelmholtzPotentialIntegrals.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libHelmholtzPotentialIntegrals.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} -shared -fPIC

${OBJECTDIR}/OperatorA.o: OperatorA.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -I../Meshing -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OperatorA.o OperatorA.cpp

${OBJECTDIR}/OperatorB.o: OperatorB.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -I../Meshing -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OperatorB.o OperatorB.cpp

${OBJECTDIR}/OperatorV.o: OperatorV.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -I../Meshing -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OperatorV.o OperatorV.cpp

# Subprojects
.build-subprojects:
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Release
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} -r ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libPotentialEvaluation.so ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libPotentialEvaluation.so
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libHelmholtzPotentialIntegrals.${CND_DLIB_EXT}

# Subprojects
.clean-subprojects:
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Release clean
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
