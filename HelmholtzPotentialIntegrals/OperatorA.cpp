/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "OperatorA.h"

#include "Single_Layer_Potential.hpp"
#include "SingleLayerSurfacePotential.h"
#include "FastVectorMult.hpp"
#include "UVdecomposer.h"

#include <complex>
#include <functional>
#include <vector>

void OperatorA::setGradientYFunction(OperatorBasisGradient gradY)
{
    m_hardSetGradientY = gradY;
    m_useNumericGradY = false;
}

void OperatorA::switchToNumericGradient()
{
    m_useNumericGradY = true;
}

void OperatorA::setCalculationType(eCalculationType type)
{
    m_calcType = type;
}
    
void OperatorA::setCacheUsage(bool isCacheUsed)
{
    m_useCache = isCacheUsed;
}

const std::vector<std::array<std::vector<double>, 3>> & OperatorA::getCache() const
{
    return m_aYTrigDecVectorCoeffs;
}

std::complex<double> OperatorA::calcPotentialCached(const triangle_parameters & Tx, 
    BasisFunction basisX, GaussRule grX,
    const triangle_parameters & Ty, BasisFunction basisY, BasisOrder orderY, int indexOfCachedYFunction, 
    GaussRule grY, std::complex<double> waveNumber, double epsilon) const
{
    auto yBasisYCoordX = [&](const VectorXD<double,3> & arg)
    {
        return basisY(arg).coords[0];
    };

    auto yBasisYCoordY = [&](const VectorXD<double,3> & arg)
    {
        return basisY(arg).coords[1];
    };        

    auto yBasisYCoordZ = [&](const VectorXD<double,3> & arg)
    {
        return basisY(arg).coords[2];
    };
    
    auto gradY = [&](const VectorXD<double,3> & arg)
    {
      return basisGradient(basisY, arg, epsilon);
    };
    
    TrigGaussRuleInfo tgrX(grX);
    SingleLayerSurfacePotential slsp;
    Single_Layer_Potential slp;
    double area = Tx.s_t * Tx.t_k * 0.5;
    VectorXD<double,3> pY[3];
    pY[0] = Ty.x0; pY[1] = Ty.x1; pY[2] = Ty.x2;
    complex<double> res = 0.0;
    while(tgrX.next())
    {
        VectorXD<double,2> gaussPoints;
        
        // точки Гаусса (L - координаты)
        gaussPoints.coords[ 0 ] = tgrX.e.e1;
        gaussPoints.coords[ 1 ] = tgrX.e.e2;
        
        // в этих точках вычисляем функцию lambda
        auto r = Tx.get_Global_point(gaussPoints);
        auto xBasisValue = basisX(r) * area * tgrX.w;

        VectorXD<complex<double>,3> result;
        result.zerofied();
        
        if (eCalculationType::RECURSIVE == m_calcType)
        {
            if (!m_useCache)
            {
                result.coords[0] = slsp.getScalarHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordX);
                result.coords[1] = slsp.getScalarHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordY);
                result.coords[2] = slsp.getScalarHelmholtzPotential(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordZ);
            }
            else
            {
                result.coords[0] = slsp.getScalarHelmholtzPotentialCached(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordX, m_aYTrigDecVectorCoeffs[indexOfCachedYFunction][0]);
                result.coords[1] = slsp.getScalarHelmholtzPotentialCached(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordY, m_aYTrigDecVectorCoeffs[indexOfCachedYFunction][1]);
                result.coords[2] = slsp.getScalarHelmholtzPotentialCached(waveNumber, epsilon, grY, pY, r, orderY,
                    yBasisYCoordZ, m_aYTrigDecVectorCoeffs[indexOfCachedYFunction][2]);
            }
        }
        else if (eCalculationType::WITHSUBDIVISIONS == m_calcType)
        {
            if (m_useNumericGradY)
            {
                slp.set_vave_num(waveNumber);
                result = slp.EvalfSingleLayerVectorPotSections(epsilon, basisY, gradY,
                    Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);
            }
            else
            {
                auto hardSetGrad = [&](const VectorXD<double,3> & arg)
                {
                  return m_hardSetGradientY(arg, indexOfCachedYFunction);  
                };
                slp.set_vave_num(waveNumber);
                result = slp.EvalfSingleLayerVectorPotSections(epsilon, basisY, hardSetGrad,
                    Ty, r, m_numberOfSubdivision, m_grRuff, m_grFine);                
            }
        }
        
        res += fast_dot_prod(result, xBasisValue);        
    }
    return res;
}

bool OperatorA::buildOperatorLocalMatrix(const std::vector<BasisFunction> & aBasisFunctionsX, 
    const triangle_parameters & Tx, 
    const std::vector<std::pair<BasisFunction,BasisOrder>> & aBasisFunctionsY, 
    const triangle_parameters & Ty, 
    std::complex<double> waveNumber, double epsilon, int numberOfSubdivision,
    GaussRule xRuff, GaussRule xFine, GaussRule yGauss,
    DinamicBlock<complex<double>> & aLocalMatrix) const
{    
    m_numberOfSubdivision = numberOfSubdivision;
    m_grRuff = xRuff, m_grFine = xFine;
    
    VectorXD<double,3> yPoints[] = {Ty.x0, Ty.x1, Ty.x2};
    SingleLayerSurfacePotential slsp;
    slsp.create_parameters_for_trinagle_only(yPoints);
 
    if (m_useCache)
    {
        UVdecomposer decomposer;
        // first we decompose the Y-basis function in a component-wise manner.
        for (auto basisPair : aBasisFunctionsY)
        {
            auto basisFunction = basisPair.first;
            BasisOrder order = basisPair.second;
            m_aYTrigDecVectorCoeffs.push_back(std::array<std::vector<double>, 3>());
            for (int xyzIndex = 0; xyzIndex < 3; ++xyzIndex)
            {
                auto xyzBasisComponent = [&](const VectorXD<double,3> & arg)->double
                {
                  return basisFunction(arg).coords[xyzIndex];
                };

                std::vector<double> & decCoefs = m_aYTrigDecVectorCoeffs.back()[xyzIndex];

                decomposer.createDecomposeList(slsp, xyzBasisComponent, yGauss, order, decCoefs);
            }
        }
    }
    
    std::vector<triangle_parameters> before_sectioning, after_sectioning;    
    double zero_flag = sqrt(Tx.t_k * Tx.s_t / 2.) * epsilon / numberOfSubdivision;
    double discrepancy = 1.0;
    VectorXD<complex<double>,3>::set_scalar_zero(0.0);
    
    aLocalMatrix.resize(aBasisFunctionsX.size() * aBasisFunctionsY.size(), false);
    
    for (std::size_t rowIndex = 0; rowIndex < aBasisFunctionsX.size(); ++rowIndex)
    {
        for (std::size_t colIndex = 0; colIndex < aBasisFunctionsY.size(); ++colIndex)
        {    
            std::complex<double> savedValues = 0.0, ret = 0.0, retRefined = 0.0;
            std::complex<double> fullSum = 0.0;            
            
            before_sectioning.push_back(Tx);
            int iter = 0;
            for(; iter<numberOfSubdivision && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
            {
                ret = savedValues;
                retRefined = savedValues;
                while(!before_sectioning.empty())
                {
                    triangle_parameters current_parameter = before_sectioning.back();
                    before_sectioning.pop_back();

                    complex<double> rude_value = calcPotentialCached(current_parameter, aBasisFunctionsX[rowIndex],
                            xRuff, Ty, aBasisFunctionsY[colIndex].first, aBasisFunctionsY[colIndex].second,
                            colIndex, yGauss, waveNumber, epsilon);
                    complex<double> refined_value = calcPotentialCached(current_parameter, aBasisFunctionsX[rowIndex],
                            xFine, Ty, aBasisFunctionsY[colIndex].first, aBasisFunctionsY[colIndex].second,
                            colIndex, yGauss, waveNumber, epsilon);

                    complex<double> testRet = savedValues + rude_value;
                    complex<double> testRetRefined = savedValues + refined_value;
                    ret = ret + rude_value;
                    retRefined = retRefined + refined_value;
                    if (!iter)
                    {
                        fullSum = testRetRefined;
                    }
                    double cur_disc = abs(testRetRefined - testRet)/abs(fullSum);
                    if(cur_disc >= epsilon && abs(rude_value) >= zero_flag
                            && abs(refined_value) >= zero_flag)
                        current_parameter.Build_Sections(after_sectioning);
                    else
                        savedValues = savedValues + refined_value;
                }
                discrepancy = abs(ret - retRefined) / abs(retRefined);
                fullSum = retRefined;
                if(discrepancy >= epsilon)
                {
                    std::swap(before_sectioning, after_sectioning);
                }
            }
            before_sectioning.clear();
            after_sectioning.clear();
            aLocalMatrix[rowIndex][colIndex] = retRefined;
        }
    }
    return true;
}