/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   OperatorA.h
 * Author: sergey
 *
 * Created on February 29, 2020, 9:19 PM
 */

#ifndef OPERATORA_H
#define OPERATORA_H

#include "base.hpp"
#include "DinamicXD.hpp"
#include "VectorBasisInterface.hpp"

#include <array>
#include <complex>
#include <vector>

using OperatorBasisGradient = std::function<Block<double,3>(const VectorXD<double,3> &arg, int basisIndex)>;

class OperatorA
{
public:
    enum class eCalculationType
    {
        RECURSIVE = 0,
        WITHSUBDIVISIONS = 1
    };
    
protected:
    // the decomposition coefficients are stored to optimize the calculation process
    mutable std::vector<std::array<std::vector<double>, 3>> m_aYTrigDecVectorCoeffs;     
    
    mutable int m_numberOfSubdivision = 5;
    
    mutable GaussRule m_grRuff = 6, m_grFine = 12;
    
    bool m_useCache = false;
    
    eCalculationType m_calcType = eCalculationType::RECURSIVE;
    
    OperatorBasisGradient m_hardSetGradientY = [&](const VectorXD<double,3> &arg, int basisIndex)
    {
        Block<double,3> block;
        block.Zerofied();
        return block;
    };
    
    bool m_useNumericGradY = true;
    
    virtual std::complex<double> calcPotentialCached(const triangle_parameters & Tx, 
        BasisFunction basisX, GaussRule grX,
        const triangle_parameters & Ty, BasisFunction basisY, BasisOrder orderY, int indexOfCachedYFunction, 
        GaussRule grY, std::complex<double> waveNumber, double epsilon) const;
public:
    void setCalculationType(eCalculationType type);
    
    void setCacheUsage(bool isCacheUsed);
    
    void setGradientYFunction(OperatorBasisGradient gradY);
    
    void switchToNumericGradient();
    
    const std::vector<std::array<std::vector<double>, 3>> & getCache() const;
    /** this function constructs the local matrix for the single layer potential using the recursive approach.
        the cached function uses the UVdecomposition directly to optimize the computation    
     * 
     * @param aBasisFunctionsX
     *  basis function which are non-zero on the first X triangle. 
     * @param Tx
     *  the support of the first basis function array. The X triangle to be subdivided.
     * @param aBasisFunctionY
     *  basis function which are non-zero on the second Y triangle. The second argument in the pair structure is the
     *  order of basis functions.
     * @param Ty
     *  the support of the second basis function array. The X triangle to be subdivided.
     * @param waveNumber
     *  The Helmholtz potential's wave number, it defines the frequency of possible oscillations.
     * @param eps
     *  The threshold defining how accurate computations are
     * @param maxDepth
     *  The maximum level of subdivisions specified for the supporting triangles, it defines how many subdivisions are allowed
     *  for the sake of computational costs.
     * @param xRuff
     *  the ruff Gauss rule specified for the integration with subdivisions for the first X triangle
     * @param xFine
     *  the fine Gauss rule specified for the integration with subdivisions for the first X triangle
     * @param yGauss
     *  the integration by the Y-triangle is not conducted via subdivisions that's why one rule is needed
     * @param aLocalMatrix
     *  The collection of pairwise integrals taken to be then stored as a local matrix which is to be added to the global one
     * @return 
     */
    virtual bool buildOperatorLocalMatrix(const std::vector<BasisFunction> & aBasisFunctionsX, 
        const triangle_parameters & Tx, 
        const std::vector<std::pair<BasisFunction,BasisOrder>> & aBasisFunctionY, 
        const triangle_parameters & Ty, 
        std::complex<double> waveNumber, double eps, int maxDepth,
        GaussRule xRuff, GaussRule xFine, GaussRule yGauss,
        DinamicBlock<complex<double>> & aLocalMatrix) const;
};

#endif /* OPERATORA_H */

