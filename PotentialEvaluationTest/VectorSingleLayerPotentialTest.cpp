#include <UnitTest++/UnitTest++.h>
#include <iostream>
#include <math.h>

#include "LinearBasisFunctions.hpp"
#include "UVdecomposer.h"
#include "VectorXD.hpp"
#include "SingleLayerSurfacePotential.h"
#include "Single_Layer_Potential.hpp"

namespace
{
    // Тест основан на предположении, что декомпозиция работает верно.
    TEST(compareVectorSingleLayerFunctions)
    {
        const complex<double> WaveNumber(1.75, 0.0);
        VectorXD<double,3> p[3], r;
        r.common_init(0.0);
        // first point
        p[0].coords[0] = 0.1;
        p[0].coords[1] = 0.2;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.3;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = -0.2;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;
        for (int i=0; i<3; ++i)
        {
            r = r + p[i];
        }
        for (int c=0; c<3; ++c)
        {
            r.coords[c] /= 3.0;
        }
        VectorXD<double,3> v1 = p[1] - p[0];
        VectorXD<double,3> v2 = p[2] - p[0];
        VectorXD<double,3> n;
        fast_vector_mutl(v1, v2, n);
        r = r + n * (0.001);
        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;
        
        LinearVectorBasisFunctions lvbf;
        lvbf.set_current_trig_parameter(0, &trig, p, 0);

        auto pBasisFunciton = dynamic_cast<VectorBasisInterface *>(&lvbf);
        
        GaussRule grFine(66), grRude(21);
        Single_Layer_Potential slp;
        
        VectorXD<double,2> gaussPoints;
        triangle_parameters virtualTx;
        virtualTx.x0 = r;
        virtualTx.x1.common_init(0.0);
        virtualTx.x2.common_init(0.0);
        gaussPoints.common_init(0.0);
        slp.set_vave_num(WaveNumber);
        
        auto globBasisFunction = [&](const VectorXD<double,3> & arg)->VectorXD<double,3>
        {
            VectorXD<double,3> loc_arg;
            loc_arg = lvbf.current_parameter.get_local_coordinate_point(arg);
            auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&lvbf);
            auto lambdaVal = pBasisFunction->lambda(loc_arg);
            return lvbf.current_parameter.get_global_coordinate_vector(lambdaVal);
        };
        
        auto globBasisGradient = [&](const VectorXD<double,3> & arg)->Block<double,3>
        {
            VectorXD<double,3> loc_arg;
            loc_arg = lvbf.current_parameter.get_local_coordinate_point(arg);
            auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&lvbf);
            auto lambdaGrad = pBasisFunction->gradient_lambda(loc_arg);
            Block<double,3> expandedGrad;
            expandedGrad.Zerofied();
            for (int i=0; i<2; ++i)
                for (int j=0; j<2; ++j)
                    expandedGrad.block[i][j] = lambdaGrad.block[i][j];
            
            auto St = transpose(lvbf.current_parameter.S);
            auto & S = lvbf.current_parameter.S;
            return (S * expandedGrad) * St;
        };
        
        auto locBasisFunctionX = [&](const VectorXD<double,3> & arg)->double
        {
            return globBasisFunction(arg).coords[0];
        };
        
        auto locBasisFunctionY = [&](const VectorXD<double,3> & arg)->double
        {
            return globBasisFunction(arg).coords[1];
        };        
        
        auto locBasisFunctionZ = [&](const VectorXD<double,3> & arg)->double
        {
            return globBasisFunction(arg).coords[2];
        };

        GaussRule gr(66);
        // теперь получаем значение потенциала
        SingleLayerSurfacePotential slsp;
        // формируем параметры треугольника
        complex<double> surfIntegralX = slsp.getScalarHelmholtzPotential(WaveNumber, 1e-6,
                                       gr, p, r, 1, locBasisFunctionX);
        complex<double> surfIntegralY = slsp.getScalarHelmholtzPotential(WaveNumber, 1e-6,
                                       gr, p, r, 1, locBasisFunctionY);
        complex<double> surfIntegralZ = slsp.getScalarHelmholtzPotential(WaveNumber, 1e-6,
                               gr, p, r, 1, locBasisFunctionZ);
        
        VectorXD<complex<double>,3> resGlobViaScalarPotentials;
        resGlobViaScalarPotentials.coords[0] = surfIntegralX;
        resGlobViaScalarPotentials.coords[1] = surfIntegralY;
        resGlobViaScalarPotentials.coords[2] = surfIntegralZ;
        
        auto resGlobal = slp.EvalfSingleLayerVectorPotSections(1e-8, globBasisFunction,
            globBasisGradient, lvbf.current_parameter, r, 4, grRude, grFine);
        
        VectorXD<complex<double>,3> res = slp.EvalfSingleLayerVectorPotSections(1e-5, pBasisFunciton,
            gaussPoints, virtualTx, 4, grRude, grFine);
        auto globCoordResOld = lvbf.current_parameter.get_global_coordinate_vector(res);
        
        for (int cmpValueIndex = 0; cmpValueIndex < 2; ++cmpValueIndex)
        {
            auto & globCoordRes = (0 == cmpValueIndex) ? globCoordResOld : resGlobViaScalarPotentials;
            double diff = 0.0, norma = 0.0;
            for (int i = 0; i < 3; ++i)
            {
                complex<double> coordDiff = (resGlobal.coords[i] - globCoordRes.coords[i]);
                double locDiff = coordDiff.real() * coordDiff.real() + coordDiff.imag() * coordDiff.imag();
                diff += locDiff;

                complex<double> resCoord = globCoordRes.coords[i];
                double locNorma = resCoord.real() * resCoord.real() + resCoord.imag() * resCoord.imag();
                norma += locNorma;
            }
            diff = sqrt(diff);
            norma = sqrt(norma);
            CHECK(diff / norma < 1e-5);  
        }
    }
}