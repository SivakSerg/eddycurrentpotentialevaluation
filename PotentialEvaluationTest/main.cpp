#include <iostream>
#include <fstream>
#include <UnitTest++/UnitTest++.h>
#include "UVdecomposer.h"
#include "VectorXD.hpp"

int main(void)
{
    VectorXD<double,2>::set_scalar_zero(0.0);
    VectorXD<double,3>::set_scalar_zero(0.0);
    VectorXD<complex<double>,3>::set_scalar_zero(0.0);

    Block<double,2>::set_one(1.0);
    Block<double,2>::set_zero(0.0);    
    Block<double,3>::set_one(1.0);
    Block<double,3>::set_zero(0.0);
    Block<complex<double>,3>::set_one(1.0);
    Block<complex<double>,3>::set_zero(0.0);

    return UnitTest::RunAllTests();
}
