#include "UVdecomposer.h"
#include "VectorXD.hpp"
#include <UnitTest++/UnitTest++.h>
#include "SingleLayerSurfacePotential.h"
#include "LinearScalarFunctions.hpp"
#include "ScalarConstBasisFunction.h"
#include "Single_Layer_Potential.hpp"
#include "Scalar_Doub_Layer_Potential_With_Subdivisions.hpp"
#include "DoubleLayerHierarchicalPotential.h"
#include "DoubleLayerPotential.h"
#include <iostream>
#include <math.h>

namespace
{
TEST(decomposeLinearFunction)
{
    VectorXD<double,3> p[3], r;
    r.common_init(0.0);
    // first point
    p[0].coords[0] = 0.0;
    p[0].coords[1] = 1.0;
    p[0].coords[2] =-1.0;
    // second point
    p[1].coords[0] = 2.0;
    p[1].coords[1] = 1.0;
    p[1].coords[2] =-1.0;
    // third point
    p[2].coords[0] = 0.0;
    p[2].coords[1] = 1.0;
    p[2].coords[2] = 1.0;

    triangle trig;
    trig.p[0] = 0;
    trig.p[1] = 1;
    trig.p[2] = 2;
    // тестовая функция для разложения является базисной функцией
    LinearScalarBasis basis;
    // инициализировали базисную функцию
    basis.set_current_trig_parameter(2,&trig,p,0);
    auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
    {
        VectorXD<double,3> loc_arg;
        loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
        ScalarBasisInterface *interface = &basis;
        return interface->f_value(loc_arg); // X coordinate to depend on
    };
    UVdecomposer decomposer;
    std::vector<double> decCoefs;
    GaussRule gr(6);
    TrigGaussRuleInfo tgr(gr);
    while (tgr.next())
    {
        r = p[0] * tgr.e.e1 + p[1] * tgr.e.e2 + p[2] * tgr.e.e3;
        SingleLayerSurfacePotential slsp;
        slsp.create_parameters(r, p);
        decomposer.createDecomposeList(slsp, functionToDecompose, gr, 1, decCoefs);
        double fValue = functionToDecompose(r);
        int err;
        double decValue = decomposer.getDecomposedValue(1, decCoefs, slsp, r, err);
        CHECK( fabs(decValue - fValue) < 1e-6);
        CHECK( !err );
    }
}

// Тест основан на предположении, что декомпозиция работает верно.
TEST(compareSingleLayers)
{
    const complex<double> WaveNumber(0.5, 0.02);
    VectorXD<double,3> p[3], r;
    r.common_init(0.0);
    // first point
    p[0].coords[0] = 0.2;
    p[0].coords[1] = 0.0;
    p[0].coords[2] = 0.0;
    // second point
    p[1].coords[0] = 1.0;
    p[1].coords[1] = 0.4;
    p[1].coords[2] = 0.0;
    // third point
    p[2].coords[0] = 0.0;
    p[2].coords[1] = 1.0;
    p[2].coords[2] = 0.5;
    for (int i=0; i<3; ++i)
    {
        r = r + p[i];
    }
    for (int c=0; c<3; ++c)
    {
        r.coords[c] /= 3.0;
    }
    triangle trig;
    trig.p[0] = 0;
    trig.p[1] = 1;
    trig.p[2] = 2;
    // тестовая функция для разложения является базисной функцией
    LinearScalarBasis basis;
    // инициализировали базисную функцию
    basis.set_current_trig_parameter(0,&trig,p,0);
    auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
    {
        VectorXD<double,3> loc_arg;
        loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
        ScalarBasisInterface *interface = &basis;
        return interface->f_value(loc_arg); // X coordinate to depend on
    };
    GaussRule gr(66);
    // теперь получаем значение потенциала
    SingleLayerSurfacePotential slsp;
    // формируем параметры треугольника
    complex<double> surfIntegral = slsp.getScalarHelmholtzPotential(WaveNumber, 1e-6,
                                   gr, p, r, 1, functionToDecompose);

    // старым способом вычислим значение потенциала на множестве.
    Single_Layer_Potential slp(gr);
    VectorXD<double,2> gaussPoints;
    triangle_parameters virtualTx;
    virtualTx.x0 = r;
    virtualTx.x1.common_init(0.0);
    virtualTx.x2.common_init(0.0);
    gaussPoints.common_init(0.0);
    slp.set_vave_num(WaveNumber);
    basis.Cache_values.set_size(gr);
    complex<double> singlePotential = slp.EvalfScalarPotVsubdivisions(1e-7, &basis, gaussPoints, virtualTx, 5, 21, gr);
    std::cout << "\npotentail value to compare\t" << singlePotential << "\n";
    std::cout << "\nvalue to check\t" << surfIntegral << "\n";
    CHECK( abs(singlePotential - surfIntegral) / abs(singlePotential) < 1e-6 );

    r = r + basis.current_parameter.n * (0.01);
    DoubleLayerPotential dlsp;
    surfIntegral = dlsp.getScalarDoubleLayerHelmholtzPotential(WaveNumber, 1e-8,
        gr, p, r, 1, functionToDecompose);

    virtualTx.x0 = r;
    virtualTx.x1.common_init(0.0);
    virtualTx.x2.common_init(0.0);
    Scalar_Doub_Layer_Potential_With_Subdivisions sdlp(gr);
    sdlp.set_vave_num(WaveNumber);
    complex<double> doublePotentialWithSubdivisions = sdlp.EvalfScalarPotWithSubdivisions
        (1e-7, &basis, gaussPoints, virtualTx, 5, 21, gr);
    CHECK( abs(doublePotentialWithSubdivisions - surfIntegral) /
        abs(doublePotentialWithSubdivisions) < 1e-5 );
    
    ScalarBasisInterface *interface = &basis;
    auto gradFunction = [&](const VectorXD<double,3> &arg)->VectorXD<double,3>
    {
        VectorXD<double,3> loc_arg;
        loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
        ScalarBasisInterface *interface = &basis;
        VectorXD<double,2> gradF = interface->f_gradient(loc_arg); // X coordinate to depend on
        VectorXD<double,3> locGradVector;
        locGradVector.coords[0] = gradF.coords[0];
        locGradVector.coords[1] = gradF.coords[1];
        locGradVector.coords[2] = 0.0;
        return interface->current_parameter.get_global_coordinate_vector(locGradVector);
    };
    
    complex<double> subdivDoubPotential = sdlp.EvalfScalarPotWithSubdivisions(1e-7, 
        functionToDecompose, gradFunction, interface->current_parameter, r, 5, 21, 66);
    CHECK( abs(subdivDoubPotential - surfIntegral) /
        abs(subdivDoubPotential) < 1e-5 );    
}

// Сравнение интегралов
TEST(compareSingleLayerIntegrals)
{
    const complex<double> WaveNumber(0.0, 2.0);
    VectorXD<double,3> p[3], r;
    r.common_init(0.0);
    // first point
    p[0].coords[0] = 0.0;
    p[0].coords[1] = 1.0;
    p[0].coords[2] =-1.0;
    // second point
    p[1].coords[0] = 2.0;
    p[1].coords[1] = 1.0;
    p[1].coords[2] =-1.0;
    // third point
    p[2].coords[0] = 0.0;
    p[2].coords[1] = 1.0;
    p[2].coords[2] = 1.0;

    triangle trig;
    trig.p[0] = 0;
    trig.p[1] = 1;
    trig.p[2] = 2;

    GaussRule gr(66);
    TrigGaussRuleInfo tgr(gr);
    while(tgr.next())
    {
        // тестовая функция для разложения является базисной функцией
        LinearScalarBasis basis;
        // инициализировали базисную функцию
        basis.set_current_trig_parameter(1,&trig,p,0);
        VectorXD<double,2> gauss_points;
        // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        // в этих точках вычисляем функцию lambda
        r = basis.current_parameter.get_Global_point(gauss_points);
        auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
        {
            VectorXD<double,3> loc_arg;
            loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
            ScalarBasisInterface *interface = &basis;
            return interface->f_value(loc_arg); // X coordinate to depend on
        };
        UVdecomposer decomposer;
        // теперь получаем значение потенциала
        SingleLayerSurfacePotential slsp;
        // формируем параметры треугольника
        complex<double> surfIntegral = slsp.getScalarHelmholtzPotential(WaveNumber, 1e-6,
                                       gr, p, r, 1, functionToDecompose);
        // старым способом вычислим значение потенциала на множестве.
        Single_Layer_Potential slp(gr);
        VectorXD<double,2> gaussPoints;
        triangle_parameters virtualTx;
        virtualTx.x0 = r;
        virtualTx.x1.common_init(0.0);
        virtualTx.x2.common_init(0.0);
        gaussPoints.common_init(0.0);
        slp.set_vave_num(WaveNumber);
        basis.Cache_values.set_size(gr);
        complex<double> singlePotential = slp.EvalfScalarPotVsubdivisions(1e-14, &basis, gaussPoints, virtualTx, 26, 21, 66);
        std::cout << "\npotentail value to compare\t" << singlePotential << "\n";
        std::cout << "\nvalue to check\t" << surfIntegral << "\n";
        CHECK( abs(singlePotential - surfIntegral) / abs(singlePotential) < 1e-4 );
    }
}
};
