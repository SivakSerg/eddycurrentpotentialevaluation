#include <UnitTest++/UnitTest++.h>
#include "GmshReader.hpp"
#include <iostream>

namespace
{
    TEST(readGmshTest)
    {
        GmshIO readGmshFormat;
        try
        {
            readGmshFormat.readGmshFile("./magnet.msh");
        }
        catch(const std::exception & e)
        {
            std::cout << "exception occured:" << e.what();
        }
        readGmshFormat.outMesh("trg.txt", "xyz.txt");
        CHECK(true);
    }
};
