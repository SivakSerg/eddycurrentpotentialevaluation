#include <UnitTest++/UnitTest++.h>
#include <iostream>
#include <math.h>

#include "LinearBasisFunctions.hpp"
#include "UVdecomposer.h"
#include "VectorXD.hpp"
#include "Mod_Doub_Pot_by_parts.hpp"

namespace
{
    // Тест основан на предположении, что декомпозиция работает верно.
    TEST(compareVectorSingleLayerFunctions)
    {
        const complex<double> WaveNumber(1.75, 0.0);
        VectorXD<double,3> p1[3], p2[3], r;
        r.common_init(0.0);
        // first point
        p1[0].coords[0] = 0.0;
        p1[0].coords[1] = 0.0;
        p1[0].coords[2] = 0.0;
        // second point
        p1[1].coords[0] = 2.0;
        p1[1].coords[1] = 0.3;
        p1[1].coords[2] =-0.1;
        // third point
        p1[2].coords[0] = 0.0;
        p1[2].coords[1] = 1.0;
        p1[2].coords[2] = 0.8;
        
        // first point
        p2[0].coords[0] = 0.0;
        p2[0].coords[1] = 0.0;
        p2[0].coords[2] = 0.0;
        // second point
        p2[1].coords[0] = 2.0;
        p2[1].coords[1] = 0.3;
        p2[1].coords[2] =-0.1;
        // third point
        p2[2].coords[0] = 0.3;
        p2[2].coords[1] = -0.1;
        p2[2].coords[2] = 1.0;        

        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;
        
        for (int firstBasisIndex = 0; firstBasisIndex < 3; ++firstBasisIndex)
        {
            LinearVectorBasisFunctions lvbfFirst;
            lvbfFirst.set_current_trig_parameter(firstBasisIndex, &trig, p1, 0);

            auto pFirstBasisFunciton = dynamic_cast<VectorBasisInterface *>(&lvbfFirst);

            auto globFirstBasisFunction = [&](const VectorXD<double,3> & arg)->VectorXD<double,3>
            {
                VectorXD<double,3> loc_arg;
                loc_arg = lvbfFirst.current_parameter.get_local_coordinate_point(arg);
                auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&lvbfFirst);
                auto uVal = pBasisFunction->u_tangen(loc_arg);
                return lvbfFirst.current_parameter.get_global_coordinate_vector(uVal);
            };

            auto globFirstBasisDiv = [&](const VectorXD<double,3> & arg)->double
            {
                VectorXD<double,3> loc_arg = lvbfFirst.current_parameter.get_local_coordinate_point(arg);
                auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&lvbfFirst);
                auto uVal = pBasisFunction->div_u_tan(loc_arg);
                return uVal;
            };

            for (int secondBasisIndex = 0; secondBasisIndex < 3; ++secondBasisIndex)
            {
                LinearVectorBasisFunctions lvbfSecond;
                lvbfSecond.set_current_trig_parameter(secondBasisIndex, &trig, p2, 0);

                auto pSecondBasisFunciton = dynamic_cast<VectorBasisInterface *>(&lvbfSecond);

                auto globSecondBasisFunction = [&](const VectorXD<double,3> & arg)->VectorXD<double,3>
                {
                    VectorXD<double,3> loc_arg = lvbfSecond.current_parameter.get_local_coordinate_point(arg);
                    auto pBasisFunction = dynamic_cast<VectorBasisInterface *>(&lvbfSecond);
                    auto lambdaVal = pBasisFunction->lambda(loc_arg);
                    return lvbfSecond.current_parameter.get_global_coordinate_vector(lambdaVal);
                };        

                GaussRule gr(21), grFine(66);
                Mod_Doub_Pot_by_parts mdpp;
                complex<double> mdpGlobValue = mdpp.EvalfIntegral(globFirstBasisFunction, globSecondBasisFunction, globFirstBasisDiv,
                    1, 1, lvbfFirst.current_parameter, lvbfSecond.current_parameter, WaveNumber, true, 5, 1e-6,
                    gr, grFine);

                auto pBasisFunctionX = dynamic_cast<VectorBasisInterface *>(&lvbfFirst);
                auto pBasisFunctionY = dynamic_cast<VectorBasisInterface *>(&lvbfSecond);
                mdpp.init_potential(pBasisFunctionX, pBasisFunctionY, grFine, WaveNumber , true);
                complex<double> mdpLocValue = mdpp.EvalfIntegral(1, 1e-6, gr, grFine);
                complex<double> diff = (mdpLocValue - mdpGlobValue);
                double diffNorm = sqrt(diff.real() * diff.real() + diff.imag() * diff.imag());
                double norma = sqrt(mdpLocValue.real() * mdpLocValue.real() + mdpLocValue.imag() * mdpLocValue.imag());
                CHECK(diffNorm / norma < 1e-3);  
            }
        }
    }
}