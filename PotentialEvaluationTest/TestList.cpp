#include "UVdecomposer.h"
#include "VectorXD.hpp"
#include <UnitTest++/UnitTest++.h>
#include <math.h>

namespace
{
    // проверяем работу разложения скалярной функции по базису
    TEST(DecomposeFunctionDependingOnX)
    {
        // тестовая функция для разложения
        auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
        {
            return arg.coords[0]; // X coordinate to depend on
        };
        VectorXD<double,3> p[3], r;
        r.common_init(0.0);
        // first point
        p[0].coords[0] = 0.0;
        p[0].coords[1] = 0.0;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.0;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = 0.0;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;
        // формируем параметры треугольника
        SingleLayerSurfacePotential slsp;
        slsp.create_parameters(r, p);
        UVdecomposer decomposer;
        std::vector<double> decCoefs;
        GaussRule gr(66);
        decomposer.createDecomposeList(slsp, functionToDecompose, gr, 1, decCoefs);// */
        double absSum = 0.0;
        for(auto & coef : decCoefs)
            absSum += fabs(coef);
        CHECK_EQUAL(true, ( fabs(absSum - 1.0) < slsp.zero) );
    }

    TEST(DecomposeFunctionDependingOnY)
    {
        // тестовая функция для разложения
        auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
        {
            return arg.coords[1]*arg.coords[1] + arg.coords[0]; // Y coordinate to depend on
        };
        VectorXD<double,3> p[3], r;
        r.common_init(0.0);
        // first point
        p[0].coords[0] = 0.0;
        p[0].coords[1] = 0.0;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.0;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = 0.0;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;
        // формируем параметры треугольника
        SingleLayerSurfacePotential slsp;
        slsp.create_parameters(r, p);
        UVdecomposer decomposer;
        std::vector<double> decCoefs;
        GaussRule gr(6);
        decomposer.createDecomposeList(slsp, functionToDecompose, gr, 2, decCoefs);// */
        double absSum = 0.0;
        for(auto & coef : decCoefs)
            absSum += fabs(coef);
        std::cout << "absSum value is\t" << absSum << "\n";
        std::cout << "\tCompare to 2.0\n";
        CHECK_EQUAL(true, ( fabs(absSum - 2.0) < slsp.zero) );
    }
};
