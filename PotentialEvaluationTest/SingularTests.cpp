#include "UVdecomposer.h"
#include "VectorXD.hpp"
#include <UnitTest++/UnitTest++.h>
#include "SingleLayerSurfacePotential.h"
#include "LinearScalarFunctions.hpp"
#include "ScalarConstBasisFunction.h"
#include "Single_Layer_Potential.hpp"
#include <iostream>
#include <math.h>

namespace
{
    // проверяем правильность разложения скалярной базисной функции на треугольнике по полиномиальным функциям
    // в локальной системе координат треугольника.
    TEST(decomposerTest)
    {
        VectorXD<double,3> p[3];
        // first point
        p[0].coords[0] = 0.3;
        p[0].coords[1] =-0.2;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.5;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = 0.5;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;
        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;
        ScalarConstBasisFunction basis;
        // инициализировали базисную функцию
        basis.set_current_trig_parameter(2,&trig,p,0);
        auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
        {
            VectorXD<double,3> loc_arg;
            loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
            ScalarBasisInterface *interface = &basis;
            return interface->f_value(loc_arg); // X coordinate to depend on
        };
        SingleLayerSurfacePotential slsp;
        UVdecomposer decomposer;
        std::vector<double> decCoefs;
        GaussRule gr(66);
        VectorXD<double,3> r;
        r = (p[0] + p[1] + p[2]) * 0.3333333;
        slsp.create_parameters(r, p);
        std::size_t N = 2;
        decomposer.createDecomposeList(slsp, functionToDecompose, gr, N, decCoefs);
        auto decomposedFunction = [&](const VectorXD<double,3> &arg)->double
        {
            auto & x0 = slsp.p[0];
            VectorXD<double,3> vectArg = arg - x0;
            double u = slsp.u.dotprod(vectArg);
            double v = slsp.v.dotprod(vectArg);
            double retValue = 0.0;
            std::size_t nextNum = 0;
            for(std::size_t currentPower = 0; currentPower <= N; ++currentPower)
            {
                for(std::size_t polynomNumber=0; polynomNumber <= currentPower; ++polynomNumber)
                {
                    std::size_t i = polynomNumber;
                    std::size_t j = currentPower  - polynomNumber;
                    retValue += pow(u, i) * pow(v, j) * decCoefs[nextNum];
                    ++nextNum;
                }
            }
            return retValue;
        };
        for(int i=0; i<3; ++i)
        {
            std::cout << "\nfunction value\t" << functionToDecompose(slsp.p[i]) << "\n";
            std::cout << "\ndecomposed value\t" << decomposedFunction(slsp.p[i]) << "\n";
            CHECK( fabs( functionToDecompose(slsp.p[i]) - decomposedFunction(slsp.p[i]) ) < 1e-6 );
        }
        std::cout << "\nfunction value\t" << functionToDecompose(r) << "\n";
        std::cout << "\ndecomposed value\t" << decomposedFunction(r) << "\n";
        CHECK(fabs( functionToDecompose(r) - decomposedFunction(r) ) < 1e-6 );
    }

    // Тест основан на предположении, что декомпозиция работает верно.
    TEST(calculateIntegralOverTriangle)
    {
        VectorXD<double,3> p[3], r;
        r.common_init(0.5); r.coords[2] = 0.0;
        // first point
        p[0].coords[0] = 0.2;
        p[0].coords[1] = 0.0;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.4;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = 0.0;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.5;
        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;
        // тестовая функция для разложения является базисной функцией
        for(int i=0; i<3; ++i)
        {
            ScalarConstBasisFunction basis;
            // инициализировали базисную функцию
            basis.set_current_trig_parameter(i,&trig,p,0);
            auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
            {
                VectorXD<double,3> loc_arg;
                loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
                ScalarBasisInterface *interface = &basis;
                return interface->f_value(loc_arg); // X coordinate to depend on
            };
            UVdecomposer decomposer;
            std::vector<double> decCoefs;
            GaussRule gr(66);
            // теперь получаем значение потенциала
            SingleLayerSurfacePotential slsp;
            // формируем параметры треугольника
            slsp.create_parameters(r, p);
            decomposer.createDecomposeList(slsp, functionToDecompose, gr, 1, decCoefs);
            double potentialValue = 0.0;
            int nextCoef = 0;
            for(int powerDeg=0; powerDeg <= 1; ++powerDeg)
            {
                for(int i=0; i<=powerDeg; ++i)
                {
                    int j = powerDeg - i;
                    double surfIntegral = slsp.get_surface_integral(-1, i, j);
                    potentialValue += surfIntegral * decCoefs[nextCoef];
                    ++nextCoef;
                }
            }
            potentialValue /= 4 * PI;
            std::cout << "\npotential value\t" << potentialValue << "\n";
            CHECK(potentialValue > 0.0);
            // старым способом вычислим значение потенциала на множестве.
            Single_Layer_Potential slp(gr);
            VectorXD<double,2> gaussPoints;
            triangle_parameters virtualTx;
            virtualTx.x0 = r;
            virtualTx.x1.common_init(0.0);
            virtualTx.x2.common_init(0.0);
            gaussPoints.common_init(0.0);
            slp.set_vave_num(0.0);
            basis.Cache_values.set_size(gr);
            double singlePotential = slp.EvalfScalarPotV(true,&basis,gaussPoints,virtualTx).real();
            std::cout << "\npotentail value to compare\t" << singlePotential << "\n";
            CHECK( fabs(singlePotential - potentialValue) / fabs(singlePotential) < 1e-6 );
        }
    }

    // фактически сравниваем
    TEST(costBasisFunctions)
    {
        VectorXD<double,3> p[3], r;
        r.coords[0] = 0.3;
        r.coords[1] = 0.5;
        r.coords[2] = 1.2;
        // first point
        p[0].coords[0] = 0.1;
        p[0].coords[1] = 0.2;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.3;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = -0.2;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;
        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;
        // тестовая функция для разложения является базисной функцией
        ScalarConstBasisFunction basis;
        // инициализировали базисную функцию
        basis.set_current_trig_parameter(0,&trig,p,0);
        auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
        {
            VectorXD<double,3> loc_arg;
            loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
            ScalarBasisInterface *interface = &basis;
            return interface->f_value(loc_arg); // X coordinate to depend on
        };
        std::vector<double> decCoefs;
        GaussRule gr(66);
        // теперь получаем значение потенциала
        SingleLayerSurfacePotential slsp;
        // формируем параметры треугольника
        slsp.create_parameters(r, p);
        double potentialValue = slsp.get_surface_integral(-1, 0, 0) / (4 * PI);
        std::cout << "\npotential value\t" << potentialValue << "\n";
        CHECK(potentialValue > 0.0);
        // старым способом вычислим значение потенциала на множестве.
        Single_Layer_Potential slp(gr);
        VectorXD<double,2> gaussPoints;
        triangle_parameters virtualTx;
        virtualTx.x0 = r;
        virtualTx.x1.common_init(0.0);
        virtualTx.x2.common_init(0.0);
        gaussPoints.common_init(0.0);
        slp.set_vave_num(0.0);
        basis.Cache_values.set_size(gr);
        double singlePotential = slp.EvalfScalarPotV(true,&basis,gaussPoints,virtualTx).real();
        std::cout << "\npotentail value to compare\t" << singlePotential << "\n";
        CHECK( fabs(singlePotential - potentialValue) / fabs(singlePotential) < 1e-6 );
    }
};
