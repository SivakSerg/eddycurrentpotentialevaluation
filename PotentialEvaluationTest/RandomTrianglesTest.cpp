#include "DoubleLayerPotential.h"
#include "LinearScalarFunctions.hpp"
#include "Scalar_Doub_Layer_Potential.hpp"
#include "Scalar_Doub_Layer_Potential_With_Subdivisions.hpp"
#include "ScalarConstBasisFunction.h"
#include "Single_Layer_Potential.hpp"
#include "SingleLayerSurfacePotential.h"
#include "Vect_Doub_Layer_Potential.hpp"
#include "VectorXD.hpp"
#include <UnitTest++/UnitTest++.h>
#include "UVdecomposer.h"

#include <functional>
#include <iostream>
#include <random>
#include <vector>

namespace
{
    const int NUMBER_OF_TESTS = 5;

    template <class randomFunctionType>
    void generateRandomTriangle(randomFunctionType & randomFunctor,
                                std::vector< VectorXD<double,3> > & trigPoints,
                                VectorXD<double,3> & outArgPoint)
    {
        // the support triangle to calculate the integral over it
        trigPoints.resize(3);
        for (int p=0; p<3; ++p)
        {
            for (int c=0; c<3; ++c)
            {
                trigPoints[p].coords[c] = randomFunctor();
            }
        }

        // the output point to calculate the resulting integral
        for (int c=0; c<3; ++c)
        {
            outArgPoint.coords[c] = randomFunctor();
        }
    }

    template <class randomFunctionType>
    void generateRandomTriangleAndArgumentInTheSamePlane(randomFunctionType & randomFunctor,
                                std::vector< VectorXD<double,3> > & trigPoints,
                                VectorXD<double,3> & argPointInTheSamePlane)
    {
        // the support triangle to calculate the integral over it
        trigPoints.resize(3);
        for (int p=0; p<3; ++p)
        {
            for (int c=0; c<3; ++c)
            {
                trigPoints[p].coords[c] = randomFunctor();
            }
        }

        // the output point to calculate the resulting integral
        VectorXD<double,2> gaussPoint;
        for (int c=0; c<2; ++c)
        {
            gaussPoint.coords[c] = randomFunctor();
        }

        VectorXD<double,3> &x0 = trigPoints[0];
        VectorXD<double,3> &x1 = trigPoints[1];
        VectorXD<double,3> &x2 = trigPoints[2];

        VectorXD<double,3> &ret = argPointInTheSamePlane;
        ret = x0 +
            (x1 - x0) * gaussPoint.coords[0] +
            (x2 - x0) * gaussPoint.coords[1];
    }


    TEST(testDoubleLayerPotentialOnRandomTriangles)
    {
        std::random_device rd;
        std::default_random_engine generator( rd() );
        std::uniform_real_distribution<double> distrib(0.0, 10.0);
        auto RandomFunctor = std::bind (distrib, generator);
        std::vector<VectorXD<double,3> > aTrianglePoints;
        VectorXD<double,3> argument;
        std::complex<double> WaveNumber(1.75, 0.0);
        std::cout << "Random test has been started 1\n";

        std::ofstream logFile("logTriangles.txt");
        for (int trigNumber=0; trigNumber < NUMBER_OF_TESTS; ++trigNumber)
        {
            generateRandomTriangle(RandomFunctor, aTrianglePoints, argument);

            logFile << trigNumber << "\n";
            for (int p=0; p<3; ++p)
            {
                logFile << aTrianglePoints[p].coords[0] << "\t"
                        << aTrianglePoints[p].coords[1] << "\t"
                        << aTrianglePoints[p].coords[2] << "\n";
            }
            logFile << "\n";
            logFile << argument.coords[0] << "\t"
                    << argument.coords[1] << "\t"
                    << argument.coords[2] << "\n";

            VectorXD<double,3> r = argument;
            triangle trig;
            trig.pnums(0) = 0;
            trig.pnums(1) = 1;
            trig.pnums(2) = 2;
            // тестовая функция для разложения является базисной функцией
            ScalarConstBasisFunction basis;
            // инициализировали базисную функцию
            basis.set_current_trig_parameter(0, &trig, aTrianglePoints.data(), 0);
            auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
            {
                VectorXD<double,3> loc_arg;
                loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
                ScalarBasisInterface *interface = &basis;
                return interface->f_value(loc_arg); // X coordinate to depend on
            };
            UVdecomposer decomposer;
            std::vector<double> decCoefs;
            GaussRule gr(66);
            // теперь получаем значение потенциала
            DoubleLayerPotential dlsp;
            // формируем параметры треугольника
            complex<double> surfIntegral = dlsp.getScalarDoubleLayerHelmholtzPotential(WaveNumber, 1e-10,
                gr, aTrianglePoints.data(), r, 1, functionToDecompose);

            // старым способом вычислим значение потенциала на множестве.
            Scalar_Doub_Layer_Potential_With_Subdivisions sdlp(gr);
            VectorXD<double,2> gaussPoints;
            triangle_parameters virtualTx;
            virtualTx.x0 = r;
            virtualTx.x1.common_init(0.0);
            virtualTx.x2.common_init(0.0);
            gaussPoints.common_init(0.0);
            sdlp.set_vave_num(WaveNumber);
            basis.Cache_values.set_size(gr);
            complex<double> doublePotential = sdlp.EvalfScalarPotWithSubdivisions(1e-7, &basis, gaussPoints, virtualTx, 12, 21, gr);
            std::cout << "\npotentail value to compare\t" << doublePotential << "\n";
            std::cout << "\nvalue to check\t" << surfIntegral << "\n";
            bool checkIf = (std::max(abs(doublePotential), abs(surfIntegral)) < 1e-5) || (abs(doublePotential - surfIntegral) / abs(doublePotential) < 1e-3);
            if (!checkIf)
            {
                logFile << "!--- Failed! ---!\n";
                logFile << "values of the potential:\n";
                logFile << "doublePotential\t" << doublePotential << "\n";
                logFile << "surfIntegral\t" << surfIntegral << "\n";
            }

            CHECK( checkIf );
        }

        std::cout << "Random test has been ended!\n";
    }

    TEST(testDoubleLayerPotentialOnRandomTriangles2)
    {
        std::random_device rd;
        std::default_random_engine generator( rd() );
        std::uniform_real_distribution<double> distrib(0.0, 10.0);
        auto RandomFunctor = std::bind (distrib, generator);
        std::vector<VectorXD<double,3> > aTrianglePoints;
        VectorXD<double,3> argument;
        std::complex<double> WaveNumber(1.75, 0.0);
        std::cout << "Random test has been started 2\n";

        std::ofstream logFile("logTrianglesPlane.txt");
        for (int trigNumber=0; trigNumber < NUMBER_OF_TESTS; ++trigNumber)
        {
            generateRandomTriangleAndArgumentInTheSamePlane(RandomFunctor, aTrianglePoints, argument);

            logFile << trigNumber << "\n";
            for (int p=0; p<3; ++p)
            {
                logFile << aTrianglePoints[p].coords[0] << "\t"
                        << aTrianglePoints[p].coords[1] << "\t"
                        << aTrianglePoints[p].coords[2] << "\n";
            }
            logFile << "\n";
            logFile << argument.coords[0] << "\t"
                    << argument.coords[1] << "\t"
                    << argument.coords[2] << "\n";

            VectorXD<double,3> r = argument;
            triangle trig;
            trig.pnums(0) = 0;
            trig.pnums(1) = 1;
            trig.pnums(2) = 2;
            // тестовая функция для разложения является базисной функцией
            ScalarConstBasisFunction basis;
            // инициализировали базисную функцию
            basis.set_current_trig_parameter(0, &trig, aTrianglePoints.data(), 0);
            auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
            {
                VectorXD<double,3> loc_arg;
                loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
                ScalarBasisInterface *interface = &basis;
                return interface->f_value(loc_arg); // X coordinate to depend on
            };
            UVdecomposer decomposer;
            std::vector<double> decCoefs;
            GaussRule gr(66);
            // теперь получаем значение потенциала
            DoubleLayerPotential dlsp;
            // формируем параметры треугольника
            complex<double> surfIntegral = dlsp.getScalarDoubleLayerHelmholtzPotential(WaveNumber, 1e-10,
                gr, aTrianglePoints.data(), r, 1, functionToDecompose);

            // старым способом вычислим значение потенциала на множестве.
            Scalar_Doub_Layer_Potential_With_Subdivisions sdlp(gr);
            VectorXD<double,2> gaussPoints;
            triangle_parameters virtualTx;
            virtualTx.x0 = r;
            virtualTx.x1.common_init(0.0);
            virtualTx.x2.common_init(0.0);
            gaussPoints.common_init(0.0);
            sdlp.set_vave_num(WaveNumber);
            basis.Cache_values.set_size(gr);
            complex<double> doublePotential = sdlp.EvalfScalarPotWithSubdivisions(1e-7, &basis, gaussPoints, virtualTx, 12, 21, gr);
            std::cout << "\npotentail value to compare\t" << doublePotential << "\n";
            std::cout << "\nvalue to check\t" << surfIntegral << "\n";
            bool checkIf = (std::max(abs(doublePotential), abs(surfIntegral)) < 1e-7) || (abs(doublePotential - surfIntegral) / abs(doublePotential) < 1e-3);
            if (!checkIf)
            {
                logFile << "!--- Failed! ---!\n";
                logFile << "values of the potential:\n";
                logFile << "doublePotential\t" << doublePotential << "\n";
                logFile << "surfIntegral\t" << surfIntegral << "\n";
            }

            CHECK( checkIf );
        }
        std::cout << "Random test has been ended!\n";
    }

        // Тест основан на предположении, что декомпозиция работает верно.
    TEST(testSingleLayerPotentialOnRandomTriangles)
    {
        const complex<double> WaveNumber(1.75, 0.0);
        VectorXD<double,3> r;
        std::vector<VectorXD<double,3> > aTrianglePoints;
        r.common_init(0.0);
        std::random_device rd;
        std::default_random_engine generator( rd() );
        std::uniform_real_distribution<double> distrib(0.0, 10.0);
        auto RandomFunctor = std::bind (distrib, generator);
        // first point
        std::ofstream logFile("logTriangles.txt");
        std::cout << "Single layer random test has been started!\n";
        for (int trigNumber=0; trigNumber < NUMBER_OF_TESTS; ++trigNumber)
        {
            generateRandomTriangle(RandomFunctor, aTrianglePoints, r);

            logFile << trigNumber << "\n";
            for (int p=0; p<3; ++p)
            {
                logFile << aTrianglePoints[p].coords[0] << "\t"
                        << aTrianglePoints[p].coords[1] << "\t"
                        << aTrianglePoints[p].coords[2] << "\n";
            }
            logFile << "\n";
            logFile << r.coords[0] << "\t"
                    << r.coords[1] << "\t"
                    << r.coords[2] << "\n";

            triangle trig;
            trig.pnums(0) = 0;
            trig.pnums(1) = 1;
            trig.pnums(2) = 2;

            // тестовая функция для разложения является базисной функцией
            ScalarConstBasisFunction basis;
            // инициализировали базисную функцию
            basis.set_current_trig_parameter(0,&trig,aTrianglePoints.data(),0);
            auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
            {
                VectorXD<double,3> loc_arg;
                loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
                ScalarBasisInterface *interface = &basis;
                return interface->f_value(loc_arg); // X coordinate to depend on
            };
            UVdecomposer decomposer;
            std::vector<double> decCoefs;
            GaussRule gr(66);
            // теперь получаем значение потенциала
            SingleLayerSurfacePotential slsp;
            // формируем параметры треугольника
            complex<double> surfIntegral = slsp.getScalarHelmholtzPotential(WaveNumber, 1e-6,
                gr, aTrianglePoints.data(), r, 1, functionToDecompose);

            // старым способом вычислим значение потенциала на множестве.
            Single_Layer_Potential slp(gr);
            VectorXD<double,2> gaussPoints;
            triangle_parameters virtualTx;
            virtualTx.x0 = r;
            virtualTx.x1.common_init(0.0);
            virtualTx.x2.common_init(0.0);
            gaussPoints.common_init(0.0);
            slp.set_vave_num(WaveNumber);
            basis.Cache_values.set_size(gr);
            complex<double> singlePotential = slp.EvalfScalarPotVsubdivisions(1e-7, &basis, gaussPoints, virtualTx, 5, 21, gr);
            std::cout << "\npotentail value to compare\t" << singlePotential << "\n";
            std::cout << "\nvalue to check\t" << surfIntegral << "\n";

            bool checkIf = (std::max(abs(singlePotential), abs(surfIntegral)) < 1e-5) || (abs(singlePotential - surfIntegral) / abs(singlePotential) < 1e-5);
            if (!checkIf)
            {
                logFile << "!--- Failed! ---!\n";
                logFile << "values of the potential:\n";
                logFile << "singlePotential\t" << singlePotential << "\n";
                logFile << "surfIntegral\t" << surfIntegral << "\n";
            }
            CHECK( checkIf );
        }
        std::cout << "Single layer random test has been ended!\n";
    }
};
