#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/DoubleLayerPotentialTest.o \
	${OBJECTDIR}/ModDoubPotByParts.o \
	${OBJECTDIR}/RandomTrianglesTest.o \
	${OBJECTDIR}/ReadGmsh.o \
	${OBJECTDIR}/SingleLayerPotentialTest.o \
	${OBJECTDIR}/SingularTests.o \
	${OBJECTDIR}/TestList.o \
	${OBJECTDIR}/VectorSingleLayerPotentialTest.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lUnitTest++ -Wl,-rpath,'../PotentialEvaluation/dist/Debug/GNU-Linux' -L../PotentialEvaluation/dist/Debug/GNU-Linux -lPotentialEvaluation

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialevaluationtest

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialevaluationtest: ../PotentialEvaluation/dist/Debug/GNU-Linux/libPotentialEvaluation.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialevaluationtest: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialevaluationtest ${OBJECTFILES} ${LDLIBSOPTIONS} -L/usr/local/ -llapack -L/usr/local/ -lblas -lrefblas -lgfortran -lquadmath

${OBJECTDIR}/DoubleLayerPotentialTest.o: DoubleLayerPotentialTest.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DoubleLayerPotentialTest.o DoubleLayerPotentialTest.cpp

${OBJECTDIR}/ModDoubPotByParts.o: ModDoubPotByParts.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModDoubPotByParts.o ModDoubPotByParts.cpp

${OBJECTDIR}/RandomTrianglesTest.o: RandomTrianglesTest.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RandomTrianglesTest.o RandomTrianglesTest.cpp

${OBJECTDIR}/ReadGmsh.o: ReadGmsh.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ReadGmsh.o ReadGmsh.cpp

${OBJECTDIR}/SingleLayerPotentialTest.o: SingleLayerPotentialTest.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SingleLayerPotentialTest.o SingleLayerPotentialTest.cpp

${OBJECTDIR}/SingularTests.o: SingularTests.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SingularTests.o SingularTests.cpp

${OBJECTDIR}/TestList.o: TestList.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TestList.o TestList.cpp

${OBJECTDIR}/VectorSingleLayerPotentialTest.o: VectorSingleLayerPotentialTest.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/VectorSingleLayerPotentialTest.o VectorSingleLayerPotentialTest.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PotentialEvaluation/include -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} -r ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libPotentialEvaluation.so
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/potentialevaluationtest

# Subprojects
.clean-subprojects:
	cd ../PotentialEvaluation && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
