#include "UVdecomposer.h"
#include "VectorXD.hpp"
#include <UnitTest++/UnitTest++.h>
#include "SingleLayerSurfacePotential.h"
#include "LinearScalarFunctions.hpp"
#include "ScalarConstBasisFunction.h"
#include "Single_Layer_Potential.hpp"
#include "Scalar_Doub_Layer_Potential.hpp"
#include "Scalar_Doub_Layer_Potential_With_Subdivisions.hpp"
#include "Vect_Doub_Layer_Potential.hpp"
#include "DoubleLayerPotential.h"
#include <iostream>
#include <math.h>

namespace
{
    // Тест основан на предположении, что декомпозиция работает верно.
    TEST(compareDoubleLayers)
    {
        const complex<double> WaveNumber(1.0, 0.0);
        VectorXD<double,3> p[3], r;
        r.common_init(0.0);
        // first point
        p[0].coords[0] = 1.0;
        p[0].coords[1] = 0.0;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = -1.0;
        p[1].coords[1] = 0.0;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = 0.0;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;

        r.coords[0] = 0.0;
        r.coords[1] = 0.3333;
        r.coords[2] = 0.001;

        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;
        // тестовая функция для разложения является базисной функцией
        /*ScalarConstBasisFunction*/LinearScalarBasis basis;
        // инициализировали базисную функцию
        basis.set_current_trig_parameter(2,&trig,p,0);
        auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
        {
            VectorXD<double,3> loc_arg;
            loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
            ScalarBasisInterface *interface = &basis;
            return interface->f_value(loc_arg); // X coordinate to depend on
        };
        UVdecomposer decomposer;
        std::vector<double> decCoefs;
        GaussRule gr(66);
        // теперь получаем значение потенциала
        DoubleLayerPotential dlsp;
        // формируем параметры треугольника
        complex<double> surfIntegral = dlsp.getScalarDoubleLayerHelmholtzPotential(WaveNumber, 1e-8,
            gr, p, r, 1, functionToDecompose);

        // старым способом вычислим значение потенциала на множестве.
        Scalar_Doub_Layer_Potential_With_Subdivisions sdlp(gr);
        VectorXD<double,2> gaussPoints;
        triangle_parameters virtualTx;
        virtualTx.x0 = r;
        virtualTx.x1.common_init(0.0);
        virtualTx.x2.common_init(0.0);
        gaussPoints.common_init(0.0);
        sdlp.set_vave_num(WaveNumber);
        basis.Cache_values.set_size(gr);

        complex<double> doublePotentialWithSubdivisions = sdlp.EvalfScalarPotWithSubdivisions(1e-7, &basis, gaussPoints, virtualTx, 5, 21, gr);
        CHECK( abs(doublePotentialWithSubdivisions - surfIntegral) / abs(doublePotentialWithSubdivisions) < 1e-5 );
    }

    // Тест основан на предположении, что декомпозиция работает верно.
    TEST(compareDoubleLayersWhereOneIsWithSubdivisions)
    {
        const complex<double> WaveNumber(1.75, 0.0);
        VectorXD<double,3> p[3], r;
        r.common_init(0.0);
        // first point
        p[0].coords[0] = 0.1;
        p[0].coords[1] = 0.2;
        p[0].coords[2] = 0.0;
        // second point
        p[1].coords[0] = 1.0;
        p[1].coords[1] = 0.3;
        p[1].coords[2] = 0.0;
        // third point
        p[2].coords[0] = -0.2;
        p[2].coords[1] = 1.0;
        p[2].coords[2] = 0.0;
        for (int i=0; i<3; ++i)
        {
            r = r + p[i];
        }
        for (int c=0; c<3; ++c)
        {
            r.coords[c] /= 3.0;
        }
        VectorXD<double,3> v1 = p[1] - p[0];
        VectorXD<double,3> v2 = p[2] - p[0];
        VectorXD<double,3> n;
        fast_vector_mutl(v1, v2, n);
        r = r + n * (0.3);
        triangle trig;
        trig.p[0] = 0;
        trig.p[1] = 1;
        trig.p[2] = 2;
        // тестовая функция для разложения является базисной функцией
        ScalarConstBasisFunction basis;
        // инициализировали базисную функцию
        basis.set_current_trig_parameter(0,&trig,p,0);
        auto functionToDecompose = [&](const VectorXD<double,3> &arg)->double
        {
            VectorXD<double,3> loc_arg;
            loc_arg = basis.current_parameter.get_local_coordinate_point(arg);
            ScalarBasisInterface *interface = &basis;
            return interface->f_value(loc_arg); // X coordinate to depend on
        };
        UVdecomposer decomposer;
        std::vector<double> decCoefs;
        GaussRule gr(66);
        // теперь получаем значение потенциала
        DoubleLayerPotential dlsp;
        // формируем параметры треугольника
        complex<double> surfIntegral = dlsp.getScalarDoubleLayerHelmholtzPotential(WaveNumber, 1e-8,
            gr, p, r, 1, functionToDecompose);

        // старым способом вычислим значение потенциала на множестве.
        Scalar_Doub_Layer_Potential_With_Subdivisions sdlp(gr);
        VectorXD<double,2> gaussPoints;
        triangle_parameters virtualTx;
        virtualTx.x0 = r;
        virtualTx.x1.common_init(0.0);
        virtualTx.x2.common_init(0.0);
        gaussPoints.common_init(0.0);
        sdlp.set_vave_num(WaveNumber);
        basis.Cache_values.set_size(gr);
        complex<double> doublePotential = sdlp.EvalfScalarPotWithSubdivisions(1e-7, &basis, gaussPoints, virtualTx, 10, 21, gr);
        CHECK( abs(doublePotential - surfIntegral) / abs(doublePotential) < 1e-5 );
    }
};

