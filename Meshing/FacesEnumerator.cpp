#include "FacesEnumerator.h"

common_elem_struct &FacesEnumerator::get_elem(common_elem_struct *elar, uint elnum)
{
	bool *ptr;
	ptr = (bool*)elar;
	return *((common_elem_struct*)(ptr + elnum * sizeof_el / sizeof(bool)));
}
double FacesEnumerator::Det(double M[][2], uint size)
{
	return M[0][0]*M[1][1] - M[0][1]*M[1][0];
}
std::pair<uint,uint> FacesEnumerator::findFaceElems(const std::vector<uint> &pnums) const
{
     std::pair<uint,uint> ret;
     common_face_struct face;
     face.p = pnums;
     face.el_gr_num = face.el_les_num = face.less_local_num = 0;
     auto foundFace = setFaces.find(face);
     if(foundFace == setFaces.end())
         ret.first = ret.second = elcount;
     else
     {
         ret.first = (*foundFace).el_les_num;
         ret.second = (*foundFace).el_gr_num;
     }
     return ret;
}
bool FacesEnumerator::Enumerate(ulint face_size, bool build_search_structs)
{
	common_face_struct cur_face;
	face_on_border cur_face_on_border;
	ulint startj, endj, alpha_el, *face, face_count, cur_point;
	long long alpha_index;
	bool is_finded;
	// построение массивов для нумерации граней
	if( cur_begins != 0 || ig != 0 || jg != 0 ) return false;
	ig = new ulint[psize+1];
	face = new ulint[face_size]; // здесь face_size - это максимальный размер грани
	FillIG(ig);				   // вначале хранятся размеры подмассивов,
	FromCountToBegins(ig);	   // а затем здесь хранятся индексы начал подмассивов.
	jg = new ulint[ig[psize]]; // нумерация элементов с нуля
	cur_begins = new ulint[psize];
	FillJG(jg);				   // заполнили подмассивы (и отсортировали их)
	// собственно, строим список граней:
	for(ulint el_number = 0; el_number<elcount; el_number++)		// проходимся по всем элементам
	{
		face_count = get_elem(elems,el_number).get_face_count(); // получили число граней
		for(ulint cur_face_num = 0; cur_face_num<face_count; cur_face_num++) // для каждого элемента перебираем его грани
		{
			face_size = get_elem(elems,el_number).build_face(face, cur_face_num);
                        cur_face.p.resize(face_size);
			cur_point = face[0];	// выбираем первую вершину граней (могла быть любой)
                        cur_face.p[0] = face[0];
			startj = ig[cur_point];
			endj = ig[cur_point+1];
			is_finded = false;		// надеемся, что смежный элемент будет найден
			for(ulint elind = startj; elind<endj && !is_finded; elind++) // находим элемент, с которым смежны остальные вершины
			{
				alpha_el = jg[elind];
				if(alpha_el != el_number)
				{
					is_finded = true;
					for(ulint p = 1; p<face_size && is_finded; p++)
					{
						alpha_index = Dich(alpha_el, ig[face[p]], ig[face[p]+1]-1);
						if(alpha_index < 0)is_finded = false;
                                                cur_face.p[p] = face[p];
					}
					if(is_finded && (alpha_el > el_number) ) // значит это смежные по границе подобласти
					{
						cur_face.el_gr_num = alpha_el;
						cur_face.el_les_num = el_number;
						cur_face.less_local_num = cur_face_num;
						this->faces.push_back(cur_face);
                                                setFaces.insert(cur_face);
					}
				}
			}
			if(!is_finded)
			{
			 // не нашли элемента, смежного с данной гранью, значит это элемент границы
			 cur_face_on_border.el_num = el_number;  // индентифицируется совпадением
			 cur_face_on_border.local_face_num = cur_face_num; // номеров смежных подобластей
			 faces_on_board.push_back(cur_face_on_border);
                         for(uint p=0; p<face_size; ++p)
                             cur_face.p[p] = face[p];
                         cur_face.el_gr_num = el_number;
                         cur_face.el_les_num = el_number;
                         cur_face.less_local_num = cur_face_num;
                         setFaces.insert(cur_face);
			}
		}
	}
	delete[] ig; ig = 0;
	delete[] jg; jg = 0;
	delete[] cur_begins; cur_begins = 0;
	delete[] face; face=0;
	if(build_search_structs)
	{
		if(!GeneratePortrait(this->elcount, this->faces.size()))return false;
		Reorder();
	}
	return true;
}
void FacesEnumerator::Reorder()
{
	long index;
	reordered = new common_face_struct[faces.size()];
	for(ulint k=0; k<faces.size(); k++)
	{
		index = Dich(faces[k].el_les_num, ig[faces[k].el_gr_num], ig[faces[k].el_gr_num+1]-1);
		reordered[index] = faces[k];
	}
	reordered_size = faces.size();
}
bool FacesEnumerator::GeneratePortrait(uint size, uint kel)
{
	/* N - число вершин(функций б. с.) Kel - число конечных элементов */
	vector<long> list1, list2;
	uint N, Kel;
	long *globals, listsize, *listbeg, k,  ind1, ind2, iaddr;
	const long localnum = 2;

	globals=new long[localnum];
	N = size; Kel = kel;

	listbeg = new long int[N];
	ig = new uint[N + 1];
	listsize = -1;

	try
	{
	/* Построение списка list(1, 2) признак конца списка: значение -1 в list2 */
	for(uint i=0; i<N; i++)
		listbeg[i] = -1;
	for(uint ielem=0; ielem<Kel; ielem++)
	{
		globals[0] = faces[ielem].el_les_num;
		globals[1] = faces[ielem].el_gr_num;

		for(uint i=0; i<localnum; i++)
		{
			k = globals[i];
			for(uint j=i+1; j<localnum; j++)
			{
				ind1 = k;
				ind2 = globals[j];
				if(ind2<ind1)
				{
					ind1 = ind2;
					ind2 = k;
				}
				iaddr = listbeg[ind2];
				/*списка для ind2 не было, создаём его*/
				if(iaddr==-1)
				{
					listsize++;
					listbeg[ind2] = listsize;
					list1.push_back(ind1);
					list2.push_back(-1);
				}
				else
				{
					while(list1[iaddr]<ind1 && list2[iaddr]> -1)
						iaddr=list2[iaddr];
					/*не нашли и встретили элемент с большим номером, добавляем перед ним*/
					if(list1[iaddr]>ind1)
					{
						listsize++;
						list1.push_back(list1[iaddr]);
						list2.push_back(list2[iaddr]);
						list1[iaddr]=ind1;
						list2[iaddr]=listsize;
					}
					else
					/* не нашли, а список кончился, добавляем в конец списка */
						if(list1[iaddr]<ind1)
						{
							listsize++;
							list2[iaddr]=listsize;
							list1.push_back(ind1);
							list2.push_back(-1);
						}
				}
			}
		}
	}

	/* Создание портрета по списку */
	jg = new uint[list1.size()];
	ig[0] = 0;
	for(uint i = 0; i<N; i++)
	{
		ig[i+1]=ig[i];
		iaddr=listbeg[i];
		while(iaddr!=-1)
		{
			jg[ig[i+1]]=list1[iaddr];
			ig[i+1]=ig[i+1]+1;
			iaddr=list2[iaddr];
		}
	}
	delete[] globals;
	delete[] listbeg;
	return true;
	}
	catch(...)
	{
		return false;
	}
}
int FacesEnumerator::Dich(uint i,uint jstart,uint jend)
{
	int middle, res;
	try
	{
	if(jg[jend]<jg[jstart])return -1;
	res = jend - jstart;
	while(abs(res)>1)
	{
		if(jg[jstart]>i || jg[jend]<i)return -1;
		middle = (jstart + jend)/2;
		if((jstart + jend)%2)
		{
			if(jg[middle+1]>i)jend = middle+1;
			else
				if(jg[middle]<i)jstart = middle;
				else
				{
					jend = middle + 1; jstart = middle;
				}
		}
		else
		{
			if(jg[middle]>i)jend = middle;
			else
				if(jg[middle]<i)jstart = middle;
				else
					return middle;
		}
		res = jstart - jend;
	}
	if(jg[jstart]==i)return jstart;
	if(jg[jend]==i)return jend;
	return -1;
	}
	catch(...)
	{
		return -2;
	}
}
void FacesEnumerator::FillIG(ulint *IG, bool init_ig)
{
	uint points_count;
	if(init_ig)
	for(ulint p = 0; p<psize+1; p++)
		IG[p] = 0;
	for(ulint el_number=0; el_number<elcount; el_number++)
	{
		points_count = get_elem(elems,el_number).pcount();
		for(ulint p=0; p<points_count; p++)
			IG[get_elem(elems,el_number).pnums(p)]++;
	}
}
void FacesEnumerator::FromCountToBegins(ulint *ig)
{
	ulint first_index=0,next_size=0;
	for(ulint p = 0; p<psize+1; p++)
	{
		next_size = ig[p];
		ig[p] = first_index;
		first_index+=next_size;
	}
}
void FacesEnumerator::FillJG(ulint *JG)
{
	ulint jstart, points_count;
	for(ulint k=0; k<psize; k++)
		cur_begins[k] = 0;
	for(ulint el_number=0; el_number<elcount; el_number++)
	{
		points_count = get_elem(elems,el_number).pcount();
		for(ulint p=0; p<points_count; p++)
		{
			jstart = ig[get_elem(elems,el_number).pnums(p)];
			JG[cur_begins[get_elem(elems,el_number).pnums(p)]+jstart] = el_number;
			cur_begins[get_elem(elems,el_number).pnums(p)]++;
		}
	}
	// теперь сортируем по возрастанию каждый подмассив массива JG
	for(ulint p=0; p<psize; p++)
		sort(&JG[ig[p]],&JG[ig[p+1]]); // передал указатели на начало и конец массивов в качестве итераторов
}
long FacesEnumerator::FastSearch(ulint el1num, ulint el2num)
{
	if(el1num == el2num) // значит ищем эелемент на границе.
		for(ulint bel=0; bel<faces_on_board.size(); bel++)
			if(faces_on_board[bel].el_num == el1num)
				return bel;
	ulint col = min(el1num, el2num);
	ulint row = max(el2num, el1num);
	return Dich(col, ig[row], ig[row+1]-1);
}
FacesEnumerator::~FacesEnumerator()
{
	if(ig!=0)delete[] ig;
	if(jg!=0)delete[] jg;
	if(cur_begins!=0)delete[] cur_begins;
	if(reordered!=0)delete[] reordered;
}
bool FacesEnumerator::out_stl_format(STR filename, VectorXD<double,3> *points, uint face_size)
{
	if(points==0 || elems == 0)
		return false;
	FILE *fp = fopen(filename,"w");
	uint *face, loc_size;
	face = new uint[face_size];
	VectorXD<double,3> normal, Multiply[2];
	fprintf(fp,"solid mesh\n");
	for(uint el_number=0; el_number<faces_on_board.size(); el_number++)
	{
		loc_size = get_elem(elems,faces_on_board[el_number].el_num).build_face(face,faces_on_board[el_number].local_face_num);
		if(loc_size < 3)
		{
			delete[] face;
			fclose(fp);
			return false;
		}
		Multiply[0] = points[face[1]] - points[face[0]];
		Multiply[1] = points[face[2]] - points[face[0]];
		//normal = VectorXD<double,3>::vector_mult(Multiply,Det);
		fast_vector_mutl(Multiply[0],Multiply[1],normal);
		fprintf(fp,"facet normal %f %f %f\n",normal.coords[0], normal.coords[1], normal.coords[2]);
		fprintf(fp,"outer loop\n");
		for(uint p=0; p<loc_size; p++)
			fprintf(fp,"vertex %f %f %f\n",points[face[p]].coords[0], points[face[p]].coords[1], points[face[p]].coords[2]);
		fprintf(fp,"endloop\n");
		fprintf(fp,"endfacet\n");
	}
	delete[] face;
	fclose(fp);
	return false;
}
