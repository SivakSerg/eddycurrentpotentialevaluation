#include "edge_enumerator.h"

bool edge_enumerator::reorder_sub_edges(const edge_enumerator& sub_edges_arg)
{
	std::vector<uint> nums;
	nums.resize(sub_edges_arg.get_adge_count());
	if(!give_nums_to_sub_edges(sub_edges_arg.edge_list[0].data(),
							   sub_edges_arg.edge_list[1].data(),
							   nums, sub_edges_arg.get_adge_count()))
		return false;
	if(!reorder_to_sub_edges(sub_edges_arg.edge_list[0].data(),
							   sub_edges_arg.edge_list[1].data(),
							   nums.data(), sub_edges_arg.get_adge_count()))
		return false;
	return true;
}

void edge_enumerator::reorder_list(vector<uint> &reorder)
{
    uint size = get_adge_count();
    reorder.resize(size);
    for(uint i=0; i<size; ++i)
            reorder[ true_nums[i] ] = i;
}

bool edge_enumerator::give_nums_to_sub_edges(const uint *sub_v1, const uint *sub_v2, vector<uint> &nums, uint size_sub_edge)
{
    for(uint ind=0; ind < size_sub_edge; ++ind)
    {
        uint edgeNum = find_adge_num(sub_v1[ind], sub_v2[ind]);
        if(founded)
            nums[ind] = edgeNum;
        else
            nums[ind] = get_adge_count();
    }
    return  true;
}

bool edge_enumerator::reorder_to_sub_edges(const uint *sub_v1, const uint *sub_v2, uint *nums, uint size_sub_edge)
{
	uint swap;
	std::vector<uint> true_vals;
	uint size = get_adge_count();
	true_vals.resize( size );
	//
	for(uint i=0; i<size; ++i)
		true_vals[ i ] = i;
	//
        uint nextEdge = 0;
	for(uint ind=0; ind < size_sub_edge; ++ind)
	{
            if(nums[ind] < size)
            {
		swap = edge_list[0][nextEdge];
		edge_list[0][nextEdge] = sub_v1[ind];
		edge_list[0][nums[ind]] = swap;

		swap = edge_list[1][nextEdge];
		edge_list[1][nextEdge] = sub_v2[ind];
		edge_list[1][nums[ind]] = swap;

		swap = true_vals[nextEdge];
		true_vals[nextEdge] = true_vals[nums[ind]];
		true_vals[nums[ind]] = swap;
                ++nextEdge;
            }
	}
	//
	for(uint i=0; i<size; ++i)
		true_nums[ true_vals[i] ] = i;
	return true;
}

bool edge_enumerator::addr_connected_points(vector<uint> &addr)
{
	if(ig.empty() || N == 0)
		return false;
	for(uint k=0; k<N; k++)
		if(ig[k+1] == ig[k])
			addr.push_back( k );
	return true;
}

void edge_enumerator::release()
{
	Kel = 0; N = 0;
	list1.clear();
	list2.clear();
}

edge_enumerator::~edge_enumerator()
{
	release();
}

uint edge_enumerator::find_adge_num(uint pnum1, uint pnum2)
{
	uint row, col;
	int res;
	if(ig.empty() || jg.empty())
	{
		founded = false;
		return 0xFFFFF;
	}
	row = (pnum1>pnum2)?pnum1:pnum2;
	col = (pnum1>pnum2)?pnum2:pnum1;
	if(ig[row+1]>=ig[row]+1)
	{
		res = Dich(col, ig[row], ig[row+1]-1);
		if(res < 0)
			{founded = false; return 0xFFFFF;}
		founded = true;
		return true_nums[res];
	}
	else
		founded = false;
	return 0xFFFFF;
}

int edge_enumerator::Dich(uint i, uint jstart, uint jend)
{
	int middle, res;
	try
	{
        if(jg[jend]<jg[jstart])return -1;
        res = jend - jstart;
        while(abs(res)>1)
        {
            if(jg[jstart]>i || jg[jend]<i)return -1;
            middle = (jstart + jend)/2;
            if((jstart + jend)%2)
            {
                if(jg[middle+1]>i)jend = middle+1;
                else
                    if(jg[middle]<i)jstart = middle;
                    else
                    {
                        jend = middle + 1; jstart = middle;
                    }
            }
            else
            {
                if(jg[middle]>i)jend = middle;
                else
                    if(jg[middle]<i)jstart = middle;
                    else
                        return middle;
            }
            res = jstart - jend;
        }
        if(jg[jstart]==i)return jstart;
        if(jg[jend]==i)return jend;
        return -1;
	}
	catch(...)
	{
		return -2;
	}
}

bool edge_enumerator::find_vertisies(ulint adge_num, ulint pnums[])
{
	if(ig.empty() || jg.empty() || adge_num >= ig[N])return false;
	pnums[0]=edge_list[0][adge_num];
	pnums[1]=edge_list[1][adge_num];
	return true;
}

// функции для поэтапной нумерации ребер для разных типов элементов
bool edge_enumerator::set_size(uint N)
{
	try
	{
		release();
		this->N = N;
		listbeg.resize(N);
		ig.resize(N+1);
		for(uint i=0; i<N; i++)
			listbeg[i] = -1;
		listsize = -1;
		return true;
	}
	catch(...)
	{
		return false;
	}
}

bool edge_enumerator::enumerate()
{
	/* Создание портрета по списку */
	long iaddr;
	jg.resize(list1.size());
	ig[0] = 0;
	for(uint i = 0; i<N; i++)
	{
		ig[i+1]=ig[i];
		iaddr=listbeg[i];
		while(iaddr!=-1)
		{
			jg[ig[i+1]]=list1[iaddr];
			ig[i+1]=ig[i+1]+1;
			iaddr=list2[iaddr];
		}
	}

	edge_list[0].resize(ig[N]);
	edge_list[1].resize(ig[N]);
	for(uint i=0; i<N; i++)
		for(uint k=ig[i]; k<ig[i+1]; k++)
		{
			edge_list[0][k] = jg[k];
			edge_list[1][k] = i;
		}
	//
	true_nums.resize(ig[N]);
	for(uint i=0; i<true_nums.size(); ++i)
		true_nums[i] = i;
	return true;
}
