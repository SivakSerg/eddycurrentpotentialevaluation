/*
Данный класс реализует алгоритм выделения из объемной сетки
поверхностной. Для этого используется нумератор граней, который
выделяет грани, смежные только с одним элементом (грани по
которым два элемента не граничат). Эти грани (при условии, что
сетка согласована) лежат на границе области. Далее по всем номерам
составляется новый нумерованный список (соответствует новой нумерации
поверхностной сетки).
*/
#pragma once
#include "base_elements.hpp"		// содержит описание поверхностных и трехмерных элементов
#include "FacesEnumerator.h"	// выделяет треугольники на поверхности
#include <complex>
#include <stdio.h>
#include "base.hpp"
#include "FastVectorMult.hpp"
#include <vector>
#include "edge_enumerator.h"
using namespace std;

#ifndef SURFACE_MESH
#define SURFACE_MESH

// параметры приложеного тока на границе
// количество параметров равно числу материалов на границе
struct area_parameters
{
	double I = 0.0, omega = 0.0, sigma = 0.0, mu = 0.0;
	std::complex< double > vave_num_k = 0.0;
	double epsilon = 0.0;
};

struct grid_structure
{
protected:
	static double det(double M[][2], uint size);
	VectorXD<double,3> vector_mult(VectorXD<double,3> a, VectorXD<double,3> b); // векторное произведение
        bool build_elem_buffer(edge_enumerator &ede);
        bool build_point_buffer(edge_enumerator &ede);
public:
	//triangle *area; VectorXD<double,3> *points;
	vector<triangle> area;
	vector<VectorXD<double, 3> > points;
	uint arsize, psize;
	grid_structure():area(0),points(0),arsize(0),psize(0){}
	grid_structure(const grid_structure &arg);
	void copy_elem(triangle &dest,const triangle &source);
	virtual ~grid_structure();
	bool out_meshlab(STR filename, uint matr);
	bool out_meshlab(STR filename);
	bool out_std_files(STR TRG, STR XYZ); // вывод в стандартные файлы trg - указатель индексов и xyz - перечислитель точек
        bool read_std_files(STR TRG, STR XYZ); // получение сетки из стандартных файлов.
	grid_structure &operator+=(const VectorXD<double,3> &point); // смещает в направлении вектора point все точки сетки
	grid_structure &operator+=(const grid_structure &arg); // объединяет две сетки в одну
	uint operator=(const uint material); // присваивает номер материала всем элементам сетки
	grid_structure &operator=(const grid_structure &arg);
	grid_structure &operator*=(Block<double,3> &scaled_rot);
    bool make_subdivision();
	void centralise(VectorXD<double, 3> &center);
	void clear();
    void invert(); // функция инвертирует направление нормали в сетке
};
// вспомогательные процедуры построения подсеток и перенумерации их в своей, локальной
// системе номеров. Для поверхностной сетки трехмерного тела определяет внешнюю нормаль
class SurfaceMesh
{
	static double det(double M[][2], uint size);
	VectorXD<double,3> vector_mult(VectorXD<double,3> a, VectorXD<double,3> b); // векторное произведение
	void define_direction(uint *face, uint pnum, VectorXD<double,3> *points); // переупорядочивание элементов face с тем, чтобы определить внешнюю нормаль
public:
	SurfaceMesh();
	bool build_sub_mesh(tetrahedron *area, VectorXD<double,3> *points, uint arsize, uint psize, grid_structure &output);
	bool build_surface_mesh(tetrahedron *area, VectorXD<double,3> *points, uint arsize, uint psize, grid_structure &output); // по трехмерной сетке строит поверхностную
	bool re_enumerate(grid_structure &in_mesh, grid_structure &out_mesh);
	bool re_enumerate_elem(tetrahedron *in_area, tetrahedron *out_area, VectorXD<double, 3> *in_points, VectorXD<double, 3> *out_points,
						   uint arsize, uint psize, grid_structure &in_mesh, grid_structure &out_mesh); // эта функция перенумерует вершины, поставив на первое место вершины на границе
	bool build_surface_mesh(tetrahedron *in_area, tetrahedron *out_area, VectorXD<double, 3> *in_points, VectorXD<double, 3> *out_points,
						   uint arsize, uint psize, grid_structure &out_mesh);
        // функция переупорядочивает узлы поверхностной сетки таким образом, чтобы
        // 1) нумерация всех вершин начиналась с нуля,
        // 2) не было висячих вершин (чтобы нумерация была плотной)
        // 3) вершины поверхностной сетки нумеровались раньше
        bool reorder_vertex_numbers(
            const std::vector<VectorXD<double,3> > & inputPoints,
            const std::vector<tetrahedron> & inputTetr,
            const std::vector<triangle> & surfaceSubMesh,
            std::vector<VectorXD<double,3> > & outputPoints,
            std::vector<tetrahedron> & outputMesh,
            std::vector<triangle> & outputSurfaceMesh);
};

#endif
