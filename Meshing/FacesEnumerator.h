#pragma once
#include <algorithm> // для функции sort
#include <vector>
#include <iostream>
#include "VectorXD.hpp"
#include "base_elements.hpp"
#include "FastVectorMult.hpp"
#include <stdio.h>
#include <unordered_set> // это множество "set" используется для хранения и поиска элементов по их значениям, нужна хэш-функция, которая реализует этот поиск
using namespace std;
/*
Поскольку данный класс работает с массивом чисто
виртуальных объектов common_elem_struct, необходимо
передавать размер в байтах класса потомка, чтобы
правильно определять смещение в массиве (в противном случае
будет использовано смещение самого класса common_elem_sturct).
*/

#ifndef FACES_ENUMERATOR
#define FACES_ENUMERATOR

// структура для задания грани через номера смежных по ней элементов
struct common_face_struct
{
	ulint el_les_num, el_gr_num;
	uint less_local_num; // локальный номер грани элемента с меньшим номером
        std::vector<uint> p;
};
struct hash_lambda
{
    size_t operator()(const common_face_struct &arg ) const
    {
        std::size_t ret = 0;
        for(uint i=0; i<arg.p.size(); ++i)
            ret += arg.p[i];
        return ret;
    }
};
struct eq_lambda
{
    bool operator()(const common_face_struct &A, const common_face_struct &B) const
    {
        std::vector<uint> Ap, Bp;
        Ap = A.p;
        Bp = B.p;
        sort(Ap.begin(),Ap.end());
        sort(Bp.begin(),Bp.end());
        if(A.p.size() == B.p.size())
        {
            bool equal = true;
            for(std::size_t i=0; i<A.p.size() && equal; ++i)
                if(Ap[i] != Bp[i])
                    equal = false;
            return equal;
        }
        return false;
    }
};

class FacesEnumerator;
#define TetrFacesEnumerator FacesEnumerator
class FacesEnumerator // Нумерует грани конечноэлементной сетки.
{
protected:
	common_elem_struct *elems; ulint elcount, psize;
	ulint *ig, *jg, *cur_begins;
	int sizeof_el;
	void FromCountToBegins(ulint *ig);
	void FillJG(ulint *JG);
	void FillIG(ulint *IG, bool init_ig=true);
	int Dich(uint i,uint jstart,uint jend);
	bool GeneratePortrait(uint size, uint kel);
	void Reorder();
	long FastSearch(ulint el1num, ulint el2num);
	long LowSearch(ulint el1num, ulint el2num);
	common_face_struct *reordered; ulint reordered_size;
	static double Det(double M[][2],uint size);
	common_elem_struct &get_elem(common_elem_struct *elar, uint elnum); // фукнция, которая по переданному смещению определяет элемент
        unordered_set<common_face_struct,hash_lambda,eq_lambda> setFaces;
        hash_lambda hash_lam_func;
        eq_lambda eq_lam_func;
public:
	struct face_on_border{
            ulint el_num;
            ulint local_face_num;
	};
	vector<common_face_struct> faces;   // список граней (всех граней на элементе)
	vector<face_on_border> faces_on_board;// список номеров граней всей области
	FacesEnumerator():elems(0),ig(0),jg(0),cur_begins(0),reordered(0),sizeof_el(sizeof(common_elem_struct)),
        setFaces(0,hash_lam_func,eq_lam_func){}
	virtual ~FacesEnumerator();
	void Dispose();
	bool SetElems(common_elem_struct *els, ulint elc, ulint points, int sizeof_el=0)
	{
		if(els==0)return false;
		elems=els;
		psize=points;
		elcount = elc;
		if(sizeof_el==0)
			this->sizeof_el = sizeof(common_elem_struct);
		else
			this->sizeof_el = sizeof_el;
		return true;
	}
	bool Enumerate(ulint face_size, bool build_search_structs); // аргумент - максимальный размер грани
	long FindFace(ulint el1num, ulint el2num);
        std::pair<uint,uint> findFaceElems(const std::vector<uint> &pnums) const;
	common_face_struct GetReordered(ulint k){if(reordered!=0)return reordered[k]; else return faces[k];}
	// функции исключительно для тестирования
	bool test1(STR filename, ulint max_face_size, ulint min_face_size);
	bool test2(STR filename, ulint max_face_size);
	// вывод граней в формате *.slt
	bool out_stl_format(STR filename, VectorXD<double,3> *points, uint face_size); // вывод граничных граней (уместная тавтология) в формате stl
};

#endif
