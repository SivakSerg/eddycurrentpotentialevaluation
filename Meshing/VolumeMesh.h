#pragma once
#include "base_elements.hpp"		// содержит описание поверхностных и трехмерных элементов
#include "FacesEnumerator.h"	// выделяет треугольники на поверхности
#include <complex>
#include <stdio.h>
#include "base.hpp"
#include "FastVectorMult.hpp"
#include "edge_enumerator.h"
#include "SurfaceMesh.h"
#include <vector>
#include <iostream>
using namespace std;

#ifndef VOLUME_MESH
#define VOLUME_MESH

// функция которая нумерует вершины поверхностной сетки раньше других вершни
bool re_enumerate(vector<triangle> &trg, vector<tetrahedron> &tetr,
    vector<VectorXD<double, 3> > &points, triangle *surface_buff, uint buffsize);

// функция получения глобальных номеров рёбер в локальном порядке
void get_edge_nums_trig(uint edge_trig[], triangle &trig, edge_enumerator &ede, uint pshift);

void get_edge_nums_tetr(uint edge_tetr[], tetrahedron &tetr, edge_enumerator &ede, uint pshift);

// один треугольник разбиваем на 4 штуки
void one_triangle_subdivision(uint edge_nums[], triangle &area, triangle new_area[]);

// один тетраэдр разбиваем на 6 штук
void one_tetr_subdivision(uint edge_nums[], tetrahedron &area, tetrahedron new_area[]);

// функция для подразбиения совместных объёмной и поверхностной сеток
bool make_subdivision(vector<triangle> &surface, vector<tetrahedron> &volume,
    vector<VectorXD<double, 3> > &points, vector<triangle> &border);

struct VolumeTetrMesh
{
private:
	bool build_point_buffer(edge_enumerator &ede);
	bool build_elem_buffer(edge_enumerator &ede);
	void copy_elem(tetrahedron &dest, const tetrahedron &source);
	void reorder_faces(uint face_nums[], uint free_num, VectorXD<double,3> *points, bool inverted=true); // function to reorder point numbers of face to define normal direction.
public:
	vector<tetrahedron> area; uint arsize;
	vector<VectorXD<double, 3> > points; uint psize;
	VolumeTetrMesh();
	VolumeTetrMesh(const VolumeTetrMesh &arg);
	virtual ~VolumeTetrMesh();
	bool make_subdivision(); // функция подразбиения объёмной секти кольца
	VolumeTetrMesh &operator+=(VectorXD<double, 3> &point);
	VolumeTetrMesh &operator*=(Block<double, 3> &scaled_rot);
	VolumeTetrMesh &operator=(uint material);
	VolumeTetrMesh &operator+=(const VolumeTetrMesh &arg);
	VolumeTetrMesh &operator=(const VolumeTetrMesh &obj_mesh);
	//
	bool out_mesh(STR AREA, STR POINTS);
        bool out_mesh_with_matr_nums(STR AREA, STR POINTS);
	bool out_mesh(STR TRG, STR XYZ, uint material);
	bool out_volume_surface(STR STD_FILE);
        bool out_tetrahedron_mesh(STR AREA, STR POINTS);
        bool out_triangle_mesh(STR TRG, STR XYZ);
        bool read_mesh(STR trg, STR xyz);
	//
	// из исходной сетки выкидывает узлы, которые лежат в объекте с материалом material, оставляя в нумерации узлы лишь на границе этого объекта и в объёме других объектов
	bool exclude_volume_nodes(grid_structure &sm, VolumeTetrMesh &vtm, uint material);
};

#endif
