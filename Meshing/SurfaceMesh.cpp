#include "SurfaceMesh.h"

bool grid_structure::build_elem_buffer(edge_enumerator &ede)
{
    this->arsize = area.size();
	uint new_arsize = this->arsize * 4;
	vector<triangle> new_area(new_arsize);
	uint edge_nums[3];
	//
	uint next_ar = 0;
	for (uint ar = 0; ar<arsize; ++ar)
	{
		edge_nums[0] = ede.find_adge_num(area[ar].pnums(0), area[ar].pnums(1)) + this->psize;
		edge_nums[1] = ede.find_adge_num(area[ar].pnums(0), area[ar].pnums(2)) + this->psize;
		edge_nums[2] = ede.find_adge_num(area[ar].pnums(1), area[ar].pnums(2)) + this->psize;
		//
		new_area[next_ar].pnums(0) = area[ar].pnums(0);
		new_area[next_ar].pnums(1) = edge_nums[0];
		new_area[next_ar].pnums(2) = edge_nums[1];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = area[ar].pnums(1);
		new_area[next_ar].pnums(1) = edge_nums[2];
		new_area[next_ar].pnums(2) = edge_nums[0];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = area[ar].pnums(2);
		new_area[next_ar].pnums(1) = edge_nums[1];
		new_area[next_ar].pnums(2) = edge_nums[2];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = edge_nums[0];
		new_area[next_ar].pnums(1) = edge_nums[2];
		new_area[next_ar].pnums(2) = edge_nums[1];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
	}
	this->arsize = new_arsize;
	area = new_area;
	return true;
}

bool grid_structure::build_point_buffer(edge_enumerator &ede)
{
	uint new_psize = ede.get_adge_count() + this->psize;
	points.resize(new_psize);// VectorXD<double, 3> *new_p_buff = new VectorXD<double, 3>[new_psize];
	//
	uint pnums[2];
	VectorXD<double, 3> midp;
	for (uint i=psize; i<new_psize; ++i) // проходимся по рёбрам сетки (точка в середине ребра).
	{
		if (!ede.find_vertisies(i - this->psize, pnums))
			return false;
		midp = (this->points[pnums[0]] + this->points[pnums[1]]) * (0.5);
		points[i] = midp;
	}
	this->psize = new_psize;
	return true;
}

bool grid_structure::make_subdivision()
{
	// вначале пронумеруем все ребра сетки
	edge_enumerator ede;
	ede.enumerate(&area[0], psize, arsize, true);
	// далее пройдёмся по элементам и из одного буфера получим два других
	if (!build_elem_buffer(ede))
		return false;
	if (!build_point_buffer(ede))
		return false;
	return true;
}

void grid_structure::invert()
{
    for(uint i=0; i<area.size(); ++i)
    {
        uint swap = area[i].pnums(0);
        area[i].pnums(0) = area[i].pnums(2);
        area[i].pnums(2) = swap;
    }
}

void grid_structure::clear()
{
	area.clear();
	points.clear();
	arsize = 0;
	psize = 0;
}
void grid_structure::centralise(VectorXD<double, 3> &center)
{
	VectorXD<double, 3> actual_center, shift;
	actual_center.common_init(0.);
	for (uint i = 0; i < psize; ++i)
	{
		actual_center = actual_center + points[i];
	}
	actual_center.coords[0] /= psize;
	actual_center.coords[1] /= psize;
	actual_center.coords[2] /= psize;
	shift = center - actual_center;
	for (uint i = 0; i < psize; ++i)
	{
		points[i] = shift + points[i];
	}
}
void grid_structure::copy_elem(triangle &dest,const triangle &source)
{
	for(uint p=0; p<3; p++)
		dest.pnums(p) = source.p[p];
	dest.material() = source.matr;
}
// смещение вдоль вектора point
grid_structure &grid_structure::operator+=(const VectorXD<double,3> &point)
{
	for(std::size_t i=0; i<points.size(); ++i)
		points[ i ] = points[ i ] + point;
	return *this;
}
uint grid_structure::operator =(const uint arg)
{
	for(uint i=0; i<arsize; i++)
		area[ i ].material() = arg;
	return arg;
}
grid_structure &grid_structure::operator=(const grid_structure &arg)
{
	this->arsize = arg.arsize; this->psize = arg.psize;
	area.resize(arg.arsize); points.resize(arg.psize);
	for(uint i=0; i<psize; i++)
		points[i] = arg.points[i];
	for(uint i=0; i<arsize; i++)
	{
		for(uint p=0; p<3; p++)
			area[i].p[p] = arg.area[i].p[p];
		area[i].material() = arg.area[i].matr;
	}
	return *this;
}
grid_structure &grid_structure::operator+=(const grid_structure &arg)
{
	area.resize(arsize + arg.arsize);
	points.resize(psize + arg.psize);
	for(uint i=psize; i<psize + arg.psize; i++)
		points[ i ] = arg.points[ i - psize ];
	for(uint i=arsize; i<arsize + arg.arsize; i++)
	{
		copy_elem(area[ i ], arg.area[ i - arsize ]);
		for(uint p=0; p<3; p++)
			area[i].pnums(p) += this->psize;
	}
	//
	this->psize += arg.psize;
	this->arsize += arg.arsize;
	return *this;
}
bool grid_structure::out_std_files(STR TRG, STR XYZ)
{
	FILE *ftrg, *fxyz;
	// выводим индексные массивы (подмассивы) элементов
	ftrg = fopen(TRG,"w");
	fprintf(ftrg,"%d\n",arsize);
	for(uint i=0; i<arsize; i++)
	{
		fprintf(ftrg,"%d %d %d\n",
			area[i].p[0],
			area[i].p[1],
			area[i].p[2]);
	}
	fclose(ftrg);
	// выводим точки
	fxyz = fopen(XYZ,"w");
	fprintf(fxyz,"%d\n",psize);
	for(uint i=0; i<psize; i++)
	{
		fprintf(fxyz,"%f %f %f\n",
			points[i].coords[0],
			points[i].coords[1],
			points[i].coords[2]);
	}
	fclose(fxyz);
	return true;
}
bool grid_structure::read_std_files(STR TRG, STR XYZ)
{
    arsize = 0;
    psize = 0;
    area.clear();
    points.clear();

    FILE *ftrg, *fxyz;
    // выводим индексные массивы (подмассивы) элементов
    ftrg = fopen(TRG,"r");
    fscanf(ftrg,"%d\n",&arsize);
    area.resize(arsize);
    for(uint i=0; i<arsize; i++)
    {
        fscanf(ftrg,"%d %d %d\n",
            &(area[i].p[0]),
            &(area[i].p[1]),
            &(area[i].p[2]));
    }
    fclose(ftrg);
    // выводим точки
    fxyz = fopen(XYZ,"r");
    fscanf(fxyz,"%d\n",&psize);
    points.resize(psize);
    for(uint i=0; i<psize; i++)
    {
        float x, y, z;
        fscanf(fxyz,"%f %f %f\n",
            &x, &y, &z);
        points[i].coords[0] = x;
        points[i].coords[1] = y;
        points[i].coords[2] = z;
    }
    fclose(fxyz);
    return true;
}

double grid_structure::det(double M[][2], uint size)
{
	return M[0][0] * M[1][1] - M[0][1] * M[1][0];
}
VectorXD<double,3> grid_structure::vector_mult(VectorXD<double,3> a, VectorXD<double,3> b)
{
	VectorXD<double,3> ret;
	//Mult[0] = a;
	//Mult[1] = b;
	fast_vector_mutl(a,b,ret);
	return ret;// VectorXD<double,3>::vector_mult(Mult,det);
}
grid_structure::grid_structure(const grid_structure &arg) :area(arg.area), points(arg.points)
{
	this->arsize = arg.arsize; this->psize = arg.psize;
}
grid_structure::~grid_structure()
{}
bool grid_structure::out_meshlab(STR filename, uint matr)
{
	VectorXD<double,3> normal, r1, r2;
	FILE *fp = fopen(filename,"w");
	if(fp == 0)
		return false;
	fprintf(fp,"solid mesh\n");
	for(uint i=0; i<arsize; i++)
	{
		if(area[i].material() == matr)
		{
			// определяем нормаль к треугольнику
			r1 = points[area[i].pnums(1)] - points[area[i].pnums(0)];
			r2 = points[area[i].pnums(2)] - points[area[i].pnums(1)];
			normal = vector_mult(r1,r2); normal.normalize();
			// затем выводим его
			fprintf(fp,"facet normal %f %f %f\n",normal.coords[0], normal.coords[1], normal.coords[2]);
			fprintf(fp,"outer loop\n");
			fprintf(fp,"vertex %f %f %f\n",points[area[i].pnums(0)].coords[0],
										   points[area[i].pnums(0)].coords[1],
										   points[area[i].pnums(0)].coords[2]);
			fprintf(fp,"vertex %f %f %f\n",points[area[i].pnums(1)].coords[0],
										   points[area[i].pnums(1)].coords[1],
										   points[area[i].pnums(1)].coords[2]);
			fprintf(fp,"vertex %f %f %f\n",points[area[i].pnums(2)].coords[0],
										   points[area[i].pnums(2)].coords[1],
										   points[area[i].pnums(2)].coords[2]);
			fprintf(fp,"endloop\n");
			fprintf(fp,"endfacet\n");
		}
	}
	fclose(fp);
	return true;
}
bool grid_structure::out_meshlab(STR filename)
{
	VectorXD<double,3> normal, r1, r2;
	FILE *fp = fopen(filename,"w");
	if(fp == 0)
		return false;
	fprintf(fp,"solid mesh\n");
	for(uint i=0; i<arsize; i++)
	{
		// определяем нормаль к треугольнику
		r1 = points[area[i].pnums(1)] - points[area[i].pnums(0)];
		r2 = points[area[i].pnums(2)] - points[area[i].pnums(1)];
		normal = vector_mult(r1,r2); normal.normalize();
		// затем выводим его
		fprintf(fp,"facet normal %f %f %f\n",normal.coords[0], normal.coords[1], normal.coords[2]);
		fprintf(fp,"outer loop\n");
		fprintf(fp,"vertex %f %f %f\n",points[area[i].pnums(0)].coords[0],
									   points[area[i].pnums(0)].coords[1],
									   points[area[i].pnums(0)].coords[2]);
		fprintf(fp,"vertex %f %f %f\n",points[area[i].pnums(1)].coords[0],
									   points[area[i].pnums(1)].coords[1],
									   points[area[i].pnums(1)].coords[2]);
		fprintf(fp,"vertex %f %f %f\n",points[area[i].pnums(2)].coords[0],
									   points[area[i].pnums(2)].coords[1],
									   points[area[i].pnums(2)].coords[2]);
		fprintf(fp,"endloop\n");
		fprintf(fp,"endfacet\n");
	}
	fclose(fp);
	return true;
}

bool SurfaceMesh::reorder_vertex_numbers(
    const std::vector<VectorXD<double,3> > & inputPoints,
    const std::vector<tetrahedron> & inputTetr,
    const std::vector<triangle> & surfaceSubMesh,
    std::vector<VectorXD<double,3> > & outputPoints,
    std::vector<tetrahedron> & outputMesh,
    std::vector<triangle> & outputSurfaceMesh)
{
    std::vector<int> indexes(inputPoints.size());
    for (std::size_t i=0; i<indexes.size(); ++i)
        indexes[i] = 0;

    for (auto & tetr : inputTetr)
    {
        for (int p=0; p<4; ++p)
            indexes[tetr.pnums(p)] = 1;
    }

    for (auto & trig : surfaceSubMesh)
    {
        for (int p=0; p<3; ++p)
            indexes[trig.pnums(p)] = -1;
    }

    int volumeNumbers = 1, surfaceNumbers = -1;
    for(uint i=0; i<inputPoints.size(); i++)
    {
        if(indexes[i] > 0)
        {
            indexes[i] = volumeNumbers;
            ++volumeNumbers;
        }
        else if (indexes[i] < 0)
        {
            indexes[i] = surfaceNumbers;
            --surfaceNumbers;
        }
    }

    ++surfaceNumbers;
    outputPoints.resize(volumeNumbers - 1 - surfaceNumbers);

    for (std::size_t i=0; i<indexes.size(); ++i)
    {
        if (0 != indexes[i])
        {
            if (indexes[i] < 0)
            {
                ++indexes[i];
                indexes[i] = -indexes[i];
            }
            else
            {
                --indexes[i];
                indexes[i] -= surfaceNumbers;
            }
            outputPoints[indexes[i]] = inputPoints[i];
        }
    }

    outputSurfaceMesh.resize(surfaceSubMesh.size());
    for (std::size_t i = 0; i < surfaceSubMesh.size(); ++i)
    {
        for (int p=0; p<3; ++p)
            outputSurfaceMesh[i].pnums(p) = indexes[surfaceSubMesh[i].pnums(p)];
    }

    outputMesh.resize(inputTetr.size());
    for (std::size_t i = 0; i < inputTetr.size(); ++i)
    {
        for (int p=0; p<4; ++p)
            outputMesh[i].pnums(p) = indexes[inputTetr[i].pnums(p)];
    }
    return true;
}

SurfaceMesh::SurfaceMesh()
{
	VectorXD<double,3>::set_scalar_zero(0.);
}
double SurfaceMesh::det(double M[][2], uint size)
{
	return M[0][0] * M[1][1] - M[0][1] * M[1][0];
}
VectorXD<double,3> SurfaceMesh::vector_mult(VectorXD<double,3> a, VectorXD<double,3> b)
{
	VectorXD<double,3> ret;
	//Mult[0] = a;
	//Mult[1] = b;
	fast_vector_mutl(a,b,ret);
	return ret;//VectorXD<double,3>::vector_mult(Mult,det);
}
void SurfaceMesh::define_direction(ulint *face, ulint pnum, VectorXD<double,3> *points)
{
	VectorXD<double,3> from0to1, from1to2;
	from0to1 = points[face[1]] - points[face[0]];
	from1to2 = points[face[2]] - points[face[1]];
	VectorXD<double,3> n;
	n = vector_mult(from0to1,from1to2);
	VectorXD<double,3> inside_direction;
	inside_direction = points[pnum] - (points[face[0]] + points[face[1]] + points[face[2]]) / 3.; // направление внутрь конечного элемента
	if(inside_direction.dotprod(n) > 0) // направление получившейся нормали совпало с направлением внутрь элемента
	{
		uint swap;
		swap = face[2]; face[2] = face[0]; face[0] = swap;
	}
}
// процедуры, отвечающие за построение поверхностной сетки с собственной нумерацией
bool SurfaceMesh::build_surface_mesh(tetrahedron *area, VectorXD<double,3> *points, uint arsize, uint psize, grid_structure &output)
{
	grid_structure sub_mesh;
	if(!build_sub_mesh(area,points,arsize,psize,sub_mesh))
		return false;
	if(!re_enumerate(sub_mesh,output)) // sum_mesh содержит указатели на все точки, которые уже были переданы sub_mesh.points == points
		return false;
	return true;
}
bool SurfaceMesh::build_sub_mesh(tetrahedron *area, VectorXD<double,3> *points, ulint arsize, ulint psize, grid_structure &output)
{
	FacesEnumerator fe;
	if(!fe.SetElems(area,arsize,psize,sizeof(tetrahedron)))
		return false;
	fe.Enumerate(3,true);
	output.psize = psize;
	output.points.resize(psize);
	output.arsize = fe.faces_on_board.size();
	output.area.resize(fe.faces_on_board.size());
	uint glob_p_num;
	for (uint i = 0; i < psize; ++i)
		output.points[i] = points[i];
	for(uint i=0; i<fe.faces_on_board.size(); i++)
	{
		output.area[i].matr = 0;
		area[ fe.faces_on_board[i].el_num ].build_face( output.area[i].p, fe.faces_on_board[i].local_face_num );
		glob_p_num = area[ fe.faces_on_board[i].el_num ].pnums( fe.faces_on_board[i].local_face_num );
		// переупорядочиваем то что получили в соответствии с направлением нормали
		define_direction( output.area[i].p, glob_p_num, points );
	}
	return true;
}
grid_structure &grid_structure::operator*=(Block<double,3> &scaled_rot)
{
	for(int i=0; i<psize; i++)
		points[ i ] = scaled_rot * points[ i ];
	return *this;
}
bool SurfaceMesh::re_enumerate(grid_structure &in_mesh, grid_structure &out_mesh)
{
	// у этого массива выделяется подмассив упорядоченных элементов,
	// которые являются новыми номерами для сетки.
	uint *p_n_ind = new uint[in_mesh.psize];
	for(uint i=0; i<in_mesh.psize; i++)
		p_n_ind[i] = 0;
	for(uint i=0; i<in_mesh.arsize; i++)
		for(uint p=0; p<3; p++)
			p_n_ind[in_mesh.area[i].pnums(p)] = 1;
	uint acc = 0;
	for(uint i=0; i<in_mesh.psize; i++)
		if(p_n_ind[i] != 0)
		{
			p_n_ind[i] = acc;
			acc++;
		}
	out_mesh.psize = acc;
	out_mesh.points.resize(acc);
	out_mesh.arsize = in_mesh.arsize;
	out_mesh.area.resize(in_mesh.arsize);
	for(uint i=0; i<in_mesh.arsize; i++)
	{
		for(uint p=0; p<3; p++)
		{
			out_mesh.area[i].pnums(p) = p_n_ind[in_mesh.area[i].pnums(p)];
			out_mesh.points[ out_mesh.area[i].pnums(p) ] = in_mesh.points[in_mesh.area[i].pnums(p)];
		}
		out_mesh.area[i].material() = 0;
	}
        delete[] p_n_ind;
	return true;
}
bool SurfaceMesh::re_enumerate_elem(tetrahedron *in_area, tetrahedron *out_area, VectorXD<double, 3> *in_points, VectorXD<double, 3> *out_points, uint arsize, uint psize, grid_structure &in_mesh, grid_structure &out_mesh)
{
	// у этого массива выделяется подмассив упорядоченных элементов,
	// которые являются новыми номерами для сетки.
	uint *p_n_ind = new uint[in_mesh.psize];
	for (uint i = 0; i<in_mesh.psize; i++)
		p_n_ind[i] = in_mesh.psize;
	for (uint i = 0; i<in_mesh.arsize; i++)
		for (uint p = 0; p<3; p++)
			p_n_ind[in_mesh.area[i].pnums(p)] = 0;
	uint acc = 0;
	// эти номера раздаются поверхностной сетке
	for (uint i = 0; i<in_mesh.psize; i++)
		if (p_n_ind[i] == 0)
		{
			p_n_ind[i] = acc;
			acc++;
		}
	uint surface_size = acc;
	// эти номера используются для нумерации внутренних узлов
	for (uint i = 0; i<in_mesh.psize; i++)
		if (p_n_ind[i] == in_mesh.psize)
		{
			p_n_ind[i] = acc;
			acc++;
		}
	// строим поверхностную сетку
	out_mesh.psize = surface_size;
	out_mesh.points.resize(surface_size);
	out_mesh.arsize = in_mesh.arsize;
	out_mesh.area.resize(in_mesh.arsize);
	for (uint i = 0; i<in_mesh.arsize; i++)
	{
		for (uint p = 0; p<3; p++)
		{
			out_mesh.area[i].pnums(p) = p_n_ind[in_mesh.area[i].pnums(p)];
			//out_mesh.points[out_mesh.area[i].pnums(p)] = in_mesh.points[in_mesh.area[i].pnums(p)];
		}
		out_mesh.area[i].material() = in_mesh.area[i].material();
	}
	// теперь располагаем перенумерованные точки в нужном порядке
	for (uint i = 0; i < in_mesh.psize; ++i)
	{
		if (p_n_ind[i] < surface_size)
			out_mesh.points[p_n_ind[i]] = in_mesh.points[i];
	}
	// строим объемную сетку
	for (uint i = 0; i<arsize; i++)
	{
		for (uint p = 0; p<4; p++)
		{
			out_area[i].pnums(p) = p_n_ind[in_area[i].pnums(p)];
			//out_points[out_area[i].pnums(p)] = in_points[in_area[i].pnums(p)];
		}
		out_area[i].material() = in_area[i].material();
	}
	// теперь располагаем перенумерованные точки в нужном порядке
	for(uint i=0; i<psize; ++i)
	{
		out_points[p_n_ind[i]] = in_points[i];
	}
	return true;
}

// строит поверхностную сетку и переупорядочивает её элементы
bool SurfaceMesh::build_surface_mesh(tetrahedron *in_area, tetrahedron *out_area, VectorXD<double, 3> *in_points, VectorXD<double, 3> *out_points,
	uint arsize, uint psize, grid_structure &out_mesh)
{
	grid_structure sub_mesh;
	if (!build_sub_mesh(in_area, in_points, arsize, psize, sub_mesh))
		return false;
	if (!re_enumerate_elem(in_area, out_area, in_points, out_points, arsize, psize, sub_mesh, out_mesh)) // sum_mesh содержит указатели на все точки, которые уже были переданы sub_mesh.points == points
		return false;
	return true;
}
