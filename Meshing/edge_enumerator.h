#pragma once

#include <vector>
#include <set>
#include <algorithm>
#include "base.hpp"
using namespace std;

#ifndef EDGE_ENUMERATOR
#define EDGE_ENUMERATOR

class edge_enumerator
{
	std::vector<uint> ig, jg;
	std::vector<uint> edge_list[2]; // список ребер
	int Dich (uint i,uint jstart,uint jend);	// функция дихотомического поиска
	uint N, Kel;
	//
	std::vector<uint> true_nums; // истинные номера вершин
	// структуры данных для формирования списка ребер
	vector<long> list1, list2;
	std::vector<long int> listbeg;
    long int listsize;
	void release();
	// вспомогательные функции переупорядочивания меньшего массива номеров
	bool give_nums_to_sub_edges(const uint *sub_v1, const uint *sub_v2, vector<uint> &nums, uint size_sub_edge); // раздаёт номера рёбрам в подсетке рёбер, если ребра не оказалось, то возвращает false
	bool reorder_to_sub_edges(const uint *sub_v1, const uint *sub_v2, uint *nums, uint size_sub_edge);   // переупорядочиваем нумерацию рёбер в двух списках: adge_list и true_nums
public:
	// функция для перенумерации подмножества рёбер (вначале нумеруются рёбра из переданного класса, затем все остальные)
	bool reorder_sub_edges(const edge_enumerator &sub_edges_arg);// вызывается для объекта с большим количеством рёбер, переданный аргумент все свои рёбра дублирует в данном объекте, требуется занумеровать их в первую очередь
	//
	bool founded;
	edge_enumerator():founded(false),Kel(0){}
	~edge_enumerator();
	template<class Common_element_iterator> bool enumerate(Common_element_iterator area, ulint size, ulint kel, bool from_zero, uint maxlocalsize=0);
    template<class Common_element_iterator> bool enumerateUsingSet(Common_element_iterator area, ulint size, ulint kel, bool from_zero, uint maxlocalsize=0);
	uint get_adge_count() const {return (ig.empty())?0:ig.back();}
	bool find_vertisies(uint adge_num, uint pnums[]); // по номеру ребра находит два номера вершины
	uint find_adge_num(uint pnum1, uint pnum2);
	// функции поэтапного нумератора вершин для разных элементов (вызывать в указанном порядке)
	bool set_size(uint N); // #1 устанавливает размер для массива начала списка поиска (УСТАНОВИТЬ ПЕРЕД НУМЕРАЦИЕЙ ЭЛЕМЕНТОВ РАЗНЫХ ТИПОВ)
	template<class Common_element_iterator>
	bool fill_list_structs(Common_element_iterator area, uint kel, bool from_zero, uint maxlocalsize=0);// #2 построить списковую структуру или пополнить её новыми элементами
	bool enumerate(); // #3 нумерует ребра по построенному списку
	bool addr_connected_points(vector<uint> &addr); // номера точек, с которых начинаются связные области конечно-элементной сетки
    void reorder_list(vector<uint> &reorder); // формирует список перестановок, которые были осуществлены функцией reorder_sub_edges
};

template<class Common_elem_iterator>
bool edge_enumerator::fill_list_structs(Common_elem_iterator area, uint kel, bool from_zero, uint maxlocalsize)
{
	try
	{
	if(listbeg.empty())
		return false;
	long k,  ind1, ind2, iaddr, localnum;
        std::vector<long> globals;
	if(maxlocalsize)
	{
		globals.resize(maxlocalsize);
		localnum = maxlocalsize;
	}
	else
	{
		globals.resize(area[0].pcount());
	}
	Kel += kel;

	for(uint ielem=0; ielem<kel; ielem++)
	{
		localnum = (area+ielem)->pcount();
		for(ulint ii=0; ii<localnum; ii++)
			globals[ii] = (area+ielem)->pnums(ii);
		/* в этом файлах нумерация конечных элементов производится с 1, а надо с нуля */
		if(!from_zero)
			for(ulint i=0; i<localnum; i++)
				globals[i] -= 1;

		for(uint i=0; i<localnum; i++)
		{
			k = globals[i];
			for(uint j=i+1; j<localnum; j++)
			{
				ind1 = k;
				ind2 = globals[j];
				if( area[ielem].is_adge_exist(ind1,ind2) )
				{
					if(ind2<ind1)
					{
						ind1 = ind2;
						ind2 = k;
					}
					iaddr = listbeg[ind2];
					/*списка для ind2 не было, создаём его*/
					if(iaddr==-1)
					{
						listsize++;
						listbeg[ind2] = listsize;
						list1.push_back(ind1);
						list2.push_back(-1);
					}
					else
					{
						while(list1[iaddr]<ind1 && list2[iaddr]> -1)
							iaddr=list2[iaddr];
						/*не нашли и встретили элемент с большим номером, добавляем перед ним*/
						if(list1[iaddr]>ind1)
						{
							listsize++;
							list1.push_back(list1[iaddr]);
							list2.push_back(list2[iaddr]);
							list1[iaddr]=ind1;
							list2[iaddr]=listsize;
						}
						else
						/* не нашли, а список кончился, добавляем в конец списка */
							if(list1[iaddr]<ind1)
							{
								listsize++;
								list2[iaddr]=listsize;
								list1.push_back(ind1);
								list2.push_back(-1);
							}
					}
				}
			}
		}
	}

            return true;
	}
	catch(...)
	{
		return false;
	}
}

template<class Common_elem_iterator>
bool edge_enumerator::enumerate(Common_elem_iterator area, ulint size, ulint kel, bool from_zero, uint maxlocalsize)
{
	/* N - число вершин(функций б. с.) Kel - число конечных элементов */
	vector<long> list1, list2;
	long listsize, k,  ind1, ind2, iaddr, localnum;
    std::vector<long> globals, listbeg;
	release();
	if(!maxlocalsize)
	{
		localnum = area[0].pcount();
		globals.resize(localnum);
	}
	else
	{
		globals.resize(maxlocalsize);
	}
	uint kelem;
	N = size; Kel = kelem = kel;

	listbeg.resize(N);
	ig.resize(N + 1);
	listsize = -1;

	try
	{
	/* Построение списка list(1, 2) признак конца списка: значение -1 в list2 */
	for(uint i=0; i<N; i++)
		listbeg[i] = -1;
	for(uint ielem=0; ielem<Kel; ielem++)
	{
		localnum = (area+ielem)->pcount();
		for(ulint ii=0; ii<localnum; ii++)
			globals[ii] = (area+ielem)->pnums(ii);
		/* в этом файлах нумерация конечных элементов производится с 1, а надо с нуля */
		if(!from_zero)
			for(ulint i=0; i<localnum; i++)
				globals[i] -= 1;

		for(uint i=0; i<localnum; i++)
		{
			k = globals[i];
			for(uint j=i+1; j<localnum; j++)
			{
				ind1 = k;
				ind2 = globals[j];
				if( (area+ielem)->is_adge_exist(ind1,ind2) )
				{
					if(ind2<ind1)
					{
						ind1 = ind2;
						ind2 = k;
					}
					iaddr = listbeg[ind2];
					/*списка для ind2 не было, создаём его*/
					if(iaddr==-1)
					{
						listsize++;
						listbeg[ind2] = listsize;
						list1.push_back(ind1);
						list2.push_back(-1);
					}
					else
					{
						while(list1[iaddr]<ind1 && list2[iaddr]> -1)
							iaddr=list2[iaddr];
						/*не нашли и встретили элемент с большим номером, добавляем перед ним*/
						if(list1[iaddr]>ind1)
						{
							listsize++;
							list1.push_back(list1[iaddr]);
							list2.push_back(list2[iaddr]);
							list1[iaddr]=ind1;
							list2[iaddr]=listsize;
						}
						else
						/* не нашли, а список кончился, добавляем в конец списка */
							if(list1[iaddr]<ind1)
							{
								listsize++;
								list2[iaddr]=listsize;
								list1.push_back(ind1);
								list2.push_back(-1);
							}
					}
				}
			}
		}
	}

	/* Создание портрета по списку */
	jg.resize(list1.size());
	ig[0] = 0;
	for(uint i = 0; i<N; i++)
	{
		ig[i+1]=ig[i];
		iaddr=listbeg[i];
		while(iaddr!=-1)
		{
			jg[ig[i+1]]=list1[iaddr];
			ig[i+1]=ig[i+1]+1;
			iaddr=list2[iaddr];
		}
	}

	kelem = Kel;
	listbeg.clear();
	globals.clear();

	// теперь формируем список ребр (чтобы по номеру ребра найти его вершины)
	edge_list[0].resize(ig[N]);
	edge_list[1].resize(ig[N]);
	for(uint i=0; i<N; i++)
		for(uint k=ig[i]; k<ig[i+1]; k++)
		{
			edge_list[0][k] = jg[k];
			edge_list[1][k] = i;
		}
	//
	true_nums.resize(ig[N]);
	for(uint i=0; i<true_nums.size(); ++i)
		true_nums[i] = i;
	return true;
	}
	catch(...)
	{
		listbeg.clear();
		return false;
	}
}

template<class Common_elem_iterator>
bool edge_enumerator::enumerateUsingSet(Common_elem_iterator area, ulint size, ulint kel, bool from_zero, uint maxlocalsize)
{
    /* для каждой вершины имеется свой список смежности */
    std::vector<std::set<uint> > listOfAdjacency(size);
    ig.resize(size + 1);
    N = size;
    this->Kel = kel;

    for(int ielem=0; ielem<kel; ielem++)
    {
        uint localnum = (area+ielem)->pcount();
        std::set<uint, std::greater<uint> > transitionSet;
        for(int i=0; i<localnum; i++)
        {
            uint k = (area+ielem)->pnums(i);
            transitionSet.insert(k);
        }

        for(int i=0; i<localnum; i++)
        {
            bool isFirst = true;
            uint k = (area+ielem)->pnums(i);
            auto itHint = listOfAdjacency[k].end(); // C++ 11, starting from this version of the C++ the hint iterator must follow the supposed insertion.
            for (auto itElement = transitionSet.begin(); itElement != transitionSet.end(); ++itElement)
            {
                if (*itElement >= k)
                    continue;

                if(isFirst)
                {
                    auto insertionPair = listOfAdjacency[k].insert(*itElement);
                    itHint = insertionPair.first;
                    isFirst = false;
                }
                else
                {
                    itHint = listOfAdjacency[k].insert(itHint, *itElement);
                }
            }
        }
    }

    /* Создание портрета по списку */
    uint fullNumberOfNonZeros = 0;
    for (uint i=0; i<listOfAdjacency.size(); ++i)
        fullNumberOfNonZeros += listOfAdjacency[i].size();
    ig[0] = 0;
    jg.clear();
    jg.reserve(fullNumberOfNonZeros);
    for(int i = 0; i<size; i++)
    {
        ig[i+1]=ig[i];
        for (auto itCol=listOfAdjacency[i].begin(); itCol!=listOfAdjacency[i].end(); ++itCol)
        {
            jg.push_back(*itCol);
            ig[i+1]=ig[i+1]+1;
        }
    }

    edge_list[0].resize(ig[N]);
    edge_list[1].resize(ig[N]);
    for(uint i=0; i<N; i++)
            for(uint k=ig[i]; k<ig[i+1]; k++)
            {
                    edge_list[0][k] = jg[k];
                    edge_list[1][k] = i;
            }
    true_nums.resize(ig[N]);
    for(uint i=0; i<true_nums.size(); ++i)
            true_nums[i] = i;
    return true;
}

#endif
