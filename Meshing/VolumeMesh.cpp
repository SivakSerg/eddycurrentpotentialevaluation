#include "VolumeMesh.h"

// функция которая нумерует вершины поверхностной сетки раньше других вершни
bool re_enumerate(vector<triangle> &trg, vector<tetrahedron> &tetr,
    vector<VectorXD<double, 3> > &points, triangle *surface_buff, uint buffsize)
{
	vector<uint> reorder_p;
	uint psize = points.size();
	reorder_p.resize(points.size());
	for (uint i = 0; i < reorder_p.size(); ++i)
		reorder_p[i] = psize;
	uint new_num = 0;
	// раздаём номера вершин поверхностной сетке
	for (uint i = 0; i < trg.size(); ++i)
		for (uint p = 0; p < 3; ++p)
		{
		if (trg[i].p[p] >= points.size())
		{
			throw std::runtime_error("incompatible surface mesh!");
		}
		if (reorder_p[trg[i].p[p]] == psize)
		{
			reorder_p[trg[i].p[p]] = new_num;
			new_num++;
		}
		}
	// раздаём номера вершин объёмной сетке
	for (uint i = 0; i < tetr.size(); ++i)
		for (uint p = 0; p < 4; ++p)
		{
		if (tetr[i].p[p] >= points.size())
		{
			throw std::runtime_error("incompatible volume mesh!");
		}
		if (reorder_p[tetr[i].p[p]] == psize)
		{
			reorder_p[tetr[i].p[p]] = new_num;
			new_num++;
		}
		}
	// теперь присваиваем новые номера
	for (uint i = 0; i < trg.size(); ++i)
		for (uint p = 0; p < 3; ++p)
		{
			trg[i].p[p] = reorder_p[trg[i].p[p]];
		}
	for (uint i = 0; i < tetr.size(); ++i)
		for (uint p = 0; p < 4; ++p)
		{
			tetr[i].p[p] = reorder_p[tetr[i].p[p]];
		}
	// поверхностный буфер для учёта краевых условий
	if (surface_buff != 0)
		for (uint i = 0; i < buffsize; ++i)
		{
		for (uint p = 0; p < 3; ++p)
		{
			if (surface_buff[i].p[p] >= points.size())
			{
				throw std::runtime_error("incompatible additional surface!");
			}
			surface_buff[i].p[p] = reorder_p[surface_buff[i].p[p]];
		}
		}
	vector<VectorXD<double, 3> > swap_p;
	swap_p.resize(psize);
	for (uint i = 0; i < psize; ++i)
	{
		swap_p[reorder_p[i]] = points[i];
	}
	points = swap_p;
}

// функция получения глобальных номеров рёбер в локальном порядке
void get_edge_nums_trig(uint edge_trig[], triangle &trig, edge_enumerator &ede, uint pshift)
{
	edge_trig[0] = ede.find_adge_num(trig.p[0], trig.p[1]) + pshift;
	edge_trig[1] = ede.find_adge_num(trig.p[0], trig.p[2]) + pshift;
	edge_trig[2] = ede.find_adge_num(trig.p[1], trig.p[2]) + pshift;
}

void get_edge_nums_tetr(uint edge_tetr[], tetrahedron &tetr, edge_enumerator &ede, uint pshift)
{
	edge_tetr[0] = ede.find_adge_num(tetr.pnums(0), tetr.pnums(1)) + pshift;
	edge_tetr[1] = ede.find_adge_num(tetr.pnums(0), tetr.pnums(2)) + pshift;
	edge_tetr[2] = ede.find_adge_num(tetr.pnums(0), tetr.pnums(3)) + pshift;
	edge_tetr[3] = ede.find_adge_num(tetr.pnums(1), tetr.pnums(2)) + pshift;
	edge_tetr[4] = ede.find_adge_num(tetr.pnums(1), tetr.pnums(3)) + pshift;
	edge_tetr[5] = ede.find_adge_num(tetr.pnums(2), tetr.pnums(3)) + pshift;
}

// один треугольник разбиваем на 4 штуки
void one_triangle_subdivision(uint edge_nums[], triangle &area, triangle new_area[])
{
	uint next_ar = 0;
	new_area[next_ar].pnums(0) = area.pnums(0);
	new_area[next_ar].pnums(1) = edge_nums[0];
	new_area[next_ar].pnums(2) = edge_nums[1];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = edge_nums[0];
	new_area[next_ar].pnums(1) = area.pnums(1);
	new_area[next_ar].pnums(2) = edge_nums[2];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = area.pnums(2);
	new_area[next_ar].pnums(1) = edge_nums[1];
	new_area[next_ar].pnums(2) = edge_nums[2];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = edge_nums[0];
	new_area[next_ar].pnums(1) = edge_nums[2];
	new_area[next_ar].pnums(2) = edge_nums[1];
	new_area[next_ar].material() = area.material();
	++next_ar;
}

// один тетраэдр разбиваем на 6 штук
void one_tetr_subdivision(uint edge_nums[], tetrahedron &area, tetrahedron new_area[])
{
	uint next_ar = 0;
	new_area[next_ar].pnums(0) = area.pnums(0);
	new_area[next_ar].pnums(1) = edge_nums[0];
	new_area[next_ar].pnums(2) = edge_nums[1];
	new_area[next_ar].pnums(3) = edge_nums[2];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = area.pnums(1);
	new_area[next_ar].pnums(1) = edge_nums[0];
	new_area[next_ar].pnums(2) = edge_nums[3];
	new_area[next_ar].pnums(3) = edge_nums[4];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = area.pnums(2);
	new_area[next_ar].pnums(1) = edge_nums[1];
	new_area[next_ar].pnums(2) = edge_nums[3];
	new_area[next_ar].pnums(3) = edge_nums[5];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = area.pnums(3);
	new_area[next_ar].pnums(1) = edge_nums[2];
	new_area[next_ar].pnums(2) = edge_nums[4];
	new_area[next_ar].pnums(3) = edge_nums[5];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = edge_nums[0];
	new_area[next_ar].pnums(1) = edge_nums[1];
	new_area[next_ar].pnums(2) = edge_nums[2];
	new_area[next_ar].pnums(3) = edge_nums[3];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = edge_nums[1];
	new_area[next_ar].pnums(1) = edge_nums[2];
	new_area[next_ar].pnums(2) = edge_nums[3];
	new_area[next_ar].pnums(3) = edge_nums[5];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = edge_nums[0];
	new_area[next_ar].pnums(1) = edge_nums[2];
	new_area[next_ar].pnums(2) = edge_nums[3];
	new_area[next_ar].pnums(3) = edge_nums[4];
	new_area[next_ar].material() = area.material();
	++next_ar;
	//
	new_area[next_ar].pnums(0) = edge_nums[2];
	new_area[next_ar].pnums(1) = edge_nums[3];
	new_area[next_ar].pnums(2) = edge_nums[4];
	new_area[next_ar].pnums(3) = edge_nums[5];
	new_area[next_ar].material() = area.material();
	++next_ar;
}

// функция для подразбиения совместных объёмной и поверхностной сеток
bool make_subdivision(vector<triangle> &surface, vector<tetrahedron> &volume,
    vector<VectorXD<double, 3> > &points, vector<triangle> &border)
{
	if(!re_enumerate(surface, volume, points, &border[0], border.size()))
		return false;
	//
	edge_enumerator ede;
	ede.set_size(points.size());
	ede.fill_list_structs(&surface[0], surface.size(), true);
	ede.fill_list_structs(&volume[0], volume.size(), true);
	ede.enumerate();
	//
	uint pshift = points.size();
	uint new_psize = pshift + ede.get_adge_count();
	uint edge_nums_tetr[6];
	uint edge_nums_trig[3];
	vector<triangle> surface_sub;
	vector<tetrahedron> volume_sub;
	vector<triangle> border_sub;
	vector<VectorXD<double, 3> > points_sub;
	surface_sub.resize(surface.size()*4); // один треугольник разбивается на 4 треугольника
	border_sub.resize(border.size() * 4);
	volume_sub.resize(volume.size()*8); // один тетраэдр разбивается на 6 тетраэдров
	points_sub.resize(new_psize);
	for (uint i = 0; i < surface.size(); ++i)
	{
		get_edge_nums_trig(edge_nums_trig, surface[i], ede, pshift);
		one_triangle_subdivision(edge_nums_trig, surface[i], &surface_sub[4 * i]);
	}
	for (uint i = 0; i < border.size(); ++i)
	{
		get_edge_nums_trig(edge_nums_trig, border[i], ede, pshift);
		one_triangle_subdivision(edge_nums_trig, border[i], &border_sub[4 * i]);
	}
	for (uint i = 0; i < volume.size(); ++i)
	{
		get_edge_nums_tetr(edge_nums_tetr, volume[i], ede, pshift);
		one_tetr_subdivision(edge_nums_tetr, volume[i], &volume_sub[8 * i]);
	}
	for (uint i = 0; i < pshift; ++i)
	{
		points_sub[i] = points[i];
	}
	uint pnums[2];
	for (uint i = 0; i < ede.get_adge_count(); ++i)
	{
		ede.find_vertisies(i, pnums);
		points_sub[i + pshift] = (points[pnums[0]] + points[pnums[1]])*(0.5);
	}
	surface = surface_sub;
	volume = volume_sub;
	border = border_sub;
	points = points_sub;
	return true;
}

bool VolumeTetrMesh::read_mesh(STR trg, STR xyz)
{
    std::ifstream elemFile(trg), pointsFile(xyz);
    if(!elemFile.is_open() || !pointsFile.is_open())
        return false;
    elemFile >> arsize;
    area.resize(arsize);
    for(std::size_t i=0; i < area.size(); ++i)
    {
        int p[4], material;
        elemFile >> p[0] >> p[1] >> p[2] >> p[3] >> material;
        for(int j=0; j<4; ++j)
            area[i].pnums(j) = p[j];
        area[i].material() = material;
    }

    pointsFile >> psize;
    points.resize(psize);
    for(std::size_t i=0; i < points.size(); ++i)
    {
        double p[3];
        pointsFile >> p[0] >> p[1] >> p[2];
        for(int j=0; j<3; ++j)
            points[i].coords[j] = p[j];
    }
    return true;
}
bool VolumeTetrMesh::out_tetrahedron_mesh(STR TRG, STR XYZ)
{
    FILE *ftrg, *fxyz;
    // выводим индексные массивы (подмассивы) элементов
    ftrg = fopen(TRG, "w");
    if (ftrg == 0)
            return false;
    fprintf(ftrg, "%d\n", arsize);
    for (uint i = 0; i<arsize; i++)
    {
        fprintf(ftrg, "%d %d %d %d %d\n",
                area[i].material(),
                area[i].p[0],
                area[i].p[1],
                area[i].p[2],
                area[i].p[3]);
    }
    fclose(ftrg);
    // выводим точки
    fxyz = fopen(XYZ, "w");
    if (fxyz == 0)
            return false;
    fprintf(fxyz, "%d\n", psize);
    for (uint i = 0; i<psize; i++)
    {
            fprintf(fxyz, "%f %f %f\n",
                    points[i].coords[0],
                    points[i].coords[1],
                    points[i].coords[2]);
    }
    fclose(fxyz);
    return true;
}
bool VolumeTetrMesh::out_triangle_mesh(STR TRG, STR XYZ)
{
    FILE *ftrg, *fxyz;
    // выводим индексные массивы (подмассивы) элементов
    ftrg = fopen(TRG, "w");
    if (ftrg == 0)
            return false;
    fprintf(ftrg, "%d\n", arsize*4);
    for (uint i = 0; i<arsize; i++)
    {
        fprintf(ftrg, "%d %d %d\n",
                area[i].p[0],
                area[i].p[1],
                area[i].p[2]);
        fprintf(ftrg, "%d %d %d\n",
                area[i].p[0],
                area[i].p[1],
                area[i].p[3]);
        fprintf(ftrg, "%d %d %d\n",
                area[i].p[1],
                area[i].p[2],
                area[i].p[3]);
        fprintf(ftrg, "%d %d %d\n",
                area[i].p[0],
                area[i].p[2],
                area[i].p[3]);
    }
    fclose(ftrg);
    // выводим точки
    fxyz = fopen(XYZ, "w");
    if (fxyz == 0)
            return false;
    fprintf(fxyz, "%d\n", psize);
    for (uint i = 0; i<psize; i++)
    {
            fprintf(fxyz, "%f %f %f\n",
                    points[i].coords[0],
                    points[i].coords[1],
                    points[i].coords[2]);
    }
    fclose(fxyz);
    return true;
}
void VolumeTetrMesh::reorder_faces(uint face_nums[], uint free_num, VectorXD<double,3> *points, bool inverted)
{
	VectorXD<double,3> normal, v1, v2, test_vector;
	v1 = points[face_nums[1]] - points[face_nums[0]];
	v2 = points[face_nums[2]] - points[face_nums[1]];
	fast_vector_mutl(v1,v2,normal);
	test_vector = points[face_nums[0]] - points[free_num];
	if(inverted)
	{
		if(normal.dotprod(test_vector) < 0.)
		{
			uint swap;
			swap = face_nums[2];
			face_nums[2] = face_nums[0];
			face_nums[0] = swap;
		}
	}
	else
	{
		if(normal.dotprod(test_vector) > 0.)
		{
			uint swap;
			swap = face_nums[2];
			face_nums[2] = face_nums[0];
			face_nums[0] = swap;
		}
	}
}
bool VolumeTetrMesh::exclude_volume_nodes(grid_structure &sm, VolumeTetrMesh &vtm, uint material)
{
	FacesEnumerator fe;
	if(!fe.SetElems( &(this->area[0]),this->arsize,this->psize,sizeof(tetrahedron)))
		return false;
	fe.Enumerate(3,true);
	vector<uint> new_pnums;
	new_pnums.resize(this->psize);
	for(uint i=0; i<this->psize; ++i)
		new_pnums[i] = psize;
	uint material_gr, material_low, cur_face_num, face[3];
	bool on_the_obj_border, grater_case, lower_case;
	uint next_num = 0;
	triangle trg_acc;// треугольник в который кладём номера вершин заданной грани
	tetrahedron tetr_acc; // тетраэдр, который лежит во всех объектах, кроме material
	trg_acc.material() = material;
	uint free_num;
	for(uint i=0; i<fe.faces_on_board.size(); ++i)
	{
		material_gr = area[fe.faces_on_board[i].el_num].material();
		if(material_gr == material)
		{
			// объект находится на границе с внешней областью
			cur_face_num = fe.faces_on_board[i].local_face_num;
			area[fe.faces_on_board[i].el_num].build_face(face, cur_face_num);
			free_num = area[fe.faces_on_board[i].el_num].get_free_num(cur_face_num);
			reorder_faces(face,free_num,&points[0],true);
			if(new_pnums[face[0]] ==  psize)
			{
				new_pnums[face[0]] = next_num;
				next_num++;
			}
			if(new_pnums[face[1]] ==  psize)
			{
				new_pnums[face[1]] = next_num;
				next_num++;
			}
			if(new_pnums[face[2]] ==  psize)
			{
				new_pnums[face[2]] = next_num;
				next_num++;
			}
			trg_acc.pnums(0) = new_pnums[face[0]];
			trg_acc.pnums(1) = new_pnums[face[1]];
			trg_acc.pnums(2) = new_pnums[face[2]];
			sm.area.push_back(trg_acc);
		}
	}
	for(uint i=0; i<fe.faces.size(); ++i)
	{
		material_gr = area[fe.faces[i].el_gr_num].material();
		material_low = area[fe.faces[i].el_les_num].material();
		grater_case = (material_gr == material);
		lower_case = (material_low == material);
		on_the_obj_border = (material_gr != material_low) &&( grater_case || lower_case );
		if(on_the_obj_border)
		{
			cur_face_num = fe.faces[i].less_local_num;
			area[fe.faces[i].el_les_num].build_face(face, cur_face_num);
			free_num = area[fe.faces[i].el_les_num].get_free_num(cur_face_num);
			// for lower case we can use a point out of face but inside the current element
			if( lower_case )
				reorder_faces(face,free_num,&points[0],true);
			else
				reorder_faces(face,free_num,&points[0],false);
			if(new_pnums[face[0]] ==  psize)
			{
				new_pnums[face[0]] = next_num;
				next_num++;
			}
			if(new_pnums[face[1]] ==  psize)
			{
				new_pnums[face[1]] = next_num;
				next_num++;
			}
			if(new_pnums[face[2]] ==  psize)
			{
				new_pnums[face[2]] = next_num;
				next_num++;
			}
			trg_acc.pnums(0) = new_pnums[face[0]];
			trg_acc.pnums(1) = new_pnums[face[1]];
			trg_acc.pnums(2) = new_pnums[face[2]];
			sm.area.push_back(trg_acc);
		}
	}
	// теперь даём номера всем оставшимся точкам в объёме
	for(uint i=0; i<area.size(); ++i)
	{
		if(area[i].material() != material)
		{
			for(uint p=0; p<4; ++p)
			{
				if(new_pnums[ area[i].pnums(p) ] ==  psize)
				{
					new_pnums[ area[i].pnums(p) ] = next_num;
					next_num++;
				}
				tetr_acc.pnums(p) = new_pnums[ area[i].pnums(p) ];
			}
			tetr_acc.material() = area[i].material();
			vtm.area.push_back(tetr_acc);
		}
	}
	sm.points.resize(next_num);
	vtm.points.resize(next_num);
	for(uint k=0; k<psize; ++k)
	{
		if(new_pnums[k] != psize)
		{
			vtm.points[new_pnums[k]] = sm.points[new_pnums[k]] = points[k];
		}
	}
	sm.psize = sm.points.size();
	sm.arsize = sm.area.size();
	vtm.psize = vtm.points.size();
	vtm.arsize = vtm.area.size();
	return true;
}

bool VolumeTetrMesh::out_volume_surface(STR STD_FILE)
{
	SurfaceMesh sf;
	grid_structure surface;
	if(!sf.build_surface_mesh(&(this->area[0]), &(this->points[0]), arsize, psize, surface))
		return false;
	surface.out_meshlab(STD_FILE);
	return true;
}
void VolumeTetrMesh::copy_elem(tetrahedron &dest,const tetrahedron &source)
{
	for (uint p = 0; p<4; p++)
		dest.pnums(p) = source.p[p];
	dest.material() = source.matr;
}
VolumeTetrMesh::VolumeTetrMesh(const VolumeTetrMesh &arg)
{
	this->arsize = arg.arsize; this->psize = arg.psize;
	area.resize(arsize);// = new tetrahedron[arsize];
	points.resize(psize);// = new VectorXD<double, 3>[psize];
	for (uint i = 0; i<psize; i++)
		points[i] = arg.points[i];
	for (uint i = 0; i<arsize; i++)
	{
		for (uint p = 0; p<4; p++)
			area[i].p[p] = arg.area[i].p[p];
		area[i].material() = arg.area[i].matr;
	}
}
VolumeTetrMesh &VolumeTetrMesh::operator+=(const VolumeTetrMesh &arg)
{
	//tetrahedron *trig_buff = new tetrahedron[arg.arsize + arsize];
	//VectorXD<double, 3> *pbuff = new VectorXD<double, 3>[arg.psize + psize];
	area.resize(arg.arsize + arsize);
	points.resize(arg.psize + psize);
	// элементы
	for (uint i = 0; i<arsize; i++)
		copy_elem(area[i], area[i]);
	for (uint i = arsize; i<arsize + arg.arsize; i++)
	{
		copy_elem(area[i], arg.area[i - arsize]);
		for (uint p = 0; p<3; p++)
			area[i].pnums(p) += this->psize;
	}
	//
	this->psize += arg.psize;
	this->arsize += arg.arsize;
	return *this;
}
VolumeTetrMesh::VolumeTetrMesh() :area(0), arsize(0), points(0), psize(0)
{}

VolumeTetrMesh::~VolumeTetrMesh()
{}

bool VolumeTetrMesh::build_elem_buffer(edge_enumerator &ede)
{
	uint new_arsize = this->arsize * 8;
	vector<tetrahedron> new_area(new_arsize);
	uint edge_nums[6];
	//
	uint next_ar = 0;
	for (uint ar = 0; ar<arsize; ++ar)
	{
		edge_nums[0] = ede.find_adge_num(area[ar].pnums(0), area[ar].pnums(1)) + this->psize;
		edge_nums[1] = ede.find_adge_num(area[ar].pnums(0), area[ar].pnums(2)) + this->psize;
		edge_nums[2] = ede.find_adge_num(area[ar].pnums(0), area[ar].pnums(3)) + this->psize;
		edge_nums[3] = ede.find_adge_num(area[ar].pnums(1), area[ar].pnums(2)) + this->psize;
		edge_nums[4] = ede.find_adge_num(area[ar].pnums(1), area[ar].pnums(3)) + this->psize;
		edge_nums[5] = ede.find_adge_num(area[ar].pnums(2), area[ar].pnums(3)) + this->psize;
		//
		new_area[next_ar].pnums(0) = area[ar].pnums(0);
		new_area[next_ar].pnums(1) = edge_nums[0];
		new_area[next_ar].pnums(2) = edge_nums[1];
		new_area[next_ar].pnums(3) = edge_nums[2];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = area[ar].pnums(1);
		new_area[next_ar].pnums(1) = edge_nums[0];
		new_area[next_ar].pnums(2) = edge_nums[3];
		new_area[next_ar].pnums(3) = edge_nums[4];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = area[ar].pnums(2);
		new_area[next_ar].pnums(1) = edge_nums[1];
		new_area[next_ar].pnums(2) = edge_nums[3];
		new_area[next_ar].pnums(3) = edge_nums[5];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = area[ar].pnums(3);
		new_area[next_ar].pnums(1) = edge_nums[2];
		new_area[next_ar].pnums(2) = edge_nums[4];
		new_area[next_ar].pnums(3) = edge_nums[5];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = edge_nums[0];
		new_area[next_ar].pnums(1) = edge_nums[1];
		new_area[next_ar].pnums(2) = edge_nums[2];
		new_area[next_ar].pnums(3) = edge_nums[3];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = edge_nums[1];
		new_area[next_ar].pnums(1) = edge_nums[2];
		new_area[next_ar].pnums(2) = edge_nums[3];
		new_area[next_ar].pnums(3) = edge_nums[5];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = edge_nums[0];
		new_area[next_ar].pnums(1) = edge_nums[2];
		new_area[next_ar].pnums(2) = edge_nums[3];
		new_area[next_ar].pnums(3) = edge_nums[4];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
		//
		new_area[next_ar].pnums(0) = edge_nums[2];
		new_area[next_ar].pnums(1) = edge_nums[3];
		new_area[next_ar].pnums(2) = edge_nums[4];
		new_area[next_ar].pnums(3) = edge_nums[5];
		new_area[next_ar].material() = area[ar].material();
		++next_ar;
	}
	this->arsize = new_arsize;
        for (auto & element : new_area)
        {
            std::sort(element.p, element.p+4);
        }
	area = new_area;
	return true;
}
bool VolumeTetrMesh::build_point_buffer(edge_enumerator &ede)
{
	uint new_psize = ede.get_adge_count() + this->psize;
	points.resize(new_psize);// VectorXD<double, 3> *new_p_buff = new VectorXD<double, 3>[new_psize];
	//
	uint pnums[2];
	VectorXD<double, 3> midp;
	for (uint i=psize; i<new_psize; ++i) // проходимся по рёбрам сетки (точка в середине ребра).
	{
		if (!ede.find_vertisies(i - this->psize, pnums))
			return false;
		midp = (this->points[pnums[0]] + this->points[pnums[1]]) * (0.5);
		points[i] = midp;
	}
	this->psize = new_psize;
	return true;
}
bool VolumeTetrMesh::make_subdivision()
{
	// вначале пронумеруем все ребра сетки
	edge_enumerator ede;
	ede.enumerateUsingSet(area.data(), psize, arsize, true);
	// далее пройдёмся по элементам и из одного буфера получим два других
	if (!build_elem_buffer(ede))
		return false;
	if (!build_point_buffer(ede))
		return false;
	return true;
}

VolumeTetrMesh &VolumeTetrMesh::operator+=(VectorXD<double, 3> &point)
{
		for (uint i = 0; i < psize; ++i)
			points[i] = points[i] + point;
	return *this;
}

VolumeTetrMesh &VolumeTetrMesh::operator*=(Block<double, 3> &mtr)
{
		for (uint i = 0; i < psize; ++i)
			points[i] = mtr * points[i];
	return *this;
}

VolumeTetrMesh &VolumeTetrMesh::operator=(uint material)
{
		for (uint i = 0; i < arsize; ++i)
			area[i].material() = material;
	return *this;
}

VolumeTetrMesh &VolumeTetrMesh::operator=(const VolumeTetrMesh &arg)
{
	this->arsize = arg.arsize; this->psize = arg.psize;
	points.resize(psize);
	area.resize(arsize);
	for(uint i=0; i<psize; i++)
		points[i] = arg.points[i];
	for(uint i=0; i<arsize; i++)
	{
		for(uint p=0; p<4; p++)
			area[i].p[p] = arg.area[i].p[p];
		area[i].material() = arg.area[i].matr;
	}
	return *this;
}

bool VolumeTetrMesh::out_mesh(STR TRG, STR XYZ)
{
	FILE *ftrg, *fxyz;
	// выводим индексные массивы (подмассивы) элементов
	ftrg = fopen(TRG, "w");
	if (ftrg == 0)
		return false;
	fprintf(ftrg, "%d\n", arsize*4);
	for (uint i = 0; i<arsize; i++)
	{
		fprintf(ftrg, "%d %d %d\n",
			area[i].p[0],
			area[i].p[1],
			area[i].p[2]);
		fprintf(ftrg, "%d %d %d\n",
			area[i].p[1],
			area[i].p[2],
			area[i].p[3]);
		fprintf(ftrg, "%d %d %d\n",
			area[i].p[0],
			area[i].p[2],
			area[i].p[3]);
		fprintf(ftrg, "%d %d %d\n",
			area[i].p[0],
			area[i].p[1],
			area[i].p[3]);
	}
	fclose(ftrg);
	// выводим точки
	fxyz = fopen(XYZ, "w");
	if (fxyz == 0)
		return false;
	fprintf(fxyz, "%d\n", psize);
	for (uint i = 0; i<psize; i++)
	{
		fprintf(fxyz, "%f %f %f\n",
			points[i].coords[0],
			points[i].coords[1],
			points[i].coords[2]);
	}
	fclose(fxyz);
	return true;
}

bool VolumeTetrMesh::out_mesh_with_matr_nums(STR TRG, STR XYZ)
{
	FILE *ftrg, *fxyz;
	// выводим индексные массивы (подмассивы) элементов
	ftrg = fopen(TRG, "w");
	if (ftrg == 0)
		return false;
	fprintf(ftrg, "%d\n", arsize*4);
	for (uint i = 0; i<arsize; i++)
	{
		fprintf(ftrg, "%d\t%d\t%d\t%d\t%d\n",
			area[i].p[0],
			area[i].p[1],
			area[i].p[2],
                        area[i].p[3],
                        area[i].material());
	}
	fclose(ftrg);
	// выводим точки
	fxyz = fopen(XYZ, "w");
	if (fxyz == 0)
		return false;
	fprintf(fxyz, "%d\n", psize);
	for (uint i = 0; i<psize; i++)
	{
		fprintf(fxyz, "%f %f %f\n",
			points[i].coords[0],
			points[i].coords[1],
			points[i].coords[2]);
	}
	fclose(fxyz);
	return true;
}

bool VolumeTetrMesh::out_mesh(STR TRG, STR XYZ, uint material)
{
	FILE *ftrg, *fxyz;
	// выводим индексные массивы (подмассивы) элементов
	ftrg = fopen(TRG, "w");
	if (ftrg == 0)
		return false;
	uint mtr_size = 0;
	for (uint i = 0; i < arsize; ++i)
		if (area[i].material() == material)
			++mtr_size;
	//
	fprintf(ftrg, "%d\n", mtr_size * 4);
	for (uint i = 0; i<arsize; i++)
	{
		if (area[i].material() == material)
		{
			fprintf(ftrg, "%d %d %d\n",
				area[i].p[0],
				area[i].p[1],
				area[i].p[2]);
			fprintf(ftrg, "%d %d %d\n",
				area[i].p[1],
				area[i].p[2],
				area[i].p[3]);
			fprintf(ftrg, "%d %d %d\n",
				area[i].p[0],
				area[i].p[2],
				area[i].p[3]);
			fprintf(ftrg, "%d %d %d\n",
				area[i].p[0],
				area[i].p[1],
				area[i].p[3]);
		}
	}
	fclose(ftrg);
	// выводим точки
	fxyz = fopen(XYZ, "w");
	if (fxyz == 0)
		return false;
	fprintf(fxyz, "%d\n", psize);
	for (uint i = 0; i<psize; i++)
	{
		fprintf(fxyz, "%f %f %f\n",
			points[i].coords[0],
			points[i].coords[1],
			points[i].coords[2]);
	}
	fclose(fxyz);
	return true;
}
