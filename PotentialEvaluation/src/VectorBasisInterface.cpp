#include "VectorBasisInterface.hpp"

VectorXD<double,3> basisGradient(ScalarBasisFunction fbasis, const VectorXD<double,3> & point, double step)
{
    VectorXD<double,3> gradient;
    for (int diffCoordIndex = 0; diffCoordIndex < 3; ++diffCoordIndex)
    {
        VectorXD<double,3> xyzStepForth = point;
        xyzStepForth.coords[diffCoordIndex] += step;
        VectorXD<double,3> xyzStepBack = point;
        xyzStepBack.coords[diffCoordIndex] -= step;
        double xyzGradRow = (fbasis(xyzStepForth) - fbasis(xyzStepBack)) / (2.0 * step);
        
        gradient.coords[diffCoordIndex] = xyzGradRow;
    }
    return gradient;
}

Block<double,3> basisGradient(BasisFunction fbasis, const VectorXD<double,3> & point, double step)
{
    Block<double,3> gradientRows;
    for (int diffCoordIndex = 0; diffCoordIndex < 3; ++diffCoordIndex)
    {
        VectorXD<double,3> xyzStepForth = point;
        xyzStepForth.coords[diffCoordIndex] += step;
        VectorXD<double,3> xyzStepBack = point;
        xyzStepBack.coords[diffCoordIndex] -= step;
        VectorXD<double,3> xyzGradRow = (fbasis(xyzStepForth) - fbasis(xyzStepBack)) / (2.0 * step);
        
        for (int gradIndex = 0; gradIndex < 3; ++gradIndex)
            gradientRows.block[gradIndex][diffCoordIndex] = xyzGradRow.coords[gradIndex];
    }
    return gradientRows;
}

void vectCache_rule::set_size(GaussRule &gr)
{
	if((int)gr < 0)
		throw std::invalid_argument("Invalid gauss rule for resizing!\n");
	if( cache.size() != ((uint)gr) )
		cache.resize((uint)gr);
	next_value = 0;
	use_cache = false;
}

VectorBasisInterface::~VectorBasisInterface()
{}

// В глобальной системе координат
VectorXD<double,3> VectorBasisInterface::get_Global_point(VectorXD<double,2> Gauss_point)
{
	VectorXD<double,3> ret;
	ret = current_parameter.x0 +
		(current_parameter.x1 - current_parameter.x0) * Gauss_point.coords[0] +
		(current_parameter.x2 - current_parameter.x0) * Gauss_point.coords[1];
	return ret;
}
// В ЛСК для данного треугольника данной базисной функции
VectorXD<double,3> VectorBasisInterface::get_Local_point(VectorXD<double,2> Gauss_point)
{
	VectorXD<double,3> ret;
	ret = get_Global_point( Gauss_point );
	return current_parameter.get_local_coordinate_point( ret );
}

// Данные функции экстраполируют готовые функции на элементе "за" элемент
// Реализовано только для базисных фукнций, но не для их дивергенции и градиентов
VectorXD<double,3> VectorBasisInterface::lambda(VectorXD<double,3> Local_arg)
{
//	VectorXD<double,3> proj;
//	Block<double,2> gradient;
//	VectorXD<double,2> GaussPoint;
//	GaussPoint = current_parameter.find_projection_point( Local_arg );
//	proj = get_Local_point( GaussPoint );
//	gradient = gradient_lambda( Local_arg );
//	// вектора, которыми манипулируем, чтобы получить интерполянт
//	VectorXD<double,3> Mult, ValueP, ValueRes;
//	Mult.common_init(0.0);
//	Local_arg = Local_arg - proj;
//	for(uint i=0; i<2; i++)
//		for(uint j=0; j<2; j++)
//			Mult.coords[ i ] += gradient.block[ i ][ j ] * Local_arg.coords[ j ];
//	ValueP = lambda( GaussPoint );
//	ValueRes = ValueP + Mult;
//	InterpValue = ValueRes;
//	return ValueRes;
    	VectorXD<double,2> GaussPoint;
	GaussPoint = current_parameter.find_projection_point( Local_arg );
	VectorXD<double,3> ValueP = lambda( GaussPoint );
        InterpValue = ValueP;
	return ValueP;
}

VectorXD<double,3> VectorBasisInterface::u_tangen(VectorXD<double,3> Local_arg)
{
//	VectorXD<double,3> proj;
//	Block<double,2> gradient;
//	VectorXD<double,2> GaussPoint;
//	GaussPoint = current_parameter.find_projection_point( Local_arg );
//	proj = get_Local_point( GaussPoint );
//	gradient = gradient_u_tang( Local_arg );
//	// вектора, которыми манипулируем, чтобы получить интерполянт
//	VectorXD<double,3> Mult, ValueP, ValueRes;
//	Mult.common_init(0.0);
//	Local_arg = Local_arg - proj;
//	for(uint i=0; i<2; i++)
//		for(uint j=0; j<2; j++)
//			Mult.coords[ i ] += gradient.block[ i ][ j ] * Local_arg.coords[ j ];
//	ValueP = u_tangen( GaussPoint );
//	ValueRes = ValueP + Mult;
//	InterpValue = ValueRes;
//	return ValueRes;
	VectorXD<double,2> GaussPoint;
	GaussPoint = current_parameter.find_projection_point( Local_arg );
	VectorXD<double,3> ValueP = u_tangen( GaussPoint );
        InterpValue = ValueP;
	return ValueP;
}

vectCache_elem &VectorBasisInterface::get_cache_value(VectorXD<double,2> &GaussPoint)
{
	uint i = 0;
	if(Cache_values.cache.size() == 0)
		throw std::runtime_error("Error! Cache does not initiated!");
	i = Cache_values.next_value;
	Cache_values.next_value++; Cache_values.next_value %= Cache_values.cache.size();
	if(Cache_values.use_cache == true)
		return Cache_values.cache[ i ];
	if(Cache_values.next_value == 0)
		Cache_values.use_cache = true;
	// вычисляем все необходимые значения для кэш
	Cache_values.cache[ i ].loc_arg = get_Local_point(GaussPoint);
	Cache_values.cache[ i ].glob_arg = get_Global_point(GaussPoint);
	Cache_values.cache[ i ].u_val = u_tangen(GaussPoint);
	Cache_values.cache[ i ].lam_val = lambda(GaussPoint);
	return Cache_values.cache[ i ];
}

VectorBasisAnsamble::~VectorBasisAnsamble()
{
	for(uint i=0; i<basis_functions.size(); i++)
		if(basis_functions[i])
			delete basis_functions[i];
}

void VectorBasisAnsamble::create_copy(vector<VectorBasisInterface*> &copy)
{
    copy.resize(this->basis_functions.size());
    for(uint i=0; i<copy.size(); ++i)
        copy[i] = this->basis_functions[i]->create_copy();
}

