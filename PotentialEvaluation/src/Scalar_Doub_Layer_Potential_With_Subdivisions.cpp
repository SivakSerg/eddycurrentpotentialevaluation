#include <complex>
#include "Single_Layer_Potential.hpp"
#include "Scalar_Doub_Layer_Potential_With_Subdivisions.hpp"

std::complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::EvalfScalarPotWithSubdivisions
    (double epsilon, std::function<double(const VectorXD<double,3> &arg)> basisFunction,
     std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
     const triangle_parameters & localFunctionSupport, const VectorXD<double,3> & yArgument,
     int numberOfSublevels, GaussRule grRude, GaussRule grFine)
{
    auto N = numberOfSublevels;
    auto yGlobal = yArgument;
    
    complex<double> rude_value, refined_value;
    before_sectioning.clear();
    after_sectioning.clear();
    before_sectioning.push_back(localFunctionSupport);
    zero_flag = sqrt(before_sectioning[0].t_k * before_sectioning[0].s_t / 2.) * epsilon / N;
    double discrepancy = 1.0;
    complex<double> saved_values(0.0, 0.0), ret(0.0, 0.0), ret_refined(0.0, 0.0);
    complex<double> fullSum = 0.0;
    int iter = 0;
    triangle_parameters current_parameter;
    for(; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();

            bool singularCondition = current_parameter.Dist(yGlobal) < 2.0 * current_parameter.Diam();
            //bool singularCondition = current_parameter.is_point_inside(yGlobal, NON_SCALED_ZERO);
            if (singularCondition)
            {
                rude_value = EvalfScalarDoubPotPartial(grRude, basisFunction, gradBasisFunction,
                    yGlobal, current_parameter);
                refined_value = EvalfScalarDoubPotPartial(grFine, basisFunction, gradBasisFunction,
                    yGlobal, current_parameter);
            }
            else
            {
                rude_value = ScalarDoubPotEvalfPartial(current_parameter, grRude, basisFunction, yGlobal);
                refined_value = ScalarDoubPotEvalfPartial(current_parameter, grFine, basisFunction, yGlobal);
            }
            complex<double> testRet = saved_values + rude_value;
            complex<double> testRetRefined = saved_values + refined_value;
            ret += rude_value;
            ret_refined += refined_value;
            if (!iter)
            {
                fullSum = testRetRefined;
            }
            double cur_disc = abs(testRetRefined - testRet)/abs(fullSum);
            if(cur_disc >= epsilon && abs(rude_value) >= zero_flag && abs(refined_value) >= zero_flag) 
                // если для данного фрагмента разбиения погрешность слишком высока, то
                // необходимо внести разбиения в этот элемент
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values += refined_value;
        }
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        fullSum = ret_refined;
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();
    return ret_refined;
}

std::complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::EvalfScalarPotWithSubdivisions
    (double epsilon, ScalarBasisInterface *f, VectorXD<double,2> gauss_points,
     triangle_parameters &Ty, int N, GaussRule grRude, GaussRule grFine)
{
    auto yGlobal = Ty.get_Global_point( gauss_points );
    auto yLocal = f->current_parameter.get_local_coordinate_point(yGlobal);

    triangle_parameters current_parameter;
    complex<double> rude_value, refined_value;
    before_sectioning.clear();
    after_sectioning.clear();
    before_sectioning.push_back(f->current_parameter);
    zero_flag = sqrt(before_sectioning[0].t_k * before_sectioning[0].s_t / 2.) * epsilon / N;
    double discrepancy = 1.0;
    complex<double> saved_values(0.0, 0.0), ret(0.0, 0.0), ret_refined(0.0, 0.0);
    complex<double> fullSum = 0.0;
    int iter = 0;
    for(; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();

            bool singularCondition = current_parameter.Dist(yGlobal) < 2.0 * current_parameter.Diam();
            //bool singularCondition = current_parameter.is_point_inside(yGlobal, NON_SCALED_ZERO);
            if (singularCondition)
            {
                rude_value = EvalfScalarDoubPotPartial(grRude, f,
                    gauss_points, Ty, current_parameter);
                refined_value = EvalfScalarDoubPotPartial(grFine, f,
                    gauss_points, Ty, current_parameter);
            }
            else
            {
                rude_value = ScalarDoubPotEvalfPartial(current_parameter, grRude, f, yLocal);
                refined_value = ScalarDoubPotEvalfPartial(current_parameter, grFine, f, yLocal);
            }
            complex<double> testRet = saved_values + rude_value;
            complex<double> testRetRefined = saved_values + refined_value;
            ret += rude_value;
            ret_refined += refined_value;
            if (!iter)
            {
                fullSum = testRetRefined;
            }
            double cur_disc = abs(testRetRefined - testRet)/abs(fullSum);
            if(cur_disc >= epsilon && abs(rude_value) >= zero_flag && abs(refined_value) >= zero_flag)  // если для данного фрагмента разбиения погрешность слишком высока, то необходимо внести разбиения в этот элемент
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values += refined_value;
        }
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        fullSum = ret_refined;
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();
    return ret_refined;
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    ScalarDoubPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, 
    std::function<double(const VectorXD<double,3> &arg)> basisFunction, 
    const VectorXD<double,3> &y)
{
    complex<double> ret, acc;
    complex< double > area, mult;
    VectorXD<double,2> gauss_points;
    fix_params = Tk;
    area = fix_params.s_t * fix_params.t_k * 0.5;
    ret = 0.;
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        // в этих точках вычисляем интегрируемую функцию
	complex<double> res = basisFunction(x);
	VectorXD<double,3> x_y = x - y;
	double x_y_lenght = x_y.norma();
	complex<double> v_h = res * fast_dot_prod(x_y, fix_params.n);
	// далее вычисления ведутся в комплексном пространстве
	complex< double > exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) * v_h;
	complex<double> acc = exp_part * (-1./(x_y_lenght*x_y_lenght*x_y_lenght) - vave_num_k/(x_y_lenght*x_y_lenght)) / (4 * PI);
        // в отличие от area, параметр весов w может менятся на итерациях
        acc = acc * area * tgr.w;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    ScalarDoubPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, ScalarBasisInterface *f, VectorXD<double,3> &y)
{
	complex<double> ret, acc;
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
    fix_params = Tk;
	area = fix_params.s_t * fix_params.t_k * 0.5;
	ret = 0.;
    TrigGaussRuleInfo tgr(gr);
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        x = f->current_parameter.get_local_coordinate_point(x);
        // в этих точках вычисляем интегрируемую функцию
        acc = ScalarDoubFunctPartial(f, y, x) * area;
        // в отличие от area, параметр весов w может менятся на итерациях
        mult = tgr.w; acc = acc * mult;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
	}
	return ret;
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    ScalarDoubFunctPartial(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x)
{
	complex<double> res = f->f_value(x);
	VectorXD<double,3> x_y = x - y;
	double x_y_lenght = x_y.norma();
	complex<double> v_h = res * x_y.coords[2];
	// далее вычисления ведутся в комплексном пространстве
	complex< double > exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) * v_h;
	return exp_part * (-1./(x_y_lenght*x_y_lenght*x_y_lenght) - vave_num_k/(x_y_lenght*x_y_lenght)) / (4 * PI);
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    EvalfScalarDoubPotPartial(GaussRule gr, std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const VectorXD<double,3> & yGlobal, triangle_parameters &TxPartial)
{
    VectorXD<double,3> fglobGrad = gradBasisFunction(yGlobal);
    VectorXD<double,3> txGrad = TxPartial.get_local_coordinate_vector(fglobGrad);
    fix_params = TxPartial;
    VectorXD<double,3> y = TxPartial.get_local_coordinate_point( yGlobal );
    double uy = y.coords[2];
    if (abs(uy)<zero)
        return 0.0;
    txGrad = txGrad * uy;
    double fy = basisFunction(yGlobal);
    complex<double> sum(0.0, 0.0);
    sum += r1_3_component(y) * txGrad.coords[0] + r2_3_component(y) * txGrad.coords[1];
    sum += fy * r3_3v2_component(y);

    sum = sum + NumericPartEvaluterDoubScalarPartial(basisFunction, gradBasisFunction, yGlobal, gr);
    return sum;
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    EvalfScalarDoubPotPartial(GaussRule gr, ScalarBasisInterface *f, VectorXD<double,2> gauss_points,
    triangle_parameters &Ty, triangle_parameters &TxPartial)
{
    complex<double> sum(0.0, 0.0);
    VectorXD<double,3> y;
    fix_params = TxPartial;

    auto yGlobal = Ty.get_Global_point( gauss_points );
    VectorXD<double,3> yFuncLoc = f->current_parameter.get_local_coordinate_point( yGlobal );
    y = TxPartial.get_local_coordinate_point( yGlobal );

    VectorXD<double,2> f_grad = f->f_gradient(yFuncLoc);
    VectorXD<double,3> f_expanded;
    f_expanded.coords[0] = f_grad.coords[0];
    f_expanded.coords[1] = f_grad.coords[1];
    f_expanded.coords[2] = 0.0;
    VectorXD<double,3> txGrad;

    VectorXD<double,3> fglobGrad = f->current_parameter.get_global_coordinate_vector(f_expanded);
    txGrad = TxPartial.get_local_coordinate_vector(fglobGrad);

    double uy = y.coords[2];
    if (abs(uy)<zero)
        return sum;
    txGrad = txGrad * uy;
    double fy = f->f_value(yFuncLoc);
    sum = 0.;
    sum += r1_3_component(y) * txGrad.coords[0] + r2_3_component(y) * txGrad.coords[1];
    sum += fy * r3_3v2_component(y);

    sum = sum + NumericPartEvaluterDoubScalarPartial(f, yFuncLoc, gr);
    return sum;
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    NumericPartEvaluterDoubScalarPartial(std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const VectorXD<double,3> & y, GaussRule gr)
{
    complex<double> ret(0.0, 0.0), acc(0.0, 0.0);
    // параметры решения
    VectorXD<double,2> gauss_points;
    complex< double > area = fix_params.s_t * fix_params.t_k * 0.5;
    double savedFy = basisFunction(y);
    VectorXD<double,3> gradient = gradBasisFunction(y);
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);        
	complex<double> sum(0.0, 0.0);
	VectorXD<double,3> x_y = x - y;
	double x_y_lenght = x_y.norma();
	if (x_y_lenght >= zero)
	{
            auto exp_part = exp(-1. * vave_num_k * x_y_lenght) - 1.;
            double uy = -fast_dot_prod(this->fix_params.n, x_y); // скалярное произведение на нормаль (x - y, n)
            // величина функции и аппроксимация градиентом
            auto fx = basisFunction(x);
            // разрешение особенности по величине экспоненты
            double rsqr = x_y_lenght * x_y_lenght;
            sum += uy * fx * ( exp_part + vave_num_k * x_y_lenght ) / (rsqr * x_y_lenght);
            sum += uy * fx * vave_num_k * exp_part / rsqr;
            double grad_x_y = 0.; // скалярное произведение градиента на разность координат
            for(int i=0; i<3; i++)
                    grad_x_y += gradient.coords[ i ] * x_y.coords[ i ];
            sum += uy * (fx - savedFy - grad_x_y) / pow(x_y_lenght,3);        
        }
        // добавляем новое слагаемое к результату
        ret = ret + sum * area * tgr.w;
    }
    return ret / ( 4 * PI );
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    NumericPartEvaluterDoubScalarPartial(ScalarBasisInterface *f,
    VectorXD<double,3> y, GaussRule gr)
{
    complex<double> ret(0.0, 0.0), acc(0.0, 0.0);
    // параметры решения
    complex< double > area, mult;
    VectorXD<double,2> gauss_points;
    area = fix_params.s_t * fix_params.t_k * 0.5;
    double savedFy = f->f_value( y );
    VectorXD<double,2> gradient = f->f_gradient( y );
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        VectorXD<double,3> x_loc = f->current_parameter.get_local_coordinate_point(x);
        // в этих точках вычисляем интегрируемую функцию
        acc = DoubAdditionalScalarFunction(f, y, x_loc, savedFy, gradient) * area * tgr.w;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

complex<double> Scalar_Doub_Layer_Potential_With_Subdivisions::
    DoubAdditionalScalarFunction(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x, double Fy, VectorXD<double,2> &gradient)
{
	complex<double> sum(0.0, 0.0);
	complex<double> exp_part(0.0, 0.0);
	VectorXD<double,3> x_y;

	double fx, fy, x_y_lenght, uy;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero)
	{
		sum = 0.;
		return sum;
	}
	exp_part = exp(-1. * vave_num_k * x_y_lenght) - 1.;
	uy = -x_y.coords[2]; // скалярное произведение на нормаль (x - y, n)
	// величина функции и аппроксимация градиентом
	fx = f->f_value(x);
	// разрешение особенности по величине экспоненты
	double rsqr = x_y_lenght * x_y_lenght;
	sum += uy * fx * ( exp_part + vave_num_k * x_y_lenght ) / (rsqr * x_y_lenght);
	sum += uy * fx * vave_num_k * exp_part / rsqr;
	double grad_x_y = 0.; // скалярное произведение градиента на разность координат
	for(int i=0; i<2; i++)
		grad_x_y += gradient.coords[ i ] * x_y.coords[ i ];
	sum += uy * (fx - Fy - grad_x_y) / pow(x_y_lenght,3);
	return sum / ( 4 * PI ); //*/
}
