#include "UVdecomposer.h"
#include "SingleLayerSurfacePotential.h"
#include "DinamicXD.hpp"
#include "ComplexSlvLAPACK.hpp"
#include "GaussRuleInfo.h"
#include <iostream>
#include <fstream>

/*
 * Class do decompose vector and scalar basis functions to obtain UV - coordinates
 */

UVdecomposer::UVdecomposer()
{
    //ctor
}

UVdecomposer::~UVdecomposer()
{
    //dtor
}

bool UVdecomposer::createDecomposeList(const SingleLayerSurfacePotential &trigInfo,
                                       std::function<double(const VectorXD<double,3> &arg)> basisFunction,
                                       GaussRule gr,
                                       int Power,
                                       std::vector<double> &decomposingCoefs)
{

    auto getVector = [&](VectorXD<double,2> &Gauss_point)->VectorXD<double,3>
    {
        auto & x0 = trigInfo.p[0];
        auto & x1 = trigInfo.p[1];
        auto & x2 = trigInfo.p[2];
    	VectorXD<double,3> ret;
        ret = /*x0 +*/
             (x1 - x0) * Gauss_point.coords[0] +
             (x2 - x0) * Gauss_point.coords[1];
        return ret;
    };
    if(Power < 0)
        return false;
    TrigGaussRuleInfo tgri(gr);
    VectorXD<double,2> gauss_points;
    std::vector< std::vector<double> > basisValues;
    std::vector<double> functionValues;
    std::vector<double> weights;
    std::size_t N = Power;
    while(tgri.next())
    {
        gauss_points.coords[ 0 ] = tgri.e.e1;
        gauss_points.coords[ 1 ] = tgri.e.e2;
        VectorXD<double,3> globalVectArg = getVector(gauss_points);
        VectorXD<double,3> globalPointArg = globalVectArg + trigInfo.p[0];
        // вычисляем базисные функции и записываем их в вектор с тем, чтобы перемножить
        double uCoord = globalVectArg.dotprod(trigInfo.u), vCoord = globalVectArg.dotprod(trigInfo.v);
        std::size_t functionStride = 0;
        for(std::size_t currentPower = 0; currentPower <= N; ++currentPower)
        {
            std::size_t nextStride = 0;
            for(std::size_t polynomNumber=0; polynomNumber <= currentPower; ++polynomNumber)
            {
                std::size_t i = polynomNumber;
                std::size_t j = currentPower  - polynomNumber;
                if(polynomNumber+functionStride >= basisValues.size())
                {
                    basisValues.push_back(std::vector<double>());
                }
                if(0 == currentPower)
                {
                    basisValues[polynomNumber+functionStride].push_back(1.0);
                }
                else
                {
                    basisValues[polynomNumber+functionStride].push_back( pow(uCoord, i) * pow(vCoord, j) );
                }
                ++nextStride;
            }
            functionStride += nextStride;
        }
        functionValues.push_back(basisFunction(globalPointArg));
        weights.push_back(tgri.w);
    }
    // теперь умножаем значения функций на вектор весов и площадь элемента, чтобы получить интеграл по треугольнику от скалярного произведение
    // базисных полиномов от координат u и v.
    // аналогично поступаем со значениями функции в правой части
    DinamicBlock<double> decomposeMatrix(basisValues.size(), false);
    DinamicBlock<double>::set_zero(0.0);
    std::vector<double> rightVector(basisValues.size());
    for(uint i=0; i<basisValues.size(); ++i)
    {
        for(uint j=0; j<basisValues.size(); ++j)
        {
            double accumulator = 0.0;
            for(uint k=0; k<basisValues[i].size(); ++k)
            {
                double val1 = basisValues[i][k], val2 = basisValues[j][k], w = weights[k], area = trigInfo.area;
                accumulator += val1 * val2 * w * area;
            }
            decomposeMatrix.block(i,j) = accumulator;
        }
        double accumulator = 0.0;
        for(uint k=0; k<functionValues.size(); ++k)
        {
            accumulator += functionValues[k] * basisValues[i][k] * weights[k] * trigInfo.area;
        }
        rightVector[i] = accumulator;
    }

    if (useLogginging)
    {
        for (unsigned int i=0; i<decomposeMatrix.get_size(); ++i)
        {
            for (unsigned int j=0; j<decomposeMatrix.get_size(); ++j)
                std::cout << decomposeMatrix.block(i,j) << "\t";

            std::cout << "r[" << i << "]" << "\t" << rightVector[i] << "\n";
        }

        std::ofstream decompositionTest("decompositionTest.txt");
        for (unsigned int i=0; i<decomposeMatrix.get_size(); ++i)
        {
            for (unsigned int j=0; j<decomposeMatrix.get_size(); ++j)
                decompositionTest << decomposeMatrix.block(i,j) << "\t";

            decompositionTest << "r[" << i << "]" << "\t" << rightVector[i] << "\n";
        }
    }

    // коэффициенты разложения находятся из решения системы линейных уравнений
    DGESV(decomposeMatrix,rightVector.data());
    decomposingCoefs.resize(rightVector.size());
    for(uint i=0; i<rightVector.size(); ++i)
        decomposingCoefs[i] = rightVector[i];
    return true;
}

double UVdecomposer::getDecomposedValue(int Power,
                          const std::vector<double> &decompositionCoefs,
                          const SingleLayerSurfacePotential &trigInfo,
                          const VectorXD<double,3> &arg,
                          int &ErrCode)
{
    ErrCode = 0.0;
    double result = 0.0;

    if (Power < 0)
    {
        ErrCode = -2;
        throw std::runtime_error("Wrong degree of power for the basis functions");
    }

    std::size_t coefsNumber = (Power + 1) * (Power + 2) / 2;
    if (coefsNumber != decompositionCoefs.size())
    {
        ErrCode = -1;
        throw std::runtime_error("Failed to obtain value: wrong number of coefficients");
    }
    VectorXD<double,3> globalVectArg = arg - trigInfo.p[0];
    // вычисляем базисные функции и записываем их в вектор с тем, чтобы перемножить
    double uCoord = globalVectArg.dotprod(trigInfo.u), vCoord = globalVectArg.dotprod(trigInfo.v);

    int nextCoef = 0;
    for(std::size_t currentPower = 0; currentPower <= Power; ++currentPower)
    {
        for(std::size_t polynomNumber=0; polynomNumber <= currentPower; ++polynomNumber)
        {
            std::size_t i = polynomNumber;
            std::size_t j = currentPower  - polynomNumber;

            if(0 == currentPower)
            {
                result += decompositionCoefs[nextCoef];
            }
            else
            {
                result += pow(uCoord, i) * pow(vCoord, j) * decompositionCoefs[nextCoef];
            }
            ++nextCoef;
        }
    }
    return result;
}
