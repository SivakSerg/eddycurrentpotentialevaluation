/****************************************************************************

	Finite Element Analysis  /core/
	Copyright (C) Siberian CAD Soft, 2002, 2009

	File:       GaussRuleInfo.cpp
	Author:		Pavel Anufrikov <apaex@ngs.ru>

	Contents:	

*****************************************************************************/
/*
Модификация от 17.02.2013 
Для многократного использования одного класса каждый раз зануляем
счетчит локальных точек интегрирования n, чтобы при новом цикле
возвращаться к исходной точке.
*/
#include "GaussRuleInfo.h"
#include <math.h>

#ifndef _countof
#define _countof(_Array) (sizeof(_Array) / sizeof(_Array[0]))
#endif

bool TetrGaussRuleInfo::next()
{
	++n;
	switch(m_rule) 
	{
		// S4
	case 1:
		switch (n)
		{	
		case 1:
			a = double(1./4.);
			w = double(1.);			
			e.e1=a; e.e2=a; e.e3=a; e.e4=a;
			break;
		default: n = 0;  
			return false;
		}
		break;

		// S31
	case 4:
		switch (n)
		{	
		case 1: //a = (5.+3.*sqrt(5.))/20.;
			//b = (5.-sqrt(5.))/20.;
			a = double(0.585410196624968454);
			b = double(0.138196601125010515);
			w = double(1./4.);			
			e.e1=a; e.e2=b; e.e3=b; e.e4=b;
			break;
		case 2: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 4: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;
		default: n = 0;   return false;
		}
		break;

		// 2S31
	case 8:
		switch (n) 
		{
		case 1: //b = (55.-3.*sqrt(17.)+sqrt(1022.-134.*sqrt(17.)))/196.;
			//a = 1.-3.*b;
			//w = 1./8.+sqrt((1715161837.-406006699.*sqrt(17.))/23101.)/3120.;
			b = double(0.328054696711426647);
			a = double(0.015835909865720058);
			w = double(0.138527966511862142); 
			e.e1=a; e.e2=b; e.e3=b; e.e4=b;
			break;
		case 2: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 4: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;
		case 5: //b = (55.-3.*sqrt(17.)-sqrt(1022.-134.*sqrt(17.)))/196.;
			//a = 1.-3.*b;
			//w = 1./8.-sqrt((1715161837.-406006699.*sqrt(17.))/23101.)/3120.;
			b = double(0.106952273932930683);
			a = double(0.679143178201207952);
			w = double(0.111472033488137858);
			e.e1=a; e.e2=b; e.e3=b; e.e4=b;
			break;
		case 6: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 7: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 8: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;
		default: n = 0;   return false;
		}
		break;

		// 2S31
	case -8:
		switch (n) 
		{
		case 1: a = double(1.);	
			b = double(0.);	
			w = double(1./40.);
			e.e1=a; e.e2=b; e.e3=b; e.e4=b;
			break;
		case 2: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 4: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;
		case 5:	a = double(0.);	
			b = double(1./3.);	
			w = double(9./40.);
			e.e1=a; e.e2=b; e.e3=b; e.e4=b; break;
		case 6: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 7: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 8: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;
		default: n = 0;   return false;
		}
		break;

	case 14:
		// 2S31+S22  order 4
		// not realised
	case -14:
		switch (n) 
		{
		case 1: a = double(0.056881379520423);
				b = double(0.314372873493192); 
				w = double(0.132838746685591);
				e.e1=a; e.e2=b; e.e3=b; e.e4=b; break;
		case 2: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 4: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;
		case 5: a = double(0.698419704324387);
				b = double(0.100526765225204);
				w = double(0.088589824742981);
				e.e1=a; e.e2=b; e.e3=b; e.e4=b; break;
		case 6: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 7: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 8: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;
		case 9: a = double(0.5);
				b = double(0);
				w = double(2./105.);
				e.e1=a; e.e2=a; e.e3=b; e.e4=b; break;
		case 10: e.e1=a; e.e2=b; e.e3=a; e.e4=b; break;
		case 11: e.e1=a; e.e2=b; e.e3=b; e.e4=a; break;
		case 12: e.e1=b; e.e2=a; e.e3=a; e.e4=b; break;
		case 13: e.e1=b; e.e2=a; e.e3=b; e.e4=a; break;
		case 14: e.e1=b; e.e2=b; e.e3=a; e.e4=a; break;
		default: n = 0;   return false;
		}
		break;

		// S4+2S31+S22	// order 5
	case 15:
		switch (n) 
		{
		case 1: b = (double(7.) - sqrt(double(15.))) / double(34.); 
				a = double(1.) - double(3.)*b;
				w = (double(2665.) + double(14.)*sqrt(double(15.)))/double(37800.);
				e.e1=a; e.e2=b; e.e3=b; e.e4=b; break;
		case 2: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 4: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;

		case 5: b = double(7./17.)-((double(7.) - sqrt(double(15.)))/double(34.)); 
				a = double(1.) - double(3.)*b;
				w = (double(2665.) - double(14.)*sqrt(double(15.)))/double(37800.);
				e.e1=a; e.e2=b; e.e3=b; e.e4=b; break;
		case 6: e.e1=b; e.e2=a; e.e3=b; e.e4=b; break;
		case 7: e.e1=b; e.e2=b; e.e3=a; e.e4=b; break;
		case 8: e.e1=b; e.e2=b; e.e3=b; e.e4=a; break;

		case 9: b = (double(10.) - double(2.)*sqrt(double(15.)))/double(40.); 
				a = double(1./2.) - b;
				w = double(10./189.);
				e.e1=a; e.e2=a; e.e3=b; e.e4=b; break;
		case 10: e.e1=a; e.e2=b; e.e3=a; e.e4=b; break;
		case 11: e.e1=a; e.e2=b; e.e3=b; e.e4=a; break;
		case 12: e.e1=b; e.e2=a; e.e3=a; e.e4=b; break;
		case 13: e.e1=b; e.e2=a; e.e3=b; e.e4=a; break;
		case 14: e.e1=b; e.e2=b; e.e3=a; e.e4=a; break;

		case 15: a = double(1./4.);
				w = double(16./135.);
				e.e1= e.e2= e.e3= e.e4= a; break;
		default: n = 0;   return false;
		}
		break;

	default: n = 0;  
		assert(0);
		return false;
	}
	return true; 
}

//////////////////////////////////////////////////////////////////////////


GaussRule TrigGaussRuleInfo::getGaussRuleForDegree(int degree)
{
	//				    	  Degree: 0 1 2 3 4 5 6
	static const GaussRule rules[] = {0,1,3,6,-6,7,12,21,66};
	assert(degree < _countof(rules));
	return rules[degree];
}


bool TrigGaussRuleInfo::next()
{
	++n;
	switch(m_rule) 
	{
	case 0:
		switch (n) {
		case 1: a = double(1.);
			b = double(0.);
			w = double(1./3.);
			e.e1=a; e.e2=b; e.e3=b; break;
		case 2: e.e1=b; e.e2=a; e.e3=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; break;
		default: n = 0;   return false;		
		}
		break;


		// S3
	case 1:
		switch (n) {		
		case 1: a = double(1./3.); 
			w = double(1.);
			e.e1=a; e.e2=a; e.e3=a; break;
		default: n = 0;   return false;		
		}
		break;

		// S21
	case 3:
		switch (n) {
		case 1: a = double(2./3.);
			b = double(1./6.);
			w = double(1./3.);
			e.e1=a; e.e2=b; e.e3=b; break;
		case 2: e.e1=b; e.e2=a; e.e3=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; break;
		default: n = 0;   return false;		
		}
		break;

		// S21
	case -3:
		switch (n) {
	case 1: a = double(0.0);
		b = double(1./2.);
		w = double(1./3.);
		e.e1=a; e.e2=b; e.e3=b; break;
	case 2: e.e1=b; e.e2=a; e.e3=b; break;
	case 3: e.e1=b; e.e2=b; e.e3=a; break;
	default: n = 0;   return false;		
		}
		break;

		// 2S21
	case 6:
		switch (n) 
		{		
		case 1: //b = (8.-sqrt(10.)+sqrt(38.-44.*sqrt(2./5.)))/18.; 
			//a = 1.-2.*b; 
			//w = (620.+sqrt(213125.-53320.*sqrt(10.)))/3720.;
			b = double(0.445948490915964886); 
			a = double(0.108103018168070227); 
			w = double(0.223381589678011466);
			e.e1=a; e.e2=b; e.e3=b; break;
		case 2: e.e1=b; e.e2=a; e.e3=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; break;

		case 4: //b = (8.-sqrt(10.)-sqrt(38.-44.*sqrt(2./5.)))/18.; 
			//a = 1.-2.*b; 
			//w = (620.-sqrt(213125.-53320.*sqrt(10.)))/3720.;
			b = double(0.091576213509770744); 
			a = double(0.816847572980458513); 
			w = double(0.109951743655321868); 
			e.e1=a; e.e2=b; e.e3=b; break;
		case 5: e.e1=b; e.e2=a; e.e3=b; break;
		case 6: e.e1=b; e.e2=b; e.e3=a; break;
		default: n = 0;   return false;		
		}
		break;

		// 2S21
	case -6:
		switch (n) 
		{		
		case 1: b = double(1./6.); 
			a = double(2./3.); 
			w = double(3./10.);
			e.e1=a; e.e2=b; e.e3=b; break;
		case 2: e.e1=b; e.e2=a; e.e3=b; break;
		case 3: e.e1=b; e.e2=b; e.e3=a; break;

		case 4: b = double(1./2.); 
			a = double(0.); 
			w = double(1./30.);
			e.e1=a; e.e2=b; e.e3=b; break;
		case 5: e.e1=b; e.e2=a; e.e3=b; break;
		case 6: e.e1=b; e.e2=b; e.e3=a; break;
		default: n = 0;   return false;		
		}
		break;

	case 7:
		break;

		// 2S21+S111
	case 12:
		{
			static const double g1= double(0.063089014491502228340331602870819157);
			static const double g2= double(0.249286745170910421291638553107019076);
			static const double g3= double(0.053145049844816947353249671631398147);
			static const double g4= double(0.310352451033784405416607733956552153);

			static const double w1=(30*g2*g2*g2*(4*g3*g3+(1-2*g4)*(1-2*g4)+4*g3*(-1+g4))+
				g3*g3*(1-15*g4)+(-1+g4)*g4-g3*(-1+g4)*(-1+15*g4)+ 
				2*g2*(1+60*g3*g4*(-1+g3+g4))-6*g2*g2*(3+10*(-1+g4)*g4+ 
				10*g3*g3*(1+3*g4)+10*g3*(-1+g4)*(1+3*g4)))/
				(180*(g1-g2)*(-(g2*(-1+2*g2)*(-1+g3)*g3)+(-1+g3)*(g2-2*g2*g2-
				2*g3+3*g2*g3)*g4-(g2*(-1+2*g2-3*g3)+2*g3)*g4*g4+ 
				2*g1*g1*(g2*(-2+3*g2)+g3-g3*g3+g4-g3*g4-g4*g4)+ 
				g1*(-4*g2*g2+(-1+g3)*g3+(-1+g3)*(1+3*g3)*g4+(1+3*g3)*
				g4*g4-2*g2*(-1+g3*g3+g3*(-1+g4)+(-1+g4)*g4)))); 

			static const double w2=(-1+12*(2-3*g1)*g1*w1+4*g3*g3*(-1+3*w1)+4*g3*(-1+g4)*(-1+3*w1)+
				4*(-1+g4)*g4*(-1+3*w1))/(12*(g2*(-2+3*g2)+g3-g3*g3+g4-g3*g4-g4*g4)); 

			static const double w3=(1-3*w1-3*w2)/6;

			switch (n) 
			{		
				case 1: 
					b = g1; 
					a = 1-2*g1; 
					w = w1;
					e.e1=a; e.e2=b; e.e3=b; break;
				case 2: e.e1=b; e.e2=a; e.e3=b; break;
				case 3: e.e1=b; e.e2=b; e.e3=a; break;

				case 4: 
					b = g2; 
					a = 1-2*g2; 
					w = w2;
					e.e1=a; e.e2=b; e.e3=b; break;
				case 5: e.e1=b; e.e2=a; e.e3=b; break;
				case 6: e.e1=b; e.e2=b; e.e3=a; break;

				case  7: w = w3;
					 	 e.e1=g3; e.e2=g4; e.e3=1-g3-g4; break;
				case  8: e.e1=g4; e.e2=g3; e.e3=1-g3-g4; break;
				case  9: e.e1=g3; e.e2=1-g3-g4; e.e3=g4; break;
				case 10: e.e1=1-g3-g4; e.e2=g3; e.e3=g4; break;
				case 11: e.e1=g4; e.e2=1-g3-g4; e.e3=g3; break;
				case 12: e.e1=1-g3-g4; e.e2=g4; e.e3=g3; break;				
				default: n = 0;   return false;		
			}
			break;

		}
	case 21:
	{
		static double p1[] = {0.0451890097844, 0.0451890097844, 0.9096219804312, 0.7475124727339, 0.2220631655373, 0.7475124727339,
			0.2220631655373, 0.0304243617288, 0.0304243617288, 0.1369912012649, 0.6447187277637, 0.1369912012649, 0.2182900709714,
			0.2182900709714, 0.6447187277637, 0.0369603304334, 0.4815198347833, 0.4815198347833, 0.4036039798179, 0.4036039798179,
			0.1927920403641};
		static double p2[] = {0.0451890097844, 0.9096219804312, 0.0451890097844, 0.0304243617288, 0.0304243617288, 0.2220631655373,
			0.7475124727339, 0.7475124727339, 0.2220631655373, 0.2182900709714, 0.2182900709714, 0.6447187277637, 0.6447187277637,
			0.1369912012649, 0.1369912012649, 0.4815198347833, 0.0369603304334, 0.4815198347833, 0.1927920403641, 0.4036039798179,
			0.4036039798179};
		static double W[] = {0.0519871420646, 0.0519871420646, 0.0519871420646, 0.0707034101784, 0.0707034101784, 0.0707034101784,
			0.0707034101784, 0.0707034101784, 0.0707034101784, 0.0909390760952, 0.0909390760952, 0.0909390760952, 0.0909390760952,
			0.0909390760952, 0.0909390760952, 0.1032344051380, 0.1032344051380, 0.1032344051380, 0.1881601469167, 0.1881601469167,
			0.1881601469167};
		if( n != 22)
		{
			e.e1 = p1[n-1]; e.e2 = p2[n-1]; e.e3 = 1 - e.e1 - e.e2; w = 0.5* W[n-1];
		}
		else
		{
			n = 0;
			return false;
		}
		break;
	}
	case 66:	
	{
		static double P1[] = {0.0116731059668, 0.9810030858388, 0.0106966317092, 0.9382476983551, 0.0126627518417,
			0.0598109409984, 0.0137363297927, 0.9229527959405, 0.0633107354993, 0.0117265100335,
			0.1554720587323, 0.8343293888982, 0.8501638031957, 0.0128816350522, 0.1510801608959,
			0.0101917879217, 0.2813372399303, 0.7124374628501, 0.2763025250863, 0.0109658368561,
			0.4289110517884, 0.4215420555115, 0.5711258590444, 0.5826868270511,	0.0130567806713,
			0.0130760400964, 0.7263437062407, 0.0687230068637, 0.8652302101529, 0.0648599071037,
			0.1483494943362, 0.0624359898396, 0.7871369011735, 0.0519104921610, 0.1543129927444,
			0.2617842745603, 0.7667257872813, 0.2582103676627, 0.0679065925147, 0.5293578274804,
			0.0666036150484, 0.0585675461899, 0.0644535360411, 0.6748138429151, 0.3914602310369,
			0.6487701492307, 0.3946498220408, 0.5390137151933, 0.1627895082785, 0.6812436322641,
			0.1542832878020, 0.2522727750445, 0.2547981532407, 0.1485580549194, 0.2930239606436,
			0.2808991272310, 0.4820989592971, 0.5641878245444, 0.1307699644344, 0.1479692221948,
			0.5638684222946, 0.4361157428790, 0.3603263935285, 0.4224188334674, 0.3719001833052,
			0.2413645006928};
		static double P2[] = {0.9812565951289, 0.0071462504863, 0.0115153933376, 0.0495570591341, 0.9370123620615,
			0.0121364578922, 0.0612783625597, 0.0141128270602, 0.9220197291727, 0.1500520475229,
			0.8325147121589, 0.0125228158759, 0.1371997508736, 0.8477627063479, 0.0136526924039,
			0.5770438618345, 0.7066853759623, 0.0124569780990, 0.0121741311386, 0.4194306712466,
			0.5599616067469, 0.0116475994785, 0.0118218313989, 0.4057889581177, 0.2725023750868,
			0.7224712523233, 0.2602984019251, 0.0631417277210, 0.0720611837338, 0.8590433543910,
			0.7888788352240, 0.1493935499354, 0.0656382042757, 0.5255635695605, 0.0716383926917,
			0.0621479485288, 0.1658211554831, 0.6800119766139, 0.7571515437782, 0.4121503841107,
			0.2612513087886, 0.3902236114535, 0.6373626559761, 0.0637583342061, 0.5503238090563,
			0.2836728360263, 0.0605175522554, 0.0611990176936, 0.6861322141035, 0.1567968345899,
			0.1667512624020, 0.2504803933395, 0.4994090649043, 0.5756023096087, 0.5656897354162,
			0.1437921574248, 0.2518557535865, 0.1462966743153, 0.4489577586117, 0.3001174386829,
			0.2813772089298, 0.4252053446420, 0.2599190004889, 0.1453238443303, 0.3780122703567,
			0.3847563284940};
		static double Weight[] = {0.0025165756986, 0.0025273452007, 0.0033269295333, 0.0081503492125, 0.0086135525742,
			0.0087786746179, 0.0097099585562, 0.0102466211915, 0.0108397688341, 0.0129385390176,
			0.0136339823583, 0.0138477328147, 0.0139421540105, 0.0144121399968, 0.0153703455534,
			0.0162489802253, 0.0169718304280, 0.0170088532421, 0.0170953520675, 0.0173888854559,
			0.0174543962439, 0.0178406757287, 0.0178446863879, 0.0179046337552, 0.0181259756201,
			0.0184784838882, 0.0185793564371, 0.0203217151777, 0.0213771661809, 0.0231916854098,
			0.0274426710859, 0.0290301922340, 0.0294522738505, 0.0299436251629, 0.0307026948119,
			0.0325263365863, 0.0327884208506, 0.0331234675192, 0.0346167526875, 0.0347081373976,
			0.0347372049404, 0.0348528762454, 0.0348601561186, 0.0355471569975, 0.0360182996383,
			0.0362926285843, 0.0381897702083, 0.0392252800118, 0.0482710125888, 0.0489912121566,
			0.0497220833872, 0.0507065736986, 0.0509771994043, 0.0521360063667, 0.0523460874925,
			0.0524440683552, 0.0527459644823, 0.0529449063728, 0.0542395594501, 0.0543470203419,
			0.0547100548639, 0.0557288345913, 0.0577734264233, 0.0585393781623, 0.0609039250680,
			0.0637273964449};
		if( n != 67)
		{
			e.e1 = P1[n-1]; e.e2 = P2[n-1]; e.e3 = 1 - e.e1 - e.e2; w = 0.5 * Weight[n-1];
		}
		else
		{
			n = 0;
			return false;
		}
		break;
	}
	default: n = 0;  
		assert(0);
		return false;
	}
	return true;
}

//////////////////////////////////////////////////////////////////////////


GaussRule LineGaussRuleInfo::getGaussRuleForDegree(int degree)
{
	//				    	  Degree: 0 1 2 3 4 5 6
	static const GaussRule rules[] = {0,1,2,2,3,3,7};
	assert(degree < _countof(rules));
	return rules[degree];
}


bool LineGaussRuleInfo::next()
{
	++n;
	w += w;

	switch(m_rule) 
	{
		// S2
	case 1:
		switch (n) {		
		case 1: a = 0.; 
				w = 2.;
				e.e1=a; break;
		default: n = 0;   return false;		
		}
		break;

		// S11
	case 2:
		switch (n) {
		case 1:	w = double(1.);
				e.e1=double(-1.)/sqrt(double(3.)); break;
		case 2: e.e1=double(1.)/sqrt(double(3.)); break;
		default: n = 0;   return false;		
		}
		break;
		
		// S2+S11
	case 3:
		switch (n) {
		case 1: w = double(5./9.);
				e.e1=-sqrt(double(3./5.)); break;
		case 2: w = double(8./9.);
				e.e1=0; break;
		case 3: w = double(5./9.);
				e.e1=sqrt(double(3./5.)); break;
		default: n = 0;   return false;		
		}
		break;

	default: n = 0;  
		assert(0);
		return false;
	}

	e.e1 = ((e.e1+double(1.)) * double(0.5));
	e.e2 = double(1.) - e.e1;
	w *= double(0.5);

	return true;
}
