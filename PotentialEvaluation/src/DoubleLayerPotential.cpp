#include "DoubleLayerPotential.h"
#include "UVdecomposer.h"

// There is no recursion in this function, we have to use inherited funciton "get_surface_integral"
double DoubleLayerPotential::get_surface_grad_integral_u(int n, uint q, uint p)
{
    double ret = 0.;
    double edge_integral = 0.;
    for(int edge = 0; edge < 3; ++edge)
    {
        edge_integral += this->u.dotprod( this->m[edge] ) * get_edge_integral(n+2, edge, q-1, p) / (n + 2);
    }
    double surface_integral = 0.;
    if( q != 1)
        surface_integral -= (q - 1) * get_surface_integral(n+2, q-2, p) / (n + 2);
    ret += surface_integral + edge_integral;
    return ret;
}

double DoubleLayerPotential::get_surface_grad_integral_v(int n, uint q, uint p)
{
    double ret = 0.;
    double edge_integral = 0.;
    for(int edge = 0; edge < 3; ++edge)
    {
        edge_integral += this->v.dotprod( this->m[edge] ) * get_edge_integral(n+2, edge, q, p-1) / (n + 2);
    }
    double surface_integral = 0.;
    if( p != 1)
        surface_integral -= (p - 1) * get_surface_integral(n+2, q, p-2) / (n + 2);
    ret += surface_integral + edge_integral;
    return ret;
}

std::complex<double> DoubleLayerPotential::getScalarDoubleLayerHelmholtzPotential(std::complex<double> k, double epsilon,
GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r, int basisOrder, std::function<double(const VectorXD<double,3> &arg)> basisFunction)
{
    std::vector<double> decCoefs;
    int n = getTaylorEstimation(k, epsilon, 1);
    if (n < 0)
    {
        throw std::runtime_error("invalid tolerance passed to this function!");
        return 0.0;
    }
    if (0 == n)
    {
        n = 10;
    }
    UVdecomposer decomposer;
    create_parameters(r, p);

    if (fabs(w_0) < zero)
    {
        return 0.0;
    }

    decomposer.createDecomposeList(*this, basisFunction, gr, basisOrder, decCoefs);
    std::complex<double> potentialValue = 0.0;
    std::complex<double> powFactorialCoef = 1.0;
    for(int p = -3; p < n; p += 2)
    {
        int nextCoef = 0;
        for(int powerDeg=0; powerDeg <= basisOrder; ++powerDeg)
        {
            for(int i=0; i<=powerDeg; ++i)
            {
                int j = powerDeg - i;
                potentialValue += (p + 2) * w_0 * get_surface_integral(p, i, j) * powFactorialCoef * (decCoefs[nextCoef]);
                ++nextCoef;
            }
        }
        powFactorialCoef *= (k / (p + 5.0)) * (k / (p + 4.0));
    }
    potentialValue /= 4 * PI;
    potentialValue += DoubleLayerNumericalPart(n, k, epsilon * sqrt(area), gr, r, basisFunction);
    return -potentialValue;
}

std::complex<double> DoubleLayerPotential::getScalarDoubleLayerHelmholtzPotential(std::complex<double> k, double epsilon,
    GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r, int basisOrder,
    std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    const std::vector<double> & decCoefs)
{
    int n = getTaylorEstimation(k, epsilon, 1);
    if (n < 0)
    {
        throw std::runtime_error("invalid tolerance passed to this function!");
        return 0.0;
    }
    if (0 == n)
    {
        n = 10;
    }
    create_parameters_for_trinagle_only(p);

    if (fabs(w_0) < zero)
    {
        return 0.0;
    }

    std::complex<double> potentialValue = 0.0;
    std::complex<double> powFactorialCoef = 1.0;
    for(int p = -3; p < n; p += 2)
    {
        int nextCoef = 0;
        for(int powerDeg=0; powerDeg <= basisOrder; ++powerDeg)
        {
            for(int i=0; i<=powerDeg; ++i)
            {
                int j = powerDeg - i;
                potentialValue += (p + 2) * w_0 * get_surface_integral(p, i, j) * powFactorialCoef * (decCoefs[nextCoef]);
                ++nextCoef;
            }
        }
        powFactorialCoef *= (k / (p + 5.0)) * (k / (p + 4.0));
    }
    potentialValue /= 4 * PI;
    potentialValue += DoubleLayerNumericalPart(n, k, epsilon * sqrt(area), gr, r, basisFunction);
    return -potentialValue;
}

complex<double> DoubleLayerPotential::DoubleLayerNumericalPart(int N, std::complex<double> k, double epsilon, GaussRule gr,
    const VectorXD<double,3> &x,
    std::function<double(const VectorXD<double,3> &arg)> basisFunction)
{
    if (fabs(w_0) < zero)
    {
        return 0.0;
    }

    auto exponentPart = [&](const VectorXD<double,3> &y)->std::complex<double>
    {
        double R = (x - y).norma();
        double Rsqared = R * R;
        double powR = 1.0 / (R * R * R);
        std::complex<double> result(0.0, 0.0), powFactorialCoef(1.0, 0.0), seriesPart(0.0, 0.0);

        for(int p = -3; p < N; p += 2)
        {
            seriesPart += (powFactorialCoef * powR * (p + 2.0));
            powFactorialCoef *= (k / (p + 5.0)) * (k / (p + 4.0));
            powR *= Rsqared;
        }

        std::complex<double> expPart = exp(-R * k);
        expPart *= (-k / Rsqared - 1.0 / (Rsqared * R));
        result = expPart - seriesPart;
        return result * w_0 / (4 * PI);
    };

    auto globalCoordinate = [&](const VectorXD<double,2> &gauss_points)
    {
        VectorXD<double,3> globCoord = this->p[0] +
            (this->p[1] - this->p[0]) * gauss_points.coords[0] +
            (this->p[2] - this->p[0]) * gauss_points.coords[1];
        return globCoord;
    };

    complex<double> integralSum(0.0, 0.0);
    TrigGaussRuleInfo tgri(gr);
    while(tgri.next())
    {
        VectorXD<double,2> gauss_points;
        gauss_points.coords[ 0 ] = tgri.e.e1;
        gauss_points.coords[ 1 ] = tgri.e.e2;
        VectorXD<double,3> glY = globalCoordinate(gauss_points);
        integralSum += exponentPart(glY) * basisFunction(glY) * area * tgri.w;
    }

    return integralSum;
}
