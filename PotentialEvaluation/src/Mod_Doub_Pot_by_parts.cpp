#include "Mod_Doub_Pot_by_parts.hpp"
#include "SingleLayerSurfacePotential.h"

complex<double> Mod_Doub_Pot_by_parts::EvalfIntegral(BasisFunction fxU, BasisFunction fyLambda,
    ScalarBasisFunction divFx, int fxBasisOrder, int fyBasisOrder,
    const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
    complex<double> waveNumber, bool isSingular, int numberOfLevels, 
    double epsilon, GaussRule grRuff, GaussRule grRefined, bool zeroDivergence)
{
    init_potential(xCarrier, yCarrier, grRuff, waveNumber, isSingular);
    complex_Integral Integrator;
    Integrator.set_6_points();
    auto fyDotNx = [&](const VectorXD<double,3> & arg)->double
    {
        return fast_dot_prod(fyLambda(arg), xCarrier.n);
    };
    
    SingleLayerSurfacePotential slsp;
    VectorXD<double,3> pyPoints[3] = {yCarrier.x0, yCarrier.x1, yCarrier.x2};
    auto funcToIntegrate = [&](double a)
    {
        VectorXD<double,3> x;
        double edge_shift;
        int i=0;
        for(;i<3; ++i)
            if(a <= lengh_steps_x[i])
                break;
        if(i == 0)
            edge_shift = a;
        else
            edge_shift = a - lengh_steps_x[i-1];
        x = vec_edges[i] * edge_shift + vert[i];
        complex<double> I_sy =slsp.getScalarHelmholtzPotential(waveNumber, 1e-6,
            grRefined, pyPoints, x, fyBasisOrder, fyDotNx);
        return I_sy * fxU(x).dotprod(vec_p_global[i]);
    };
    // криволинейные интегралы имеет смысл считать без адаптивного расчёта,
    // 2-дробления добавлены, чтобы гарантировано уменьшать погрешность в 2^k раз, где k-порядок метода
    complex<double> curved_int = Integrator.get_value(0.,lengh_steps_x[0], 1, funcToIntegrate);
    curved_int+= Integrator.get_value(lengh_steps_x[0],lengh_steps_x[1], 1, funcToIntegrate);
    curved_int+= Integrator.get_value(lengh_steps_x[1],lengh_steps_x[2], 1, funcToIntegrate);
    // здесь применяется адаптивный расчёт,
    // поскольку порядок интегрирования обещает быть как у трапеций при любом методе интегрирования
    if (!zeroDivergence)
    {
        complex<double> over_Sy = adaptedIntegralBasisFunction(fxU, fyLambda, 
            divFx, fxBasisOrder, fyBasisOrder, xCarrier, yCarrier, waveNumber, isSingular, numberOfLevels, 
            epsilon, grRuff, grRefined);
        return curved_int - over_Sy ;
    }
    return curved_int;
}

complex<double> Mod_Doub_Pot_by_parts::EvalfIntegral(int N, double epsilon, GaussRule gr_rude, GaussRule gr_refine)
{
    complex<double> curved_int, over_Sy;
    complex_Integral Integrator;
    Integrator.set_6_points();
    // криволинейные интегралы имеет смысл считать без адаптивного расчёта,
    // 2-дробления добавлены, чтобы гарантировано уменьшать погрешность в 2^k раз, где k-порядок метода
    curved_int = Integrator.get_value(0.,lengh_steps_x[0],1,*this);
    curved_int+= Integrator.get_value(lengh_steps_x[0],lengh_steps_x[1],1,*this);
    curved_int+= Integrator.get_value(lengh_steps_x[1],lengh_steps_x[2],1,*this);
    // здесь применяется адаптивный расчёт,
    // поскольку порядок интегрирования обещает быть как у трапеций при любом методе интегрирования
    over_Sy = integral_over_S_y_addapted(N,epsilon,gr_rude,gr_refine);
    return curved_int - over_Sy ;
}

complex<double> Mod_Doub_Pot_by_parts::adaptedIntegralBasisFunction(BasisFunction fx, BasisFunction fy, 
    ScalarBasisFunction divFx, int fxBasisOrder, int fyBasisOrder,
    const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
    complex<double> waveNumber, bool isSingular, int numberOfLevels, 
    double epsilon, GaussRule grRuff, GaussRule grRefined)
{
    double discrepancy = 1., zero, cur_disc;
    complex<double> ret(0.), ret_refined(0.), saved_values(0.);
    vector<triangle_parameters> before_sectioning;
    vector<triangle_parameters> after_sectioning;
    triangle_parameters current_parameter;
    complex<double> rude_value, refined_value;
    if(!isSingular)
        return adaptedIntegralOnSubset(fy, divFx, fxBasisOrder, fyBasisOrder,
            xCarrier, yCarrier, waveNumber, grRefined, isSingular);
    before_sectioning.push_back(xCarrier);
    zero = before_sectioning[0].s_t * before_sectioning[0].t_k * 0.5 * epsilon;
    for(int iter=0; iter<numberOfLevels && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = 0.0;
        ret_refined = 0.0;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();
            rude_value = adaptedIntegralOnSubset(fy, divFx, fxBasisOrder, fyBasisOrder,
                current_parameter, yCarrier, waveNumber, grRuff, isSingular);
            refined_value = adaptedIntegralOnSubset(fy, divFx, fxBasisOrder, fyBasisOrder,
                current_parameter, yCarrier, waveNumber, grRefined, isSingular);
            cur_disc = abs(rude_value - refined_value)/abs(refined_value);
            if(cur_disc >= epsilon && abs(rude_value) >= zero && abs(refined_value) >= zero) // если для данного фрагмента разбиения погрешность слишком высока, то необходимо внести разбиения в этот элемент
            {
                current_parameter.Build_Sections(after_sectioning);
                ret += rude_value;
                ret_refined += refined_value;
            }
            else
                saved_values += refined_value;
        }
        ret += saved_values;
        ret_refined += saved_values;
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    return ret_refined;
}

complex<double> Mod_Doub_Pot_by_parts::integral_over_S_y_addapted(int N, double epsilon, GaussRule gr_rude, GaussRule gr_refine)
{
    double discrepancy = 1., zero, cur_disc;
    complex<double> ret(0.), ret_refined(0.), saved_values(0.);
    vector<triangle_parameters> before_sectioning;
    vector<triangle_parameters> after_sectioning;
    triangle_parameters current_parameter;
    complex<double> rude_value, refined_value;
    if(!is_singular)
        return integral_over_S_y(fx->current_parameter,gr_refine);
    before_sectioning.push_back(fx->current_parameter);
    zero = before_sectioning[0].s_t * before_sectioning[0].t_k * 0.5 * epsilon;
    for(int iter=0; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = 0.0;
        ret_refined = 0.0;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();
            rude_value = integral_over_S_y(current_parameter, gr_rude);
            refined_value = integral_over_S_y(current_parameter, gr_refine);
            cur_disc = abs(rude_value - refined_value)/abs(refined_value);
            if(cur_disc >= epsilon && abs(rude_value) >= zero && abs(refined_value) >= zero) // если для данного фрагмента разбиения погрешность слишком высока, то необходимо внести разбиения в этот элемент
            {
                current_parameter.Build_Sections(after_sectioning);
                ret += rude_value;
                ret_refined += refined_value;
            }
            else
                saved_values += refined_value;
        }
        ret += saved_values;
        ret_refined += saved_values;
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    return ret_refined;
}

complex<double> Mod_Doub_Pot_by_parts::adaptedIntegralOnSubset(BasisFunction fyLambda,
    ScalarBasisFunction divFx, int fxBasisOrder, int fyBasisOrder,
    const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
    complex<double> waveNumber, GaussRule gr, bool isSingular)
{
    complex<double> ret = 0.0;
    VectorXD<double,2> gauss_points;
    // считаем площадь треугольника
    complex<double> area = xCarrier.s_t * xCarrier.t_k * 0.5;
    // суммируем в процедуре численного интегрирования
    ret = 0.;
    TrigGaussRuleInfo tgr(gr);
    VectorXD<double,3> pyPoints[3] = {yCarrier.x0, yCarrier.x1, yCarrier.x2};
    auto fyDotNx = [&](const VectorXD<double,3> & arg)->double
    {
        return fast_dot_prod(fyLambda(arg), xCarrier.n);
    };
        
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        auto globPoint = xCarrier.get_Global_point(gauss_points);
        
        SingleLayerSurfacePotential slsp;        
        // в этих точках вычисляем интегрируемую функцию, от этого параметра нужна только нормаль (fx->current_parameter)
        complex<double> acc = slsp.getScalarHelmholtzPotential(waveNumber, 1e-6,
            gr, pyPoints, globPoint, fyBasisOrder, fyDotNx) * divFx(globPoint) * (tgr.w * area);
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

complex<double> Mod_Doub_Pot_by_parts::integral_over_S_y(const triangle_parameters &Tx, GaussRule gr)
{
    complex<double> ret, acc;
    ret = 0.;
    // параметры решения
    complex< double > area;
    VectorXD<double,3> glob_x_arg, loc_coord;
    VectorXD<double,2> gauss_points;
    // считаем площадь треугольника
    area = fx->current_parameter.s_t * fx->current_parameter.t_k * 0.5;
    // суммируем в процедуре численного интегрирования
    ret = 0.;
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        glob_x_arg = Tx.get_Global_point(gauss_points);
        loc_coord = fx->current_parameter.get_local_coordinate_point(glob_x_arg);
        // в этих точках вычисляем интегрируемую функцию, ............от этого параметра нужна только нормаль (fx->current_parameter)
        acc = slp.EvalfLambdaPot(this->is_singular,fy,glob_x_arg,fx->current_parameter) * fx->div_u_tan(loc_coord);
        // в отличие от area, параметр весов w может менятся на итерациях
        acc *= tgr.w * area;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

void Mod_Doub_Pot_by_parts::init_potential(const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
    GaussRule gr, complex<double> vave_num , bool is_singular)
{
    vec_edges[0] = xCarrier.x1 - xCarrier.x0;
    vec_edges[1] = xCarrier.x2 - xCarrier.x1;
    vec_edges[2] = xCarrier.x0 - xCarrier.x2;
    for(int i=0; i<3; ++i)
    {
        lengh_steps_x[i] = vec_edges[i].norma();
        vec_edges[i].normalize();
    }
    lengh_steps_x[1] += lengh_steps_x[0];
    lengh_steps_x[2] += lengh_steps_x[1];
    vert[0] = xCarrier.x0;
    vert[1] = xCarrier.x1;
    vert[2] = xCarrier.x2;
    for(int i=0; i<3; ++i)
    {
        fast_vector_mutl(vec_edges[i],xCarrier.n,vec_p_global[i]);
        vec_p[i] = xCarrier.get_local_coordinate_vector( vec_p_global[i] );
        if(xCarrier.reversed)
        {
            vec_p[i] = vec_p[i] * (-1.0);
            vec_p_global[i] = vec_p_global[i] * (-1.0);
        }
    }
    slp.tgr.SetRule(gr);
    slp.set_vave_num(vave_num);
    this->is_singular = is_singular;
}

void Mod_Doub_Pot_by_parts::init_potential(VectorBasisInterface *fx, VectorBasisInterface *fy, GaussRule gr, complex<double> vave_num , bool is_singular)
{
    this->fx = fx;
    this->fy = fy;
    vec_edges[0] = fx->current_parameter.x1 - fx->current_parameter.x0;
    vec_edges[1] = fx->current_parameter.x2 - fx->current_parameter.x1;
    vec_edges[2] = fx->current_parameter.x0 - fx->current_parameter.x2;
    for(int i=0; i<3; ++i)
    {
        lengh_steps_x[i] = vec_edges[i].norma();
        vec_edges[i].normalize();
    }
    lengh_steps_x[1] += lengh_steps_x[0];
    lengh_steps_x[2] += lengh_steps_x[1];
    vert[0] = fx->current_parameter.x0;
    vert[1] = fx->current_parameter.x1;
    vert[2] = fx->current_parameter.x2;
    for(int i=0; i<3; ++i)
    {
        fast_vector_mutl(vec_edges[i],fx->current_parameter.n,vec_p_global[i]);
        vec_p[i] = fx->current_parameter.get_local_coordinate_vector( vec_p_global[i] );
        if(fx->current_parameter.reversed)
        {
            vec_p[i] = vec_p[i] * (-1.0);
            vec_p_global[i] = vec_p_global[i] * (-1.0);
        }
    }
    slp.tgr.SetRule(gr);
    slp.set_vave_num(vave_num);
    this->is_singular = is_singular;
}

complex<double> Mod_Doub_Pot_by_parts::operator ()(double a)
{
    VectorXD<double,3> x;
    double edge_shift;
    int i=0;
    for(;i<3; ++i)
        if(a <= lengh_steps_x[i])
            break;
    if(i == 0)
        edge_shift = a;
    else
        edge_shift = a - lengh_steps_x[i-1];
    x = vec_edges[i] * edge_shift + vert[i];
    complex<double> I_sy = slp.EvalfLambdaPot(this->is_singular,fy,x,fx->current_parameter);
    VectorXD<double,3> loc_arg_x = fx->current_parameter.get_local_coordinate_point( x );
    return I_sy * fx->u_tangen( loc_arg_x ).dotprod( vec_p[i] );
}

Mod_Doub_Pot_by_parts::Mod_Doub_Pot_by_parts(GaussRule &gr):fx(0), fy(0), slp(gr), is_singular(false)
{

}

Mod_Doub_Pot_by_parts::Mod_Doub_Pot_by_parts():fx(0), fy(0), is_singular(false)
{

}

Mod_Doub_Pot_by_parts::~Mod_Doub_Pot_by_parts() {

}
