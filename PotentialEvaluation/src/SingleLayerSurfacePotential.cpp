#include "SingleLayerSurfacePotential.h"
#include <math.h>
#include <cmath>
#include <boost/math/special_functions/binomial.hpp>
#include "base.hpp"
#include "UVdecomposer.h"

#define DISCREPANCY (1e-8)

void SingleLayerSurfacePotential::create_parameters_for_trinagle_only(const VectorXD<double,3> p[3])
{
    VectorXD<double,4>::set_scalar_zero(0.0);
    VectorXD<double,3>::set_scalar_zero(0.0);
    VectorXD<double,2>::set_scalar_zero(0.0);
    this->r = r;
    for(int i=0; i<3; ++i)
        this->p[i] = p[i];
    for(uint i=0; i<3; ++i)
    {
        this->s[i] = (p[(i+2)%3] - p[(i+1)%3]);
        this->l_[i] = s[i].norma();
        s[i] = s[i] / l_[i];
    }
    // координатная система ассоциированная с треугольником
    this->u = s[2];
    this->v = this->u * (this->s[1].dotprod(this->u)) - this->s[1];
    fast_vector_mutl(this->u, this->v, this->w);

    VectorXD<double,3> testAreaVector;
    testAreaVector.common_init(0.0);
    fast_vector_mutl(s[0], s[1], testAreaVector);
    area = testAreaVector.norma() * l_[0] * l_[1] / 2.0;
    zero = sqrt(area) * DISCREPANCY;
    u.normalize();
    v.normalize();
    w.normalize();
    for(int i=0; i<3; ++i)
    {
        p_locals[i].coords[0] = (this->p[i] - this->p[0]).dotprod(this->u);
        p_locals[i].coords[1] = (this->p[i] - this->p[0]).dotprod(this->v);
        p_locals[i].coords[2] = (this->p[i] - this->p[0]).dotprod(this->w);
    }
}

// получаем параметры для вычисления интеграла от функции 1./R^n
// для вычисления интегралов по кривой
void SingleLayerSurfacePotential::create_parameters(const VectorXD<double,3> &r,
                                                    const VectorXD<double,3> p[3])
{
    create_parameters_for_trinagle_only(p);
    u_0 = (r - p[0]).dotprod(u);
    v_0 = (r - p[0]).dotprod(v);
    w_0 = (r - p[0]).dotprod(w);
    for(uint i=0; i<3; ++i)
    {
        fast_vector_mutl(w, s[i], m[i]);
        m[i].normalize();
        if( (p[(i+1)%3] - p[i]).dotprod(m[i]) < 0. ||  (p[(i+2)%3] - p[i]).dotprod(m[i]) < 0.)// m - должна быть внешней к области треугольника
            m[i] = m[i] * (-1.);
        t_i_0[i] = (p[(i+1)%3] - r).dotprod(m[i]);
        //this->l_[i] = (p[(i+2)%3] - p[(i+1)%3]).norma();
        s_i_minus[i] = (p[(i+1)%3] - r).dotprod(s[i]);
        s_i_plus[i] = s_i_minus[i] + l_[i];
        R_plus[(i+1)%3] = R_minus[(i+2)%3] = (r - p[i]).norma();
        R_i_0[i] = sqrt(t_i_0[i]*t_i_0[i] + w_0*w_0);
    }
}

double SingleLayerSurfacePotential::R_pow_n_edge_integral(uint edge_number, int n_order)
{
    double result;
    const uint i = edge_number;
    if(n_order == -1)
    {
        double frac = (s_i_plus[i] + R_plus[i]) / (s_i_minus[i] + R_minus[i]);
        result = log(frac);
        return result;
    }
    if(n_order == 0)
        return l_[edge_number];
    result = (s_i_plus[i]*pow(R_plus[i],n_order) - s_i_minus[i]*pow(R_minus[i],n_order)) / (1.0 + n_order);
    if(abs(R_i_0[i]) > zero)
    {
        result += R_pow_n_edge_integral(edge_number, n_order - 2) * n_order * R_i_0[i] * R_i_0[i] / (1.0 + n_order);
    }
    return result;
}

double SingleLayerSurfacePotential::R_pow_n_times_s_pow_m_edge_integral(uint edge_number, int n_R_order, int m_s_order)
{
    double result;
    uint i = edge_number;
    if(m_s_order == 1)
    {
        result = (pow(R_plus[i], n_R_order + 2) - pow(R_minus[i], n_R_order + 2)) / (n_R_order + 2);
        return result;
    }
    if(m_s_order == 0)
    {
        return R_pow_n_edge_integral(edge_number, n_R_order);
    }
    result = (pow(s_i_plus[i], m_s_order - 1) * pow(R_plus[i], n_R_order + 2) -
              pow(s_i_minus[i], m_s_order - 1) * pow(R_minus[i], n_R_order + 2)) / (n_R_order + 2);
    result+= ((1.0 - m_s_order) / (n_R_order + 2.0)) * R_pow_n_times_s_pow_m_edge_integral(edge_number, n_R_order + 2, m_s_order - 2);
    return result;
}

double SingleLayerSurfacePotential::R_surface_integral(int n)
{
    double beta_[3];
    if( -3 == n ) // odd number of n to resolve recursion
    {
        for(int i=0; i<3; ++i)
        {
            double arg_plus, arg_minus;
            arg_plus = t_i_0[i]*s_i_plus[i] / (R_i_0[i]*R_i_0[i] + abs(w_0)*R_plus[i]);
            arg_minus = t_i_0[i]*s_i_minus[i] / (R_i_0[i]*R_i_0[i] + abs(w_0)*R_minus[i]);
            beta_[i] = atan(arg_plus) - atan(arg_minus);
        }
        if(abs(w_0) < zero)
            return 0.0;
        double result = (beta_[0] + beta_[1] + beta_[2]) / abs(w_0);
        return result;
    }
    if( 0 == n) // even number to solve a recursion
    {
        return area;
    }
    // for every other case we can resolve this problem by refering to the integrals over edges
    double edge_integral = 0.;
    for(int edge = 0; edge < 3; ++edge)
    {
        if (abs(t_i_0[edge]) <= zero) // as x log (x) -> 0 when x -> 0 in case of n = -1
            continue;
        
        edge_integral += R_pow_n_edge_integral(edge, n) * t_i_0[edge] / (n + 2);
    }

    double surface_integral = 0.;
    surface_integral += n * w_0 * w_0 * R_surface_integral(n - 2) / (n + 2);

    return surface_integral + edge_integral;
}

/* Необходимо научиться получать коеффициенты прямой для каждой координаты (u, v) - в общем случае,
   затем использовать разложение базисной функции по координатам (u,v), чтобы потом разложить координаты
   (u,v) рёбер треугольника и, пользуясь биномиальным разложением степеней u и v, получить рекурсию.
 */
void SingleLayerSurfacePotential::get_u_line_coefs(double &a, double &b, int edge)
{
    switch(edge)
    {
        case 0:
            a = (p_locals[2].coords[0] - l_[2])/l_[0];
            b = (l_[2]*s_i_plus[0] - p_locals[2].coords[0]*s_i_minus[0])/l_[0];
        break;
        case 1:
            a = -p_locals[2].coords[0] / l_[1];
            b = p_locals[2].coords[0]*s_i_plus[1] / l_[1];
        break;
        case 2:
            a = 1.;
            b = -p_locals[2].coords[2];
        break;
    }
}

void SingleLayerSurfacePotential::get_v_line_coefs(double &a, double &b, int edge)
{
    switch(edge)
    {
        case 0:
            a = p_locals[2].coords[1]/l_[0];
            b = -p_locals[2].coords[1]*s_i_minus[0]/l_[0];
        break;
        case 1:
            a = -p_locals[2].coords[1]/l_[1];
            b = p_locals[2].coords[1]*s_i_plus[1]/l_[1];
        break;
        case 2:
            a = 0.;
            b = 0.;
        break;
    }
}

/*
    Базисная функция представляется в виде полинома, который является произведением u и v
    координат треугольника. Таким образом, получив вид базисной функции в виде полинома
    можно проинтегрировать её произведение на расстояние между точкой наблюдения и точкой
    источника поля по поверхности треугольника. Рекурентная формула предусматривает сведение
    этого интеграла к сумме двух: по поверхности треугольника и по его границе (по рёбрам).
    Чтобы получить интеграл по рёбрам необходимо коэфициенты полинома и сами параметры u и v
    преобразовать к координатной системе, ассоциированной с рёбрами треугольника и, таким образом,
    свести одно выражение к другому.
*/

double SingleLayerSurfacePotential::get_edge_integral(int n, int edge, unsigned q, unsigned p)
{
    double sum = 0.;
    double a_u, b_u, a_v, b_v;
    get_u_line_coefs(a_u, b_u, edge);
    get_v_line_coefs(a_v, b_v, edge);
    for(unsigned i=0; i<=p; ++i)
        for(unsigned j=0; j<=q; ++j)
        {
            double bin_i = boost::math::binomial_coefficient<double>(p, i);
            double bin_j = boost::math::binomial_coefficient<double>(q, j);
            double koef = bin_i * bin_j * pow(a_u,i) * pow(b_u, p - i) * pow(a_v, j) * pow(b_v, q - j);
            sum += koef * R_pow_n_times_s_pow_m_edge_integral(edge, n, i+j);
        }
    return sum;
}

double SingleLayerSurfacePotential::get_surface_integral(int n, uint q, uint p)
{
    double ret = 0.;
    if(q == 0 && p == 0)
        return R_surface_integral(n);
    if(q >= p)
    {
        double edge_integral = 0.;
        for(int edge = 0; edge < 3; ++edge)
        {
            edge_integral += this->u.dotprod( this->m[edge] ) * get_edge_integral(n+2, edge, q-1, p) / (n + 2);
        }
        double surface_integral = 0.;
        surface_integral += u_0 * get_surface_integral(n, q-1, p);
        if( q != 1)
            surface_integral -= (q - 1) * get_surface_integral(n+2, q-2, p) / (n + 2);
        ret += surface_integral + edge_integral;
    }
    else
    {
        double edge_integral = 0.;
        for(int edge = 0; edge < 3; ++edge)
        {
            edge_integral += this->v.dotprod( this->m[edge] ) * get_edge_integral(n+2, edge, q, p-1) / (n + 2);
        }
        double surface_integral = 0.;
        surface_integral += v_0 * get_surface_integral(n, q, p-1);
        if( p != 1)
            surface_integral -= (p - 1) * get_surface_integral(n+2, q, p-2) / (n + 2);
        ret += surface_integral + edge_integral;
    }
    return ret;
}

int SingleLayerSurfacePotential::getTaylorEstimation(std::complex<double> k, double epsilon, double M)
{
    if (abs(k) < epsilon || abs(M) < epsilon)
        return 1;

    double epsBorder = -log(epsilon) - log(2*PI);
    double exponentBorder = EXP * EXP * abs(k) * M;
    return std::min(15.1, std::ceil(std::max(epsBorder, exponentBorder)));
}

// обычный потенциал простого слоя (скалярный МКЭ для задачи магнитостатитки)
double SingleLayerSurfacePotential::getScalarPotential(GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r, int basisOrder, std::function<double(const VectorXD<double,3> &arg)> basisFunction)
{
    std::vector<double> decCoefs;
    UVdecomposer decomposer;
    create_parameters(r, p);
    decomposer.createDecomposeList(*this, basisFunction, gr, basisOrder, decCoefs);
    double potentialValue = 0.0;
    int nextCoef = 0;
    for(int powerDeg=0; powerDeg <= basisOrder; ++powerDeg)
    {
        for(int i=0; i<=powerDeg; ++i)
        {
            int j = powerDeg - i;
            potentialValue += get_surface_integral(-1, i, j) * decCoefs[nextCoef];
            ++nextCoef;
        }
    }
    potentialValue /= 4 * PI;
    return potentialValue;
}

std::complex<double> SingleLayerSurfacePotential::getScalarHelmholtzPotentialCached(std::complex<double> k, double epsilon,
    GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r, int basisOrder,
    std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    const std::vector<double> & decCoefs)
{
    std::vector<double> distances(3);
    VectorXD<double,3>::set_scalar_zero(0.0);
    for (int i=0; i<3; ++i)
        distances[i] = (r - p[i]).norma();
    double M = *std::max_element(distances.begin(), distances.end());

    std::vector<double> trigSides(3);
    trigSides[0] = (p[0] - p[1]).norma();
    trigSides[1] = (p[2] - p[1]).norma();
    trigSides[2] = (p[2] - p[0]).norma();
    double maxSide = *std::max_element(trigSides.begin(), trigSides.end());
    int n = 0;
    if (M > 1.3 * maxSide)
        n = 1;
    else
    {
        n = getTaylorEstimation(k, epsilon, M);
        if (n < 0)
        {
            throw std::runtime_error("invalid tolerance passed to this function!");
            return 0.0;
        }
    }
    if (0 == n)
    {
        n = 10;
    }
    create_parameters(r, p);
    std::complex<double> potentialValue = 0.0;
    std::complex<double> powFactorialCoef = 1.0;
    for(int p = -1; p < n; p += 2)
    {
        int nextCoef = 0;
        for(int powerDeg=0; powerDeg <= basisOrder; ++powerDeg)
        {
            for(int i=0; i<=powerDeg; ++i)
            {
                int j = powerDeg - i;
                potentialValue += get_surface_integral(p, i, j) * powFactorialCoef * (decCoefs[nextCoef]);
                ++nextCoef;
            }
        }
        powFactorialCoef *= (k / (p + 2.0)) * (k / (p + 3.0));
    }
    potentialValue /= 4 * PI;
    potentialValue += NumericalPart(n, k, epsilon * sqrt(area), gr, r, basisFunction);
    return potentialValue;
}

std::complex<double> SingleLayerSurfacePotential::getScalarHelmholtzPotential(std::complex<double> k, double epsilon,
GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r, int basisOrder, std::function<double(const VectorXD<double,3> &arg)> basisFunction)
{
    std::vector<double> decCoefs;

    std::vector<double> distances(3);
    VectorXD<double,3>::set_scalar_zero(0.0);
    for (int i=0; i<3; ++i)
        distances[i] = (r - p[i]).norma();
    double M = *std::max_element(distances.begin(), distances.end());

    std::vector<double> trigSides(3);
    trigSides[0] = (p[0] - p[1]).norma();
    trigSides[1] = (p[2] - p[1]).norma();
    trigSides[2] = (p[2] - p[0]).norma();
    double maxSide = *std::max_element(trigSides.begin(), trigSides.end());
    int n = 0;
    if (M > 1.3 * maxSide)
        n = 1;
    else
    {
        n = getTaylorEstimation(k, epsilon, M);
        if (n < 0)
        {
            throw std::runtime_error("invalid tolerance passed to this function!");
            return 0.0;
        }
    }
    if (0 == n)
    {
        n = 10;
    }
    UVdecomposer decomposer;
    create_parameters(r, p);
    decomposer.createDecomposeList(*this, basisFunction, gr, basisOrder, decCoefs);
    std::complex<double> potentialValue = 0.0;
    std::complex<double> powFactorialCoef = 1.0;
    for(int p = -1; p < n; p += 2)
    {
        int nextCoef = 0;
        for(int powerDeg=0; powerDeg <= basisOrder; ++powerDeg)
        {
            for(int i=0; i<=powerDeg; ++i)
            {
                int j = powerDeg - i;
                potentialValue += get_surface_integral(p, i, j) * powFactorialCoef * (decCoefs[nextCoef]);
                ++nextCoef;
            }
        }
        powFactorialCoef *= (k / (p + 2.0)) * (k / (p + 3.0));
    }
    potentialValue /= 4 * PI;
    potentialValue += NumericalPart(n, k, epsilon * sqrt(area), gr, r, basisFunction);
    return potentialValue;
}

complex<double> SingleLayerSurfacePotential::NumericalPart(int N, std::complex<double> k, double epsilon, GaussRule gr,
    const VectorXD<double,3> &x,
    std::function<double(const VectorXD<double,3> &arg)> basisFunction)
{
    auto exponentPart = [&](const VectorXD<double,3> &y)
    {
        double R = (x - y).norma();
        double Rsqared = R * R;
        complex<double> exponentPart(0.0, 0.0), seriesPart(1.0, 0.0);

        double nextR = 1.0;
        complex<double> powFactorialCoef(1.0, 0.0);

        for (int n = 2; n < N; n += 2)
        {
            nextR *= Rsqared;
            powFactorialCoef *= (k / static_cast<double>(n) ) * (k / (n - 1.0));
            seriesPart += powFactorialCoef * nextR;
        }

        complex<double> expPart = exp(-R * k);
        complex<double> result = -k / (4 * PI);
        if (R > epsilon)
            result = (expPart - seriesPart) / (4 * PI * R);
        return result;
    };

    auto globalCoordinate = [&](const VectorXD<double,2> &gauss_points)
    {
        VectorXD<double,3> globCoord = this->p[0] +
            (this->p[1] - this->p[0]) * gauss_points.coords[0] +
            (this->p[2] - this->p[0]) * gauss_points.coords[1];
        return globCoord;
    };

    complex<double> integralSum(0.0, 0.0);
    TrigGaussRuleInfo tgri(gr);
    while(tgri.next())
    {
        VectorXD<double,2> gauss_points;
        gauss_points.coords[ 0 ] = tgri.e.e1;
        gauss_points.coords[ 1 ] = tgri.e.e2;
        VectorXD<double,3> glY = globalCoordinate(gauss_points);
        integralSum += exponentPart(glY) * basisFunction(glY) * area * tgri.w;
    }

    return integralSum;
}

SingleLayerSurfacePotential::SingleLayerSurfacePotential()
{
    //ctor
}

SingleLayerSurfacePotential::~SingleLayerSurfacePotential()
{
    //dtor
}
