#include <iostream>
#include <algorithm>
#include "GmshReader.hpp"

GmshIO::TokenType GmshIO::classifyToken(const std::string &token)
{
    std::string tokenCopy = token;
    std::transform(tokenCopy.begin(), tokenCopy.end(), tokenCopy.begin(), ::tolower);
    if (tokenCopy == std::string("$comments"))
        return TokenType::COMMENT;
    if (tokenCopy == std::string("$meshformat"))
        return TokenType::FORMAT;
    if (tokenCopy == std::string("$physicalnames"))
        return TokenType::PHYSICALNAMES;
    if (tokenCopy == std::string("$nodes"))
        return TokenType::NODES;
    if (tokenCopy == std::string("$elements"))
        return TokenType::ELEMENTS;
    if (tokenCopy == std::string("$periodic"))
        return TokenType::PERIODIC;
    if (tokenCopy == std::string("$nodedata"))
        return TokenType::NODEDATA;
    if (tokenCopy == std::string("$elementdata"))
        return TokenType::ELEMENTDATA;
    if (tokenCopy == std::string("$elementnodedata"))
        return TokenType::ELEMENTNODEDATA;
    if (tokenCopy == std::string("$interpolationscheme"))
        return TokenType::INTERPOLATIONSCHEME;
    return TokenType::UNCLASSIFIED;
}

bool GmshIO::readToken(std::stringstream & tokenStore, std::ifstream & file, std::string & token)
{
    std::string testString;
    if (tokenStore.eof() || !(tokenStore >> testString))
    {
        std::string line;
        std::getline(file,line);
        tokenStore.clear();
        if (!line.empty())
            tokenStore.str(line);
        else
            return true; // end of file.
    }
    tokenStore >> token;
    return true;
}

bool GmshIO::actionOnToken(std::stringstream & tokenStore, TokenType type, std::ifstream & file)
{
    tokenStore.str(std::string()); // clearing the store
    switch(type)
    {
        case UNCLASSIFIED:
            return false;
        break;
        case COMMENT:
        {
            while (file.eof())
            {
                std::string line;
                std::getline(file,line);
                tokenStore.str(line);
                std::string nextToken;
                while(tokenStore >> nextToken)
                {
                    std::transform(nextToken.begin(), nextToken.end(), nextToken.begin(), ::tolower);
                    if (nextToken == std::string("$endcomments"))
                    {
                        return true;
                    }
                }
            }
            throw std::runtime_error("Impossbile to find the end to the comment section");
            return false; // didn't find the end of comment section
        }
        break;
        case FORMAT:
        {
            std::string line;
            std::getline(file, line);
            tokenStore.clear();
            tokenStore.str(line);
            tokenStore >> versionNumber;
            tokenStore >> fileType;
            tokenStore >> dataSize;
            std::getline(file, line);
            std::transform(line.begin(), line.end(), line.begin(), ::tolower);
            tokenStore.clear();
            tokenStore.str(line);
            tokenStore >> line;
            if (line != std::string("$endmeshformat"))
                return false;
        }
        break;
        case PHYSICALNAMES:
        {
            file >> matrSize;
            std::string line;
            std::getline(file, line);
            std::string firstToken;
            while (!file.eof())
            {
                std::getline(file, line);
                std::transform(line.begin(), line.end(), line.begin(), ::tolower);
                tokenStore.clear();
                tokenStore.str(line);
                tokenStore >> firstToken;
                if (firstToken == std::string("$endphysicalnames"))
                {
                    break;
                }
                tokenStore.str(line);
                int dimentions, materialNumber;
                if (!(tokenStore >> dimentions))
                {
                    throw std::runtime_error("cannot read dimentions!");
                    return false;
                }
                if (!(tokenStore >> materialNumber))
                {
                    throw std::runtime_error("cannot read material number!");
                    return false;
                }
                std::string materialName;
                if (!(tokenStore >> materialName))
                {
                    throw std::runtime_error("cannot read material name!");
                    return false;
                }
                materials.insert(materialDescription(dimentions, materialNumber, materialName));
            }
            if (materials.size() != matrSize)
            {
                throw std::runtime_error("material size inconsistency");
                return false;
            }
            if (firstToken != std::string("$endphysicalnames"))
            {
                throw std::runtime_error("didn't reach the end of file");
                return false;
            }
        }
        break;
        case NODES:
        {
            int nsize;
            file >> nsize;
            std::string line;
            nodeDescription nextNode;
            std::getline(file, line);

            while(!file.eof())
            {
                std::getline(file, line);
                std::transform(line.begin(), line.end(), line.begin(), ::tolower);
                tokenStore.clear();
                tokenStore.str(line);
                std::string firstToken;
                tokenStore >> firstToken;
                if (firstToken == std::string("$endnodes"))
                {
                    break;
                }
                tokenStore.clear();
                tokenStore.str(line);
                int nodeNumber;
                tokenStore >> nodeNumber;
                double x, y, z;
                tokenStore >> x;
                tokenStore >> y;
                tokenStore >> z;
                nextNode.nodeNumber = nodeNumber;
                nextNode.point.resize(3);
                nextNode.point[0] = x;
                nextNode.point[1] = y;
                nextNode.point[2] = z;
                points.insert(nextNode);
            }
        }
        break;
        case ELEMENTS:
        {
            int elsize;
            file >> elsize;
            std::string line;
            std::getline(file, line);

            while(!file.eof())
            {
                std::getline(file, line);
                std::transform(line.begin(), line.end(), line.begin(), ::tolower);
                tokenStore.clear();
                tokenStore.str(line);
                std::string firstToken;
                tokenStore >> firstToken;
                if (firstToken == std::string("$endelements"))
                {
                    break;
                }
                tokenStore.str(line);
                int elemNumber, elemType, numberOfTags;
                tokenStore >> elemNumber;
                tokenStore >> elemType;
                tokenStore >> numberOfTags;
                if (elemType != 4)
                {
                    throw std::runtime_error("only tetrahedral elements are supported!");
                    return false;
                }
                if (numberOfTags <= 0)
                {
                    throw std::runtime_error("no tags for material!");
                    return false;
                }
                int materialNumber, nextTag;
                tokenStore >> materialNumber;
                for(int i=1; i<numberOfTags; ++i)
                    tokenStore >> nextTag; // pass all the tags through, we do not need  them except for one.
                std::vector<int> nodeNumbers;
                for(int i=0; i<4; ++i)
                {
                    tokenStore >> nextTag;
                    nodeNumbers.push_back(nextTag);
                }
                elementDescription newElem;
                newElem.elemNumber = elemNumber;
                newElem.materialNumber = materialNumber;
                newElem.number = nodeNumbers;
                elements.insert(newElem);
            }
        }
        break;
        default:
        {
            return true; // the rest tags are unsopported yet.
        }
        break;
    }
    return true;
}

bool GmshIO::readGmshFile(STR filename)
{
    std::stringstream tokenStore;
    std::ifstream GmshFile(filename);
    if (!GmshFile.is_open())
    {
        throw std::runtime_error("File is not opened");
        return false;
    }
    while(!GmshFile.eof())
    {
        std::string token;
        bool result = readToken(tokenStore, GmshFile, token);
        if (result != true)
        {
            throw std::runtime_error("Failed to read the next token.");
            return false;
        }
        TokenType type = classifyToken(token);
        if (type == UNCLASSIFIED)
        {
            throw std::runtime_error("Token is unclassified!");
            return true;
        }
        if (!actionOnToken(tokenStore, type, GmshFile))
        {
            throw std::runtime_error("Performing action has failed!");
            return false;
        }
    }
    return true;
}

bool GmshIO::outMesh(STR trg, STR xyz)
{
    std::ofstream pFile(xyz);
    if (!pFile.is_open())
        return false;
    std::vector<std::vector<double> > tableOfPoints(points.size());
    for(auto nextPoint : points)
    {
        int pnum = nextPoint.nodeNumber;
        if (pnum <= 0)
        {
            throw std::runtime_error("invalid point number");
            return false;
        }
        pnum--; // to start numeration from zero
        if (pnum >= tableOfPoints.size())
        {
            tableOfPoints.resize(pnum+1);
        }
        tableOfPoints[pnum] = nextPoint.point;
    }
    pFile << tableOfPoints.size() << "\n";
    for (std::size_t i=0; i < tableOfPoints.size(); ++i)
    {
        for (std::size_t j = 0; j < tableOfPoints[i].size(); ++j)
        {
            pFile.precision(16);
            pFile << tableOfPoints[i][j] << "\t";
        }
        pFile << "\n";
    }

    std::ofstream elFile(trg);
    if (!elFile.is_open())
        return false;
    std::vector<elementDescription> tableOfElements(elements.size());
    for(auto nextElement : elements)
    {
        int elNum = nextElement.elemNumber;
        if (elNum <= 0)
        {
            throw std::runtime_error("invalid element number");
            return false;
        }
        elNum--; // to start numeration from zero
        if(elNum >= tableOfElements.size())
        {
            tableOfElements.resize(elNum);
        }
        tableOfElements[elNum] = nextElement;
    }
    elFile << tableOfElements.size() << "\n";
    for(std::size_t i = 0; i < tableOfElements.size(); ++i)
    {
        for(std::size_t j=0; j < tableOfElements[i].number.size(); ++j)
        {
            if (tableOfElements[i].number[j] <= 0)
            {
                throw std::runtime_error("invalid point number occured in the element list");
                return false;
            }
            elFile << tableOfElements[i].number[j] - 1 << "\t";
        }
        elFile << tableOfElements[i].materialNumber << "\n";
    }
    return true;
}
