#include "ScalarConstBasisFunction.h"

ScalarConstBasisFunction::ScalarConstBasisFunction()
{
    //ctor
}

ScalarConstBasisFunction::~ScalarConstBasisFunction()
{
    //dtor
}

void ScalarConstBasisFunction::set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum)
{
    if(!this->current_parameter.build(elnum, base_num, area, points))
        throw std::runtime_error("cannot create parameter list for a triangle");
}

ScalarBasisInterface *ScalarConstBasisFunction::create_copy() const
{
    ScalarConstBasisFunction *function = new ScalarConstBasisFunction();
    *function = *this;
    return function;
}

double ScalarConstBasisFunction::f_value(VectorXD<double,2> Gauss_point)
{
    return 1.0;
}

VectorXD<double,2> ScalarConstBasisFunction::f_gradient(VectorXD<double,3> Local_arg)
{
    VectorXD<double,2> ret;
    ret.common_init(0.0);
    return ret;
}

int ScalarConstBasisFunction::get_local_base_count()
{
    return 1;
}
