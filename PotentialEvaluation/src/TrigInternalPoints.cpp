#include "TrigInternalPoints.hpp"

bool GetInternalTrigPoints(vector<VectorXD<double,3> > &points, int m, int n, VectorXD<double,3> x0, VectorXD<double,3> x1, VectorXD<double,3> x2)
{
    VectorXD<double,3> midp = (x0 + x1 + x2) * (1./3);
    VectorXD<double,3> start_points[3], directions[3], to_center[3];
    double steps[3], to_center_steps[3];
    if(n < 0)
        return false;
    if(n == 0)
    {
        points.push_back(midp);
        return true;
    }
    start_points[0] = x0;
    start_points[1] = x1;
    start_points[2] = x2;
    directions[0] = x1 - x0;
    directions[1] = x2 - x1;
    directions[2] = x0 - x2;
    for(int i=0; i<3; ++i)
    {
        // перемещение вдоль рёбер треугольника
        steps[i] = directions[i].norma();
        directions[i] = directions[i] / steps[i];
        steps[i] = steps[i] / n;
        // перемещение к геометрическому центру
        to_center[i] = midp - start_points[i];
        to_center_steps[i] = to_center[i].norma();
        to_center[i] = to_center[i] / to_center_steps[i];
        to_center_steps[i] = to_center_steps[i] / m;
    }
    for(int i=0; i<3; ++i) // для каждого ребра выводим очередную вершину, лежащую на ребре
    {
        for(int k=0; k<n; ++k)
            points.push_back(start_points[i] + directions[i] * (k*steps[i]));
        start_points[i] = start_points[i] + to_center[i] * to_center_steps[i];
    }
    return GetInternalTrigPoints(points, m-1, n-1, start_points[0], start_points[1], start_points[2]);
}
