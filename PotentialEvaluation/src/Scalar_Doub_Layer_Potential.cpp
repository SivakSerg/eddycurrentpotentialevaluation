#include "Scalar_Doub_Layer_Potential.hpp"

double Scalar_Doub_Layer_Potential::Det(Block<double,3> &M)
{
	return M.block[0][0] * M.block[1][1] * M.block[2][2] +
		   M.block[1][0] * M.block[2][1] * M.block[0][2] +
		   M.block[2][0] * M.block[0][1] * M.block[1][2] -
		  (M.block[2][0] * M.block[1][1] * M.block[0][2] ) -
		  (M.block[1][0] * M.block[0][1] * M.block[2][2] ) -
		  (M.block[0][0] * M.block[2][1] * M.block[1][2] );
}
void Scalar_Doub_Layer_Potential::Kramer(Block<double,3> &M, VectorXD<double,3> &right, VectorXD<double,3> &V)
{
	double D;
	Block<double,3> KrMatr;
	D = Det(M);
	for(int i=0; i<3; i++)
	{
		KrMatr = M;
		for(int j=0; j<3; j++)
			KrMatr.block[j][i] = right.coords[j];
		V.coords[i] = Det(KrMatr) / D;
	}
}
//------------------------------------------------------------------
// Функции, вычисляющие интегралы для нормальных компонент особеннос-
// тей второго и третьего порядков.
//------------------------------------------------------------------
// Гиперболический арктангенс (определен однозначно для системы Maple и GSL) разночтений нет
double Scalar_Doub_Layer_Potential::arctanh(double x)
{
	return 0.5*log(abs((x+1)/(1-x))); // Гиперболический арктангенс
}
// Функция нахождения корней замены переменной
void Scalar_Doub_Layer_Potential::find_roots(VectorXD<double,3> &local_coords, double alpha, double &mu, double &nu)
{
	double s_y, t_y, u_y, a, b, c, A, B;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	// коэффициенты замены переменной
	A = alpha; B = alpha * s_y - t_y;
	a = 1 + alpha * alpha;
	b = 2 * alpha * ( alpha * s_y - t_y );
	c = B * B + u_y * u_y;
	double disc;
	// корни квадратного уравнения
	disc = sqrt( pow(u_y * u_y - c / a, 2) + b*b*u_y*u_y / (a*a) );
	nu = (u_y*u_y - c / a + disc)*a / b;
	mu = (u_y*u_y - c / a - disc)*a / b;
	if (abs(fix_params.s_t - s_y - mu) < zero || abs(s_y + mu) < zero) // s_vave == s_t - s_y или -s_y
	{	// меняем корни местами, по смыслу они неотличимы
		//double acc = nu;
		//nu = mu; mu = acc;
		//printf("Swap!\n");
		mu -= zero * mu;
	}
}
// Этот интеграл так же получен сведением к криволинейному
// см. например тот же самый файл. Идею интегрирования см. в Фихтенгольц том 2 стр. 69-71
double Scalar_Doub_Layer_Potential::F_r3_3(double s, double alpha, double mu, double nu, VectorXD<double,3> &local_coords)
{
	double A, B;
	double s_y, t_y, u_y; // параметры источника в локальных координатах треугольника
	double s_vave;		  // замена переменной в локальных координатах
	double a, b, c, D, C; // константы
	double res = 0.;
	double sig;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	s_vave = s - s_y;
	// коэффициенты замены переменной
	A = alpha; B = alpha * s_y - t_y;
	a = 1 + alpha * alpha;
	b = 2 * alpha * ( alpha * s_y - t_y );
	c = B * B + u_y * u_y;
	// замены переменной
	double r, k, v, u, t;
	if (abs(b / a) > zero) // возможно, например, если alpha == 0
	{
		// находим сами коэффициенты и замену переменной
		r = mu * mu + u_y * u_y;
		v = nu * nu + u_y * u_y;
		k = a * mu * mu + b * mu + c;
		u = a * nu * nu + b * nu + c;
		C = (mu * A + B)*(mu - nu);
		D = (nu * A + B)*(mu - nu);
		t = (nu - s_vave) / (s_vave - mu);
		sig = sign(t+1);
	}
	else // тогда дробно-линейной замены делать не надо, интеграл уже приведен к нужному виду
	{
		r = 1.;
		v = u_y * u_y;
		k = a;
		u = c;
		C = A;
		D = B;
		t = s_vave;
		sig = 1.;
	}
	// первое слагаемое интеграла
	// есть подозрение, что C == 0 но мне не удалось доказать (если mu и nu не менять местами)
	double C2, lambda;
	C2 = k * v / r - u;
	lambda = sqrt(k*t*t + u);
	if (abs(C) > zero)
	{
		if(C2 > 0)
			res += C * atan( lambda / sqrt(C2) )/(r*sqrt(C2));
		else
			if(C2 < 0)
				res -= C * arctanh( lambda / sqrt(-C2) )/(r*sqrt(-C2));
			//else // С2 == 0
			//	res -= C / (lambda * r);
	}
	// второе слагаемое интеграла (вторая замена переменной g)
	double g, A1, A2;
	g = k * t / sqrt( k*t*t + u );
	lambda = v / r; // здесь lambda не замена переменной, а постоянная в выражении
	A1 = (u - k * lambda);
	A2 = lambda * k * k;
	if (abs(A1) < zero)
		res += D * k * g / A2;
	else
		if( abs(A1 * A2) < 1e+8 ) // работаем с небольшими числами (иначе ноль)
		{
			if(A1 * A2 > 0 )
				res += D * k * atan( A1 * g / sqrt(A1*A2) ) / (r * sqrt(A1*A2));
			else
				res -= D * k * this->arctanh( g * A1 / sqrt(-A1*A2) ) / (r * sqrt(-A1*A2));
		}
	return res * u_y * sig;
}

// фукции, которые через общие интегралы выражают значения аналитической части
// интеграл с особенностью r^3, полностью аналогично вычисляется второй интеграл
double Scalar_Doub_Layer_Potential::r3_3_component(VectorXD<double,3> &local_coords)
{
	double s_x, alpha2, alpha1;
	double res = 0., mu, nu;
	s_x = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	// находим корни для замены переменной
	find_roots(local_coords,alpha2,mu,nu);
	// соотносим расстояние до объекта с площадью треугольника
	if (abs(local_coords.coords[2]) < zero)
		return 0.;
	// в противном случае высота существенна
	res = F_r3_3(s_x,alpha2,mu,nu,local_coords) -
		  F_r3_3(0,alpha2,mu,nu,local_coords);
	// снова находим корни (интеграл разбит на сумму двух по разным заменам переменной)
	find_roots(local_coords,alpha1,mu,nu);
	res +=F_r3_3(0,alpha1,mu,nu,local_coords) -
		  F_r3_3(s_x,alpha1,mu,nu,local_coords);
	return res / (4 * PI);
}
//------------------------------------------------------------------
// Функции, реализующие интерфейс класса потенциала двойного слоя
// для функции Гельмгольца.
//------------------------------------------------------------------
// реализует численное интегрирование без учета сингулярности (в лоб)
complex<double> Scalar_Doub_Layer_Potential::DoubPotentialEvalf(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex< double > acc, area, ret;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = DoubPotentFunct(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret += acc;
	}
	return ret;
}
// интегрируемая функция (в процедуре выше), не учитывает сингулярности
complex<double> Scalar_Doub_Layer_Potential::DoubPotentFunct(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double vx, x_y_lenght;
	double v_h;			  // расстояние между агрументами
	scalCache_elem fxCache = f->get_cache_value(GaussPoints);
	// переводим в локальные координаты треугольника точки аргументов
	//
	x = fxCache.loc_arg;  //f->get_Local_point( GaussPoints );
	//
	x_y = x - y;
	x_y_lenght = x_y.norma();
	//
	vx = fxCache.value;	  //f->f_value( GaussPoints ); // в данном случае не делается интерполяция
	//
	v_h = vx * (x_y.coords[2]);
	// далее вычисления ведутся в комплексном пространстве
	complex< double > exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k )*v_h;
	return exp_part * (-1./(x_y_lenght*x_y_lenght*x_y_lenght) - vave_num_k/(x_y_lenght*x_y_lenght)) / (4 * PI);
}
// реализует численное интегрирование с учетом сингулярности
complex<double> Scalar_Doub_Layer_Potential::NumericScalarDoubLayer(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex< double > acc, area, ret;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	f->f_value( y ); // сохранится значение
	f->InterpGrad = f->f_gradient(y);
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalScalarDoubLayer(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
// функция добавки к посчитанному численно интегралу
complex<double> Scalar_Doub_Layer_Potential::AdditionalScalarDoubLayer(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	complex<double> sum;
	complex<double> exp_part;
	VectorXD<double,3> x, x_y;
	VectorXD<double,2> grad_y;
	sum = 0.;
	double fx, fy, x_y_lenght, uy;
	scalCache_elem fxCache = f->get_cache_value(GaussPoints);
	x = fxCache.loc_arg;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero)
	{
		sum = 0.;
		return sum;
	}
	exp_part = exp(-1. * vave_num_k * x_y_lenght) - 1.;
	uy = -x_y.coords[2]; // скалярное произведение на нормаль (x - y, n)
	// величина функции и аппроксимация градиентом
	fx = fxCache.value;
	fy = f->InterpValue;
	grad_y = f->InterpGrad;
	// разрешение особенности по величине экспоненты
	double rsqr = x_y_lenght * x_y_lenght;
	sum += uy * fx * ( exp_part + vave_num_k * x_y_lenght ) / (rsqr * x_y_lenght);
	sum += uy * fx * vave_num_k * exp_part / rsqr;
	double grad_x_y = 0.; // скалярное произведение градиента на разность координат
	for(int i=0; i<2; i++)
		grad_x_y += grad_y.coords[ i ] * x_y.coords[ i ];
	sum += uy * (fx - fy - grad_x_y) / pow(x_y_lenght,3);
	return sum / ( 4 * PI );
}
//------------------------------------------------------------------
// Функция, вычисляющая потенциал двойного слоя (пользовательская).
// Флаг is_singular отвечает за способ вычисления потенциала.
// Выбор способа должен осуществляться внешним критерием.
//------------------------------------------------------------------
complex<double> Scalar_Doub_Layer_Potential::EvalfDoubPotential(bool is_singular, ScalarBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	complex< double > sum(0.);
	VectorXD<double,3> y;
	VectorXD<double,2> f_grad;
	zero = sqrt(y_params.s_t * y_params.t_k / 2.)*1e-6;
	double fy;
	double uy;
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
	// x и y треугольники совпадают или находятся достаточно близко
	y = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
	y = fix_params.get_local_coordinate_point( y );
	if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
		return DoubPotentialEvalf(f, y, y_params); // x и y треугольники в этом случае достаточно далеко друг от друга
	else
	{
		f_grad = f->f_gradient( y ); uy = y.coords[ 2 ]; // (x - y, n) = -uy, нормальная составляющая x должна быть равна нулю
		if (abs(uy)<zero)
			return sum;
		f_grad = f_grad * uy;
		fy = f->f_value( y ); // не в точке Гаусса, а в локальной точке, т. к. ищется аналитическое продолжение функции за её носитель
		sum = 0.;
		// особенность 3-го порядка (тангенсальные составляющие)
		sum += r1_3_component( y ) * f_grad.coords[ 0 ] + r2_3_component( y ) * f_grad.coords[ 1 ];
		// особенность 3-го порядка (нормальная составляющая)
		sum += fy * r3_3v2_component( y ); // uy уже вошел в вычисления с нужным знаком
		// добавляем интеграл, который считается численно
		sum += NumericScalarDoubLayer(f, y, y_params);
	}
	return sum;
}
/*
Функция ниже служит для проверки метода граничных элементов
для нулевого волнового числа и линейных базисных функций на треугольнике.
*/
// полагаем, что x дан в локальных координатах текущего параметра
// ссылка Riasanow, Steinbach стр. 250 (262 в нумерации pdf).
double Scalar_Doub_Layer_Potential::F_linear(double s, double alpha, VectorXD<double,3> x)
{
	double s_x, t_x, u_x, s_t;
	s_x = x.coords[0];
	t_x = x.coords[1];
	u_x = x.coords[2];
	s_t = fix_params.s_t;
	double p, q, v, I1, I2, J, sig;
	// управляющие константы (для замены переменной)
	p = ( alpha * t_x + s_x ) / (1 + alpha * alpha);
	q = sqrt(u_x * u_x + pow(t_x - alpha * s_x, 2.) / (1 + alpha * alpha));
	sig = sign( u_x );
	// замена переменной
	if (abs(u_x) < zero)
		return 0.;
	if (abs(s - p) < zero)
		v = 0;
	else
	{
		v = sqrt((1 + alpha * alpha) * pow(s - p,2.) + q * q) - q;
		v /= sqrt(1 + alpha * alpha) * (s - p);
	}
	// первый интегралл суммы
	J = -u_x *( alpha / sqrt(1 + alpha * alpha) ) *
		log(sqrt((1 + alpha * alpha) * pow(s - p,2.) + q * q)
		+ sqrt(1 + alpha * alpha) * (s - p));
	// I1/2 - второй и третий интегралл суммы интегралов
	double A1, B1, G1;
	double A2, B2, G2;
	A1 = -2 * alpha * sqrt(1 + alpha*alpha) * q / (u_x * u_x + alpha * alpha * q * q) *
		 ((t_x - alpha * s_x) / (1 + alpha * alpha) + q);
	A2 = -2 * alpha * sqrt(1 + alpha*alpha) * q / (u_x * u_x + alpha * alpha * q * q) *
		 ((t_x - alpha * s_x) / (1 + alpha * alpha) - q);
	//
	B1 = (1 + alpha * alpha) / (u_x * u_x + alpha * alpha * q * q) *
		pow((t_x - alpha * s_x) / (1 + alpha * alpha) + q, 2.);
	B2 = (1 + alpha * alpha) / (u_x * u_x + alpha * alpha * q * q) *
		pow((t_x - alpha * s_x) / (1 + alpha * alpha) - q, 2.);
	//
	G1 = sqrt(1 + alpha * alpha) * abs(u_x) / (u_x * u_x + alpha * alpha * q * q) *
		(q + (t_x - alpha * s_x) / (1 + alpha * alpha));
	G2 =  sqrt(1 + alpha * alpha) * abs(u_x) / (u_x * u_x + alpha * alpha * q * q) *
		(q - (t_x - alpha * s_x) / (1 + alpha * alpha));
	// интегралы
	I1 = -0.5 * u_x * log( v*v + A1 * v + B1) +
		 (s_t - s_x) * sig * atan( (2 * v + A1) / (2 * G1) );
	I2 = 0.5 * u_x * log( v*v + A2 * v + B2) -
		 (s_t - s_x) * sig * atan( (2 * v + A2) / (2 * G2) );
	return I1 + I2 + J;
}
double Scalar_Doub_Layer_Potential::LinearDoubPot(VectorXD<double,2> GaussPoints_x, triangle_parameters &x_params)
{
	VectorXD<double,3> x;
	// находим локальные координаты точки
	x = x_params.get_Global_point( GaussPoints_x );
	x = fix_params.get_local_coordinate_point( x );
	// далее берем интеграл по множеству
	double s_t, alpha2, alpha1;
	s_t = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	double res = F_linear(s_t,alpha2,x) - F_linear(0,alpha2,x) -
				 F_linear(s_t,alpha1,x) + F_linear(0,alpha1,x);
	return res / (4 * PI * s_t);
}
// второй способ вычислять особенность 3-го порядка
double Scalar_Doub_Layer_Potential::F_r3_3v2(double s, double alpha, VectorXD<double,3> &local_coords)
{
	double s_x, t_x, u_x, s_t;
	s_x = local_coords.coords[0];
	t_x = local_coords.coords[1];
	u_x = local_coords.coords[2];
	s_t = fix_params.s_t;
	double p, q, v, I1, I2, sig;
	// управляющие константы (для замены переменной)
	p = ( alpha * t_x + s_x ) / (1 + alpha * alpha);
	q = sqrt(u_x * u_x + pow(t_x - alpha * s_x, 2) / (1 + alpha * alpha));
	sig = sign( u_x );
	// замена переменной
	if( abs(u_x) < zero )
		return 0.;
	if (abs(s - p) < zero)
		v = 0;
	else
	{
		v = sqrt((1 + alpha * alpha) * pow(s - p,2) + q * q) - q;
		v /= sqrt(1 + alpha * alpha) * (s - p);
	}
	// I1/2 - суммарные интегралы
	double A1, B1, C1, G1;
	double A2, B2, C2, G2;
	VectorXD<double,3> coefs;
	A1 = -2 * alpha * sqrt(1 + alpha*alpha) * q / (u_x * u_x + alpha * alpha * q * q) *
		 ((t_x - alpha * s_x) / (1 + alpha * alpha) + q);
	A2 = -2 * alpha * sqrt(1 + alpha*alpha) * q / (u_x * u_x + alpha * alpha * q * q) *
		 ((t_x - alpha * s_x) / (1 + alpha * alpha) - q);
	//
	B1 = (1 + alpha * alpha) / (u_x * u_x + alpha * alpha * q * q) *
		pow((t_x - alpha * s_x) / (1 + alpha * alpha) + q, 2);
	B2 = (1 + alpha * alpha) / (u_x * u_x + alpha * alpha * q * q) *
		pow((t_x - alpha * s_x) / (1 + alpha * alpha) - q, 2);
	//
	C2 = -sqrt(1 + alpha * alpha) * (alpha * p - t_x);
	C1 = 2 * alpha * q;
	//
	G1 = sqrt(1 + alpha * alpha) * abs(u_x) / (u_x * u_x + alpha * alpha * q * q) *
		(q + (t_x - alpha * s_x) / (1 + alpha * alpha));
	G2 =  sqrt(1 + alpha * alpha) * abs(u_x) / (u_x * u_x + alpha * alpha * q * q) *
		(q - (t_x - alpha * s_x) / (1 + alpha * alpha));
	// предельные случаи: alpha == 0 и alpha * s_x - t_x == 0 (тогда не годится разложить на сумму двух слагаемых I1 и I2)
	// тогда C1 == 0 и C2 == 0, в этом случае интеграл равен нулю
	if (abs(C1) < zero && abs(C2) < zero)
		return 0.;
	GetCoefs(coefs, A1, A2, B1, B2, C1, C2);
	// далее, при помощи этих коэффициентов, вычисляются интегралы
	double D1, D2, E1, E2;
	D1 = coefs.coords[0]; D2 = -D1; E1 = coefs.coords[1]; E2 = coefs.coords[2];
        I1 = 0.0;
        if(fabs(G1) >= zero)
        {
            I1 = 0.5 * D1 * log( v * v + A1 * v + B1);
            if (std::isinf(I1) || std::isnan(I1))
            {
                I1 = log( pow(v * v + A1 * v + B1, D1 / 2) );
            }
            I1 += (E1 - 0.5 * A1 * D1) * atan( (2 * v + A1) / (2 * G1) ) / G1;
        }
        if (std::isinf(I1) || std::isnan(I1))
        {
            I1 = 0.0;
        }
        I2 = 0.0;
        if(fabs(G2) >= zero)
        {
            I2 = 0.5 * D2 * log( v * v + A2 * v + B2);
            if (std::isinf(I2) || std::isnan(I2))
            {
                I2 = log( pow(v * v + A2 * v + B2, D2 / 2));
            }
            I2 += (E2 - 0.5 * A2 * D2) * atan( (2 * v + A2) / (2 * G2) ) / G2;
        }
	if (std::isinf(I2) || std::isnan(I2))
        {
            I2 = 0.0;
        }
	return (I1 + I2) * 2 * u_x / (u_x * u_x + alpha * alpha * q * q);
}
void Scalar_Doub_Layer_Potential::GetCoefs(VectorXD<double,3> &coefs,
											 double A1, double A2, double B1, double B2, double C1, double C2)
{
	Block<double,3> M;
	//
	M.block[0][0] = A2 - A1; M.block[0][1] = 1.; M.block[0][2] = 1.;
	M.block[1][0] = B2 - B1; M.block[1][1] = A2; M.block[1][2] = A1;
	M.block[2][0] = 0.;		 M.block[2][1] = B2; M.block[2][2] = B1;
	//
	coefs.coords[0] = C2; coefs.coords[1] = C1; coefs.coords[2] = -C2;
	//
	//Kramer(M,coefs,res); coefs = res;
	M.linsolve(&coefs,1);

	//if((res - coefs).norma() / coefs.norma() > 1e-8)
	//	printf("Hello!\n");
}
// фукции, которые через общие интегралы выражают значения аналитической части
// интеграл с особенностью r^3, полностью аналогично вычисляется второй интеграл
double Scalar_Doub_Layer_Potential::r3_3v2_component(VectorXD<double,3> &local_coords)
{
	double s_x, alpha2, alpha1;
	double res = 0.;
	s_x = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	// соотносим расстояние до объекта с площадью треугольника
	if (abs(local_coords.coords[2]) < zero)
		return 0.;
	// в противном случае высота существенна
	res = F_r3_3v2(s_x,alpha2,local_coords) -
		  F_r3_3v2(0,alpha2,local_coords);
	res +=F_r3_3v2(0,alpha1,local_coords) -
		  F_r3_3v2(s_x,alpha1,local_coords);
//        res = (F_r3_3v2(s_x,alpha2,local_coords) - F_r3_3v2(s_x,alpha1,local_coords)) +
//                (F_r3_3v2(0,alpha1,local_coords) - F_r3_3v2(0,alpha2,local_coords));
	return res / (4 * PI);
}
