#include "Vect_Doub_Layer_Potential.hpp"

VectorXD<complex< double >,3> Vect_Doub_Layer_Potential::EvalfVectDoubPotential_no_r_int(VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
    VectorXD<complex< double >, 3> sum;
    VectorXD<double,3> y;
    Block<double,2> f_grad;
    VectorXD<double,3> fy;
    double uy;
    sum.common_init(0.);
    this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
    // x и y треугольники совпадают или находятся достаточно близко
    y = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
    y = fix_params.get_local_coordinate_point( y );
    //
    zero = sqrt(fix_params.t_k * fix_params.s_t / 2.) * 1e-8;
    {
        f_grad = f->gradient_u_tang( y ); uy = y.coords[ 2 ]; // (x - y, n) = -uy, нормальная составляющая x должна быть равна нулю
        if(abs(uy)<zero)
                return sum;
        f_grad = f_grad * uy;
        fy = f->u_tangen( y ); // не в точке Гаусса, а в локальной точке, т. к. ищется аналитическое продолжение функции за её носитель
        sum = 0.;
        // особенность 3-го порядка (тангенсальные составляющие)
        double val_r1_3, val_r2_3;
        val_r1_3 = r1_3_component( y );
        val_r2_3 = r2_3_component( y );
        // умножение градиентной матрицы (расписано для приведения типов)
        VectorXD<double,3> mult;
        mult.coords[0] = val_r1_3 * f_grad.block[ 0 ][ 0 ] + val_r2_3 * f_grad.block[ 0 ][ 1 ];
        mult.coords[1] = val_r1_3 * f_grad.block[ 1 ][ 0 ] + val_r2_3 * f_grad.block[ 1 ][ 1 ];
        mult.coords[2] = 0.;
        // особенность 3-го порядка (нормальная составляющая)
        fy = fy * r3_3v2_component( y ); // uy уже вошел в вычисления с нужным знаком
        //fy = fy * r3_3_component( y );
        for(int p=0; p<3; ++p)
                sum.coords[p] = fy.coords[p] + mult.coords[p];
        // добавляем интеграл, который считается численно
        sum = sum + NumericVectDoubLayer(f, y, y_params);
    }
    return sum;
}

VectorXD<complex< double >,3> Vect_Doub_Layer_Potential::EvalfVectDoubPotential(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	VectorXD<complex< double >, 3> sum;
	VectorXD<double,3> y;
	Block<double,2> f_grad;
	VectorXD<double,3> fy;
	double uy;
	sum.common_init(0.);
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
	// x и y треугольники совпадают или находятся достаточно близко
	y = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
	y = fix_params.get_local_coordinate_point( y );
	//
	zero = sqrt(fix_params.t_k * fix_params.s_t / 2.) * 1e-8;
	if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
		return VectDoubPotentialEvalf(f, y, y_params); // x и y треугольники в этом случае достаточно далеко друг от друга
	else
	{
		f_grad = f->gradient_u_tang( y ); uy = y.coords[ 2 ]; // (x - y, n) = -uy, нормальная составляющая x должна быть равна нулю
		if(abs(uy)<zero)
			return sum;
		f_grad = f_grad * uy;
		fy = f->u_tangen( y ); // не в точке Гаусса, а в локальной точке, т. к. ищется аналитическое продолжение функции за её носитель
		sum = 0.;
		// особенность 3-го порядка (тангенсальные составляющие)
		double val_r1_3, val_r2_3;
		val_r1_3 = r1_3_component( y );
		val_r2_3 = r2_3_component( y );
		// умножение градиентной матрицы (расписано для приведения типов)
		VectorXD<double,3> mult;
		mult.coords[0] = val_r1_3 * f_grad.block[ 0 ][ 0 ] + val_r2_3 * f_grad.block[ 0 ][ 1 ];
		mult.coords[1] = val_r1_3 * f_grad.block[ 1 ][ 0 ] + val_r2_3 * f_grad.block[ 1 ][ 1 ];
		mult.coords[2] = 0.;
		// особенность 3-го порядка (нормальная составляющая)
		fy = fy * r3_3v2_component( y ); // uy уже вошел в вычисления с нужным знаком
		for(int p=0; p<3; ++p)
			sum.coords[p] = fy.coords[p] + mult.coords[p];
		// добавляем интеграл, который считается численно
		sum = sum + NumericVectDoubLayer(f, y, y_params);
	}
	return sum;
}
// Функции для сингулярного численного интегрирования (производятся с учетом особенностей, аналогично интегралам потенциала двойного слоя из скалярного случая)
VectorXD<complex< double >,3> Vect_Doub_Layer_Potential::NumericVectDoubLayer(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	VectorXD<complex<double>,3> acc, ret;
	complex<double> area, one(1.,0.);
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	f->u_tangen( y ); // сохранится значение
	f->InterpGrad = f->gradient_u_tang( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalVectDoubLayer(f, y, gauss_points);
		// в отличие от area, параметр весов w может менятся на итерациях
		acc = acc * (tgr.w * area);
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
// функция добавки для полуаналитического интегрирования (не равна нулю, если не равно нулю волновое число)
VectorXD<complex<double>,3> Vect_Doub_Layer_Potential::AdditionalVectDoubLayer(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<complex<double>,3> sum;
	complex<double> exp_part;
	VectorXD<double,3> x, x_y;
	Block<double,2> grad_y;
	VectorXD<double,3> fx, fy;
	double x_y_lenght, uy;
	sum.common_init(0.);
	vectCache_elem fxCache = f->get_cache_value(GaussPoints);
	x = fxCache.loc_arg;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if(x_y_lenght < zero)
	{
		sum.common_init(0.);
		return sum;
	}
	exp_part = exp(-1. * vave_num_k * x_y_lenght) - 1.;
	uy = -x_y.coords[2]; // скалярное произведение на нормаль (x - y, n)
	// величина функции и аппроксимация градиентом
	fx = fxCache.u_val;
	fy = f->InterpValue;
	grad_y = f->InterpGrad;
	// разрешение особенности по величине экспоненты
	double rsqr = x_y_lenght * x_y_lenght;
	complex<double> mult1, mult2;
	mult1 = uy * ( exp_part + vave_num_k * x_y_lenght ) / (rsqr * x_y_lenght);
	mult2 = uy * vave_num_k * exp_part / rsqr;
	for(int p=0; p<3; ++p)
	{
		sum.coords[p] += fx.coords[p] * mult1;
		sum.coords[p] += fx.coords[p] * mult2;
	}
	VectorXD<complex<double>,3> grad_x_y; // скалярное произведение градиента на разность координат
	grad_x_y.common_init(0.);
	for(int i=0; i<2; i++)
		for(int j=0; j<2; j++)
			grad_x_y.coords[ i ] += grad_y.block[ i ][ j ] * x_y.coords[ j ];
	complex<double> mult3 = uy / pow(x_y_lenght,3);
	for(int p=0; p<3; ++p)
	{
		sum.coords[p] += mult3 * (fx.coords[p] - fy.coords[p] - grad_x_y.coords[p]);
		sum.coords[p] /= ( 4 * PI );
	}
	return sum;
}
// Функции полного интегрирования (без учета сингулярных особенностей)
VectorXD<complex<double>,3> Vect_Doub_Layer_Potential::VectDoubPotentFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;
	VectorXD<double,3> vx;
	VectorXD<double,3> v_h;		  // расстояние между агрументами
	vectCache_elem fxCache = f->get_cache_value(GaussPoints);
	// переводим в локальные координаты треугольника точки аргументов
	//
	x = fxCache.loc_arg;  //f->get_Local_point( GaussPoints );
	//
	x_y = x - y;
	x_y_lenght = x_y.norma();
	//
	vx = fxCache.u_val;	  //f->f_value( GaussPoints ); // в данном случае не делается интерполяция
	//
	v_h = vx * (x_y.coords[2]);
	// далее вычисления ведутся в комплексном пространстве
	complex< double > exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) * (-1./(x_y_lenght*x_y_lenght*x_y_lenght) - vave_num_k/(x_y_lenght*x_y_lenght)) / (4 * PI);
	VectorXD<complex<double>,3> value;
	for(int i=0; i<3; ++i)
		value.coords[ i ] = exp_part * v_h.coords[ i ];
	return value;
}
// вторая функция из набора процедур, которые не учитывают численное интегрирование
// внешняя оболочка для вызова процедур точек Гаусса
VectorXD<complex<double>,3> Vect_Doub_Layer_Potential::VectDoubPotentialEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	VectorXD<complex< double >,3> acc, ret;
	VectorXD<double,2> gauss_points;
	complex<double> area;
	// считаем площадь треугольника
	area = zero = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	zero = sqrt(zero);
	zero *= 1e-8;
	// суммируем в процедуре численного интегрирования
	ret.common_init(0.);
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = VectDoubPotentFunct(f, y, gauss_points);
		// в отличие от area, параметр весов w может менятся на итерациях
		acc = acc * (tgr.w * area);
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
