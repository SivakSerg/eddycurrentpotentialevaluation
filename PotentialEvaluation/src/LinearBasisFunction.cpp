#include "LinearBasisFunctions.hpp"

VectorBasisInterface *LinearVectorBasisFunctions::create_copy() const
{
    LinearVectorBasisFunctions *ret = new LinearVectorBasisFunctions();
    *ret = *this;
    return ret;
}
// пока не придумал, чем тебя занять
LinearVectorBasisFunctions::LinearVectorBasisFunctions()
{ }

// а если придумаю, то и тебя чем-нибудь займу
LinearVectorBasisFunctions::~LinearVectorBasisFunctions()
{ }

//VectorXD<double,3> LinearVectorBasisFunctions::lambda(VectorXD<double,3> Local_arg)
//{
//	VectorXD<double,3> ret, arg;
//	double Area;			// удвоенная площадь треугольника
//	Area = current_parameter.s_t * current_parameter.t_k;
//	arg = current_parameter.get_global_coordinate_point(Local_arg);
//	ret = (current_parameter.x0 - arg) / Area;
//	if(current_parameter.lam_negate)
//		ret = ret * (-1.);
//	// переводим в локальные координаты треугольника
//	InterpValue = current_parameter.get_local_coordinate_point( ret + current_parameter.x0 );
//	return InterpValue;
//}
//
//VectorXD<double,3> LinearVectorBasisFunctions::u_tangen(VectorXD<double,3> Local_arg)
//{
//	VectorXD<double,3> mult[2], ret;
//	VectorXD<double,3> x;	// вычисляется по параметрам треугольника
//	x = current_parameter.get_global_coordinate_point(Local_arg); // уже вычли точку x0 (как начало координатной системы)!
//	ret = current_parameter.x0 - x;
//	double Area;
//	Area = current_parameter.s_t * current_parameter.t_k;
//	mult[0] = n;
//	mult[1] = ret;
//	ret = VectorXD<double,3>::vector_mult(mult,Det);
//	ret = ret / Area;
//	// переводим в локальные координаты треугольника
//	VectorXD<double,3> loc_ret = current_parameter.get_local_coordinate_point( ret + current_parameter.x0 );
//	InterpValue = loc_ret;
//	return loc_ret;
//}

void LinearVectorBasisFunctions::set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum)
{
	uint pnum = abs(base_num)%3; // номер базисной функции совпадает с номером ребра
	this->current_parameter.build(elnum,pnum,area,points);
	// ВАЖНО!!! Сохранить нормаль до возможного инвертирования, для согласования элементов.
	n = current_parameter.n;
	sign = 1;
	//if (base_num != 1) { n = n * (-1.); sign = -1; }
	if(base_num % 2) {n = n * (-1.); sign = -1;}
}

double LinearVectorBasisFunctions::Det(double M[][2], uint size)
{
	return M[0][0] * M[1][1] - M[0][1] * M[1][0];
}

void LinearVectorBasisFunctions::set_current_trig_parameter(triangle_parameters &arg)
{
	this->current_parameter = arg;
	n = current_parameter.n;
}
// Определение функций см. в Analitical Methods стр 38
VectorXD<double,3> LinearVectorBasisFunctions::lambda(VectorXD<double,2> Gauss_points)
{
	VectorXD<double,3> x;	// вычисляется по параметрам треугольника
	x = (current_parameter.x1 - current_parameter.x0)*(-Gauss_points.coords[0]) -
		(current_parameter.x2 - current_parameter.x0)*Gauss_points.coords[1]; // уже вычли точку x0 (как начало координатной системы)!
	VectorXD<double,3> ret; // вектор результат
	double Area;			// удвоенная площадь треугольника
	Area = current_parameter.s_t * current_parameter.t_k;
	ret = x / Area;
	if(current_parameter.lam_negate)
		ret = ret * (-1.);
//        ret = ret * current_parameter.t_k;
	// переводим в локальные координаты треугольника
	return current_parameter.get_local_coordinate_point( ret + current_parameter.x0 );
}

VectorXD<double,3> LinearVectorBasisFunctions::u_tangen(VectorXD<double,2> Gauss_points)
{
	VectorXD<double,3> ret;
	VectorXD<double,3> x;	// вычисляется по параметрам треугольника
	x = (current_parameter.x1 - current_parameter.x0)*(-Gauss_points.coords[0]) -
		(current_parameter.x2 - current_parameter.x0)*Gauss_points.coords[1]; // уже вычли точку x0 (как начало координатной системы)!
	double Area;
	Area = current_parameter.s_t * current_parameter.t_k;
	//mult[0] = n;
	//mult[1] = x;
	//ret = VectorXD<double,3>::vector_mult(mult,Det);
        fast_vector_mutl(n,x,ret);
	ret = ret / Area;
//        ret = ret * current_parameter.t_k;
	// переводим в локальные координаты треугольника
	VectorXD<double,3> loc_ret = current_parameter.get_local_coordinate_point( ret + current_parameter.x0 );
	return loc_ret;
}

// определение граничных операторов в Breuer (нем) стр. 18 (pdf 28)
double LinearVectorBasisFunctions::div_F_lam(VectorXD<double,2> Gauss_points)
{
	double ret = -2./(current_parameter.s_t * current_parameter.t_k);
	if(current_parameter.lam_negate)
		ret *= -1;
//        ret = ret * current_parameter.t_k;
	return ret;
}

// Полу-шутка
double LinearVectorBasisFunctions::div_u_tan(VectorXD<double,2> Gauss_points)
{
	return 0.0;
}

// Оператор ротор
double LinearVectorBasisFunctions::curl_F_lam(VectorXD<double,2> Gauss_points)
{
	return 0.0; // эти функции могут быть ассоциированы с вершинами треугольника
}

double LinearVectorBasisFunctions::curl_u_tan(VectorXD<double,2> Gauss_points)
{
	double ret;
	ret = -2. / (current_parameter.s_t * current_parameter.t_k);
	if(current_parameter.reversed)
		ret *= -1;
//        ret = ret * current_parameter.t_k;
	ret *= sign;
	return ret;
}
// Аналитическое продолжение этих же операторов
// определение граничных операторов в Breuer (нем) стр. 18 (pdf 28)
double LinearVectorBasisFunctions::div_F_lam(VectorXD<double,3> Local_arg)
{
	double ret;
	ret = -2./(current_parameter.s_t * current_parameter.t_k);
	if(current_parameter.lam_negate)
		ret *= -1;
//        ret = ret * current_parameter.t_k;
	return ret;
}

// Полу-шутка (по видимому для тангенсальных полей всегда так!)
double LinearVectorBasisFunctions::div_u_tan(VectorXD<double,3> Local_arg)
{
	return 0.0;
}

// Оператор ротор
double LinearVectorBasisFunctions::curl_F_lam(VectorXD<double,3> Local_arg)
{
	return 0.0; // эти функции могут быть ассоциированы с вершинами треугольника
}

double LinearVectorBasisFunctions::curl_u_tan(VectorXD<double,3> Local_arg)
{
	double ret;
	ret = -2. / (current_parameter.s_t * current_parameter.t_k);
	if(current_parameter.reversed)
		ret *= -1.;
	ret *= sign;
//        ret = ret * current_parameter.t_k;
	return ret;
}

// Исходя из того что функции линейные и зависят от t и s по этой и последующей функции
// см. справку в Analitical Methods Thesis стр. 41
// строки в данной матрице - это градиенты соответствующих номерам строк компонент
// В новой версии исправлены знаки градиента после теста Vect_Doub_Pot_TEST.cpp
Block<double,2> LinearVectorBasisFunctions::gradient_u_tang(VectorXD<double,3> Local_arg)
{
	Block<double,2> ret;
	double area;
	area = current_parameter.s_t * current_parameter.t_k;
	ret.block[0][0] =  0;		 ret.block[0][1] = 1 / area;
	ret.block[1][0] =  -1 / area;     ret.block[1][1] = 0;
	//if(current_parameter.reversed)
	//	ret = ret * (-1.);
	ret = ret * sign;
//        ret = ret * current_parameter.t_k;
	return ret;
}

// векторно на нормаль эта функция свой агрумент не умножает.
// этот аргумент при численном интегрировании будет строго принадлежать
// треугольнику, следовательно отображение тождественно и матрица будет равна еденичной
// с точностью до масштабного множителя
Block<double,2> LinearVectorBasisFunctions::gradient_lambda(VectorXD<double,3> Local_arg)
{
	Block<double,2> ret;
	double area = current_parameter.s_t * current_parameter.t_k;
	ret.block[0][0] =  -1 / area;   ret.block[0][1] = 0;
	ret.block[1][0] =  0;		ret.block[1][1] = -1 / area;
	if(current_parameter.lam_negate)
		ret = ret * (-1.);
//        ret = ret * current_parameter.t_k;
	return ret;
}
