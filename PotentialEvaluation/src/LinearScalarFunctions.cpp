#include "LinearScalarFunctions.hpp"

ScalarBasisInterface *LinearScalarBasis::create_copy() const
{
    LinearScalarBasis *function = new LinearScalarBasis();
    *function = *this;
    return function;
}
double LinearScalarBasis::f_value(VectorXD<double,2> Gauss_point)
{
	//// так было раньше.
	//{
	//	switch(base_num)
	//	{
	//	case 1:
	//		return Gauss_point.coords[0];
	//	case 2:
	//		return Gauss_point.coords[1];
	//	}
	//	return 1 - Gauss_point.coords[0] - Gauss_point.coords[1]; // case 0
	//}
	// Сейчас по честному вычисляем L - координаты
	VectorXD<double,3> loc_coords, L_arg, L_coords;
	loc_coords = get_Local_point(Gauss_point);
	L_arg.coords[ 0 ] = 1.0;
	L_arg.coords[ 1 ] = loc_coords.coords[ 0 ];
	L_arg.coords[ 2 ] = loc_coords.coords[ 1 ];
	L_coords = current_parameter.LMtr * L_arg;
	return L_coords.coords[0];
}
void LinearScalarBasis::set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum)
{
	this->base_num = abs(base_num)%3;
	this->current_parameter.build(elnum,this->base_num,area,points);
}
double LinearVectorBasisDecomposer::u_test(VectorBasisInterface *funct, VectorXD<double,3> n, basis_storage &combination, VectorXD<double,3> &glob_arg)
{
	double val1 = combination.get_value(glob_arg);
	double val2 = funct->u_tangen( funct->current_parameter.get_local_coordinate_point(glob_arg) ).dotprod(n);
	return abs(val1 - val2);
}
bool LinearVectorBasisDecomposer::CreateCombination(VectorBasisInterface *funct, basis_storage &combination)
{
	triangle area; // создаем псевдо-буффер для передачи его в вычислительный класс скалярных базисных функций
	VectorXD<double,3> points[3];
	LinearScalarBasis *arg;
	build_preudo_buffs(funct->current_parameter,area,points);
	if(funct == 0)
		return false;
	// базисных функций в контейнере нет, надо выделить память
	if(combination.basis_functions.size() == 0)
	{
		// записываем функции, по которым ведется разложение
		arg = new LinearScalarBasis();
		arg->set_current_trig_parameter(0, &area, points, 0);

		combination.basis_functions.push_back( arg );

		arg = new LinearScalarBasis();
		arg->set_current_trig_parameter(1, &area, points, 0);

		combination.basis_functions.push_back( arg );

		arg = new LinearScalarBasis();
		arg->set_current_trig_parameter(2, &area, points, 0);

		combination.basis_functions.push_back( arg );
	}
	else
	{
		for(uint i=0; i<combination.basis_functions.size(); i++)
			combination.basis_functions[i]->set_current_trig_parameter(i,&area,points,0);
	}
	for(uint i=0; i<combination.basis_functions.size(); i++)
		if(funct->current_parameter.reversed)
			combination.basis_functions[i]->current_parameter.reverse_normal();
	return true;
}
void LinearVectorBasisDecomposer::build_preudo_buffs(triangle_parameters &params, triangle &trg, VectorXD<double,3> *points)
{
	//
	trg.p[0] = 0;
	trg.p[1] = 1;
	trg.p[2] = 2;
	//
	points[0] = params.x0;
	points[1] = params.x1;
	points[2] = params.x2;
}
bool LinearVectorBasisDecomposer::Decompose_u(VectorBasisInterface *funct, VectorXD<double,3> &n, basis_storage &combination)
{
	if(funct == 0)
		return false;
	// а теперь заносим веса разложения
	double cur_val;
	VectorXD<double,3> main_arg;
	combination.weights.clear();
	//
	for(uint i=0; i<3; i++)
	{
		main_arg = combination.basis_functions[ i ]->current_parameter.x0;
		cur_val = funct->u_tangen( funct->current_parameter.get_local_coordinate_point(main_arg) ).dotprod(n);
		combination.weights.push_back(cur_val);
	}
	return true;
}
bool LinearVectorBasisDecomposer::Decompose_lambda(VectorBasisInterface *funct, VectorXD<double,3> &n, basis_storage &combination)
{
	// а теперь заносим веса разложения
	double cur_val;
	VectorXD<double,3> main_arg;
	combination.weights.clear();
	if(funct == 0)
		return false;
	//
	for(uint i=0; i<3; i++)
	{
		main_arg = combination.basis_functions[ i ]->current_parameter.x0;
		cur_val = funct->lambda( funct->current_parameter.get_local_coordinate_point(main_arg) ).dotprod(n);
		combination.weights.push_back(cur_val);
	}
	return true;
}

