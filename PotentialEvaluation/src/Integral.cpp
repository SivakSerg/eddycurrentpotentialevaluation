#include "Integral.h"

void Integral::set_function(double (*f)(double a))
{this->func = f;}
bool Integral::set_pair(double coef, double x)
{
	if(x<0 || x>1)
		return false;
	Leg_pair pair; pair.coef = coef; pair.x = x;
	Leg_pair sym_pair; sym_pair.coef = coef; sym_pair.x = -x;
	coefs.push_back(pair);
	coefs.push_back(sym_pair);
	return true;
}
bool Integral::remove_last_coefs()
{
	if(coefs.size()==4)
		return false;
	coefs.pop_back();
	coefs.pop_back();
	return true;
}
Integral::Integral()
{
	Leg_pair pair[2];
	pair[0].coef = 0.652145154862546;
	pair[0].x = 0.339981043584856;
	pair[1].coef = 0.347854845137454;
	pair[1].x = 0.861136311594053;
	coefs.push_back(pair[0]);
	coefs.push_back(pair[1]);
	pair[0].x = -pair[0].x;
	pair[1].x = -pair[1].x;
	coefs.push_back(pair[0]);
	coefs.push_back(pair[1]);
}
Integral::~Integral()
{
}
double Integral::get_value(double a, double b, int n)
{
	if(n<0 || func==0)return 0.;
	double h = (b-a)/n, x=a;
	double acc = 0.0;
	for(int k=0; k<n; k++)
	{
		for(int s=0; s<coefs.size(); s++)
			acc += func(x+h/2.+coefs[s].x*h/2.)*coefs[s].coef;
		x += h;
	}
	acc *= h/2.;
	return acc;
}
double Integral::get_value(double a, double b, int n, Functor &func)
{
	if(n<0)return 0.;
	double h = (b-a)/n, x=a;
	double acc = 0.0;
	for(int k=0; k<n; k++)
	{
		for(int s=0; s<coefs.size(); s++)
			acc += func(x+h/2.+coefs[s].x*h/2.)*coefs[s].coef;
		x += h;
	}
	acc *= h/2.;
	return acc;
}
double Integral::get_value(double a, double b, int n, const std::function<double(double)> &func)
{
	if(n<0)return 0.;
	double h = (b-a)/n, x=a;
	double acc = 0.0;
	for(int k=0; k<n; k++)
	{
		for(int s=0; s<coefs.size(); s++)
			acc += func(x+h/2.+coefs[s].x*h/2.)*coefs[s].coef;
		x += h;
	}
	acc *= h/2.;
	return acc;
}
std::complex<double> Integral::get_value(double a, double b, int n, const std::function<std::complex<double>(double)> &func)
{
    using namespace std;
    if(n<0)return 0.;
    double h = (b-a)/n, x=a;
    complex<double> acc = 0.0;
    for(int k=0; k<n; k++)
    {
        for(int s=0; s<coefs.size(); s++)
        {
            acc += func(x+h/2.+coefs[s].x*h/2.)*coefs[s].coef;
        }
        x += h;
    }
    acc *= h/2.;
    return acc;
}
VectorXD<double,2> Integral::get_value(double a, double b, int n, Vector2DFunctor &func)
{
    double h = (b-a)/n, x=a;
    VectorXD<double,2> acc;
    acc.common_init(0.);
    for(int k=0; k<n; k++)
    {
        for(int s=0; s<coefs.size(); s++)
            acc = acc + func(x+h/2.+coefs[s].x*h/2.)*coefs[s].coef;
        x += h;
    }
    acc = acc * ( h/2. );
    return acc;
}
