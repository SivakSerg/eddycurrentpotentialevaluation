#include "ScalarBasisInterface.hpp"

ScalarBasisInterface::~ScalarBasisInterface(){}
// структуры данных для задания скалярного базиса (ансамбля)
VectorXD<double,3> B_curl(ScalarBasisInterface *f, VectorXD<double,2> gauss_points)
{
    VectorXD<double,3> loc_point, glob_point, result;
    VectorXD<double,2> grad;
    glob_point = f->get_Global_point(gauss_points);
    loc_point = f->current_parameter.get_local_coordinate_point(glob_point);
    grad = f->f_gradient(loc_point);
    loc_point.coords[0] = grad.coords[0];
    loc_point.coords[1] = grad.coords[1];
    loc_point.coords[2] = 0.;
    glob_point = f->current_parameter.get_global_coordinate_vector(loc_point);
    fast_vector_mutl(glob_point,f->current_parameter.n,result);
    return result;
}
VectorXD<double,3> B_curl(ScalarBasisInterface *f, VectorXD<double,3> loc_point)
{
    VectorXD<double,3> glob_point, result;
    VectorXD<double,2> grad;
    grad = f->f_gradient(loc_point);
    loc_point.coords[0] = grad.coords[0];
    loc_point.coords[1] = grad.coords[1];
    loc_point.coords[2] = 0.;
    glob_point = f->current_parameter.get_global_coordinate_vector(loc_point);
    fast_vector_mutl(glob_point,f->current_parameter.n,result);
    return result;
}

// В глобальной системе координат
VectorXD<double,3> ScalarBasisInterface::get_Global_point(VectorXD<double,2> Gauss_point)
{
	VectorXD<double,3> ret;
	ret = current_parameter.x0 +
		(current_parameter.x1 - current_parameter.x0) * Gauss_point.coords[0] +
		(current_parameter.x2 - current_parameter.x0) * Gauss_point.coords[1];
	return ret;
}
// В ЛСК для данного треугольника для данной базисной функции
VectorXD<double,3> ScalarBasisInterface::get_Local_point(VectorXD<double,2> Gauss_point)
{
	VectorXD<double,3> ret;
	ret = get_Global_point( Gauss_point ); // перевод в глобальную систему
	return current_parameter.get_local_coordinate_point( ret ); // перевод в ЛСК треугольника
}
// Данная функция экстраполирует готовую функцию на элементе "за" элемент (за её конечный носитель)
double ScalarBasisInterface::f_value(VectorXD<double,3> Local_arg)
{
//	VectorXD<double,3> proj;
//	VectorXD<double,2> gradient;
//	VectorXD<double,2> GaussPoint;
//	GaussPoint = current_parameter.find_projection_point( Local_arg );
//	proj = get_Local_point( GaussPoint );
//	gradient = f_gradient( Local_arg );
//	// вектора, которыми манипулируем, чтобы получить интерполянт
//	double Mult, ValueP, ValueRes;
//	Mult = 0.;
//	Local_arg = Local_arg - proj;
//	for(int i=0; i<2; i++)
//		Mult += gradient.coords[i] * Local_arg.coords[i];
//	ValueP = f_value( GaussPoint );
//	ValueRes = ValueP + Mult;
//	InterpValue = ValueRes;
//	return ValueRes;
    VectorXD<double,2> GaussPoint;
	GaussPoint = current_parameter.find_projection_point( Local_arg );
	double ValueP = f_value( GaussPoint );
        InterpValue = ValueP;
	return ValueP;
}

void scalCache_rule::set_size(GaussRule &gr)
{
	if((int)gr < 0)
		throw std::invalid_argument("Invalid gauss rule for resizing!\n");
	if( cache.size() != ((uint)gr) )
		cache.resize((uint)gr);
	next_value = 0;
	use_cache = false;
}
// кэшируем значения базисной функции
scalCache_elem &ScalarBasisInterface::get_cache_value(VectorXD<double,2> &GaussPoint)
{
	uint i = 0;
	if(Cache_values.cache.size() == 0)
		throw std::runtime_error("Error! Cache does not initiated!");
	i = Cache_values.next_value;
	Cache_values.next_value++; Cache_values.next_value %= Cache_values.cache.size();
	if(Cache_values.use_cache == true)
		return Cache_values.cache[ i ];
	if(Cache_values.next_value == 0)
		Cache_values.use_cache = true;
	// вычисляем все необходимые значения для кэш
	Cache_values.cache[ i ].loc_arg = get_Local_point(GaussPoint);
	Cache_values.cache[ i ].value = f_value(GaussPoint);
        Cache_values.cache[ i ].glob_curl = B_curl(this,GaussPoint);
	return Cache_values.cache[ i ];
}

void basis_storage::clear()
{
	// удаляем базисные функции
	for(uint i=0; i<basis_functions.size(); i++)
		if(basis_functions[i]!=0)
			delete[] basis_functions[i];
	// очищаем вектор
	basis_functions.clear();
	weights.clear();
}
double basis_storage::get_value(VectorXD<double,3> Glob_arg)
{
	double ret = 0.;
	VectorXD<double,3> loc_coord;
	uint size = min(basis_functions.size(),weights.size());
	for(uint i=0; i<size; i++)
	{
		loc_coord = basis_functions[i]->current_parameter.get_local_coordinate_point( Glob_arg );
		ret += basis_functions[i]->f_value(loc_coord) * weights[i];
	}
	return ret;
}

VectorXD<double,3> basis_storage::get_gradient(VectorXD<double,3> Glob_arg)
{
	VectorXD<double,3> ret;
	ret.common_init(0.);
	VectorXD<double,3> loc_coord;
	VectorXD<double,2> loc_grad;
	uint size = min(basis_functions.size(),weights.size());
	for(uint i=0; i<size; i++)
	{
		loc_coord = basis_functions[i]->current_parameter.get_local_coordinate_point( Glob_arg );
		loc_grad = basis_functions[i]->f_gradient(loc_coord);
		ret.coords[0] += weights[i]*(basis_functions[i]->current_parameter.S.block[0][0] * loc_grad.coords[0] +
						 basis_functions[i]->current_parameter.S.block[0][1] * loc_grad.coords[1]);
		ret.coords[1] += weights[i]*(basis_functions[i]->current_parameter.S.block[1][0] * loc_grad.coords[0] +
						 basis_functions[i]->current_parameter.S.block[1][1] * loc_grad.coords[1]);
		ret.coords[2] += weights[i]*(basis_functions[i]->current_parameter.S.block[2][0] * loc_grad.coords[0] +
						 basis_functions[i]->current_parameter.S.block[2][1] * loc_grad.coords[1]);
	}
	return ret;
}

basis_storage::~basis_storage()
{
	// удаляем ссылки на объекты
	for(uint i=0; i<basis_functions.size(); i++)
	{
		if(basis_functions[i] != 0)
			delete basis_functions[i];
	}
}

ScalarBasisAnsamble::~ScalarBasisAnsamble()
{
	for(uint i=0; i<basis_functions.size(); i++)
		if(basis_functions[i])
			delete basis_functions[i];
}
void ScalarBasisAnsamble::create_copy(vector<ScalarBasisInterface*> &copy)
{
    copy.resize(this->basis_functions.size());
    for(uint i=0; i<copy.size(); ++i)
        copy[i] = this->basis_functions[i]->create_copy();
}
