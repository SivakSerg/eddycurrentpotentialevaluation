#include "TriangleParam.h"
#include <vector>
using namespace std;

double trig_area(triangle & trig, std::vector<VectorXD<double,3> > points)
{
    VectorXD<double,3>::set_scalar_zero(0.0);
    VectorXD<double,3> v1 = points[trig.pnums(1)] - points[trig.pnums(0)];
    VectorXD<double,3> v2 = points[trig.pnums(2)] - points[trig.pnums(0)];
    VectorXD<double,3> res;
    fast_vector_mutl(v1, v2, res);
    return res.norma() / 2.0;
}

// Функция проверяет, принадлежит ли точка треугольнику или нет.
bool is_inside(triangle & trig, const std::vector<VectorXD<double,3> > & points, const VectorXD<double,3> & argToCheck, double tolerance)
{
    std::vector<VectorXD<double,3> > internalTrigPoints(3);
    double area, sumArea = 0.0;
    for(int i=0; i<3; ++i)
    {
        triangle trgAcc;
        for(int p=0; p<3; ++p)
        {
            int c = (p+i)%3;
            trgAcc.pnums(p) = p;
            internalTrigPoints[p] = points[ trig.pnums(c) ];
        }
        internalTrigPoints[2] = argToCheck;
        sumArea += trig_area(trgAcc,internalTrigPoints);
    }
    area = trig_area(trig,points);
    if(sqrt(fabs(area - sumArea) / sumArea) < tolerance)
        return true;
    return false;
}

double triangle_parameters::get_max_edge()
{
    double l[] = {(x1 - x0).norma(), (x2 - x1).norma(), (x0 - x2).norma()};
    return *std::max_element(l, l+3);
}

double triangle_parameters::get_internal_radius()
{
    double l[] = {(x1 - x0).norma(), (x2 - x1).norma(), (x0 - x2).norma()};
    double T = (l[0] + l[1] + l[2]) / 2.0;
    double S = s_t * t_k * 0.5;
    double r = S / T;
    return r;
}

double triangle_parameters::get_Radius()
{
    double S = this->s_t * this->t_k * 0.5;
    double l[] = {(x1 - x0).norma(), (x2 - x1).norma(), (x0 - x2).norma()};
    double R = l[0] * l[1] * l[2] / (4 * S);
    return R;
}

double triangle_parameters::Build_Sections(std::vector< triangle_parameters > &params, double k)
{
    if (k < 0 || k > 1)
    {
        params.push_back(*this);
        return false;
    }

    VectorXD<double,3> x[] = {x0, x1, x2};
    VectorXD<double,3> midP;
    midP.common_init(0.0);
    for (int p=0; p<3; ++p)
    {
        midP = midP + x[p];
    }
    midP = midP / 3.0;

    VectorXD<double,3> newX[3];
    for (int p=0; p<3; ++p)
    {
        VectorXD<double,3> dir;
        dir = x[p] - midP;
        dir = dir * k;
        newX[p] = midP + dir;
    }

    VectorXD<double,3> points[6];
    triangle area[7];
    for (int p=0; p<3; ++p)
    {
        points[p] = x[p];
        points[p+3] = newX[p];
    }

    for (int trigNum = 0; trigNum < 3; ++trigNum)
    {
        int firstLocPointNumber = 0 == trigNum ? 2 : trigNum - 1;
        int secLocPointNumber = firstLocPointNumber + 3;
        area[2*trigNum].pnums(0) = firstLocPointNumber;
        area[2*trigNum].pnums(1) = trigNum;
        area[2*trigNum].pnums(2) = trigNum + 3;

        area[2*trigNum + 1].pnums(0) = secLocPointNumber;
        area[2*trigNum + 1].pnums(1) = firstLocPointNumber;
        area[2*trigNum + 1].pnums(2) = trigNum + 3;
    }

    area[6].pnums(0) = 3;
    area[6].pnums(1) = 4;
    area[6].pnums(2) = 5;

    for(int i=0; i<7; ++i)
    {
        triangle_parameters current_parameter;
        current_parameter.build(i, 0, area, points);
        if(reversed)
            current_parameter.reverse_normal();
        params.push_back(current_parameter);
    }

    double subAreas[6];
    for (int i=0; i<6; ++i)
        subAreas[i] = params[i].s_t * params[i].t_k * 0.5;
    return *std::max_element(subAreas, subAreas+6);
}

bool triangle_parameters::Build_Sections( std::vector< triangle_parameters > &params )
{
    VectorXD<double,3> points[6];
    triangle area[4];
    points[0] = x0;
    points[1] = x1;
    points[2] = x2;
    points[3] = (x0 + x1)*0.5;
    points[4] = (x1 + x2)*0.5;
    points[5] = (x0 + x2)*0.5;
    area[0].pnums(0) = 0;
    area[0].pnums(1) = 3;
    area[0].pnums(2) = 5;
    //
    area[1].pnums(0) = 5;
    area[1].pnums(1) = 3;
    area[1].pnums(2) = 4;
    //
    area[2].pnums(0) = 2;
    area[2].pnums(1) = 5;
    area[2].pnums(2) = 4;
    //
    area[3].pnums(0) = 3;
    area[3].pnums(1) = 1;
    area[3].pnums(2) = 4;
    for(int i=0; i<4; ++i)
    {
        triangle_parameters current_parameter;
        current_parameter.build(i, 0, area, points);
        if(reversed)
            current_parameter.reverse_normal();
        params.push_back(current_parameter);
    }
    return true;
}

bool triangle_parameters::is_point_inside(const VectorXD<double,3>& argToCheck, double tolerance)
{
    triangle trig;
    for(uint p=0; p<3; ++p)
        trig.pnums(p) = p;
    vector< VectorXD<double,3> > points;
    points.push_back(x0);
    points.push_back(x1);
    points.push_back(x2);
    return is_inside(trig, points, argToCheck, tolerance);
}

bool triangle_parameters::GetInternalPoints(vector<VectorXD<double,3> >& points, int n)
{
    return GetInternalTrigPoints(points,n,n,x0,x1,x2);
}

void triangle_parameters::get_circumcircle_params(double &R, VectorXD<double,3> &point_glob) const
{
    double abc[3];
    VectorXD<double,3> a_mid_p[3], a_edge_vec[3];
    a_mid_p[0] = (this->x0 + this->x1)*0.5;
    a_mid_p[1] = (this->x0 + this->x2)*0.5;
    a_mid_p[2] = (this->x1 + this->x2)*0.5;
    a_edge_vec[0] = (this->x1 - this->x0);
    a_edge_vec[1] = (this->x0 - this->x2);
    a_edge_vec[2] = (this->x2 - this->x1);
    for(int j=0; j<3; ++j)
    {
        abc[j] = a_edge_vec[j].norma();
        a_edge_vec[j] = a_edge_vec[j] / abc[j];
    }
    VectorXD<double,3> N; // по причине возможных инверсий исходной нормали придумали свою нормаль
    fast_vector_mutl(a_edge_vec[0], a_edge_vec[1], N);
    N.normalize();
    double S = this->s_t * this->t_k * 0.5;
    R = abc[0]*abc[1]*abc[2]/(4.*S);
    int i = max_element(abc,abc+3) - &(abc[0]);
    double k = sqrt(R*R - abc[i]*abc[i] / 4.);
    VectorXD<double,3> center_direction;
    fast_vector_mutl(a_edge_vec[i],N,center_direction); // благодаря придуманному направлению нормали, полученный вектор указывает на центр описанной окружности
    point_glob = a_mid_p[i] + center_direction * k;
}

// точки на прямой задаются в локальных координатах
void triangle_parameters::create_section(std::vector< std::vector<VectorXD<double, 3> > > &lines, uint edge_pnum, 
        uint section_pnum, uint pnum, double height) const
{
	pnum = pnum % 3;
	uint A, B;
	A = (pnum + 1) % 3;
	B = (pnum + 2) % 3;
	VectorXD<double, 3> x[3];
	x[0] = x0; x[1] = x1; x[2] = x2;
	VectorXD<double, 3> edge_vect, vertexA, vertexB, LineStart, sect_vect, next_p;
	edge_vect = x[B] - x[A];
	edge_vect = edge_vect * (1. / ((double)edge_pnum));
	vertexA = x[A];
	vertexB = x[pnum];
	lines.resize(edge_pnum + 1);

	for (uint i = 0; i < edge_pnum + 1; ++i)
	{
		sect_vect = vertexA - vertexB;
		LineStart = vertexB - sect_vect;
		sect_vect = sect_vect * (1. / ((double)section_pnum));
		next_p = LineStart + n * height; // сразу подняли на высоту height над треугольником
		lines[i].clear();
		for (uint j = 0; j < section_pnum*3; ++j)
		{
			lines[i].push_back(next_p);
			next_p = next_p + sect_vect;
		}
		lines[i].push_back(next_p); // чтобы добавить конец прямой, лежащей на ребре
		vertexA = vertexA + edge_vect;
	}
}

bool triangle_parameters::build(ulint el_num, uint k, const triangle *elem_buff, const VectorXD<double,3> *points)
{	elnum = el_num;
	uint A, B;
	uint edge_nums[2];
	A = (k+1)%3; // две оставшиеся точки, локальные номера которых
	B = (k+2)%3;	// не совпадают с номером k.
	edge_nums[0] = A;/*std::max(A,B);*/
	edge_nums[1] = B;/*std::min(A,B);*/
	try {
		// определяем второй вектор базиса (направленный вдоль ребра)
		trg_num = el_num;
		r2 = points[ elem_buff[el_num].pnums(edge_nums[1]) ] - points[ elem_buff[el_num].pnums(edge_nums[0]) ];
		t_k = r2.norma(); // длина ребра треугольника
		r2 = r2 / t_k;

		// определяем точку пересечения с противолежащим вершине k ребром и первый вектор направления
		t_star = r2.dotprod(points[ elem_buff[el_num].pnums(k) ] - points[ elem_buff[el_num].pnums(edge_nums[0]) ]);
		x_star = points[ elem_buff[el_num].pnums(edge_nums[0]) ] + r2 * t_star;
		r1 = x_star - points[ elem_buff[el_num].pnums(k) ];
		s_t = r1.norma(); // высота треугольника
		r1 = r1 / s_t;

		// нормаль определяется как векторное произведение
		//x_mult_args[0] = r1; x_mult_args[1] = r2;
		//n = VectorXD<double,3>::vector_mult(x_mult_args,Det);
		fast_vector_mutl(r1,r2,n);

		// узнаем тангенсы углов при высоте треугольника
		tg_a1 = - t_star / s_t;
		tg_a2 = (t_k - t_star) / s_t;

		// выбираем вершину локальной системы координат треугольника
		x0 = points[ elem_buff[el_num].pnums(k) ];
		// выбираем остальные вершины
		x1 = points[ elem_buff[el_num].pnums(edge_nums[0]) ];
		x2 = points[ elem_buff[el_num].pnums(edge_nums[1]) ];

		// теперь строим матрицу преобразования из локальной системы координат в глобальную
		for(uint i=0; i<3; i++)
		{	// столбцы - это вектора ортогональной системы координат (r1, r2, n)
			S.block[i][0] = r1.coords[i];
			S.block[i][1] = r2.coords[i];
			S.block[i][2] = n.coords[i];
		}

		// Строим так же матрицу преобразования из локальных координат в координаты численного интегрирования (L2 и L3 координаты
		// L1 - координата соответствует точке x0)
		VectorXD<double,3> acc;
		GaussMtr.block[0][0] = GaussMtr.block[0][1] = GaussMtr.block[0][2] = 1.0;
		acc = get_local_coordinate_point(x0);
		GaussMtr.block[1][0] = acc.coords[ 0 ];
		GaussMtr.block[2][0] = acc.coords[ 1 ];
		acc = get_local_coordinate_point(x1);
		GaussMtr.block[1][1] = acc.coords[ 0 ];
		GaussMtr.block[2][1] = acc.coords[ 1 ];
		acc = get_local_coordinate_point(x2);
		GaussMtr.block[1][2] = acc.coords[ 0 ];
		GaussMtr.block[2][2] = acc.coords[ 1 ];
		LMtr = GaussMtr.inverse(); // а это матрица уже для получения L-координат в базовой точке
		// умножение на нее вектора в локальной системе координат, лежащего в плоскости нашего треугольника
		// с предварительной заменой последней координаты по нормали на ЕДИНИЦУ и перемещении ее на ПЕРВОЕ МЕСТО
		reversed = false; // по умолчанию "кручения", т. е. инвертирования нормали нет.
	}
	catch(...)
	{
		printf("Msg=BAD MEMORY ALLOCATION!!!\n");
		return false;
	}
	return true;
}
bool triangle_parameters::build2(ulint el_num, uint k, const triangle *elem_buff, const VectorXD<double,3> *points)
{
	elnum = el_num;
	uint A, B;
	uint edge_nums[2];
	A = (k+1)%3; // две оставшиеся точки, локальные номера которых
	B = (k+2)%3;	// не совпадают с номером k.
	edge_nums[0] = std::min(A,B);
	edge_nums[1] = std::max(A,B);
	try {
		// определяем второй вектор базиса (направленный вдоль ребра)
		trg_num = el_num;
		r2 = points[ elem_buff[el_num].pnums(edge_nums[1]) ] - points[ elem_buff[el_num].pnums(edge_nums[0]) ];
		t_k = r2.norma(); // длина ребра треугольника
		r2 = r2 / t_k;

		// определяем точку пересечения с противолежащим вершине k ребром и первый вектор направления
		t_star = r2.dotprod(points[ elem_buff[el_num].pnums(k) ] - points[ elem_buff[el_num].pnums(edge_nums[0]) ]);
		x_star = points[ elem_buff[el_num].pnums(edge_nums[0]) ] + r2 * t_star;
		r1 = x_star - points[ elem_buff[el_num].pnums(k) ];
		s_t = r1.norma(); // высота треугольника
		r1 = r1 / s_t;

		// нормаль определяется как векторное произведение
		//x_mult_args[0] = r1; x_mult_args[1] = r2;
		//n = VectorXD<double,3>::vector_mult(x_mult_args,Det);
		fast_vector_mutl(r1,r2,n);

		// узнаем тангенсы углов при высоте треугольника
		tg_a1 = - t_star / s_t;
		tg_a2 = (t_k - t_star) / s_t;

		// выбираем вершину локальной системы координат треугольника
		x0 = points[ elem_buff[el_num].pnums(k) ];
		// выбираем остальные вершины
		x1 = points[ elem_buff[el_num].pnums(edge_nums[0]) ];
		x2 = points[ elem_buff[el_num].pnums(edge_nums[1]) ];

		// теперь строим матрицу преобразования из локальной системы координат в глобальную
		for(uint i=0; i<3; i++)
		{	// столбцы - это вектора ортогональной системы координат (r1, r2, n)
			S.block[i][0] = r1.coords[i];
			S.block[i][1] = r2.coords[i];
			S.block[i][2] = n.coords[i];
		}

		// Строим так же матрицу преобразования из локальных координат в координаты численного интегрирования (L2 и L3 координаты
		// L1 - координата соответствует точке x0)
		VectorXD<double,3> acc;
		GaussMtr.block[0][0] = GaussMtr.block[0][1] = GaussMtr.block[0][2] = 1.0;
		acc = get_local_coordinate_point(x0);
		GaussMtr.block[1][0] = acc.coords[ 0 ];
		GaussMtr.block[2][0] = acc.coords[ 1 ];
		acc = get_local_coordinate_point(x1);
		GaussMtr.block[1][1] = acc.coords[ 0 ];
		GaussMtr.block[2][1] = acc.coords[ 1 ];
		acc = get_local_coordinate_point(x2);
		GaussMtr.block[1][2] = acc.coords[ 0 ];
		GaussMtr.block[2][2] = acc.coords[ 1 ];
		LMtr = GaussMtr.inverse(); // а это матрица уже для получения L-координат в базовой точке
		// умножение на нее вектора в локальной системе координат, лежащего в плоскости нашего треугольника
		// с предварительной заменой последней координаты по нормали на ЕДИНИЦУ и перемещении ее на ПЕРВОЕ МЕСТО
		reversed = false; // по умолчанию "кручения", т. е. инвертирования нормали нет.
	}
	catch(...)
	{
		printf("Msg=BAD MEMORY ALLOCATION!!!\n");
		return false;
	}
	return true;
}
void triangle_parameters::reverse_normal()
{
	this->n = n * (-1.);
	for(uint i=0; i<3; i++)
		S.block[i][2] *= -1.;
}
// В глобальной системе координат (через точки Гаусса)
VectorXD<double,3> triangle_parameters::get_Global_point(VectorXD<double,2> Gauss_point) const
{
	VectorXD<double,3> ret;
	ret = x0 +
		(x1 - x0) * Gauss_point.coords[0] +
		(x2 - x0) * Gauss_point.coords[1];
	return ret;
}

VectorXD<double,3> triangle_parameters::get_global_coordinate_point(VectorXD<double,3> local_point) const
{
	VectorXD<double,3> ret;
	ret.common_init(0.0);
	ret = S * local_point + x0;
	return ret;
}

VectorXD<double,3> triangle_parameters::get_global_coordinate_vector(VectorXD<double,3> local_vector) const
{
    VectorXD<double,3> ret;
    ret.common_init(0.0);
    //ret = S * local_vector;
    //ret.coords[0] = vect.dotprod(r1);
    //ret.coords[1] = vect.dotprod(r2);
    //ret.coords[2] = vect.dotprod(n);
    ret = r1 * local_vector.coords[0] + r2 * local_vector.coords[1] + n * local_vector.coords[2];
    return ret;
}

VectorXD<complex<double>,3> triangle_parameters::get_global_coordinate_vector(VectorXD<complex<double>,3> local_vector) const
{
    VectorXD<complex<double>,3> ret;
    ret.common_init(0.0);
    for (int coordIndex = 0; coordIndex < 3; ++coordIndex)
    {
        ret.coords[coordIndex] += r1.coords[coordIndex] * local_vector.coords[0] +
                r2.coords[coordIndex] * local_vector.coords[1] +
                n.coords[coordIndex] * local_vector.coords[2];
    }
    return ret;
}

// выдает координаты точки безотносительно начала координат (преобразует векторное поле)
VectorXD<std::complex<double>,3> triangle_parameters::get_global_coordinate_point
    (VectorXD<std::complex<double>,3> local_point) const
{
	VectorXD<double,3> re, im;
	VectorXD<std::complex<double>,3> ret;
	std::complex<double> I(0.,1.);
	re.coords[0] = local_point.coords[0].real(); im.coords[0] = local_point.coords[0].imag();
	re.coords[1] = local_point.coords[1].real(); im.coords[1] = local_point.coords[1].imag();
	re.coords[2] = local_point.coords[2].real(); im.coords[2] = local_point.coords[2].imag();
	re = S * re; im = S * im;
	for(uint i=0; i<3; i++)
		ret.coords[i] = re.coords[i] + I * im.coords[i];
	return ret;
}

VectorXD<double,3> triangle_parameters::get_local_coordinate_point(VectorXD<double,3> global_point) const
{
	VectorXD<double,3> ret, vect;
	ret.common_init(0.0);
	vect = global_point - x0;
	ret.coords[0] = vect.dotprod(r1);
	ret.coords[1] = vect.dotprod(r2);
	ret.coords[2] = vect.dotprod(n);
	return ret;
}

VectorXD<double,3> triangle_parameters::get_local_coordinate_vector(VectorXD<double,3> vect) const
{
	VectorXD<double,3> ret;
	ret.common_init(0.0);
	ret.coords[0] = vect.dotprod(r1);
	ret.coords[1] = vect.dotprod(r2);
	ret.coords[2] = vect.dotprod(n);
	return ret;
}

VectorXD<complex<double>,3> triangle_parameters::get_local_coordinate_vector(VectorXD<complex<double>,3> vect) const
{
    VectorXD<complex<double>,3> ret;
    ret.common_init(0.0);
    ret.coords[0] = fast_dot_prod(vect, r1);
    ret.coords[1] = fast_dot_prod(vect, r2);
    ret.coords[2] = fast_dot_prod(vect, n);
    return ret;
}

VectorXD<double,2> triangle_parameters::get_Gauss_point( VectorXD<double,3> Local_point ) const
{
	VectorXD<double,3> acc;
	VectorXD<double,2> ret;
	acc.coords[ 0 ] = 1.0;
	acc.coords[ 1 ] = Local_point.coords[ 0 ];
	acc.coords[ 2 ] = Local_point.coords[ 1 ];
	Local_point = GaussMtr * acc; // сюда передана копия вектора, исходный вектор остался не тронутый, исходные данные затерли без потерь
	ret.coords[ 0 ] = Local_point.coords[ 1 ];
	ret.coords[ 1 ] = Local_point.coords[ 2 ];
	return ret;
}

// ВАЖНО!!!
// Заметим важную деталь. В этих выражениях могут выбираться точки Гаусса, которые лежат на ребре
// треугольника (точнее лежат их отображения в глобальные координаты). Эти координаты допустимо использовать
// только для вычисления базисных функций в этих точках (это нужно для численного получения внешнего интеграла)
VectorXD<double,2> triangle_parameters::find_projection_point( VectorXD<double,3> Local_arg ) const
{
	VectorXD<double,3> arg;
	VectorXD<double,3> L;
	VectorXD<double,2> Gauss_ret;
	arg.coords[ 0 ] = 1.0;
	arg.coords[ 1 ] = Local_arg.coords[ 0 ];
	arg.coords[ 2 ] = Local_arg.coords[ 1 ];
	L = LMtr * arg;
	// Точки Гаусса однозначно определяются ПОСЛЕДНИМИ двумя L-координатами
	for(uint i=0; i<2; i++)
		Gauss_ret.coords[ i ] = L.coords[ i+1 ];
//	// если точка находится внутри, то все L координаты положительны
//	if(L.coords[ 0 ] > 0 && L.coords[ 1 ] > 0 && L.coords[ 2 ] > 0)
//		return Gauss_ret;
//	// Если сразу две L - координаты отрицательных, значит проекционной точкой будет вершина треугольника
//	if(L.coords[1] < 0 && L.coords[2] < 0 && std::min(abs(L.coords[1]),abs(L.coords[2])) > 1e-6)
//	{
//		Gauss_ret.common_init(0.);
//		return Gauss_ret;
//	}
//	if(L.coords[0] < 0 && L.coords[2] < 0 && std::min(abs(L.coords[0]),abs(L.coords[2])) > 1e-6)
//	{
//		Gauss_ret.coords[0] = 1.;
//		Gauss_ret.coords[1] = 0.;
//		return Gauss_ret;
//	}
//	if(L.coords[0] < 0 && L.coords[1] < 0 && std::min(abs(L.coords[0]),abs(L.coords[1])) > 1e-6)
//	{
//		Gauss_ret.coords[0] = 0.;
//		Gauss_ret.coords[1] = 1.;
//		return Gauss_ret;
//	}
//	// Иначе, должна быть такая пара положительных L координат, которые определят ребро треугольника
//	// на котором должна лежать проекция точки
//	if(L.coords[ 0 ] < 0 && abs(L.coords[0]) > 1e-6)
//	{
//		sum = L.coords[ 1 ] + L.coords[ 2 ];
//		Gauss_ret.coords[ 0 ] /= sum; Gauss_ret.coords[ 1 ] /= sum;
//	}
//	else // во всех остальных случаях, точка находится на границе
//	if(L.coords[ 1 ] < 0 && abs(L.coords[1]) > 1e-6)
//	{
//		sum = L.coords[ 0 ] + L.coords[ 2 ];
//		Gauss_ret.coords[ 0 ] = 0.; Gauss_ret.coords[ 1 ] /= sum;
//	}
//	else
//	if(L.coords[ 2 ] < 0 && abs(L.coords[2]) > 1e-6)
//	{
//		sum = L.coords[ 0 ] + L.coords[ 1 ];
//		Gauss_ret.coords[ 0 ] /= sum; Gauss_ret.coords[ 1 ] = 0.;
//	}
	return Gauss_ret;
}

double triangle_parameters::Dist(const VectorXD<double,3> & arg) const
{
    VectorXD<double,3> pointsA[3];
    double dist[3];
    pointsA[0] = x0;
    pointsA[1] = x1;
    pointsA[2] = x2;
    for(uint j=0; j<3; ++j)
    {
        dist[j] = (arg - pointsA[j]).norma();
    }
    double ret = *(min_element(dist,dist+3));
    return ret;
}

double triangle_parameters::Diam() const
{
    double edges[3];
    edges[0] = (x1 - x0).norma();
    edges[1] = (x2 - x0).norma();
    edges[2] = (x1 - x2).norma();
    double maxVal = *(max_element(edges, edges+3));
    return maxVal;
}
