#include "Single_Layer_Potential.hpp"

VectorXD<complex<double>,3> Single_Layer_Potential::EvalfScalarPotVCurl(bool is_singular,ScalarBasisInterface *f,VectorXD<double,2> gauss_points,triangle_parameters &Ty)
{
    VectorXD<complex< double >,3> sum;
    sum.common_init(0.);
    VectorXD<double,3> y;
    VectorXD<double,3> basis_value;
    this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
    //
    zero_flag = sqrt(fix_params.t_k * fix_params.s_t / 2.) * NON_SCALED_ZERO;
    // x и y треугольники совпадают или находятся достаточно близко
    auto yGlob = Ty.get_Global_point( gauss_points );
    y = fix_params.get_local_coordinate_point( yGlob );
    if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
        return ScalarPotEvalfCurl(f, y, Ty); // x и y треугольники в этом случае достаточно далеко друг от друга
    else
    {
        basis_value = B_curl(f,y); // слишком часто производим одно и то же действие, оптимизируй!
        // вычисляем аналитическую часть этого потенциала
        basis_value = basis_value * r_1componentV2global(yGlob);//r_1component( y );
        for(int i=0; i<3; ++i)
            sum.coords[i] = basis_value.coords[i];
        // добавка, которая считается численно
        if (!is_linear)
            sum = sum + NumericPartEvaluterScalarCurl(f, y, Ty);
    }
    return sum;
}
VectorXD<complex<double>,3> Single_Layer_Potential::ScalarPotEvalfCurl(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &Ty)
{
    VectorXD<complex<double>,3> ret, acc;
    ret = 0.;
    complex< double > area, mult;
    VectorXD<double,2> gauss_points;
    area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
    ret = 0.;
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        // в этих точках вычисляем интегрируемую функцию
        acc = ScalarFunctCurl(f, y, gauss_points) * area;
        // в отличие от area, параметр весов w может менятся на итерациях
        mult = tgr.w; acc = acc * mult;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}
VectorXD<complex<double>,3> Single_Layer_Potential::ScalarFunctCurl(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<complex<double>,3> res;
        VectorXD<double,3> curl;
	scalCache_elem fxCache = f->get_cache_value(GaussPoints);
	//
	curl = fxCache.glob_curl;
	// точки и расстояния
	VectorXD<double,3> x, x_y;
	double x_y_lenght;
	x = fxCache.loc_arg;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
	res.coords[0] = curl.coords[0] * exp_part;
        res.coords[1] = curl.coords[1] * exp_part;
        res.coords[2] = curl.coords[2] * exp_part;
	return res;
}
VectorXD<complex<double>,3> Single_Layer_Potential::NumericPartEvaluterScalarCurl(ScalarBasisInterface *f, VectorXD<double,3> y, triangle_parameters &Ty)
{
    VectorXD<complex<double>,3> ret, acc;
    ret = 0.;
    // параметры решения
    complex< double > area, mult;
    VectorXD<double,2> gauss_points;
    area = (f->current_parameter.s_t * f->current_parameter.t_k) * 0.5;
    saved_curl = B_curl(f,y);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        // в этих точках вычисляем интегрируемую функцию
        acc = AdditionalScalarFunctionCurl(f, y, gauss_points) * area;
        // в отличие от area, параметр весов w может менятся на итерациях
        mult = tgr.w; acc = acc * mult;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}
VectorXD<complex<double>,3> Single_Layer_Potential::AdditionalScalarFunctionCurl(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> gauss_points)
{
	VectorXD<complex<double>,3> curl_x, curl_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	VectorXD<complex<double>,3> res;
        res.common_init(0.);
	scalCache_elem fxCache;
	fxCache = f->get_cache_value(gauss_points);
        for(int i=0; i<3; ++i)
        {
            curl_y.coords[i] = saved_curl.coords[i];
            curl_x.coords[i] = fxCache.glob_curl.coords[i];
        }
	// точки и расстояния
	VectorXD<double,3> x, x_y;
	double x_y_lenght;
	// переводим в локальные координаты треугольника точки аргументов
	x = fxCache.loc_arg;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero_flag)
        {
            res = curl_x * (-vave_num_k) / (4 * PI);
            return res;
        }
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
	res = curl_x * exp_part;
      	// разрешение особенности с функцией
        for(uint i=0; i<3; ++i)
            res.coords[i] = res.coords[i] + (curl_x.coords[i] - curl_y.coords[i]) / (x_y_lenght * 4 * PI);
	return res;
}
complex<double> Single_Layer_Potential::EvalfScalarPotVsubdivisions(double epsilon, ScalarBasisInterface *f,
    VectorXD<double,2> gauss_points, triangle_parameters &Ty, int N, GaussRule grRude, GaussRule grFine)
{
    auto yGlobal = Ty.get_Global_point( gauss_points );
    auto yLocal = f->current_parameter.get_local_coordinate_point(yGlobal);

    triangle_parameters current_parameter;
    complex<double> rude_value, refined_value;
    before_sectioning.clear();
    after_sectioning.clear();
    before_sectioning.push_back(f->current_parameter);
    zero_flag = sqrt(before_sectioning[0].t_k * before_sectioning[0].s_t / 2.) * epsilon / N;
    double discrepancy = 1.0;
    complex<double> saved_values(0.0, 0.0), ret(0.0, 0.0), ret_refined(0.0, 0.0);
    complex<double> fullSum = 0.0;
    int iter = 0;
    for(; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();
            if (current_parameter.is_point_inside(yGlobal, NON_SCALED_ZERO))
            {
                rude_value = EvalfScalarPotVpartial(grRude, f,
                    gauss_points, Ty, current_parameter);
                refined_value = EvalfScalarPotVpartial(grFine, f,
                    gauss_points, Ty, current_parameter);
            }
            else
            {
                rude_value = ScalarPotEvalfPartial(current_parameter, grRude, f, yLocal);
                refined_value = ScalarPotEvalfPartial(current_parameter, grFine, f, yLocal);
            }
            complex<double> testRet = saved_values + rude_value;
            complex<double> testRetRefined = saved_values + refined_value;
            ret += rude_value;
            ret_refined += refined_value;
            if (!iter)
            {
                fullSum = testRetRefined;
            }
            double cur_disc = abs(testRetRefined - testRet)/abs(fullSum);
            if(cur_disc >= epsilon && abs(rude_value) >= zero_flag && abs(refined_value) >= zero_flag)  // если для данного фрагмента разбиения погрешность слишком высока, то необходимо внести разбиения в этот элемент
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values += refined_value;
        }
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        fullSum = ret_refined;
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();
    return ret_refined;
}

complex<double> Single_Layer_Potential::EvalfScalarPotVpartial(GaussRule gr, ScalarBasisInterface *f,
    VectorXD<double,2> gauss_points, triangle_parameters &Ty, triangle_parameters &TxPartial)
{
    complex<double> sum(0.0, 0.0);
    VectorXD<double,3> y;
    fix_params = TxPartial;
    // x и y треугольники совпадают или находятся достаточно близко
    auto yGlobal = Ty.get_Global_point( gauss_points );
    y = fix_params.get_local_coordinate_point( yGlobal );
    VectorXD<double,3> yLoc = f->current_parameter.get_local_coordinate_point(yGlobal);

    VectorXD<double,2> gradient = f->f_gradient( yLoc );
    VectorXD<double,3> gradientLocal;
    gradientLocal.coords[0] = gradient.coords[0];
    gradientLocal.coords[1] = gradient.coords[1];
    gradientLocal.coords[2] = 0.0;
    // переводим вектор градиента из локальных координат треугольника-носителя в локальные координаты
    // треугольника подразбиения
    VectorXD<double,3> globGrad = f->current_parameter.get_global_coordinate_vector(gradientLocal);
    VectorXD<double,3> fixLocGrad = fix_params.get_local_coordinate_vector(globGrad);

    VectorXD<double,3> vect_Int;
    vect_Int.coords[1] = A1_Value(y, 1.);
    vect_Int.coords[0] = A2_Value(y, 1.);
    vect_Int.coords[2] = 0.0;
    double vm = fixLocGrad.dotprod(vect_Int); // учитываем градиентные составляющие базисной функции
    // вычисляем аналитическую часть этого потенциала
    sum = f->f_value( yLoc ) * r_1componentV2global(yGlobal);//r_1component( y );
    sum += vm;

    // добавка, которая считается численно
    sum = sum + NumericPartEvaluterScalarPartial(f, yLoc, gr);
    return sum;
}

// Вычисление скалярного потенциала простого слоя (для скалярных базисных функций)
complex<double> Single_Layer_Potential::EvalfScalarPotV(bool is_singular,ScalarBasisInterface *f,VectorXD<double,2> gauss_points, triangle_parameters &Ty)
{
    complex< double > sum; sum = 0.;
    VectorXD<double,3> y;
    double basis_value;
    this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
    VectorXD<double,2> gradient;
    VectorXD<double,2> vect_Int;
    double vm;
    //
    zero_flag = sqrt(fix_params.t_k * fix_params.s_t / 2.) * NON_SCALED_ZERO;
    // x и y треугольники совпадают или находятся достаточно близко
    auto yGlobal = Ty.get_Global_point( gauss_points );
    y = fix_params.get_local_coordinate_point( yGlobal );
    if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
            return ScalarPotEvalf(f, y, Ty); // x и y треугольники в этом случае достаточно далеко друг от друга
    else
    {
            basis_value = f->f_value( y ); // слишком часто производим одно и то же действие, оптимизируй!
            gradient = f->f_gradient( y );
            vect_Int.coords[1] = A1_Value(y, 1.);
            vect_Int.coords[0] = A2_Value(y, 1.);
            vm = gradient.dotprod(vect_Int); // учитываем градиентные составляющие базисной функции
            // вычисляем аналитическую часть этого потенциала
            basis_value = basis_value * r_1componentV2global(yGlobal);//r_1component( y );
            sum = basis_value;
            sum += vm;
            // добавка, которая считается численно
            sum = sum + NumericPartEvaluterScalar(f, y, Ty);
    }
    return sum;
}
complex<double> Single_Layer_Potential::NumericPartEvaluterScalar(ScalarBasisInterface *f, VectorXD<double,3> y, triangle_parameters  &Ty)
{
	complex<double> ret, acc;
	ret = 0.;
	// параметры решения
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
        f->f_value( y );
        f->InterpGrad = f->f_gradient( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalScalarFunction(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		mult = tgr.w; acc = acc * mult;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
complex<double> Single_Layer_Potential::NumericPartEvaluterScalarPartial(ScalarBasisInterface *f,
    VectorXD<double,3> y, GaussRule gr)
{
    complex<double> ret, acc;
    ret = 0.;
    // параметры решения
    complex< double > area, mult;
    VectorXD<double,2> gauss_points;
    area = fix_params.s_t * fix_params.t_k * 0.5;
    f->f_value( y );
    f->InterpGrad = f->f_gradient( y );
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        VectorXD<double,3> x_loc = f->current_parameter.get_local_coordinate_point(x);
        // в этих точках вычисляем интегрируемую функцию
        acc = AdditionalScalarFunction(f, y, x_loc) * area;
        // в отличие от area, параметр весов w может менятся на итерациях
        mult = tgr.w; acc = acc * mult;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}
complex<double> Single_Layer_Potential::AdditionalScalarFunction(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x)
{
        VectorXD<double,2> grad_y;
	complex<double> f_x, f_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	complex<double> res(0.,0.);
	f_y = f->f_value(y);
    grad_y = f->f_gradient(y);
	f_x = f->f_value(x);
	// точки и расстояния
	VectorXD<double,3> x_y;
	double x_y_lenght;
	// переводим в локальные координаты треугольника точки аргументов
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero_flag)
        {
            res = f_x * (-vave_num_k) / (4. * PI);
            return res;
        }
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
	res = f_x * exp_part;
        double GradMult;
	GradMult = 0.;
	// умножаем ее на вектор разницы координат
	GradMult = grad_y.coords[0]*x_y.coords[0] + grad_y.coords[1]*x_y.coords[1];
	// разрешение особенности с функцией
        res += (f_x - f_y - GradMult) / (x_y_lenght * 4 * PI);
	return res;
}

complex<double> Single_Layer_Potential::AdditionalScalarFunction(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> gauss_points)
{
        VectorXD<double,2> grad_y;
	complex<double> f_x, f_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	complex<double> res(0.,0.);
	scalCache_elem fxCache;
	fxCache = f->get_cache_value(gauss_points);
	f_y = f->f_value(y);
    grad_y = f->f_gradient(y);
	f_x = fxCache.value;
	// точки и расстояния
	VectorXD<double,3> x, x_y;
	double x_y_lenght;
	// переводим в локальные координаты треугольника точки аргументов
	x = fxCache.loc_arg;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero_flag)
        {
            res = f_x * (-vave_num_k) / (4. * PI);
            return res;
        }
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
	res = f_x * exp_part;
        double GradMult;
	GradMult = 0.;
	// умножаем ее на вектор разницы координат
	GradMult = grad_y.coords[0]*x_y.coords[0] + grad_y.coords[1]*x_y.coords[1];
	// разрешение особенности с функцией
        res += (f_x - f_y - GradMult) / (x_y_lenght * 4 * PI);
	return res;
}
complex<double> Single_Layer_Potential::ScalarPotEvalf(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &Ty)
{
	complex<double> ret, acc;
	ret = 0.;
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	ret = 0.;
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            // в этих точках вычисляем интегрируемую функцию
            acc = ScalarFunct(f, y, gauss_points) * area;
            // в отличие от area, параметр весов w может менятся на итерациях
            mult = tgr.w; acc = acc * mult;
            // добавляем новое слагаемое к результату
            ret = ret + acc;
	}
	return ret;
}
complex<double> Single_Layer_Potential::ScalarFunct(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	complex<double> res;
	scalCache_elem fxCache = f->get_cache_value(GaussPoints);
	//
	res = fxCache.value;
	// точки и расстояния
	VectorXD<double,3> x, x_y;
	double x_y_lenght;
	x = fxCache.loc_arg;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
	res = res * exp_part;
	return res;
}
complex<double> Single_Layer_Potential::ScalarPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, ScalarBasisInterface *f, VectorXD<double,3> &y)
{
	complex<double> ret, acc;
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
        fix_params = Tk;
	area = fix_params.s_t * fix_params.t_k * 0.5;
	ret = 0.;
        TrigGaussRuleInfo tgr(gr);
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
            x = f->current_parameter.get_local_coordinate_point(x);
            // в этих точках вычисляем интегрируемую функцию
            acc = ScalarFunctPartial(f, y, x) * area;
            // в отличие от area, параметр весов w может менятся на итерациях
            mult = tgr.w; acc = acc * mult;
            // добавляем новое слагаемое к результату
            ret = ret + acc;
	}
	return ret;
}
complex<double> Single_Layer_Potential::ScalarFunctPartial(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x)
{
	complex<double> res;
	//
	res = f->f_value(x);
	// точки и расстояния
	VectorXD<double,3> x_y;
	double x_y_lenght;
	x_y = x - y;
	x_y_lenght = x_y.norma();
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
	res = res * exp_part;
	return res;
}
//------------------------------------------------------------------------------
// Вычисление векторных потенциалов
//------------------------------------------------------------------------------

double Single_Layer_Potential::r_1componentV2(const VectorXD<double,3> &local_coords)
{
    VectorXD<double,3> loc_vert[3];
    loc_vert[0] = fix_params.get_local_coordinate_point(fix_params.x0);
    loc_vert[1] = fix_params.get_local_coordinate_point(fix_params.x1);
    loc_vert[2] = fix_params.get_local_coordinate_point(fix_params.x2);
    for(int i=0; i<3; ++i)
        loc_vert[i] = loc_vert[i] - local_coords;
    BemPotentialData data = CalcPotentialNew(loc_vert);
    return data.SingleLayer;
}

double Single_Layer_Potential::r_1componentV2global(const VectorXD<double,3> &global_coords) const
{
    VectorXD<double,3> loc_vert[3];
    loc_vert[0] = fix_params.x0;
    loc_vert[1] = fix_params.x1;
    loc_vert[2] = fix_params.x2;
    for(int i=0; i<3; ++i)
        loc_vert[i] = loc_vert[i] - global_coords;
    BemPotentialData data = CalcPotentialNew(loc_vert);
    return data.SingleLayer;
}

BemPotentialData Single_Layer_Potential::CalcPotentialNew(VectorXD<double,3> p[]) const
{
    VectorXD<double,3> t[3];
    double l[3];
    for (int i = 0; i < 3; i++)
    {
        t[i] = p[(i + 1) % 3] - p[i];
        l[i] = t[i].norma();
    }

    VectorXD<double,3> n;
    fast_vector_mutl(t[0],t[1],n);
    n.normalize();

    VectorXD<double,3> b [3];
    double R[3], x[3], y[3];

    for (int i = 0; i < 3; i++)
    {
        t[i] = t[i] / l[i];
	fast_vector_mutl(t[i], n, b[i]);
        R[i] = p[i].norma();
        x[i] = p[i].dotprod(b[i]);
        y[i] = p[i].dotprod(t[i]);
    }

    double h = abs(p[0].dotprod(n));
    double S = -sign(p[0].dotprod(n));

    double int_slp = 0, int_dlp = 0;
    VectorXD<double,3> int_slp_grad, int_dlp_grad;
    int_dlp_grad.common_init(0.);
    int_slp_grad.common_init(0.);

    for (int i = 0; i < 3; i++)
    {
        double Rn = R[(i + 1) % 3];
        double yn = y[i] + l[i];
        complex<double> ln_arg((yn + Rn) / (y[i] + R[i]));
        complex<double> ln = log(ln_arg); // в оригинале были var - переменные, вполне возможно, для комплексных значений, если для действительных логарифм не определён
        double atan_arg_1(x[i] * yn / (x[i] * x[i] + h * (h + Rn))), atan_arg_2(x[i] * y[i] / (x[i] * x[i] + h * (h + R[i])));
        complex<double> tan = atan(atan_arg_1)
                - atan(atan_arg_2);

        complex<double> tmp = 0.0;
        {
            ln = (::isnan((double)ln.imag()) ||
                  ::isinf((double)ln.imag()) ||
                  ::isnan((double)ln.real()) ||
                  ::isinf((double)ln.real())) ? 0.0 : ln;
            tan = (::isnan((double)tan.imag()) ||
                   ::isinf((double)tan.imag()) ||
                   ::isnan((double)tan.real()) ||
                   ::isinf((double)tan.real())) ? 0.0 : tan;
            int_slp += (x[i] * ln - h * tan).real();
            tmp += x[i] * x[i] * ln;
        }
        if (h > zero_flag && fabs(y[i] + R[i]) > zero_flag)
        {
            int_dlp += tan.real();
            int_dlp_grad = int_dlp_grad - b[i] * (h * ln.real());
            tmp += h * h * ln;
        }
        int_slp_grad = int_slp_grad + b[i] * (yn * Rn * 0.5 - y[i] * R[i] * 0.5 + tmp.real() * 0.5);
    }

    BemPotentialData ret;
    ret.SingleLayer = int_slp / (4. * PI);
    ret.SingleLayerVec = int_slp_grad - n * (int_slp * S * h); ret.SingleLayerVec = ret.SingleLayerVec / (4.*PI);
    ret.DoubleLayer = S * int_dlp; ret.DoubleLayer /= (4.*PI);
    ret.DoubleLayerVec = int_dlp_grad * S - n * (int_dlp * h); // Нормальная компонента считается криво...
    ret.DoubleLayerVec = ret.DoubleLayerVec / (4.*PI);
    return ret;
}

complex<double> Single_Layer_Potential::EvalfLambdaPot(bool is_singular, VectorBasisInterface* f, VectorXD<double,3> glob_y_arg, triangle_parameters &Sx)
{
	complex< double > sum; sum = 0.;
	VectorXD<double,3> y, loc_nx;
	double normal_lam_proj;
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
        Block<double,2> gradient;
        VectorXD<double,2> vect_Int, vm;
	//
	zero_flag = sqrt(fix_params.t_k * fix_params.s_t / 2.) * NON_SCALED_ZERO;
	// x и y треугольники совпадают или находятся достаточно близко
	y = fix_params.get_local_coordinate_point( glob_y_arg );
        loc_nx = fix_params.get_local_coordinate_vector( Sx.n );
	if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
		return SinglePotentialLambdaEvalf(f, y, loc_nx); // x и y треугольники в этом случае достаточно далеко друг от друга
	else
	{
		normal_lam_proj = f->lambda( y ).dotprod(loc_nx); // слишком часто производим одно и то же действие, оптимизируй!
                gradient = f->gradient_lambda( y );
                vect_Int.coords[1] = A1_Value(y, 1.);
                vect_Int.coords[0] = A2_Value(y, 1.);
                vm = gradient * vect_Int;
		// вычисляем аналитическую часть этого потенциала
		normal_lam_proj = normal_lam_proj * r_1componentV2(y);//r_1component( y );
		sum = normal_lam_proj;
                sum += vm.coords[0] * loc_nx.coords[0] + vm.coords[1] * loc_nx.coords[1];
		// добавка, которая считается численно
		sum = sum + NumericPartEvaluterLambda(f, y, loc_nx);
	}
	return sum;
}

complex<double> Single_Layer_Potential::NumericPartEvaluterLambda(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> loc_nx)
{
	complex<double> ret, acc;
	ret = 0.;
	// параметры решения
	complex< double > area;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalFunctionLambda(f, y, gauss_points, loc_nx) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}

complex< double > Single_Layer_Potential::AdditionalFunctionLambda(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints, VectorXD<double,3> loc_nx)
{
        Block<double,2> grad_y;
    	complex<double> f_x, f_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	complex<double> res;
	f_x = f->lambda(GaussPoints).dotprod(loc_nx); // функция внутри области
	f_y = f->lambda( y ).dotprod(loc_nx);		 // функция аналитически продолжается за носитель
        grad_y = f->gradient_lambda( y );
	// точки и расстояния
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	// переводим в локальные координаты треугольника точки аргументов
	x = f->get_Local_point( GaussPoints );
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if(x_y_lenght < zero_flag)
	{
		res = f_x * (-vave_num_k) / (4 * PI);
		return res;
	}
        VectorXD<double,3> MatrixMult;
	MatrixMult.common_init(0.);
	// умножаем ее на вектор разницы координат
	for(uint i=0; i<2; i++)
		for(uint j=0; j<2; j++)
			MatrixMult.coords[ i ] += grad_y.block[ i ][ j ] * x_y.coords[ j ];
	// разрешение особенности с экспонентой
	res = (f_x * exp( -x_y_lenght * vave_num_k ) - f_y - MatrixMult.dotprod(loc_nx)) / (x_y_lenght * 4 * PI);
	return res;
}

complex< double > Single_Layer_Potential::SinglePotentialLambdaEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> loc_nx)
{
    	complex<double> ret, acc;
	ret = 0.;
	// параметры решения
	complex< double > area;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = LambdaPotFunct(f, y, gauss_points, loc_nx) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}

complex<double> Single_Layer_Potential::LambdaPotFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints, VectorXD<double,3> loc_nx)
{
    	complex<double> res;
	res = f->lambda(GaussPoints).dotprod(loc_nx); // функция внутри области
	// точки и расстояния
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	// переводим в локальные координаты треугольника точки аргументов
	x = f->get_Local_point( GaussPoints );
	x_y = x - y;
	x_y_lenght = x_y.norma();
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
	res = res * exp_part;
	return res;
}

double Single_Layer_Potential::s_t_grad(double alpha, double s, double t_y, double s_y, double u_y) const
{
    double a = sqrt(1 + alpha * alpha);
    double p = (alpha * t_y + s_y) / (a * a);
    double q_2 = u_y * u_y + pow(t_y - alpha * s_y, 2) / (a * a);
    double s_vave = s - p;
    double C = q_2 / (a * a);
    double ret;
    ret = a * 0.5 * s_vave * sqrt(fabs(s_vave * s_vave + C));
    if( fabs(C) < zero_flag)
        return ret;
    else
    {
        double testLog = a * 0.5 * C * log(fabs(s_vave + sqrt(s_vave * s_vave + C)) );
        double zeroProbe = testLog * 0.0;
        if (!(zeroProbe < 1.0)) // C << s_vave - в этому случае возможно вычисление логарифма от нуля
            return ret;
        ret += testLog;
    }
    return ret;
}
// то же самое, что и предыдущая функция, только квадрат вторичный (т. е. по t а не по s)
double Single_Layer_Potential::A2_Value(VectorXD<double,3> &local_coords, double A_const) const
{
	double s_y, t_y, u_y;
	double s_x, t_star, t_x, alpha2, alpha1;
	double res=0.0;
	if (abs(A_const) < zero_flag)
		return 0.;
	s_x = fix_params.s_t;
	t_x = fix_params.t_k;
	t_star = fix_params.t_star;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	// интеграл разбивается по аддитивности (берем по t а не по s)
	if (abs(alpha1) > NON_SCALED_ZERO) // тангенс угла величина безразмерная, с нулём сравниваем величины одной размерности с расстоянием
	{
            res += s_t_grad(0., 0., s_y - s_x, t_y, u_y) -
                    s_t_grad(0., alpha1*s_x, s_y - s_x, t_y, u_y);
            res -= s_t_grad(1./alpha1, 0., s_y, t_y, u_y) -
                    s_t_grad(1./alpha1, alpha1*s_x, s_y, t_y, u_y);

	}
	//То же самое для второго прямоугольного треугольника (c alpha2)
	if (abs(alpha2) > NON_SCALED_ZERO)
	{
            res += s_t_grad(0., alpha2*s_x, s_y - s_x, t_y, u_y) -
                    s_t_grad(0., 0., s_y - s_x, t_y, u_y);
            res -= s_t_grad(1./alpha2, alpha2*s_x, s_y, t_y, u_y) -
                    s_t_grad(1./alpha2, 0., s_y, t_y, u_y);
	}
	return res * A_const / (4 * PI);
}

double Single_Layer_Potential::A1_Value(VectorXD<double,3> &local_coords, double A_const) const
{
	double s_y, t_y, u_y;
	double s_x, alpha2, alpha1;
	double res;
	if (abs(A_const) < zero_flag)
		return 0.;
	s_x = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
        res = s_t_grad(alpha2, s_x, t_y, s_y, u_y) -
                s_t_grad(alpha2, 0, t_y, s_y, u_y);
        res+=-s_t_grad(alpha1, s_x, t_y, s_y, u_y)+
                s_t_grad(alpha1, 0, t_y, s_y, u_y);
	return res * A_const / (4 * PI);
}

std::complex<double> Single_Layer_Potential::EvalfSingleLayerScalarPotSections(double epsilon,
    std::function<double(const VectorXD<double,3> &arg)> vectorBasisFunction,
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const triangle_parameters & localFunctionSupport, const VectorXD<double,3> & yGlobal,
    int numberOfSubdivision, GaussRule grRude, GaussRule grFine) const
{
    triangle_parameters current_parameter;
    std::complex<double> rude_value, refined_value;
    before_sectioning.clear();
    after_sectioning.clear();
    before_sectioning.push_back(localFunctionSupport);
    zero_flag = sqrt(before_sectioning[0].t_k * before_sectioning[0].s_t / 2.) * epsilon / numberOfSubdivision;
    double discrepancy = 1.0;
    std::complex<double> saved_values = 0.0, ret = 0.0, ret_refined = 0.0;
    std::complex<double> fullSum = 0.0;
    int iter = 0;
    for(; iter<numberOfSubdivision && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();

            bool singularCondition = current_parameter.Dist(yGlobal) < 2.0 * current_parameter.Diam();
            if (singularCondition)
            {
                rude_value = EvalfSingleLayerScalarPotPartial(grRude, vectorBasisFunction, gradBasisFunction,
                    yGlobal, current_parameter);
                refined_value = EvalfSingleLayerScalarPotPartial(grFine, vectorBasisFunction, gradBasisFunction,
                    yGlobal, current_parameter);
            }
            else
            {
                rude_value = ScalarPotEvalfPartial(current_parameter, grRude, vectorBasisFunction, yGlobal);
                refined_value = ScalarPotEvalfPartial(current_parameter, grFine, vectorBasisFunction, yGlobal);
            }
            std::complex<double> testRet = saved_values + rude_value;
            std::complex<double> testRetRefined = saved_values + refined_value;
            ret = ret + rude_value;
            ret_refined = ret_refined + refined_value;
            if (!iter)
            {
                fullSum = testRetRefined;
            }
            double cur_disc = abs((testRetRefined - testRet))/abs(fullSum);
            if(cur_disc >= epsilon && abs(rude_value) >= zero_flag
                    && abs(refined_value) >= zero_flag)
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values = saved_values + refined_value;
        }
        discrepancy = abs(ret - ret_refined) / abs(ret_refined);
        fullSum = ret_refined;
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();
    return ret_refined;
}

VectorXD<complex<double>,3> Single_Layer_Potential::EvalfSingleLayerVectorPotSections(double epsilon,
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> vectorBasisFunction,
    std::function<Block<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const triangle_parameters & localFunctionSupport, const VectorXD<double,3> & yGlobal,
    int numberOfSubdivision, GaussRule grRude, GaussRule grFine) const
{
    triangle_parameters current_parameter;
    VectorXD<complex<double>,3> rude_value, refined_value;
    before_sectioning.clear();
    after_sectioning.clear();
    before_sectioning.push_back(localFunctionSupport);
    zero_flag = sqrt(before_sectioning[0].t_k * before_sectioning[0].s_t / 2.) * epsilon / numberOfSubdivision;
    double discrepancy = 1.0;
    VectorXD<complex<double>,3>::set_scalar_zero(0.0);
    VectorXD<complex<double>,3> saved_values, ret, ret_refined;
    VectorXD<complex<double>,3> fullSum;
    saved_values.zerofied();
    ret.zerofied();
    ret_refined.zerofied();
    fullSum.zerofied();
    int iter = 0;
    for(; iter<numberOfSubdivision && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();

            bool singularCondition = current_parameter.Dist(yGlobal) < 2.0 * current_parameter.Diam();
            if (singularCondition)
            {
                rude_value = EvalfSingleLayerVectorPotPartial(grRude, vectorBasisFunction, gradBasisFunction,
                    yGlobal, current_parameter);
                refined_value = EvalfSingleLayerVectorPotPartial(grFine, vectorBasisFunction, gradBasisFunction,
                    yGlobal, current_parameter);
            }
            else
            {
                rude_value = VectorPotEvalfPartial(current_parameter, grRude, vectorBasisFunction, yGlobal);
                refined_value = VectorPotEvalfPartial(current_parameter, grFine, vectorBasisFunction, yGlobal);
            }
            VectorXD<complex<double>,3> testRet = saved_values + rude_value;
            VectorXD<complex<double>,3> testRetRefined = saved_values + refined_value;
            ret = ret + rude_value;
            ret_refined = ret_refined + refined_value;
            if (!iter)
            {
                fullSum = testRetRefined;
            }
            double cur_disc = abs((testRetRefined - testRet).norma())/abs(fullSum.norma());
            if(cur_disc >= epsilon && abs(rude_value.norma()) >= zero_flag
                    && abs(refined_value.norma()) >= zero_flag)
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values = saved_values + refined_value;
        }
        discrepancy = abs((ret - ret_refined).norma()) / abs(ret_refined.norma());
        fullSum = ret_refined;
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();
    return ret_refined;
}

VectorXD<complex<double>,3> Single_Layer_Potential::EvalfSingleLayerVectorPotSections(double epsilon, VectorBasisInterface *f,
    VectorXD<double,2> gauss_points, triangle_parameters &Ty, int N, GaussRule grRude, GaussRule grFine)
{
    auto yGlobal = Ty.get_Global_point( gauss_points );
    auto yLocal = f->current_parameter.get_local_coordinate_point(yGlobal);

    triangle_parameters current_parameter;
    VectorXD<complex<double>,3> rude_value, refined_value;
    before_sectioning.clear();
    after_sectioning.clear();
    before_sectioning.push_back(f->current_parameter);
    zero_flag = sqrt(before_sectioning[0].t_k * before_sectioning[0].s_t / 2.) * epsilon / N;
    double discrepancy = 1.0;
    VectorXD<complex<double>,3>::set_scalar_zero(0.0);
    VectorXD<complex<double>,3> saved_values, ret, ret_refined;
    VectorXD<complex<double>,3> fullSum;
    saved_values.zerofied();
    ret.zerofied();
    ret_refined.zerofied();
    fullSum.zerofied();
    int iter = 0;
    for(; iter<N && discrepancy >= epsilon; ++iter)// iter - определяет глубину дробления discrepancy - погрешность вычисления всего интеграла
    {
        ret = saved_values;
        ret_refined = saved_values;
        while(!before_sectioning.empty())
        {
            current_parameter = before_sectioning.back();
            before_sectioning.pop_back();

            bool singularCondition = current_parameter.Dist(yGlobal) < 2.0 * current_parameter.Diam();
            if (singularCondition)
            {
                rude_value = EvalfSingleLayerVectorPotPartial(grRude, f,
                    gauss_points, Ty, current_parameter);
                refined_value = EvalfSingleLayerVectorPotPartial(grFine, f,
                    gauss_points, Ty, current_parameter);
            }
            else
            {
                rude_value = VectorPotEvalfPartial(current_parameter, grRude, f, yLocal);
                refined_value = VectorPotEvalfPartial(current_parameter, grFine, f, yLocal);
            }
            VectorXD<complex<double>,3> testRet = saved_values + rude_value;
            VectorXD<complex<double>,3> testRetRefined = saved_values + refined_value;
            ret = ret + rude_value;
            ret_refined = ret_refined + refined_value;
            if (!iter)
            {
                fullSum = testRetRefined;
            }
            double cur_disc = abs((testRetRefined - testRet).norma())/abs(fullSum.norma());
            if(cur_disc >= epsilon && abs(rude_value.norma()) >= zero_flag
                    && abs(refined_value.norma()) >= zero_flag)
                current_parameter.Build_Sections(after_sectioning);
            else
                saved_values = saved_values + refined_value;
        }
        discrepancy = abs((ret - ret_refined).norma()) / abs(ret_refined.norma());
        fullSum = ret_refined;
        if(discrepancy >= epsilon)
        {
            before_sectioning = after_sectioning;
            after_sectioning.clear();
        }
    }
    before_sectioning.clear();
    after_sectioning.clear();
    return ret_refined;
}

std::complex<double> Single_Layer_Potential::EvalfSingleLayerScalarPotPartial(GaussRule gr, 
    std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const VectorXD<double,3> & yGlobal, triangle_parameters &TxPartial) const
{
    std::complex<double> sum = 0.0;
    fix_params = TxPartial;

    double lam_y = basisFunction( yGlobal );
    lam_y = lam_y * r_1componentV2global( yGlobal );
    VectorXD<double,3> gradient = gradBasisFunction( yGlobal );
    
    VectorXD<double,3> y = fix_params.get_local_coordinate_point( yGlobal );
    VectorXD<double,3>::set_scalar_zero(0.0);
    VectorXD<double,3> vect_Int;
    vect_Int.coords[1] = A1_Value(y, 1.);
    vect_Int.coords[0] = A2_Value(y, 1.);
    vect_Int.coords[2] = 0.0;
    
    VectorXD<double,3> global_vect_Int = TxPartial.get_global_coordinate_vector(vect_Int);
    double vm = fast_dot_prod(gradient, global_vect_Int);
    sum = lam_y  + vm;
    // добавка, которая считается численно
    std::complex<double> numericPart = NumericScalarPartEvaluterPartial(basisFunction, gradBasisFunction, yGlobal, gr);
    sum = sum + numericPart;
    return sum;
}

VectorXD<complex<double>,3> Single_Layer_Potential::EvalfSingleLayerVectorPotPartial(GaussRule gr, 
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> basisFunction,
    std::function<Block<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const VectorXD<double,3> & yGlobal, triangle_parameters &TxPartial) const
{
    VectorXD<complex<double>,3>::set_scalar_zero(0.0);
    Block<complex<double>,3>::set_zero(0.0);
    Block<complex<double>,3>::set_one(1.0);
    
    VectorXD<complex<double>,3> sum;
    sum.zerofied();    
    fix_params = TxPartial;

    VectorXD<double,3> lam_y = basisFunction( yGlobal );
    lam_y = lam_y * r_1componentV2global( yGlobal );
    Block<double,3> gradient = gradBasisFunction( yGlobal );
    
    VectorXD<double,3> y = fix_params.get_local_coordinate_point( yGlobal );
    VectorXD<double,3>::set_scalar_zero(0.0);
    VectorXD<double,3> vect_Int;
    vect_Int.coords[1] = A1_Value(y, 1.);
    vect_Int.coords[0] = A2_Value(y, 1.);
    vect_Int.coords[2] = 0.0;
    
    VectorXD<double,3> global_vect_Int = TxPartial.get_global_coordinate_vector(vect_Int);
    VectorXD<double,3> vm = gradient * global_vect_Int;
    for(uint i=0; i<3; i++) // из double-вектора в complex< double >-вектор
        sum.coords[ i ] = lam_y.coords[ i ];
    for(uint i=0; i<3; i++)
        sum.coords[ i ] += vm.coords[ i ];
    // добавка, которая считается численно
    VectorXD<complex<double>,3> numericPart = NumericPartEvaluterPartial(basisFunction, gradBasisFunction, yGlobal, gr);
    sum = sum + numericPart;
    return sum;
}

VectorXD<complex<double>,3> Single_Layer_Potential::EvalfSingleLayerVectorPotPartial(GaussRule gr, VectorBasisInterface *f,
    VectorXD<double,2> gauss_points, triangle_parameters &Ty, triangle_parameters &TxPartial)
{
    VectorXD<complex<double>,3> sum;
    sum.zerofied();

    fix_params = TxPartial;
    // x и y треугольники совпадают или находятся достаточно близко
    auto yGlobal = Ty.get_Global_point( gauss_points );    
    auto yLoc = f->current_parameter.get_local_coordinate_point(yGlobal);
    
    VectorXD<double,3> lam_y = f->lambda(yLoc); // слишком часто производим одно и то же действие, оптимизируй!
    // вычисляем аналитическую часть этого потенциала
    lam_y = lam_y * r_1componentV2global( yGlobal );
    Block<double,2> gradient = f->gradient_lambda(yLoc);
    VectorXD<double,2>::set_scalar_zero(0.0);
    VectorXD<double,3> vect_Int;
    
    VectorXD<double,3> y = fix_params.get_local_coordinate_point( yGlobal );
    vect_Int.coords[1] = A1_Value(y, 1.);
    vect_Int.coords[0] = A2_Value(y, 1.);
    vect_Int.coords[2] = 0.0;
    VectorXD<double,3> globVInt = TxPartial.get_global_coordinate_vector(vect_Int);
    VectorXD<double,3> localFVInt = f->current_parameter.get_local_coordinate_vector(globVInt);
    VectorXD<double,2> vm;
    vm.zerofied();
    for (int i=0; i<2; ++i)
        for (int j=0; j<2; ++j)
            vm.coords[i] += gradient.block[i][j] * localFVInt.coords[j];
    for(uint i=0; i<3; i++) // из double-вектора в complex< double >-вектор
            sum.coords[ i ] = lam_y.coords[ i ];
    for(uint i=0; i<2; i++)
            sum.coords[ i ] += vm.coords[ i ];
    // добавка, которая считается численно    
    VectorXD<complex<double>,3> numericPart = NumericPartEvaluterPartial(f, yLoc, gr);
    sum = sum + numericPart;
    return sum;
}

std::complex<double> Single_Layer_Potential::NumericScalarPartEvaluterPartial(
    std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const VectorXD<double,3> & y, GaussRule gr) const
{
    std::complex<double> ret = 0.0;
    VectorXD<double,2> gauss_points;
    complex<double> area = fix_params.s_t * fix_params.t_k * 0.5;
    auto f_y = basisFunction( y );
    auto grad_y = gradBasisFunction( y );
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        // в этих точках вычисляем интегрируемую функцию
        std::complex<double> acc = 0.0;
        
	auto f_x = basisFunction(x);
	VectorXD<double,3> x_y = x - y;
	double x_y_lenght = x_y.norma();
	if (x_y_lenght >= zero_flag)
	{
            // разрешение особенности с экспонентой
            complex<double> exp_part;
            exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
            acc = f_x * exp_part;
            double MatrixMult = fast_dot_prod(grad_y, x_y);
            // разрешение особенности с функцией
            acc += (f_x - f_y - MatrixMult) / (x_y_lenght * 4 * PI);
	}
        else
        {
            acc = f_x * (-vave_num_k) / (4 * PI);
        }        
        // в отличие от area, параметр весов w может менятся на итерациях
        acc = acc * area * tgr.w;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

VectorXD<complex<double>,3> Single_Layer_Potential::NumericPartEvaluterPartial(
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> basisFunction,
    std::function<Block<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
    const VectorXD<double,3> & y, GaussRule gr) const
{
    VectorXD<complex<double>,3> ret;
    ret = 0.0;
    VectorXD<double,2> gauss_points;
    complex<double> area = fix_params.s_t * fix_params.t_k * 0.5;
    auto f_y = basisFunction( y );
    auto grad_y = gradBasisFunction( y );
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        // в этих точках вычисляем интегрируемую функцию
        VectorXD<complex<double>,3> acc;
        acc.zerofied();
        
	auto f_x = basisFunction(x);
	VectorXD<double,3> x_y = x - y;
	double x_y_lenght = x_y.norma();
	if (x_y_lenght >= zero_flag)
	{
            // разрешение особенности с экспонентой
            complex<double> exp_part;
            exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
            for (int i=0; i<3; ++i)
                acc.coords[i] = f_x.coords[i] * exp_part;
            VectorXD<double,3> MatrixMult = grad_y * x_y;
            // разрешение особенности с функцией
            for(uint i=0; i<3; i++)
                acc.coords[ i ] += (f_x.coords[ i ] - f_y.coords[ i ] - MatrixMult.coords[ i ]) / (x_y_lenght * 4 * PI);
	}
        else
        {
            for (int i=0; i<3; ++i)
                acc.coords[ i ] = f_x.coords[ i ] * (-vave_num_k) / (4 * PI);
        }        
        // в отличие от area, параметр весов w может менятся на итерациях
        acc = acc * area * tgr.w;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

VectorXD<complex<double>,3> Single_Layer_Potential::NumericPartEvaluterPartial(VectorBasisInterface *f,
    VectorXD<double,3> y, GaussRule gr)
{
    VectorXD<complex<double>,3> ret, acc;
    ret = 0.;
    // параметры решения
    complex< double > area, mult;
    VectorXD<double,2> gauss_points;
    area = fix_params.s_t * fix_params.t_k * 0.5;
    f->InterpValue = f->lambda( y );
    f->InterpGrad = f->gradient_lambda( y );
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        x = f->current_parameter.get_local_coordinate_point(x);
        // в этих точках вычисляем интегрируемую функцию
        acc = AdditionalFunctionPartial(f, y, x);
        // в отличие от area, параметр весов w может менятся на итерациях
        mult = tgr.w * area; acc = acc * mult;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

// Учитывает градиентную составляющую для большей гладкости решения
VectorXD<complex<double>,3> Single_Layer_Potential::AdditionalFunctionPartial(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x)
{
	VectorXD<double,3> func_x, func_y;	  // для перевода векторных значений в комплексный тип
        Block<double,2> grad_y;
	VectorXD<complex<double>,3> f_x, f_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	VectorXD<complex<double>,3> res;
	func_y = f->lambda( y );	 // функция аналитически продолжается за носитель
        grad_y = f->gradient_lambda(y);
	func_x = f->lambda(x);
	for(uint i = 0; i < 3; i++)
	{
		f_x.coords[ i ] = func_x.coords[ i ];
		f_y.coords[ i ] = func_y.coords[ i ];
	}
	// точки и расстояния
	VectorXD<double,3> x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	// переводим в локальные координаты треугольника точки аргументов
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero_flag)
	{
		res = f_x * (-vave_num_k) / (4 * PI);
		return res;
	}
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
	res = f_x * exp_part;
        VectorXD<double,3> MatrixMult;
	MatrixMult.common_init(0.);
	// умножаем ее на вектор разницы координат
	for(uint i=0; i<2; i++)
		for(uint j=0; j<2; j++)
			MatrixMult.coords[ i ] += grad_y.block[ i ][ j ] * x_y.coords[ j ];
	// разрешение особенности с функцией
	for(uint i=0; i<3; i++)
		res.coords[ i ] += (f_x.coords[ i ] - f_y.coords[ i ] - MatrixMult.coords[ i ]) / (x_y_lenght * 4 * PI);
	return res;
}

VectorXD<complex<double>,3> Single_Layer_Potential::VectorPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, 
    std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> basisFunction,
    const VectorXD<double,3> &y) const
{    
    VectorXD<double,2> gauss_points;
    fix_params = Tk;
    complex<double> area = fix_params.s_t * fix_params.t_k * 0.5;
    VectorXD<complex<double>,3> ret;
    ret = 0.0;
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        const VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        // в этих точках вычисляем интегрируемую функцию
        VectorXD<complex<double>,3> acc;
        acc = basisFunction(x);
        
        // точки и расстояния
        VectorXD<double,3> x_y = x - y;
        double x_y_lenght = x_y.norma();
        
        // разрешение особенности с экспонентой
        complex<double> exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
        acc = acc * exp_part;

        // в отличие от area, параметр весов w может менятся на итерациях
        acc = acc * tgr.w * area;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

std::complex<double> Single_Layer_Potential::ScalarPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, 
    std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    const VectorXD<double,3> &y) const
{    
    VectorXD<double,2> gauss_points;
    fix_params = Tk;
    complex<double> area = fix_params.s_t * fix_params.t_k * 0.5;
    std::complex<double> ret;
    ret = 0.0;
    TrigGaussRuleInfo tgr(gr);
    while( tgr.next() ) // цикл по числу точек Гаусса
    {   // точки Гаусса (L - координаты)
        gauss_points.coords[ 0 ] = tgr.e.e1;
        gauss_points.coords[ 1 ] = tgr.e.e2;
        const VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
        // в этих точках вычисляем интегрируемую функцию
        std::complex<double> acc = basisFunction(x);
        
        // точки и расстояния
        VectorXD<double,3> x_y = x - y;
        double x_y_lenght = x_y.norma();
        
        // разрешение особенности с экспонентой
        complex<double> exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
        acc = acc * exp_part;

        // в отличие от area, параметр весов w может менятся на итерациях
        acc = acc * tgr.w * area;
        // добавляем новое слагаемое к результату
        ret = ret + acc;
    }
    return ret;
}

VectorXD<complex<double>,3> Single_Layer_Potential::VectorPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, VectorBasisInterface *f, VectorXD<double,3> &y)
{
	VectorXD<complex<double>,3> ret, acc;
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
        fix_params = Tk;
	area = fix_params.s_t * fix_params.t_k * 0.5;
	ret = 0.;
        TrigGaussRuleInfo tgr(gr);
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
            gauss_points.coords[ 0 ] = tgr.e.e1;
            gauss_points.coords[ 1 ] = tgr.e.e2;
            VectorXD<double,3> x = fix_params.get_Global_point(gauss_points);
            x = f->current_parameter.get_local_coordinate_point(x);
            // в этих точках вычисляем интегрируемую функцию
            acc = VectorFunctPartial(f, y, x) * area;
            // в отличие от area, параметр весов w может менятся на итерациях
            mult = tgr.w; acc = acc * mult;
            // добавляем новое слагаемое к результату
            ret = ret + acc;
	}
	return ret;
}
VectorXD<complex<double>,3> Single_Layer_Potential::VectorFunctPartial(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x)
{
    VectorXD<complex<double>,3> res;
    //
    res = f->lambda(x);
    // точки и расстояния
    VectorXD<double,3> x_y;
    double x_y_lenght;
    x_y = x - y;
    x_y_lenght = x_y.norma();
    // разрешение особенности с экспонентой
    complex<double> exp_part;
    exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
    res = res * exp_part;
    return res;
}

// Интерфейсные функции вычисления потенциала простого слоя.
// Векторный потенциал простого слоя
VectorXD<complex<double>,3> Single_Layer_Potential::EvalfSingleLayerVectorPot2(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	complex<double> zero; zero = 0.;
        Block<double,2> gradient;
        VectorXD<double,2> vect_Int, vm;
	VectorXD<complex< double >,3> sum; sum.common_init(zero);
	VectorXD<double,3> y, lam_y;
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
	//
	zero_flag = sqrt(fix_params.t_k * fix_params.s_t / 2.) * NON_SCALED_ZERO;
	// x и y треугольники совпадают или находятся достаточно близко
	auto yGlob = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
	y = fix_params.get_local_coordinate_point( yGlob );
	if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
		return SinglePotentialEvalf(f, y, y_params); // x и y треугольники в этом случае достаточно далеко друг от друга
	else
	{
		lam_y = f->lambda( y ); // слишком часто производим одно и то же действие, оптимизируй!
		// вычисляем аналитическую часть этого потенциала
		lam_y = lam_y * r_1componentV2global( yGlob );
                gradient = f->gradient_lambda( y );
                vect_Int.coords[1] = A1_Value(y, 1.);
                vect_Int.coords[0] = A2_Value(y, 1.);
                vm = gradient * vect_Int;
		for(uint i=0; i<3; i++) // из double-вектора в complex< double >-вектор
			sum.coords[ i ] = lam_y.coords[ i ];
                for(uint i=0; i<2; i++)
                        sum.coords[ i ] += vm.coords[ i ];
		// добавка, которая считается численно
		sum = sum + NumericPartEvaluter2(f, y, y_params); // !!!! ОШИБКА !!!
	}
	return sum;
}
// Функции, которые вычисляют численные добавки к интегралу потенциала простого слоя,
// а так же к стабилизирующей добавке
VectorXD<complex<double>,3> Single_Layer_Potential::NumericPartEvaluter2(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex<double> zero;
	zero = 0.;
	VectorXD<complex<double>,3> ret, acc;
	ret.common_init(zero);
	// параметры решения
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	//this->fix_params = f->current_parameter;
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
        f->lambda( y );
        f->InterpGrad = f->gradient_lambda( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalFunction2(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		mult = tgr.w; acc = acc * mult;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
// Учитывает градиентную составляющую для большей гладкости решения
VectorXD<complex<double>,3> Single_Layer_Potential::AdditionalFunction2(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<double,3> func_x, func_y;	  // для перевода векторных значений в комплексный тип
        Block<double,2> grad_y;
	VectorXD<complex<double>,3> f_x, f_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	VectorXD<complex<double>,3> res;
	vectCache_elem fxCache;
	//func_x = f->lambda(GaussPoints);
	fxCache = f->get_cache_value(GaussPoints);//f->lambda(GaussPoints); // функция внутри области
	func_y = f->InterpValue;	//f->lambda( y );	 // функция аналитически продолжается за носитель
        grad_y = f->InterpGrad;
	func_x = fxCache.lam_val;
	for(uint i = 0; i < 3; i++)
	{
		f_x.coords[ i ] = func_x.coords[ i ];
		f_y.coords[ i ] = func_y.coords[ i ];
	}
	// точки и расстояния
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	// переводим в локальные координаты треугольника точки аргументов
	x = fxCache.loc_arg;		 //f->get_Local_point( GaussPoints );
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero_flag)
	{
		res = f_x * (-vave_num_k) / (4 * PI);
		return res;
	}
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
	res = f_x * exp_part;
        VectorXD<double,3> MatrixMult;
	MatrixMult.common_init(0.);
	// умножаем ее на вектор разницы координат
	for(uint i=0; i<2; i++)
		for(uint j=0; j<2; j++)
			MatrixMult.coords[ i ] += grad_y.block[ i ][ j ] * x_y.coords[ j ];
	// разрешение особенности с функцией
	for(uint i=0; i<3; i++)
		res.coords[ i ] += (f_x.coords[ i ] - f_y.coords[ i ] - MatrixMult.coords[ i ]) / (x_y_lenght * 4 * PI);
	return res;
}
// Готовые формулы были взяты у [Rjasanow_S.,_Steinbach_O.]_The_fast_solution_of_b(BookFi.org)
double Single_Layer_Potential::F_r_1(double s, double alpha, VectorXD<double,3> &local_coords)
{
	double res = 0.;
	double s_x, t_x, u_x;
	s_x = local_coords.coords[0];
	t_x = local_coords.coords[1];
	u_x = local_coords.coords[2];
	// первое слагаемое интеграла (формула была получена интегрированием по частям)
	double sqrt_log_arg, p, q;
	p = ( alpha * t_x + s_x ) / (1 + alpha * alpha);
	q = sqrt( u_x * u_x + pow(t_x - alpha * s_x,2) / (1 + alpha * alpha) );
	sqrt_log_arg = sqrt( (1 + alpha * alpha) * pow(s - p, 2) + q * q);
	double log_arg = abs(alpha * s - t_x + sqrt_log_arg);
	if (abs(s - s_x) < zero_flag || abs(log_arg) < zero_flag) // исследование особенности логарифма V log(V) -> 0 при V -> 0
		res -= s;
	else
		res += (s - s_x) * log( log_arg ) - s;
	// I
	double mult = (alpha * s_x - t_x) / sqrt( 1 + alpha*alpha );
	log_arg = abs(sqrt( 1 + alpha*alpha ) * (s - p) + sqrt_log_arg);
	// иналогично условию выше, исследуем особенность логарифма
	if (abs(mult) >= zero_flag && abs(log_arg) >= zero_flag)
		res += mult * log( log_arg );
	// в противном случае ноль
	// II
	double arc_arg, A1;
	if(abs(s - p) < zero_flag)
	{	// в этом случае замены переменных в выражении дают ноль
		A1 = 2 * alpha * q;
		if( !(abs(u_x) < zero_flag) ) // вычисляем предел арктангеса
		{
                    res += 2 * u_x * atan( A1 / (2 * u_x));
                }
	}
	else
	{
		arc_arg = (q - (alpha * s_x - t_x) / (1 + alpha * alpha)) * sqrt_log_arg + (alpha * s - t_x - q) * q;
		arc_arg /= 0.5 * (s - p);
		if( !(abs(u_x) < zero_flag) ) // вычисляем предел арктангеса
		{
			arc_arg /= 2 * u_x;
			res += 2 * u_x * atan( arc_arg );//
		}
	}
	// сразу делим на 4 * PI;
	return res;
}
double Single_Layer_Potential::r_1component(VectorXD<double,3> &local_coords)
{
	double s_t, alpha2, alpha1;
	double res;
	s_t = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
//	res = F_r_1(s_t,alpha2,local_coords) - F_r_1(0.,alpha2,local_coords);
//	res -= F_r_1(s_t,alpha1,local_coords) - F_r_1(0.,alpha1,local_coords);
        res = (F_r_1(s_t,alpha2,local_coords) + F_r_1(0.,alpha1,local_coords)) - (F_r_1(0.,alpha2,local_coords) + F_r_1(s_t,alpha1,local_coords));
	res /= 4 * PI;
	return res;
}
// Функции, которые вычисляют численные добавки к интегралу потенциала простого слоя,
// а так же к стабилизирующей добавке
VectorXD<complex<double>,3> Single_Layer_Potential::NumericPartEvaluter(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex<double> zero;
	zero = 0.;
	VectorXD<complex<double>,3> ret, acc;
	ret.common_init(zero);
	// параметры решения
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	//this->fix_params = f->current_parameter;
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalFunction(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		mult = tgr.w; acc = acc * mult;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
// Функция добавки для разрешения особенностей первого порядка (при численном интегрировании)
// Так как оператор применяется для билинейных форм, относительно следа Дирихле, то используются функции lambda
VectorXD<complex<double>,3> Single_Layer_Potential::AdditionalFunction(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<double,3> func_x, func_y;	  // для перевода векторных значений в комплексный тип
	VectorXD<complex<double>,3> f_x, f_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	VectorXD<complex<double>,3> res;
	vectCache_elem fxCache;
	//func_x = f->lambda(GaussPoints);
	fxCache = f->get_cache_value(GaussPoints);//f->lambda(GaussPoints); // функция внутри области
	func_y = f->InterpValue;	//f->lambda( y );	 // функция аналитически продолжается за носитель
	func_x = fxCache.lam_val;
	for(uint i = 0; i < 3; i++)
	{
		f_x.coords[ i ] = func_x.coords[ i ];
		f_y.coords[ i ] = func_y.coords[ i ];
	}
	// точки и расстояния
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	// переводим в локальные координаты треугольника точки аргументов
	x = fxCache.loc_arg;		 //f->get_Local_point( GaussPoints );
	x_y = x - y;
	x_y_lenght = x_y.norma();
        // градениентная часть (внутренний интеграл от линейных функций теперь считается точно)
//        Block<double,2> grad_lam;
//        VectorXD<double,3> grad_val;
//        grad_val.common_init(0.);
//        grad_lam = f->InterpGrad;
//        for(uint i=0; i<2; ++i)
//            for(uint j=0; j<2; ++j)
//                grad_val.coords[0] += grad_lam.block[i][j] * x_y.coords[j];
	if (x_y_lenght < zero_flag)
	{
            res = f_x * (-vave_num_k) / (4 * PI);
            return res;
	}
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = (exp( -x_y_lenght * vave_num_k ) - 1.) / (x_y_lenght * 4 * PI);
	res = f_x * exp_part;
	// разрешение особенности с функцией
	for(uint i=0; i<3; i++)
		res.coords[ i ] += (f_x.coords[ i ] - f_y.coords[ i ] /*- grad_val.coords[i]*/) / (x_y_lenght * 4 * PI);
	return res;
}
complex<double> Single_Layer_Potential::NumericPartEvaluterDiv(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex<double> ret, acc;
	ret = 0.;
	// параметры решения
	complex< double > area;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	//this->fix_params = f->current_parameter;
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalFunctionDiv(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
complex<double> Single_Layer_Potential::AdditionalFunctionDiv(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	complex<double> f_x, f_y; // значение функции в точке и аналитическое продолжение функции за её носитель
	complex<double> res;
	f_x = f->div_F_lam(GaussPoints); // функция внутри области
	f_y = f->div_F_lam( y );		 // функция аналитически продолжается за носитель
	// точки и расстояния
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	// переводим в локальные координаты треугольника точки аргументов
	x = f->get_Local_point( GaussPoints );
	x_y = x - y;
	x_y_lenght = x_y.norma();
	if(x_y_lenght < zero_flag)
	{
		res = f_x * (-vave_num_k) / (4 * PI);
		return res;
	}
	// разрешение особенности с экспонентой
	res = (f_x * exp( -x_y_lenght * vave_num_k ) - f_y) / (x_y_lenght * 4 * PI);
	return res;
}
// Функции, вычисляющие напрямую векторный и скалярный потенциал (примененный к дивергентной функции)
VectorXD<complex<double>,3> Single_Layer_Potential::SinglePotentialEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex<double> zero;
	zero = 0.;
	VectorXD<complex<double>,3> ret, acc;
	ret.common_init(zero);
	// параметры решения
	complex< double > area, mult;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	//this->fix_params = f->current_parameter;
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = SinglePotentFunct(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		mult = tgr.w; acc = acc * mult;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
VectorXD<complex<double>,3> Single_Layer_Potential::SinglePotentFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<double,3> func_x;	  // для перевода векторных значений в комплексный тип
	VectorXD<complex<double>,3> res;
	vectCache_elem fxCache = f->get_cache_value(GaussPoints);
	//
	func_x = fxCache.lam_val;	  // f->lambda(GaussPoints); // функция внутри области
	//
	for(uint i = 0; i < 3; i++)
		res.coords[ i ] = func_x.coords[ i ];
	// точки и расстояния
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	// переводим в локальные координаты треугольника точки аргументов
	//
	x = fxCache.loc_arg;		  //f->get_Local_point( GaussPoints );
	//
	x_y = x - y;
	x_y_lenght = x_y.norma();
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
	res = res * exp_part;
	return res;
}
// Так же вычисляют напрямую, но уже стабилизирующую добавку
complex<double> Single_Layer_Potential::SinglePotentialEvalfDiv(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex<double> ret, acc;
	ret = 0.;
	// параметры решения
	complex< double > area;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	//this->fix_params = f->current_parameter;
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = SinglePotentFunctDiv(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
complex<double> Single_Layer_Potential::SinglePotentFunctDiv(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	complex<double> res;
	res = f->div_F_lam(GaussPoints); // функция внутри области
	// точки и расстояния
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	double x_y_lenght;			  // расстояние между агрументами
	vectCache_elem fxCache = f->get_cache_value(GaussPoints);
	// переводим в локальные координаты треугольника точки аргументов
	x = fxCache.loc_arg;		  //f->get_Local_point( GaussPoints );
	x_y = x - y;
	x_y_lenght = x_y.norma();
	// разрешение особенности с экспонентой
	complex<double> exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k ) / (x_y_lenght * 4 * PI);
	res = res * exp_part;
	return res;
}
// Интерфейсные функции вычисления потенциала простого слоя.
// Векторный потенциал простого слоя
VectorXD<complex<double>,3> Single_Layer_Potential::EvalfSingleLayerVectorPot(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	complex<double> zero; zero = 0.;
	VectorXD<complex< double >,3> sum; sum.common_init(zero);
	VectorXD<double,3> y, lam_y;
        Block<double,2> grad_lam;
        VectorXD<double,2> vect;
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
	//
	zero_flag = sqrt(fix_params.t_k * fix_params.s_t / 2.) * NON_SCALED_ZERO;
	// x и y треугольники совпадают или находятся достаточно близко
	y = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
	y = fix_params.get_local_coordinate_point( y );
	if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
		return SinglePotentialEvalf(f, y, y_params); // x и y треугольники в этом случае достаточно далеко друг от друга
	else
	{
		lam_y = f->lambda( y ); // слишком часто производим одно и то же действие, оптимизируй!
//                grad_lam = f->gradient_lambda( y );
//                f->InterpGrad = grad_lam;
//                vect.coords[0] = A2_Value(y,1.);
//                vect.coords[1] = A1_Value(y,1.);
//                vect = grad_lam * vect;
		// вычисляем аналитическую часть этого потенциала
		lam_y = lam_y * r_1component( y );
		for(uint i=0; i<3; i++) // из double-вектора в complex< double >-вектор
			sum.coords[ i ] = lam_y.coords[ i ];
//                sum.coords[0] += vect.coords[0];
//                sum.coords[1] += vect.coords[1];
		// добавка, которая считается численно
		sum = sum + NumericPartEvaluter(f, y, y_params);
	}
	return sum;
}
// Функция, вычисляющая стабилизирующую добавку
complex<double> Single_Layer_Potential::EvalfSingleLayerPot(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	complex< double > sum; sum = 0.;
	VectorXD<double,3> y;
	double div_lam_y;
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
	//
	zero_flag = sqrt(fix_params.t_k * fix_params.s_t / 2.) * NON_SCALED_ZERO;
	// x и y треугольники совпадают или находятся достаточно близко
	y = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
	y = fix_params.get_local_coordinate_point( y );
	if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
		return SinglePotentialEvalfDiv(f, y, y_params); // x и y треугольники в этом случае достаточно далеко друг от друга
	else
	{
		div_lam_y = f->div_F_lam( y ); // слишком часто производим одно и то же действие, оптимизируй!
		// вычисляем аналитическую часть этого потенциала
		div_lam_y = div_lam_y * r_1componentV2( y );
		sum = div_lam_y;
		// добавка, которая считается численно
		sum = sum + NumericPartEvaluterDiv(f, y, y_params);
	}
	return sum;
}
