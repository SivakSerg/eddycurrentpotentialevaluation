#include "ComplexIntegral.h"

void complex_Integral::set_6_points()
{
    coefs.clear();
    set_pair(0.467913935, 0.238619186);
    set_pair(0.360761573, 0.661209386);
    set_pair(0.171324492, 0.932469514);
}
void complex_Integral::set_function(complex<double> (*f)(double a))
{
    this->func = f;
}
bool complex_Integral::set_pair(double coef, double x)
{
    if (x<0 || x>1)
            return false;
    Leg_pair pair; pair.coef = coef; pair.x = x;
    Leg_pair sym_pair; sym_pair.coef = coef; sym_pair.x = -x;
    coefs.push_back(pair);
    coefs.push_back(sym_pair);
    return true;
}
bool complex_Integral::remove_last_coefs()
{
    if (coefs.size() == 4)
            return false;
    coefs.pop_back();
    coefs.pop_back();
    return true;
}
complex_Integral::complex_Integral()
{
    Leg_pair pair[2];
    pair[0].coef = 0.652145154862546;
    pair[0].x = 0.339981043584856;
    pair[1].coef = 0.347854845137454;
    pair[1].x = 0.861136311594053;
    coefs.push_back(pair[0]);
    coefs.push_back(pair[1]);
    pair[0].x = -pair[0].x;
    pair[1].x = -pair[1].x;
    coefs.push_back(pair[0]);
    coefs.push_back(pair[1]);
}
complex_Integral::~complex_Integral()
{
}
complex<double>  complex_Integral::get_value(double a, double b, int n)
{
    if (n<0 || func == 0)return 0.;
    double h = (b - a) / n, x = a;
    complex<double>  acc = 0.0;
    for (int k = 0; k<n; k++)
    {
            for (unsigned int s = 0; s<coefs.size(); s++)
                    acc += func(x + h / 2. + coefs[s].x*h / 2.)*coefs[s].coef;
            x += h;
    }
    acc *= h / 2.;
    return acc;
}
complex<double>  complex_Integral::get_value(double a, double b, int n, complex_Functor &func)
{
    if (n<0)return 0.;
    double h = (b - a) / n, x = a;
    complex<double>  acc = 0.0;
    for (int k = 0; k<n; k++)
    {
            for (unsigned int s = 0; s<coefs.size(); s++)
                    acc += func(x + h / 2. + coefs[s].x*h / 2.)*coefs[s].coef;
            x += h;
    }
    acc *= h / 2.;
    return acc;
}
complex<double>  complex_Integral::get_value(double a, double b, int n, function<complex<double>(double a)> f)
{
    if (n<0)return 0.;
    double h = (b - a) / n, x = a;
    complex<double>  acc = 0.0;
    for (int k = 0; k<n; k++)
    {
        for (unsigned int s = 0; s<coefs.size(); s++)
            acc += f(x + h / 2. + coefs[s].x*h / 2.)*coefs[s].coef;
        x += h;
    }
    acc *= h / 2.;
    return acc;
}
