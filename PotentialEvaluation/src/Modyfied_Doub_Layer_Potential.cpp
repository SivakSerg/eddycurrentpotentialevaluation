#include "Modyfied_Doub_Layer_Potential.hpp"

double Modyfied_Doub_Layer_Potential::operator()(double uy)
{
    double sy = coefs[0] * ty + coefs[1] * uy + coefs[2];
    // ty - фиксировано, uy - параметр интегрирвания, sy - функция от ty и uy
    VectorXD<double,3> local_coords;
    local_coords.coords[0] = sy;
    local_coords.coords[1] = ty;
    local_coords.coords[2] = uy;
    return r1_3_component(local_coords);
}
double Modyfied_Doub_Layer_Potential::F_arctan_2_mult(double A1,double A2,double arg)
{
    	double disc = A2 * A2 - 4 * A1; // функция в любом случае действительная, она либо гиперболическая, либо триганометрическая
	double res=0.0, x;
	if(disc < 0)
	{
            if(fabs(disc) > zero)
		res = -atan( (2 * arg + A2) / sqrt( -disc ) ) * sqrt(-disc);
	}
	if(disc > 0)
	{
		x = 2 * arg + A2;
		x /= sqrt(disc);
                double arg = fabs((1 + x)/(1 - x));
                if( fabs(arg) > zero )
                    res = -0.5 * log( arg ) * sqrt(disc);
	}
	return res;
}
//------------------------------------------------------------------
//ФУНКЦИИ РАЗРЕШЕНИЯ ЛОГАРИФМИЧЕСКИХ ОСОБЕННОСТЕЙ
//------------------------------------------------------------------
double Modyfied_Doub_Layer_Potential::log_doub_area_int(double V, double L, double D, double C, double x)
{
//    double res2;
//    // определяем вспомогательные константы
//    double A1, A2, u, t, disc;
//    A1 = D * (1 - V) / (1 + V);
//    A2 = 2 * L / (1 + V);
//    u = x;
//    t = u + sqrt(u * u + D);
//    // непосредственно вычисляем интегральное выражение
//    disc = A2 * A2 - 4 * A1;
//    if(abs(disc)<zero)
//    {
//            res2 = t / 2 - D / (2 *t) + log(abs(2 * t + A2)) * ( 2 * D / A2 - A2 / 2) - 2 * D * log(abs(t))/A2; // в этом случае A1 = A2 * A2 / 4 <- корень квардратного выражения
//    }
//    else
//    {	// если нулю равен D, то нулю равен коэффициент A1
//            if(abs(D)>zero)
//            {
//                    res2 = t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) -
//                               log( abs(t*t + A2*t + A1) ) * (A2 / 4 - D * A2 / (4 * A1));
//                    res2+= F_arctan(A1,A2,t) * (2*D + 2*A1 - A2*A2*D/(2*A1) - A2 * A2 / 2);
//            }
//            else
//            {
//                    res2 = t - A2 * log(abs(t + A2));
//            }
//    }
//    res2 *= -1.0 * C;
//    res2 += log( abs(V*u + L + sqrt(u*u+D)) )*u*C;
//    return res2/(4 * PI);
    double res2;
    // определяем вспомогательные константы
    double A1, A2, u, t, disc;
    A1 = D * (1 - V) / (1 + V);
    A2 = 2 * L / (1 + V);
    u = x;
    t = u + sqrt(fabs(u * u + D));
    // непосредственно вычисляем интегральное выражение
    disc = A2 * A2 - 4 * A1;
    // Вначале разберём частные случаи
    if(fabs(D) > zero)
    {
        if(fabs(V - 1) < 1e-8) // случай V == 1 возможен
        {
            if(fabs(t + L) > zero)
            {
                if(fabs(L) > zero)
                    return 0.5 * C * ( (t + L) * log(t + L) - (t + L) ) - D * C * 0.5 * (log(t + L) / t - log(t / (t + L)) / L);
                else
                    return 0.5 * C * ( (t + L) * log(t + L) - (t + L) ) - D * C * 0.5 * (log(t + L) / t + 1/t);
            }
        }
        if(fabs(V + 1) < 1e-8)
        {
            // V == -1
            t = -u + sqrt( fabs(u*u + D) );
            if(fabs(L) > zero)
            {
                double ret = 0.0;
                if(fabs(t + L) > zero)
                {
                    double I = -t + L * log(fabs(t + L)) + (D / L) * log(fabs(t)) - (D / L) * log(fabs(t + L));
                    ret = (log(t + L)*u - 0.5 * I)*C;
                }
                return ret;
            }
            else
            {
                double ret = 0.0;
                if(fabs(t) > zero)
                {
                    ret = u*log(t) + D / (2 * t) + t / 2.;
                }
                return ret*C;
            }

        }
        // в противном случае рассматривается более общий случай
    }
    else
    {
        if(fabs(L) < zero)
        {
            double arg = fabs(V * u + L + sqrt(fabs(u*u + D)));
            if(fabs(arg) < zero)
            {
                if(fabs(u) < zero)
                    return (- 0.5 * t)*C; // Если L = 0, То и аргумент логарифма тоже может быть равен нулю (поэтому случай рассмотрен отдельно)
                else
                    return 0.;
            }
            else
                return (u*log(arg) - 0.5 * t) * C;
        }
        else
        {
            double arg = (V + 1)*t + 2.*L;
            if(fabs(arg) < zero)
                std::runtime_error("Impossible case!");
            else
                return (u*log(arg) - 0.5 * t + L * log( arg ) / (V + 1))*C;
        }
    }
    // более общий случай
    if(abs(disc)<zero)
    {
        //res2 = t / 2 - D / (2 *t) + log(abs(2 * t + A2)) * ( 2 * D / A2 - A2 / 2) - 2 * D * log(abs(t))/A2; // в этом случае A1 = A2 * A2 / 4 <- корень квардратного выражения
        res2 = 0.;
        res2 -= t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) +
                F_arctan_2_mult(A1,A2,t)*(0.5 + D / (2.*A1));
        double arg1, arg2, mult_log;
        mult_log = A2 / 4 - D * A2 / (4 * A1);
        if(fabs(mult_log + u) < zero) // НЕ СМОГ ВЫЧИСЛИТЬ ПРЕДЕЛ АНАЛИТИЧЕСКИ!!! ВЫРАЖАЮ ЕГО ЧИСЛЕННО ОТСТУПОМ ОТ НУЛЯ!
        {
            u += fabs(u / 1e+6) + zero*10;
            // так же возмущаем константу L, т. к. другая может быть равна нулю.
            L += fabs(L / 1e+6) + zero*10;
            D += fabs(D / 1e+6) + zero*10;
            t = u + sqrt(u*u + D);
        }
        arg1 = abs(V*u + L + sqrt(u*u+D));
        arg2 = abs(t*t + A2*t + A1);
        if( arg1 < zero && arg2 < zero && fabs(mult_log + u) < zero)
        {
            return res2 * C;
        }
        else
        {
            double res3 = 0.0;
            if(fabs(arg1) > zero && fabs(arg2) > zero)
                res3 = log( arg1 ) * u + log( arg2 ) * mult_log;
            res2 += res3;
            res2 *= C;
            return res2;
        }
    }
    else
    {	// если нулю равен D, то нулю равен коэффициент A1
            if(abs(D)>zero)
            {
                res2 = 0.;
                res2 -= t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) -
                       log( abs(t*t + A2*t + A1) ) * (A2 / 4 - D * A2 / (4 * A1));
                res2 -= F_arctan_2_mult(A1,A2,t)*(0.5 + D / (2.*A1));
            }
            else
            {
                    res2 = t - A2 * log(abs(t + A2)); // странный момент! разберись, здесь ошибка!
            }
    }
    res2 *= C;
    double res1 = log( abs(V*u + L + sqrt(u*u+D)) )*u*C;
    return res2 + res1;
}
//------------------------------------------------------------------
//ФУНКЦИИ ДЛЯ ВЫЧИСЛЕНИЯ ОСОБЕННОСТЕЙ ТРЕТЬЕГО ПОРЯДКА РАДИУСА
//------------------------------------------------------------------
// Вспомогательные функции, для вычисления компонент (Общие интегралы)
double Modyfied_Doub_Layer_Potential::F_r2_3(double s, double alpha, double a, double b, double c)
{
	double ret, Disc, arg; // Disc - нужен для определения наличия корней полинома, arg - линейное выражение, в случае наличия корня
	Disc = b*b - 4 * a * c;
//	if(abs(Disc)<zero*10)
//	{
//            arg = sqrt(a) * s + b / (2 * sqrt(a));
//            ret = log(abs(arg)) / sqrt(a); // a > 0
//            ret *= sign(arg);
//	}
//	else	// в этом случае корней у полинома нет (подкоренное выражение полложительно)
        arg = fabs( (b/2. + a * s)/sqrt(a) + sqrt( a * s * s + b * s + c ) );
//        if(arg < zero)
//        {
//            s += s / 1e+7 + zero * 10.;
//            a += a / 1e+7 + zero * 10.;
//            b += b / 1e+7 + zero * 10.;
//            c += c / 1e+7 + zero * 10.;
//            arg = fabs( (b/2. + a * s)/sqrt(a) + sqrt( a * s * s + b * s + c ) );
//        }
        ret = 0.0;
        if(arg > zero)
            ret = -1.0 * log( arg ) / sqrt(a);
	return ret;
}

double Modyfied_Doub_Layer_Potential::F_r1_3(double t, double alpha, VectorXD<double,3> &local_coords)
{
	double ret;
	double t_y, s_y, u_y;
	double a, b, c;
	double Disc, arg; // Disc - нужен для определения наличия корней полинома, arg - линейное выражение, в случае наличия корня
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	a = alpha * alpha + 1;
	b = -2 * (t_y * alpha * alpha + s_y * alpha );
	c = (s_y * s_y + t_y * t_y + u_y * u_y)*alpha*alpha;
	Disc = alpha * s_y - t_y; // вычислили выражение, эквивалентное дискриминанту (возвести в квадрат и взять знак минус)
	//if(abs(Disc) < zero)	  // есть корень в наличии (первое слагаемое интегралла)
	//{
	//	arg =  sqrt(a) * t + b / (2 * sqrt(a) );
	//	ret = log(abs(arg)) * abs( alpha ) / sqrt(a); // a > 0
	//	ret *= (t < -b / (2 * a) )?1.0:-1.0;
	//}
	//else
        double test_arg_1 = a * t * t + b * t + c;
        double test_arg_2 = fabs( (b / 2. + a * t)/sqrt(a) + sqrt(test_arg_1) );
        ret = 0.;
//        if(test_arg_2 < zero)
//        {
//            t += t / 1e+7 + zero * 10.;
//            a += a / 1e+7 + zero * 10.;
//            b += b / 1e+7 + zero * 10.;
//            c += c / 1e+7 + zero * 10.;
//            test_arg_1 = a * t * t + b * t + c;
//            test_arg_2 = fabs( (b / 2. + a * t)/sqrt(a) + sqrt(test_arg_1) );
//        }
        if(fabs(test_arg_2) > zero)
            ret = log( test_arg_2 ) * abs( alpha ) / sqrt(a);
	// второе слагаемое интеграла
	double sk = fix_params.s_t;
	//if(abs(u_y) < zero && abs(sk - s_y) < zero)
	//{
	//	arg = t - t_y;
	//	ret -= log(abs(arg)) * ( (t<t_y)?-1.0:1.0 );
	//}
	//else
        double test_arg = (t - t_y)*(t - t_y) + u_y * u_y + (sk - s_y) * (sk - s_y);
        arg = fabs( t - t_y + sqrt( test_arg ) );
//        if(test_arg < zero)
//        {
//            t += t / 1e+7 + zero * 10.;
//            t_y += t_y / 1e+7 + zero * 10.;
//            u_y += u_y / 1e+7 + zero * 10.;
//            s_y += s_y / 1e+7 + zero * 10.;
//            arg = fabs( t - t_y + sqrt( test_arg ) );
//        }
        if(fabs(arg) > zero)
            ret -= log( arg );
	return ret;
}

// Функции вычисления компонент слагаемых модифицированного протенциалла двойного слоя
double Modyfied_Doub_Layer_Potential::r2_3_component(VectorXD<double,3> &local_coords)
{
	double a, b, c;
	double s_y, t_y, u_y;
	double s_x, alpha2, alpha1;
	double res;
	s_x = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	a = fix_params.tg_a2 * fix_params.tg_a2 + 1;
	b = -2 * (fix_params.tg_a2 * t_y + s_y);
	c = s_y * s_y + t_y * t_y + u_y * u_y;
	res = F_r2_3(s_x,alpha2,a,b,c) -
		  F_r2_3(0,alpha2,a,b,c);
	a = fix_params.tg_a1 * fix_params.tg_a1 + 1;
	b = -2 * (fix_params.tg_a1 * t_y + s_y);
	c = s_y * s_y + t_y * t_y + u_y * u_y;
	res+= -1.0*F_r2_3(s_x,alpha1,a,b,c) +
		  F_r2_3(0,alpha1,a,b,c);
	return res / (4 * PI);
}

double Modyfied_Doub_Layer_Potential::r1_3_component(VectorXD<double,3> &local_coords)
{
	double t_star, alpha2, alpha1, t_vave;
	double res;
	t_star = fix_params.t_star;
	t_vave = fix_params.t_k;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	res = 0;
	if(abs(alpha1) > 1e-8)
	res+= F_r1_3(0,alpha1,local_coords) -
		  F_r1_3(-t_star,alpha1,local_coords);
	if(abs(alpha2) > 1e-8)
	res+= F_r1_3(t_vave-t_star,alpha2,local_coords) -
		  F_r1_3(0,alpha2,local_coords);
	return res / (4 * PI);
}

//------------------------------------------------------------------
//ФУНКЦИИ ДЛЯ ВЫЧИСЛЕНИЯ ОСОБЕННОСТЕЙ ВТОРОГО ПОРЯДКА РАДИУСА
//------------------------------------------------------------------
// Первые две функции вспомогательные (интегралы)
double Modyfied_Doub_Layer_Potential::F_r2_2(double s, double alpha, VectorXD<double,3> &local_coords)
{
	double ret;
	double t_y, s_y, u_y, x;
	double a, b, c, R, Disc;
	// удобные имена локальных координат (совпадает с аналитикой)
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	// константы
	a = alpha*alpha + 1;
	b = -2*(alpha * t_y + s_y);
	c = u_y * u_y + s_y * s_y + t_y * t_y;
	R = c - b*b / (4 * a);
	// замена переменной
	x = sqrt(a) * s + b / ( 2 *sqrt(a) );
	//Disc = alpha * s_y - t_y;
	//if(abs(Disc)<zero)
	//{
	//	arg = x;
	//	ret = x * log(abs(x)) - x;
	//}
	//else
		ret = x * log(x*x+R) / 2 - x + sqrt(R)*atan(x / sqrt(R));
	ret /= sqrt(a);
	return ret;
}

double Modyfied_Doub_Layer_Potential::F_r1_2(double t, double alpha, VectorXD<double,3> &local_coords)
{
	double ret;
	double t_y, s_y, u_y, x;
	double a, b, c, R, C, arg;
	// удобные имена локальных координат (совпадает с аналитикой)
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	// константы
	a = alpha * alpha + 1;
	b = -2 * (t_y * alpha * alpha + s_y * alpha );
	c = (s_y * s_y + t_y * t_y + u_y * u_y)*alpha*alpha;
	R = c - b*b / (4 * a);
	double sk = fix_params.s_t;
	C = u_y * u_y + (sk - s_y) * (sk - s_y);
	// замена переменной
	x = sqrt(a) * t + b / ( 2 *sqrt(a) );
	// интегралл
	double Disc = alpha * s_y - t_y;
	ret = 0.;
	//if(abs(Disc)<zero)
	//{
	//	arg = x;
	//	ret -= x * log(abs(x)) - x;
	//}
	//else
		ret -= x * log(x*x+R) / 2 - x + sqrt(R)*atan(x / sqrt(R));
	ret /= sqrt(a);
	ret *= abs( alpha );
	// второе слагаемое интеграла
	arg = t - t_y;
	if(sqrt(C) < zero)
	{
		ret = arg*log(abs(arg)) - arg;
	}
	else
		ret = arg * log(arg*arg+C) / 2 - arg + sqrt(C)*atan(arg / sqrt(C));
	return ret;
}

// Функции вычисления касательных компонент потенциаллов
double Modyfied_Doub_Layer_Potential::r1_2_component(VectorXD<double,3> &local_coords)
{
	double t_star, alpha2, alpha1, t_vave;
	double res;
	t_star = fix_params.t_star;
	t_vave = fix_params.t_k;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	res = 0;
	if(abs(alpha1) > zero)
	res+= F_r1_2(0,alpha1,local_coords) -
		  F_r1_2(-t_star,alpha1,local_coords);
	if(abs(alpha2) > zero)
	res+= F_r1_2(t_vave-t_star,alpha2,local_coords) -
		  F_r1_2(0,alpha2,local_coords);
	return res / (4 * PI);
}
double Modyfied_Doub_Layer_Potential::r2_2_component(VectorXD<double,3> &local_coords)
{
	double s_y, t_y, u_y;
	double s_x, alpha2, alpha1;
	double res;
	s_x = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	res = F_r2_2(s_x,alpha2,local_coords) -
		  F_r2_2(0,alpha2,local_coords)	-
		  F_r2_2(s_x,alpha1,local_coords) +
		  F_r2_2(0,alpha1,local_coords);
	return res / (4 * PI);
}
//------------------------------------------------------------------
//ФУНКЦИИ ДЛЯ ВЫЧИСЛЕНИЯ ОСОБЕННОСТЕЙ ПЕРВОГО ПОРЯДКА РАДИУСА,
//ГРАДИЕНТНАЯ СОСТАВЛЯЮЩАЯ ПОТЕНЦИАЛЛОВ
//------------------------------------------------------------------
// вспомогательная функция для определения общего итеграла в функции ниже [АРКТАНГЕНС]
double Modyfied_Doub_Layer_Potential::F_arctan(double A1,double A2,double arg)
{
	double disc = A2 * A2 - 4 * A1; // функция в любом случае действительная, она либо гиперболическая, либо триганометрическая
	double res=0.0, x;
	if(disc < 0)
	{
		res = -atan( (2 * arg + A2) / sqrt( -disc ) ) / sqrt(-disc);
	}
	if(disc > 0)
	{
		x = 2 * arg + A2;
		x /= sqrt(disc);
		res = 0.5 * log( abs((1 + x)/(1 - x)) )/sqrt(disc);
                //res = atanh( (2 * arg + A2) / sqrt( disc ) ) / sqrt(disc);
	}
	return res;

}
// общий интеграл для вычисления интеграла от квадрата
double Modyfied_Doub_Layer_Potential::s2_t2_grad(double A , double B, double a, double b, double c, VectorXD<double,3> &local_coords, double s)
{
	// первое слагаемое этой суммы
	double res, Disc, F, arg;
	Disc = b * b - 4 * a * c;
	res = -A * sqrt(a * s*s + b * s + c) / a;
	//if(abs(Disc)<=zero)
	//{
	//	arg = sqrt(a) * s + b / (2 * sqrt(a));
	//	F = log(abs(arg)) / sqrt(a); // a > 0
	//	F *= (s < -b / (2 * a) )?-1.0:1.0;
	//}
	//else	// в этом случае корней у полинома нет (подкоренное выражение полложительно)
		F = log( (b/2. + a * s)/sqrt(a) + sqrt( a * s * s + b * s + c ) ) / sqrt(a);
	res -= (B - A * b / (2 * a) )*F;
	// второе слагаемое выражения
	double res2;
	// определяем вспомогательные константы
	double V, L, D, A1, A2;
	V = A / sqrt(a);
	L = B - b * A / (2 * a );
	D = c - b * b / (4 * a);
	A1 = D * (1 - V) / (1 + V);
	A2 = 2 * L / (1 + V);
	// определяем замены переменной
	double u, t;
	double s_y, t_y, u_y;
	double disc; // определяем, есть ли в наличии корни или их нет
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	u = sqrt(a) * s + b / (2 * sqrt(a) );
	t = u + sqrt(u * u + D);
	// непосредственно вычисляем интегральное выражение
	disc = A2 * A2 - 4 * A1;
	if(abs(disc)<zero)
	{
		res2 = t / 2 - D / (2 *t) + log(abs(2 * t + A2)) * ( 2 * D / A2 - A2 / 2) - 2 * D * log(abs(t))/A2; // в этом случае A1 = A2 * A2 / 4 <- корень квардратного выражения
	}
	else
	{	// если нулю равен D, то нулю равен коэффициент A1
		if(abs(D)>zero)
		{
			res2 = t / 2 - D / (2 * t) - D * A2 * log(abs(t)) / (2 * A1) -
				   log( abs(t*t + A2*t + A1) ) * (A2 / 4 - D * A2 / (4 * A1));
			res2+= F_arctan(A1,A2,t) * (2*D + 2*A1 - A2*A2*D/(2*A1) - A2 * A2 / 2);
		}
		else
		{
			res2 = t - A2 * log(abs(t + A2));
		}
	}
	res2 /= -1.0 * sqrt(a);
	res2 += u * log( abs(V*u + L + sqrt(u*u+D)) )/sqrt(a);
	return (res + res2)/(4 * PI);
}
// общий интеграл (вторая версия) для вычисления градиентных составляющих потенциалов
double Modyfied_Doub_Layer_Potential::s2_t2_grad2(double alpha, double s, double t_y, double s_y, double u_y)
{
    double s_vave = s - s_y;
    double u = sqrt(alpha * alpha + 1) * s_vave + alpha * (alpha * s_y - t_y) / sqrt(alpha * alpha + 1);
    double D = pow(alpha * s_y - t_y, 2) / (alpha * alpha + 1) + u_y * u_y;
    double V = alpha / sqrt(alpha * alpha + 1);
    double L = (alpha * s_y - t_y) / (alpha * alpha + 1);
    double res1 = 0.;
    double res2 = 0.;
    double s_stripe = alpha * s - t_y;
    double a = (alpha * alpha + 1);
    double b = 2.*(t_y - alpha * s_y);
    double c = alpha*alpha*u_y*u_y + pow(t_y - alpha * s_y, 2);
    res1 = log_doub_area_int(V,L,D,1./sqrt(a),u);
    double sign;
    if(abs(alpha) < 1e-8) // тогда замена s_stripe не годится
    {
        double arg = s_vave + sqrt(s_vave * s_vave + t_y*t_y + u_y*u_y);
        arg = fabs(arg);
        if(arg < zero)
        {
            s_vave += s_vave / 1e+6 + zero * 10.;
            t_y += t_y / 1e+6 + zero * 10.;
            u_y += u_y / 1e+6 + zero * 10.;
            arg = s_vave + sqrt(s_vave * s_vave + t_y*t_y + u_y*u_y);
            arg = fabs(arg);
        }
//        if(arg > zero)
            res2 = t_y * log( arg );
    }
    else
    {
        res2 = sqrt(a * s_stripe * s_stripe + b * s_stripe + c) / a;
        double arg = (b/2 + a*s_stripe)/sqrt(a) + sqrt(a * s_stripe * s_stripe + b * s_stripe + c);
        arg = fabs(arg);
        if(fabs(arg) < zero)
        {
            s_stripe += s_stripe / 1e+6 + zero * 10;
            a += a / 1e+6 + zero*10;
            b += b / 1e+6 + zero*10;
            c += c / 1e+6 + zero*10;
            arg = (b/2 + a*s_stripe)/sqrt(a) + sqrt(a * s_stripe * s_stripe + b * s_stripe + c);
            arg = fabs(arg);
        }
//        if(arg > zero)
            res2+= ( -b / (2 * a * sqrt(a)) ) * log( arg );
        sign = (alpha > 0)?1. : -1;
        res2*=(-1. * sign);
    }
    return res1 + res2;

}
// общий интеграл для вычисления интеграла от смешанного произведения
double Modyfied_Doub_Layer_Potential::st_grad(double a, double b, double c, VectorXD<double,3> &local_coords, double s)
{
	double ret, Disc; // Disc - нужен для определения наличия корней полинома, arg - линейное выражение, в случае наличия корня
        Disc = b*b - 4 * a * c;
        double arg_sq = a * s * s + b * s + c;
        double arg = (b / 2 + a * s) / sqrt(a);
	ret = sqrt(fabs(arg_sq)) / a;
        double log_arg = arg + sqrt(fabs(arg_sq));
        if(fabs(log_arg) < zero)
            return ret;
        else
            ret += -( b / (2 * a * sqrt(a)) ) * log( arg + sqrt(fabs(arg_sq)) );
	return ret;
}

// см. №11 в списке интегралов
double Modyfied_Doub_Layer_Potential::s0_t2_grad(VectorXD<double,3> &local_coords, double t)
{
	double x, R, F, res;
	double B, C2;
	double s_y, t_y, u_y;
	double t_vave;
	double sk = fix_params.s_t;
	s_y = sk - local_coords.coords[0];
	t_y =	   local_coords.coords[1];
	u_y =	   local_coords.coords[2];
	C2 = u_y * u_y + s_y * s_y;
	B = - s_y;
	t_vave = t - t_y;
	x = sqrt(t_vave * t_vave + C2) + t_vave;
	res = 0.;
	// проверяем, есть ли особенности в подынтегральном выражении
	if(C2 < zero)
	{
		res = t_vave * log(abs(t_vave)) - t_vave - s_y * log(abs(t_vave)) * ( (t_vave > 0)?1.0:-1.0 );
	}
	else
	{
		F = F_arctan(0.5 * x, -B, C2 / 4);
		R = 0.5 * x - 0.5 * C2 / x + B * log(x) + 2 * F * (C2 - B*B);
		if (abs(t_vave) > zero)
			res = t_vave * log(sqrt(t_vave * t_vave + C2) - s_y) - s_y * log(t_vave + sqrt(t_vave * t_vave + C2)) - R;
	}
	return res;
}
// Функция вычисления интеграла от смешанного произведения
double Modyfied_Doub_Layer_Potential::A21_A12_Value(VectorXD<double,3> &local_coords, double A_const, double B_const)
{
	double s_y, t_y, u_y;
	double s_x, alpha2, alpha1;
	double res = 0.;
	s_x = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	// вычислим заранее все коэффициенты
	double a, b, c, a2, b2, c2;
	a = alpha2 * alpha2 + 1;
	b = 2 * alpha2 * (alpha2 * s_y - t_y);
	c = pow(alpha2 * s_y - t_y, 2) + u_y * u_y;
	a2 = alpha1 * alpha1 + 1;
	b2 = 2 * alpha1 * (alpha1 * s_y - t_y);
	c2 = pow(alpha1 * s_y - t_y, 2)+ u_y * u_y;
//        res =  st_grad(a,b,c,local_coords,s_x - s_y) -
//		  st_grad(a,b,c,local_coords, -s_y);
//	res +=  -st_grad(a2,b2,c2,local_coords,s_x - s_y) +
//		  st_grad(a2,b2,c2,local_coords,-s_y);
        res = (st_grad(a,b,c,local_coords,s_x - s_y) + st_grad(a2,b2,c2,local_coords,-s_y))
                - (st_grad(a,b,c,local_coords, -s_y) + st_grad(a2,b2,c2,local_coords,s_x - s_y));
	return res * (A_const + B_const) / (-4 * PI);
}

// интеграл от квадрата, деленного на функцию с особенностью
double Modyfied_Doub_Layer_Potential::A22_Value(VectorXD<double,3> &local_coords, double A_const)
{
	double s_y, t_y, u_y;
	double s_x, alpha2, alpha1;
	double res;
	if (abs(A_const) < zero)
		return 0.;
	s_x = fix_params.s_t;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	// вычислим заранее все коэффициенты
//	double a, b, c, A, B;
//	a = fix_params.tg_a2 * fix_params.tg_a2 + 1;
//	b = -2 * (fix_params.tg_a2 * t_y + s_y);
//	c = s_y * s_y + t_y * t_y + u_y * u_y;
//	A = alpha2;
//	B = -t_y;
//	res =  s2_t2_grad(A,B,a,b,c,local_coords,s_x) -
//		  s2_t2_grad(A,B,a,b,c,local_coords,0);
//	a = fix_params.tg_a1 * fix_params.tg_a1 + 1;
//	b = -2 * (fix_params.tg_a1 * t_y + s_y);
//	c = s_y * s_y + t_y * t_y + u_y * u_y;
//	A = alpha1;
//	res +=  -s2_t2_grad(A,B,a,b,c,local_coords,s_x) +
//		  s2_t2_grad(A,B,a,b,c,local_coords,0);
//        res = s2_t2_grad2(alpha2, s_x, t_y, s_y, u_y) -
//                s2_t2_grad2(alpha2, 0, t_y, s_y, u_y);
//        res+=-s2_t2_grad2(alpha1, s_x, t_y, s_y, u_y)+
//                s2_t2_grad2(alpha1, 0, t_y, s_y, u_y);
        res = (s2_t2_grad2(alpha2, s_x, t_y, s_y, u_y) + s2_t2_grad2(alpha1, 0, t_y, s_y, u_y)) -
                (s2_t2_grad2(alpha2, 0, t_y, s_y, u_y) + s2_t2_grad2(alpha1, s_x, t_y, s_y, u_y));
	return res * A_const / (4 * PI);
}

// то же самое, что и предыдущая функция, только квадрат вторичный (т. е. по t а не по s)
double Modyfied_Doub_Layer_Potential::A11_Value(VectorXD<double,3> &local_coords, double A_const)
{
	double s_y, t_y, u_y;
	double s_x, t_star, t_x, alpha2, alpha1;
	double res=0., area;
	if (abs(A_const) < zero)
		return 0.;
	area = fix_params.s_t * fix_params.t_k / 2.;
	s_x = fix_params.s_t;
	t_x = fix_params.t_k;
	t_star = fix_params.t_star;
	alpha2 = fix_params.tg_a2;
	alpha1 = fix_params.tg_a1;
	s_y = local_coords.coords[0];
	t_y = local_coords.coords[1];
	u_y = local_coords.coords[2];
	// интеграл разбивается по аддитивности (берем по t а не по s)
	double a, b, c, A, B;
	if (abs(alpha1) > 1e-8)
	{
//            res += s2_t2_grad2(0., 0., s_y - s_x, t_y, u_y) -
//                    s2_t2_grad2(0., alpha1*s_x, s_y - s_x, t_y, u_y);
//            res -= s2_t2_grad2(1./alpha1, 0., s_y, t_y, u_y) -
//                    s2_t2_grad2(1./alpha1, alpha1*s_x, s_y, t_y, u_y);
            res += (s2_t2_grad2(0., 0., s_y - s_x, t_y, u_y) + s2_t2_grad2(1./alpha1, alpha1*s_x, s_y, t_y, u_y)) -
                    (s2_t2_grad2(0., alpha1*s_x, s_y - s_x, t_y, u_y) + s2_t2_grad2(1./alpha1, 0., s_y, t_y, u_y));

	}
	//То же самое для второго прямоугольного треугольника (c alpha2)
	if (abs(alpha2) > 1e-8)
	{
//            res += s2_t2_grad2(0., alpha2*s_x, s_y - s_x, t_y, u_y) -
//                    s2_t2_grad2(0., 0., s_y - s_x, t_y, u_y);
//            res -= s2_t2_grad2(1./alpha2, alpha2*s_x, s_y, t_y, u_y) -
//                    s2_t2_grad2(1./alpha2, 0., s_y, t_y, u_y);
            res += (s2_t2_grad2(0., alpha2*s_x, s_y - s_x, t_y, u_y) + s2_t2_grad2(1./alpha2, 0., s_y, t_y, u_y)) -
                    (s2_t2_grad2(0., 0., s_y - s_x, t_y, u_y) + s2_t2_grad2(1./alpha2, alpha2*s_x, s_y, t_y, u_y));
	}
	// Теперь, получим выражения, для центральных точек (t = 0)
	//res += s0_t2_grad(local_coords,t_x - t_star) - s0_t2_grad(local_coords,-t_star);
	return res * A_const / (4 * PI);
}

// Главная функция, которая считает вклад от матрицы градиентов
double Modyfied_Doub_Layer_Potential::GradientValue(VectorXD<double,3> &local_coords, Block<double,2> &gradients)
{
	double a11 = A11_Value(local_coords,gradients.block[0][0]);
	double a21_12 = A21_A12_Value(local_coords,gradients.block[0][1],gradients.block[1][0]);
	double a22 = A22_Value(local_coords,gradients.block[1][1]);
	return a11 + a21_12 + a22;
}
//------------------------------------------------------------------
// ФУНКЦИИ РЕАЛИЗАЦИИ ЧИСЛЕННОГО ИНТЕГРИРОВАНИЯ
// ВАЖНО!!! GaussPoints_y - дана в координатах другого треугольника, отличного от того, для которого строилась f!
// Формулы интегрируемых численно функций см. в собственных выкладках (самое начало)
//------------------------------------------------------------------
complex<double> Modyfied_Doub_Layer_Potential::NumericPartEvaluter(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex< double > acc, area, ret;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	f->u_tangen( y ); // не присваиваем, значение сохранено
	f->InterpGrad = f->gradient_u_tang( y ); // так же, сохраняем значение градиента
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = AdditionalFunction(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret = ret + acc;
	}
	return ret;
}
// функция, которую интегрируем численно
//------------------------------------------------------------------
//ВАЖНО!!! Функция нужна для оператора "B" билинейная форма которого
// выглядит так < B*lambda, v >, где "v" это функция из того же пространства,
// что и функция "u". Т. е. надо брать для внутреннего интегрирования базисную
// функцию "u". см. Breuer стр. 96
//------------------------------------------------------------------
complex< double > Modyfied_Doub_Layer_Potential::AdditionalFunction(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints_x)
{
	VectorXD<double, 3> acc;
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	VectorXD<double,2> vx, vy, x_y_red; //
	double x_y_lenght;			  // расстояние между агрументами
	double x_y_vx, x_y_vy;		  // скалярные произведения с этими функциям
	vectCache_elem fxCache = f->get_cache_value(GaussPoints_x);
	// переводим в локальные координаты треугольника точки аргументов
	x = fxCache.loc_arg;
	x_y = x - y;
	for(uint i=0; i<2; i++)
		x_y_red.coords[i]=x_y.coords[i];
	x_y_lenght = x_y.norma();
	if (x_y_lenght < zero)
	{
		complex<double> sub_ret = 0.;
		return sub_ret;
	}
	// находим значения интересующей функции в этой точке
	acc = fxCache.u_val; for(uint i=0; i<2; i++) vx.coords[ i ] = acc.coords[ i ];
	x_y_vx = acc.dotprod( x_y );
	acc = f->InterpValue; for(uint i=0; i<2; i++) vy.coords[ i ] = acc.coords[ i ];;
	x_y_vy = acc.dotprod( x_y );
	// первая интегрируемая часть (с экспонентой)
	complex< double > exp_part, sum;
	sum = 0.;
	exp_part = exp( -x_y_lenght * vave_num_k ) - 1.;
	sum += x_y_vx * exp_part * vave_num_k / (x_y_lenght * x_y_lenght);
	sum += ( exp_part + vave_num_k * x_y_lenght ) * x_y_vx /(x_y_lenght * x_y_lenght * x_y_lenght);
	// вторая часть, получена приближением интегралов от функций, не содержащим множителями экспоненту в степени мнимого числа
	VectorXD<double,2> MatrixMult;
	Block<double,2> grad;
	MatrixMult.common_init(0.);
	// получаем матрицу градиентов
	grad = f->InterpGrad;
	// умножаем ее на вектор разницы координат
	for(uint i=0; i<2; i++)
		for(uint j=0; j<2; j++)
			MatrixMult.coords[ i ] += grad.block[ i ][ j ] * x_y.coords[ j ]; // последнюю координату игнорируем
	sum = sum + x_y_red.dotprod( vx - vy - MatrixMult ) / (x_y_lenght * x_y_lenght * x_y_lenght);
	return sum / ( 4 * PI );
}

// функция, которую интегрируем численно
// Предполагается, что особенностей нет!
//------------------------------------------------------------------
// ВАЖНО!!! Данная функция вычисляет модифицированный потенциал
// на прямую, т. к. предполагается, что особенностей у подынтегральной
// функции нет. Замечания для предыдущей функции справедливы и здесь.
//------------------------------------------------------------------
complex< double > Modyfied_Doub_Layer_Potential::ModDoubPotentialEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params)
{
	complex< double > acc, area, ret;
	VectorXD<double,2> gauss_points;
	// считаем площадь треугольника
	area = f->current_parameter.s_t * f->current_parameter.t_k * 0.5;
	// суммируем в процедуре численного интегрирования
	ret = 0.;
	//// получаем координаты y в локальной системе нашего треугольника, для этого
	//// вначале переведем его в глобальные координаты, а затем в локальные
	//y = y_params.get_Global_point( GaussPoints_y );
	//y = fix_params.get_local_coordinate_point( y );
	while( tgr.next() ) // цикл по числу точек Гаусса
	{   // точки Гаусса (L - координаты)
		gauss_points.coords[ 0 ] = tgr.e.e1;
		gauss_points.coords[ 1 ] = tgr.e.e2;
		// в этих точках вычисляем интегрируемую функцию
		acc = ModDoubPotentFunct(f, y, gauss_points) * area;
		// в отличие от area, параметр весов w может менятся на итерациях
		acc *= tgr.w;
		// добавляем новое слагаемое к результату
		ret += acc;
	}
	return ret;
}

complex< double > Modyfied_Doub_Layer_Potential::ModDoubPotentFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints)
{
	VectorXD<double,3> x, x_y;	  // точки в локальных координатах треугольника
	VectorXD<double,3> vx;
	double x_y_lenght;			  // расстояние между агрументами
	double x_y_vx;				  // скалярные произведения с этими функциям
	vectCache_elem fxCache = f->get_cache_value(GaussPoints);
	// переводим в локальные координаты треугольника точки аргументов
	//
	x = fxCache.loc_arg;		  // f->get_Local_point( GaussPoints );
	//
	x_y = x - y;
	x_y_lenght = x_y.norma();
	//
	vx =fxCache.u_val;			  // f->u_tangen( GaussPoints ); // в данном случае не делается интерполяция
								  // по скольку в точке y функцию считать не надо.
	//
	x_y_vx = vx.dotprod( x_y );
	// далее вычисления ведутся в комплексном пространстве
	complex< double > exp_part;
	exp_part = exp( -x_y_lenght * vave_num_k )*x_y_vx;
	return exp_part * (1./(x_y_lenght*x_y_lenght*x_y_lenght) + vave_num_k/(x_y_lenght*x_y_lenght)) / (4 * PI);
}

// ФУНКЦИЯ, ВЫЧИСЛЯЮЩАЯ ЭТУ КОМПОНЕНТУ ПОТЕНЦИАЛА
complex< double > Modyfied_Doub_Layer_Potential::EvalfPotential(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	complex< double > sum;
	Block<double, 2> gradient;
	VectorXD<double,3> y, vy;
	zero = sqrt(y_params.s_t * y_params.t_k * 0.5) * 1e-8;
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
	// x и y треугольники совпадают или находятся достаточно близко
	y = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
	y = fix_params.get_local_coordinate_point( y );
	if(!is_singular) // никаких особенностей в подынтегральном выражении нет!
		return ModDoubPotentialEvalf(f, y, y_params); // x и y треугольники в этом случае достаточно далеко друг от друга
	else
	{
		vy = f->u_tangen( y ); // слишком часто производим одно и то же действие, оптимизируй!
		sum = 0.;
		// особенность 3-го порядка
		sum += r1_3_component(y) * vy.coords[0];
		sum += r2_3_component(y) * vy.coords[1];
		// градиентная составляющая
		gradient = f->gradient_u_tang(y);
		sum += GradientValue(y, gradient);
		// добавляем интеграл, который считается численно
		sum += NumericPartEvaluter(f, y, y_params);
	}
	return sum;
}

complex< double > Modyfied_Doub_Layer_Potential::EvalfPotential_no_r_int(VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	complex< double > sum;
	Block<double, 2> gradient;
	VectorXD<double,3> y, vy;
	zero = sqrt(y_params.s_t * y_params.t_k * 0.5) * 1e-8;
	this->fix_params = f->current_parameter; // важно для полуаналитического интегрирования
	// x и y треугольники совпадают или находятся достаточно близко
	y = y_params.get_Global_point( GaussPoints_y ); // f - это параметр x
	y = fix_params.get_local_coordinate_point( y );
	vy = f->u_tangen( y ); // слишком часто производим одно и то же действие, оптимизируй!
	sum = 0.;
	// особенность 3-го порядка
	//sum += r1_3_component(y) * vy.coords[0]; <- этот интеграл вычисляется двукратным вычитанием особенности
	//sum += r2_3_component(y) * vy.coords[1];
	// градиентная составляющая
	gradient = f->gradient_u_tang(y);
	sum += GradientValue(y, gradient);
	// добавляем интеграл, который считается численно
	sum += NumericPartEvaluter(f, y, y_params);
	return sum;
}
//------------------------------------------------------------------
// Специальные тестовые функции для проверки частных случаев.
// взято из Ananlitical Methods стр. 45
//------------------------------------------------------------------
// Гиперболический арксинус
double Modyfied_Doub_Layer_Potential::arcsinh( double x )
{
	return log(x + sqrt(x * x + 1));
}

double Modyfied_Doub_Layer_Potential::TestFunc(double a, double b, double c, double x)
{
	double A, disc, B, arg;
	disc = 4*a*c - b*b;
//	if(abs(disc)<1e-3/*this->zero*/)
//	{
//            A = sign(2*a*x + b) * x / sqrt(a);
//            B = -b * sign(2*a*x + b) / (2 * a * sqrt(a));
//            if(abs(2*a*x+b) > 1e-3)
//                B *= log( abs( 2 * a * x + b ) );
//	}
//	else
	{
            A = a * x * x + b * x + c;
            A = sqrt( A )/a;
            B = -b / (2 * a * sqrt(a));
            arg = ( 2 * a * x + b ) / sqrt( disc );
            B *= arcsinh( arg );
	}
	return (A + B) / (4 * PI);
}

// Analitical Methods стр. 45
double Modyfied_Doub_Layer_Potential::LinearPotential(VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params)
{
	// вначале преобразуем к локальным координатам (как это всегда и делалось) НАШЕГО треугольника
	VectorXD<double,3> y;
	y = y_params.get_Global_point( GaussPoints_y );
	y = fix_params.get_local_coordinate_point( y );
	// теперь, по готовой формуле тупо считаем
	double s_y, t_y, tau_y; // переобозначили координаты точки
	double sk, tk, t_star;	// параметры треугольника
	double alpha_1, alpha_2;
	double area = fix_params.s_t * fix_params.t_k;
	//
	sk = fix_params.s_t;
	tk = fix_params.t_k;
	t_star = fix_params.t_star;
	alpha_1 = fix_params.tg_a1;
	alpha_2 = fix_params.tg_a2;
	//
	s_y = y.coords[ 0 ];
	t_y = y.coords[ 1 ];
	tau_y = y.coords[ 2 ];
	//
	double a, b, c;		// коэффициенты для расчета интеграла по формуле, указаннной выше
	double ret = 0.;
	a = 1.; b = -2*t_y; c = pow(sk - s_y,2) + pow(t_y,2) + pow(tau_y,2);
        double disc = 4*a*c - b*b;
//        if( fabs(disc) < 1e-5)
//        {
//            double x1 = tk - t_star;
//            double x2 = -t_star;
//            if(fabs(x1 + b/2) > 1e-6)
//                ret += sign( x1 + b/2 ) * (x1 - b * log(abs(x1 + b/2)) / 2);
//            else
//                ret += sign( x1 + b/2 ) * (x1);
//            if(fabs(x2 + b/2) > 1e-6)
//                ret -= sign( x2 + b/2 ) * (x2 - b * log(abs(x2 + b/2)) / 2);
//            else
//                ret -= sign( x2 + b/2 ) * (x2);
//        }
//        else
	ret += -1. * (
		TestFunc(a,b,c,tk - t_star) - TestFunc(a,b,c,-t_star)	);
	// теперь постоянная c теперь не меняется ни для одного слагаемого
	c = pow(s_y,2) + pow(t_y,2) + pow(tau_y,2);
	//
	if( abs(alpha_1) > 1e-8) // Тогда, будем интегрировать
	{
		a = 1./(alpha_1*alpha_1) + 1.; b = -2.*(s_y / alpha_1 + t_y);
		ret += TestFunc(a,b,c,0.) - TestFunc(a,b,c,-t_star);
	}
	//
	if( abs(alpha_2) > 1e-8 ) // Тогда, будем интегрировать
	{
		a = 1./(alpha_2*alpha_2) + 1.; b = -2.*(s_y / alpha_2 + t_y);
		ret += TestFunc(a,b,c,tk-t_star) - TestFunc(a,b,c,0.);
	}
	// если alpha_2 = alpha_1 = 0. - то треугольник выродился в прямую, проходящую через x0, x_star
	a = alpha_2 * alpha_2 + 1.; b = -2.*(alpha_2 * t_y + s_y);
	ret += TestFunc(a,b,c,sk) - TestFunc(a,b,c,0.);
	//
	a = alpha_1 * alpha_1 + 1.; b = -2.*(alpha_1 * t_y + s_y);
	ret -= TestFunc(a,b,c,sk) - TestFunc(a,b,c,0.);
	//
	return ret / area;//
}
