/****************************************************************************

	Finite Element Analysis  /core/
	Copyright (C) Siberian CAD Soft, 2002, 2009

	File:       GaussRuleInfo.h
	Author:		Pavel Anufrikov <apaex@ngs.ru>

	Contents:

*****************************************************************************/
#pragma once
#include <cassert>
#include <iostream>

#pragma GCC diagnostic ignored "-Wreorder"

typedef int GaussRule;
typedef int subindex_t;
#define BUILD_DOUBLE


class GaussRuleInfoBase
{
public:

protected:
	GaussRuleInfoBase(GaussRule rule)
		: w(0), m_rule(rule), n(0), a(0.0), b(0.0)
	{}

	~GaussRuleInfoBase()
	{}
//private:
//	GaussRuleInfoBase(const GaussRuleInfoBase&);
//	GaussRuleInfoBase& operator = (const GaussRuleInfoBase&);

public:
	subindex_t GetPointNum() const
	{	assert(n>0); return n-1;	}

	GaussRule GetRule() const
	{	return m_rule;	}

        void SetRule(GaussRule rule)
        { m_rule = rule; w = 0; n = 0; a = 0.; b = 0.; }

public:
	double w;
public:
	mutable double a, b;
	GaussRule m_rule;
	subindex_t n;
};



struct LineCoordinates
{
	LineCoordinates()
		: e1(0.0), e2(0.0)
	{	;	}

	double e1;
	double e2;
};


class LineGaussRuleInfo : public GaussRuleInfoBase
{
public:
	LineGaussRuleInfo(GaussRule rule)
		: GaussRuleInfoBase(rule)
	{	;	}

	bool next();

	LineCoordinates e;

	static GaussRule getGaussRuleForDegree(int degree);
};



struct TriangleCoordinates
{
	TriangleCoordinates()
		: e1(0.0), e2(0.0), e3(0.0)
	{	;	}

	double e1;
	double e2;
	double e3;

	friend std::ostream& operator << (std::ostream& f, const TriangleCoordinates& c)
	{
		return f << c.e1 << '\t' << c.e2 << '\t' << c.e3;
	}
};


class TrigGaussRuleInfo : public GaussRuleInfoBase
{
public:
	TrigGaussRuleInfo(GaussRule rule)
		: GaussRuleInfoBase(rule)
	{	;	}

	bool next();

	TriangleCoordinates e;

	static GaussRule getGaussRuleForDegree(int degree);
};



struct TetrahedronCoordinates
{
	TetrahedronCoordinates()
		: e1(0.0), e2(0.0), e3(0.0), e4(0.0)
	{	;	}

	double e1;
	double e2;
	double e3;
	double e4;

	TetrahedronCoordinates(const TriangleCoordinates& coord, subindex_t face_index)
	{
		switch(face_index)
		{
		case 0:	e1=coord.e1;	e2=coord.e2;	e3=coord.e3;	e4=0.;			break;
		case 1:	e1=coord.e1;	e2=coord.e2;	e3=0.;			e4=coord.e3;	break;
		case 2:	e1=0.;			e2=coord.e1;	e3=coord.e2;	e4=coord.e3;	break;
		case 3:	e1=coord.e1;	e2=0.;			e3=coord.e2;	e4=coord.e3;	break;
		default:		assert(0);
		}
	}

	friend std::ostream& operator << (std::ostream& f, const TetrahedronCoordinates& c)
	{
		return f << c.e1 << '\t' << c.e2 << '\t' << c.e3 << '\t' << c.e4;
	}
};



class TetrGaussRuleInfo : public GaussRuleInfoBase
{
public:
	TetrGaussRuleInfo(GaussRule rule)
		: GaussRuleInfoBase(rule)
	{	;	}

	bool next();

	TetrahedronCoordinates e;
};
