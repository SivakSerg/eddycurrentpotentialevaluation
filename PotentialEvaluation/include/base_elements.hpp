/*
Данный модуль включает различные варианты
реализации конечных подобластей (тетраэдры,
треугольники, шестигранники, четырехугольни-
ки и, возможно, другие)
*/
#pragma once
#include "base.hpp"

#pragma GCC diagnostic ignored "-Wunused-parameter"
//---------------------------------------------------
//ПРИЗМА
//---------------------------------------------------
struct prism : public common_elem_struct
{
	prism(){p = new uint[6];}
	~prism(){delete[] p; p=0;}

	inline
	uint build_face(uint *face, uint face_local_num);

	uint get_face_count(){return 5;}

	inline
	uint get_free_num(uint fase_local_num);

	inline
	static uint build_local_face(uint *face, uint face_local_num);
	uint get_max_face_size(){return 4;}

	inline
	bool is_adge_exist(uint pnum1, uint pnum2) const;

	uint pcount()const{return 8;}
};

inline
uint prism::get_free_num(uint fase_local_num)
{
	fase_local_num %= 5;
	uint ret;
	switch(fase_local_num)
	{
	case 0:
		ret = 3;
		break;
	case 1:
		ret = 0;
		break;
	case 2:
		ret = 2;
		break;
	case 3:
		ret = 0;
		break;
	case 4:
		ret = 1;
		break;
	}
	return ret;
}

inline
uint prism::build_local_face(uint *face, uint face_local_num)
{
	face_local_num %= 5;
	uint ret;
	switch(face_local_num)
	{
	case 0:
		face[0] = 0;
		face[1] = 1;
		face[2] = 2;
		ret = 3;
		break;
	case 1:
		face[0] = 3;
		face[1] = 4;
		face[2] = 5;
		ret = 3;
		break;
	case 2:
		face[0] = 0;
		face[1] = 1;
		face[2] = 3;
		face[3] = 4;
		ret = 4;
		break;
	case 3:
		face[0] = 1;
		face[1] = 2;
		face[2] = 4;
		face[3] = 5;
		ret = 4;
		break;
	case 4:
		face[0] = 0;
		face[1] = 2;
		face[2] = 3;
		face[3] = 5;
		ret = 4;
		break;
	}
	return ret;
}

inline
uint prism::build_face(uint *face, uint face_local_num)
{
	face_local_num %= 5;
	uint ret;
	switch(face_local_num)
	{
	case 0:
		face[0] = p[0];
		face[1] = p[1];
		face[2] = p[2];
		ret = 3;
		break;
	case 1:
		face[0] = p[3];
		face[1] = p[4];
		face[2] = p[5];
		ret = 3;
		break;
	case 2:
		face[0] = p[0];
		face[1] = p[1];
		face[2] = p[3];
		face[3] = p[4];
		ret = 4;
		break;
	case 3:
		face[0] = p[1];
		face[1] = p[2];
		face[2] = p[4];
		face[3] = p[5];
		ret = 4;
		break;
	case 4:
		face[0] = p[0];
		face[1] = p[2];
		face[2] = p[3];
		face[3] = p[5];
		ret = 4;
		break;
	}
	return ret;
}

inline
bool prism::is_adge_exist(uint pnum1, uint pnum2) const
{
	if( (pnum1 == pnums(0) && pnum2 == pnums(1)) || (pnum1 == pnums(1) && pnum2 == pnums(0)) )
		return true;
	if( (pnum1 == pnums(0) && pnum2 == pnums(2)) || (pnum1 == pnums(2) && pnum2 == pnums(0)) )
		return true;
	if( (pnum1 == pnums(1) && pnum2 == pnums(2)) || (pnum1 == pnums(2) && pnum2 == pnums(1)) )
		return true;

	if( (pnum1 == pnums(0) && pnum2 == pnums(3)) || (pnum1 == pnums(3) && pnum2 == pnums(0)) )
		return true;
	if( (pnum1 == pnums(1) && pnum2 == pnums(4)) || (pnum1 == pnums(4) && pnum2 == pnums(1)) )
		return true;
	if( (pnum1 == pnums(2) && pnum2 == pnums(5)) || (pnum1 == pnums(5) && pnum2 == pnums(2)) )
		return true;

	if( (pnum1 == pnums(4) && pnum2 == pnums(3)) || (pnum1 == pnums(3) && pnum2 == pnums(4)) )
		return true;
	if( (pnum1 == pnums(4) && pnum2 == pnums(5)) || (pnum1 == pnums(5) && pnum2 == pnums(4)) )
		return true;
	if( (pnum1 == pnums(5) && pnum2 == pnums(3)) || (pnum1 == pnums(3) && pnum2 == pnums(5)) )
		return true;
	return false;
}
//---------------------------------------------------
//ТЕТРАЭДР
//---------------------------------------------------
struct tetrahedron : public common_elem_struct
{
	tetrahedron(){p = new uint[4];}
	tetrahedron(const tetrahedron &arg){ p = new uint[4]; for (uint i = 0; i < 4; ++i) { p[i] = arg.p[i]; }; matr = arg.matr; }

	inline
	ulint build_face(ulint *face, ulint face_local_num);

	ulint get_face_count(){return 4;}

	inline
	uint get_free_num(uint fase_local_num); // возвращает номер вершины, не лежащей на грани

	inline
	static uint build_local_face(uint *face, uint face_local_num); // выдаёт локальную нумерацию вершин на грани

	uint get_max_face_size(){return 3;}

	inline
	bool is_adge_exist(uint pnum1,uint pnum2)const{return true;} // у тетраэдров все вершины связаны

	uint pcount() const {return 4;}
};
inline
uint tetrahedron::get_free_num(ulint fase_local_num)
{
	switch(fase_local_num)
	{
	case 0:
		return pnums(0);
	case 1:
		return pnums(1);
	case 2:
		return pnums(2);
	case 3:
		return pnums(3);
	}
	return 0xFFFFFF;
}
inline
ulint tetrahedron::build_face(ulint *face, ulint face_local_num)
{
	switch(face_local_num)
	{
	case 3:
		face[0] = pnums(0);
		face[1] = pnums(1);
		face[2] = pnums(2);
		return 3;
	case 2:
		face[0] = pnums(0);
		face[1] = pnums(1);
		face[2] = pnums(3);
		return 3;
	case 1:
		face[0] = pnums(0);
		face[1] = pnums(2);
		face[2] = pnums(3);
		return 3;
	case 0:
		face[0] = pnums(1);
		face[1] = pnums(2);
		face[2] = pnums(3);
		return 3;
	}
	return 0xFFFFF;
}
inline
ulint tetrahedron::build_local_face(ulint *face, ulint face_local_num)
{
	switch(face_local_num)
	{
	case 3:
		face[0] = 0;
		face[1] = 1;
		face[2] = 2;
		return 3;
	case 2:
		face[0] = 0;
		face[1] = 1;
		face[2] = 3;
		return 3;
	case 1:
		face[0] = 0;
		face[1] = 2;
		face[2] = 3;
		return 3;
	case 0:
		face[0] = 1;
		face[1] = 2;
		face[2] = 3;
		return 3;
	}
	return 0xFFFFF;
}

//---------------------------------------------------
//ТРЕУГОЛЬНИК
//---------------------------------------------------
struct triangle : public common_elem_struct
{
	triangle(){p = new uint[3];}
	triangle(const triangle &arg){ p = new uint[3]; for (uint i = 0; i < 3; ++i) { p[i] = arg.p[i]; }; matr = arg.matr; }

	inline
	ulint build_face(ulint *face, ulint face_local_num); // для двумерного элемента гранью будет его ребро.

	inline
	uint get_free_num(ulint fase_local_num);

	ulint get_face_count() {return 3;}

	uint get_max_face_size() {return 2;}

	inline
	bool is_adge_exist(uint pnum1, uint pnum2)const{return true;}

	uint pcount() const {return 3;}
};

inline
ulint triangle::build_face(ulint *face, ulint face_local_num)
{
	switch(face_local_num)
	{
	case 2:
		face[0] = pnums(0);
		face[1] = pnums(1);
		break;
	case 1:
		face[0] = pnums(0);
		face[1] = pnums(2);
		break;
	case 0:
		face[0] = pnums(1);
		face[1] = pnums(2);
		break;
	}
	return 2;
}

inline
uint triangle::get_free_num(ulint fase_local_num)
{
	switch(fase_local_num)
	{
	case 0:
		return pnums(0);
	case 1:
		return pnums(1);
	case 2:
		return pnums(2);
	}
	return 0xFFFFF;
}
//---------------------------------------------------
//ШЕСТИГРАННИК
//---------------------------------------------------
struct hexahedron : public common_elem_struct
{
	hexahedron(){p = new uint[8];}

	inline
	ulint build_face(ulint *face, ulint face_local_num);

	ulint get_face_count(){return 6;}

	uint get_free_num(ulint fase_local_num);

	uint get_max_face_size(){return 4;}

	inline
	bool is_adge_exist(uint pnum1, uint pnum2)const;

	uint pcount()const{return 8;}
};

inline
uint hexahedron::build_face(ulint *face, ulint face_local_num)
{
	switch(face_local_num)
	{
	case 0:
		face[0] = pnums(0); // v4
		face[1] = pnums(1);
		face[2] = pnums(2);
		face[3] = pnums(3);
		return 4;
	case 1:
		face[0] = pnums(4);// v0
		face[1] = pnums(5);
		face[2] = pnums(6);
		face[3] = pnums(7);
		return 4;
	case 2:
		face[0] = pnums(1);// v0
		face[1] = pnums(3);
		face[2] = pnums(5);
		face[3] = pnums(7);
		return 4;
	case 3:
		face[0] = pnums(2);// v0
		face[1] = pnums(3);
		face[2] = pnums(6);
		face[3] = pnums(7);
		return 4;
	case 4:
		face[0] = pnums(0); // v1
		face[1] = pnums(2);
		face[2] = pnums(4);
		face[3] = pnums(6);
		return 4;
	case 5:
		face[0] = pnums(0); // v2
		face[1] = pnums(1);
		face[2] = pnums(4);
		face[3] = pnums(5);
		return 4;
	}
	return 0xFFFFF;
}

inline
uint hexahedron::get_free_num(ulint fase_local_num)
{
	switch(fase_local_num)
	{
	case 0:
		return pnums(4);
	case 1:
		return pnums(0);
	case 2:
		return pnums(0);
	case 3:
		return pnums(0);
	case 4:
		return pnums(1);
	case 5:
		return pnums(2);
	}
	return 0xFFFFF;
}

inline
bool hexahedron::is_adge_exist(uint pnum1, uint pnum2)const
{
	if( (pnum1 == pnums(0) && pnum2 == pnums(1)) || (pnum1 == pnums(1) && pnum2 == pnums(0)) )
		return true;
	if( (pnum1 == pnums(0) && pnum2 == pnums(2)) || (pnum1 == pnums(2) && pnum2 == pnums(0)) )
		return true;
	if( (pnum1 == pnums(2) && pnum2 == pnums(3)) || (pnum1 == pnums(3) && pnum2 == pnums(2)) )
		return true;
	if( (pnum1 == pnums(1) && pnum2 == pnums(3)) || (pnum1 == pnums(3) && pnum2 == pnums(1)) )
		return true;

	if( (pnum1 == pnums(0) && pnum2 == pnums(4)) || (pnum1 == pnums(4) && pnum2 == pnums(0)) )
		return true;
	if( (pnum1 == pnums(1) && pnum2 == pnums(5)) || (pnum1 == pnums(5) && pnum2 == pnums(1)) )
		return true;
	if( (pnum1 == pnums(2) && pnum2 == pnums(6)) || (pnum1 == pnums(6) && pnum2 == pnums(2)) )
		return true;
	if( (pnum1 == pnums(3) && pnum2 == pnums(7)) || (pnum1 == pnums(7) && pnum2 == pnums(3)) )
		return true;

	if( (pnum1 == pnums(4) && pnum2 == pnums(5)) || (pnum1 == pnums(5) && pnum2 == pnums(4)) )
		return true;
	if( (pnum1 == pnums(4) && pnum2 == pnums(6)) || (pnum1 == pnums(6) && pnum2 == pnums(4)) )
		return true;
	if( (pnum1 == pnums(6) && pnum2 == pnums(7)) || (pnum1 == pnums(7) && pnum2 == pnums(6)) )
		return true;
	if( (pnum1 == pnums(5) && pnum2 == pnums(7)) || (pnum1 == pnums(7) && pnum2 == pnums(5)) )
		return true;
	return false;
};

// условный тетраэдр, заданный набором глобальных номеров ребер
struct extend_tetrahedron : public common_elem_struct
{
	extend_tetrahedron(){p = new uint[6];}
	ulint build_face(ulint *face, ulint face_local_num){return 0;}
	ulint get_face_count(){return 0;}
	uint get_free_num(ulint fase_local_num){return 0;}
	uint get_max_face_size(){return 5;}
	bool is_adge_exist(uint pnum1, uint pnum2)const{return true;}
	uint pcount()const{return 6;}
};
