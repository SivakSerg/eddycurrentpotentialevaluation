/*
 * File:   Mod_Doub_Pot_by_parts.h
 * Author: root
 *
 * Created on October 24, 2015, 3:06 PM
 *
 *  Модифицированный потенциал двойного слоя вычисляется альтернативным путём,
 *  с использованием интегрирования по частям.
 */

#ifndef MOD_DOUB_POT_BY_PARTS_H
#define	MOD_DOUB_POT_BY_PARTS_H

#pragma once
#include "TriangleParam.h"
#include "VectorXD.hpp"
#include "VectorBasisInterface.hpp"
#include "GaussRuleInfo.h"
#include <complex>
#include "Single_Layer_Potential.hpp"
#include "ComplexIntegral.h"
#include "base.hpp"

class Mod_Doub_Pot_by_parts : public complex_Functor
{
    bool is_singular;
    VectorBasisInterface *fy, *fx;
    Single_Layer_Potential slp;
    complex<double> integral_over_S_y(const triangle_parameters &Tx, GaussRule gr_rude);
    complex<double> integral_over_S_y_addapted(int N, double epsilon, GaussRule gr_rude, GaussRule gr_refine); // адаптивный метод расчёта интеграла, имеет смысл применять для смежных треугольников
    double lengh_steps_x[3]; // параметр интегрирования по кривой - это сумма длин рёбер S_x
    VectorXD<double,3> vec_edges[3]; // номрированные векторы вдоль рёбер
    VectorXD<double,3> vec_p[3], vec_p_global[3]; // нормальные вектора к соответствующим рёбрам
    VectorXD<double,3> vert[3];
   public:
    void init_potential(VectorBasisInterface *fx, VectorBasisInterface *fy, GaussRule gr, complex<double> vave_num, bool is_singular);
    void init_potential(const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
        GaussRule gr, complex<double> vave_num , bool is_singular);
    complex<double> operator()(double a);
    complex<double> EvalfIntegral(int N=5, double epsilon=1e-6, GaussRule gr_rude=12, GaussRule gr_refine=21); // функция, которая вычисляет требуемый интеграл от модифицированного потенциала двойного слоя методом интегрирования по частям.
    
    // this is the functional way of the potential calculation. This is, obviously, done 
    // in global coordinates.
    complex<double> EvalfIntegral(BasisFunction fxU, BasisFunction fyLambda, 
        ScalarBasisFunction divFx, int fxBasisOrder, int fyBasisOrder,
        const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
        complex<double> waveNumber, bool isSingular = true, int numberOfLevels = 5, 
        double epsilon = 1e-6, GaussRule grRuff = 12, GaussRule grRefined = 21, bool zeroDivergence = false);
    complex<double> adaptedIntegralBasisFunction(BasisFunction fx, BasisFunction fy, 
        ScalarBasisFunction divFx, int fxBasisOrder, int fyBasisOrder,
        const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
        complex<double> waveNumber, bool isSingular = true, int numberOfLevels = 5, 
        double epsilon = 1e-6, GaussRule grRuff = 12, GaussRule grRefined = 21);
    complex<double> adaptedIntegralOnSubset(BasisFunction fy,
        ScalarBasisFunction divFx, int fxBasisOrder, int fyBasisOrder,
        const triangle_parameters & xCarrier, const triangle_parameters & yCarrier,
        complex<double> waveNumber, GaussRule gr, bool isSingular);
    Mod_Doub_Pot_by_parts(GaussRule &gr);
    Mod_Doub_Pot_by_parts();
   ~Mod_Doub_Pot_by_parts();
};

#endif	/* MOD_DOUB_POT_BY_PARTS_H */

