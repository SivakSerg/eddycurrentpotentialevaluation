#pragma once
#include "Scalar_Doub_Layer_Potential.hpp"
#include <vector>
#include "VectorBasisInterface.hpp"
/*
Векторный потенциал двойного слоя. Вычисляется как скалярный потенциал,
скалярная функция заменена векторной базисной функцией.
ВСЕ ВЫЧИСЛЕНИЯ ВЕДУТСЯ В ЛОКАЛЬНЫХ КООРДИНАТАХ ТРЕУГОЛЬНИКА ПЕРЕДАННОЙ ФУНКЦИИ.
*/
class Vect_Doub_Layer_Potential : public Scalar_Doub_Layer_Potential
{
protected:
	// вспомогательные функции для вычисления потенциала двойного слоя
	VectorXD<complex< double >,3> NumericVectDoubLayer(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);		// прибавка к вычислению (получается численным интегрированием), y - точка, заданная в глобальный координатах (для удобства перевода ее в ЛСК данного треугольника)
	VectorXD<complex< double >,3> AdditionalVectDoubLayer(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // функция, которая используется выше (для получения численного интеграла)
	// Исчерпывают вычисление потенциала (полностью численный интеграл)
	VectorXD<complex< double >,3> VectDoubPotentialEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);
	VectorXD<complex< double >,3> VectDoubPotentFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // интегрируемая фукнция
public:
	VectorXD<complex< double >,3> EvalfVectDoubPotential(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);
        VectorXD<complex< double >,3> EvalfVectDoubPotential_no_r_int(VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);
	Vect_Doub_Layer_Potential(GaussRule gr):Scalar_Doub_Layer_Potential(gr){}
	Vect_Doub_Layer_Potential():Scalar_Doub_Layer_Potential(21){}
};
