/*
 * File:   TrigInternalPoints.h
 * Author: root
 *
 * Created on September 12, 2015, 5:05 PM
 */
#pragma once
#ifndef TRIGINTERNALPOINTS_H
#define	TRIGINTERNALPOINTS_H

/*
 * Функция нужна для выдачи точек внутри треугольной области
 */
#include "VectorXD.hpp"
#include <vector>
using namespace std;

bool GetInternalTrigPoints(vector<VectorXD<double,3> > &points, int m, int n, VectorXD<double,3> x0, VectorXD<double,3> x1, VectorXD<double,3> x2);

#endif	/* TRIGINTERNALPOINTS_H */

