#pragma once
#include "VectorXD.hpp"
#include <complex>

using namespace std;

inline
void fast_vector_mutl(const VectorXD<double,3> &a, const VectorXD<double,3> &b, VectorXD<double,3> &res)
{
	res.coords[0] = a.coords[1]*b.coords[2] - a.coords[2]*b.coords[1]; // ay * bz - az * by
	res.coords[1] = a.coords[2]*b.coords[0] - a.coords[0]*b.coords[2]; // az * bx - ax * bz
	res.coords[2] = a.coords[0]*b.coords[1] - a.coords[1]*b.coords[0]; // ax * by - ay * bx
}

inline
void fast_vector_mutl(const VectorXD<complex<double>,3> &a, const VectorXD<double,3> &b, VectorXD<complex<double>,3> &res)
{
	res.coords[0] = a.coords[1]*b.coords[2] - a.coords[2]*b.coords[1]; // ay * bz - az * by
	res.coords[1] = a.coords[2]*b.coords[0] - a.coords[0]*b.coords[2]; // az * bx - ax * bz
	res.coords[2] = a.coords[0]*b.coords[1] - a.coords[1]*b.coords[0]; // ax * by - ay * bx
}

inline
void fast_vector_mutl(const VectorXD<double,3> &a, const VectorXD<complex<double>,3> &b, VectorXD<complex<double>,3> &res)
{
	res.coords[0] = a.coords[1]*b.coords[2] - a.coords[2]*b.coords[1]; // ay * bz - az * by
	res.coords[1] = a.coords[2]*b.coords[0] - a.coords[0]*b.coords[2]; // az * bx - ax * bz
	res.coords[2] = a.coords[0]*b.coords[1] - a.coords[1]*b.coords[0]; // ax * by - ay * bx
}

inline
void fast_vector_mutl(const VectorXD<complex<double>,3> &a, const VectorXD<complex<double>,3> &b, VectorXD<complex<double>,3> &res)
{
	res.coords[0] = a.coords[1]*b.coords[2] - a.coords[2]*b.coords[1]; // ay * bz - az * by
	res.coords[1] = a.coords[2]*b.coords[0] - a.coords[0]*b.coords[2]; // az * bx - ax * bz
	res.coords[2] = a.coords[0]*b.coords[1] - a.coords[1]*b.coords[0]; // ax * by - ay * bx
}

inline
complex<double> fast_dot_prod(const VectorXD<complex<double>,3> &arg1, const VectorXD<double,3> &arg2)
{
	complex<double> ret;
	ret = arg1.coords[0] * arg2.coords[0] +
		arg1.coords[1] * arg2.coords[1] +
		arg1.coords[2] * arg2.coords[2];
	return ret;
}

inline
double fast_dot_prod(const VectorXD<double,3> &arg1, const VectorXD<double,3> &arg2)
{
	double ret;
	ret = arg1.coords[0] * arg2.coords[0] +
		arg1.coords[1] * arg2.coords[1] +
		arg1.coords[2] * arg2.coords[2];
	return ret;
}
