#ifndef UVDECOMPOSER_H
#define UVDECOMPOSER_H

/*
 * Class do decompose vector and scalar basis functions to obtain UV - coordinates.
 * Function should be represendted as the following sum:
 */

 #include "SingleLayerSurfacePotential.h"
 #include <vector>
 #include <functional>
 #include <GaussRuleInfo.h>

class UVdecomposer
{
    public:
        UVdecomposer();
        void setUseLogginingFlang(bool useL) {useLogginging = useL;}
        bool createDecomposeList(const SingleLayerSurfacePotential &trigInfo,
                                 std::function<double(const VectorXD<double,3> &arg)> basisFunction,
                                 GaussRule gr,
                                 int Power,
                                 std::vector<double> &decomposingCoefs);
        double getDecomposedValue(int Power,
                                  const std::vector<double> &decompositionCoefs,
                                  const SingleLayerSurfacePotential &trigInfo,
                                  const VectorXD<double,3> &arg,
                                  int &ErrCode);
        virtual ~UVdecomposer();
    protected:
        bool useLogginging = false;
    private:
};

#endif // UVDECOMPOSER_H
