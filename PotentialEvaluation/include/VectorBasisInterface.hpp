// Этот класс нужен для использования
// базисных функций высокого порядка.
// Базовый интерфейс включает функции
// вычисления граничных операторов дивергенции и ротора.
// Используется пара функций, одни из пространства H1/2(divГ) другая из H1/2(curlГ)
#pragma once
#include "TriangleParam.h"
#include "VectorXD.hpp"
#include "GaussRuleInfo.h"
#include <vector>
#include "base.hpp"
#include <functional>
#include <stdexcept>
#include <exception>
#include <iostream>

using namespace std;

using BasisFunction = std::function<VectorXD<double,3>(const VectorXD<double,3> & arg)>;

using BasisGradient = std::function<Block<double,3>(const VectorXD<double,3> & arg)>;

using ScalarBasisFunction = std::function<double(const VectorXD<double,3> & arg)>;

using ScalarBasisGradient = std::function<VectorXD<double,3>(const VectorXD<double,3> & arg)>;

using BasisOrder = int;

VectorXD<double,3> basisGradient(ScalarBasisFunction fbasis, const VectorXD<double,3> & point, double step);

Block<double,3> basisGradient(BasisFunction fbasis, const VectorXD<double,3> & point, double step);

struct vectCache_elem
{
	VectorXD<double,3> loc_arg, glob_arg, u_val, lam_val; // значения даны в локальных координатах используемого треугольника
};
// структура данных, упрявляющая содержимым кэш-данных функции
class vectCache_rule
{
public:
	uint next_value; // счетчик очередного значения
	vector< vectCache_elem > cache;
	bool use_cache;
public:
	vectCache_rule():use_cache(false),next_value(0){};
	void set_size(GaussRule &gr); // задает размер вектору
};

class VectorBasisInterface
{
public:
	vectCache_rule Cache_values; // хранит кэшированные значения базисной функции и её аргументов в локальных координатах
	VectorXD<double,3> InterpValue; // интерполированное значение в точке для векторных функций (скалярные аргументы не интерполируются)
	Block<double,2> InterpGrad; // сюда сохраняем градиент базисной функции (не автоматически, виртуальной функции нет, user, записывай сам)
	triangle_parameters current_parameter;
	// set-функция определения параметров области
	virtual int get_local_base_count()=0;	// возвращает количество базисных функций (линейно независимые)
	virtual void set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum)=0; // определяем номер базисной функции
	//------------------------------------------------------------------
	// Вычисление базисных функций в точках Гаусса
	//------------------------------------------------------------------
	// базисные функции обоих пространств (в локальных координатах треугольника третья координата всегда НОЛЬ!)
	virtual VectorXD<double,3> lambda(VectorXD<double,2> Gauss_point)=0; // с непрерывными нормальными составляющими на ребре. третья координата НОЛЬ!
	virtual VectorXD<double,3> u_tangen(VectorXD<double,2> Gauss_point)=0; // с непрерывными касательными составляющими на ребре. третья координата НОЛЬ!
	// Граничная дивергенция
	virtual double div_F_lam(VectorXD<double,2> Gauss_point)=0;
	virtual double div_u_tan(VectorXD<double,2> Gauss_point)=0;
	// Граничный ротор
	virtual double curl_F_lam(VectorXD<double,2> Gauss_point)=0;
	virtual double curl_u_tan(VectorXD<double,2> Gauss_point)=0;
	//------------------------------------------------------------------
	// Вычисление в точках, заданных в координатах треугольника
	// т. к. могут не совпадать с элементом, функции аналитически
	// за элемент. Первые две функции реализованы через виртуальные посредством
	// нахождения точки проекции (центральная процедура для этих методов).
	// РЕАЛИЗОВАНА В КЛАССЕ "triangle_parameters". Аналогично можно реализовать
	// функции curl-/- и div-/-. Для этого нужно знать градиенты этих СКАЛЯРНЫХ функций.
	//------------------------------------------------------------------
	// базисные функции обоих пространств (в локальных координатах треугольника третья координата всегда НОЛЬ!)
	virtual VectorXD<double,3> lambda(VectorXD<double,3> Local_arg); // с непрерывными нормальными составляющими на ребре. третья координата НОЛЬ!
	virtual VectorXD<double,3> u_tangen(VectorXD<double,3> Local_arg); // с непрерывными касательными составляющими на ребре. третья координата НОЛЬ!
	// Граничная дивергенция
	virtual double div_F_lam(VectorXD<double,3> Local_arg)=0;
	virtual double div_u_tan(VectorXD<double,3> Local_arg)=0;
	// Граничный ротор
	virtual double curl_F_lam(VectorXD<double,3> Local_arg)=0;
	virtual double curl_u_tan(VectorXD<double,3> Local_arg)=0;
	// Градиент (используется для вычисления в интегрировании по частям)
	// Считаются градиенты только в направлениях тангенсальных составляющих
	// Зависит только от двух касательных составляющих локальной координатной системы треугольника
	virtual Block<double,2> gradient_u_tang(VectorXD<double,3> Local_arg)=0;
	virtual Block<double,2> gradient_lambda(VectorXD<double,3> Local_arg)=0;
	// Эта функция нужна для вычисления численной части векторных потенциаллов С и B
	VectorXD<double,3> get_Global_point(VectorXD<double,2> Gauss_point); // точка Гаусса в глобальных системах координат
	VectorXD<double,3> get_Local_point(VectorXD<double,2> Gauss_point); // точка Гаусса в локальных системах координат текущего треугольника
	// функция оптимизации задачи
	vectCache_elem &get_cache_value(VectorXD<double,2> &GaussPoint);
    // функция, которая используется для согласования параметров задачи.
    // при заданных параметрах согласования записыает изменения во внутренних процедурах функций класса потомка
    virtual void store_local_changes()=0;
    // создаёт копию базисной функции
    virtual VectorBasisInterface *create_copy()const=0;
    virtual ~VectorBasisInterface();
};

// структуры данных для задания векторного базиса (ансамбля)
struct VectorBasisAnsamble
{
	uint edge_basis; // Количество локальных реберных функций должно быть кратно 3-м
	vector<VectorBasisInterface *> basis_functions; // size - edge_basis = количество элементов, ассоциированных с элементом
	VectorBasisAnsamble():edge_basis(0){}
	virtual ~VectorBasisAnsamble();
        void create_copy(vector<VectorBasisInterface*> &copy);
};
