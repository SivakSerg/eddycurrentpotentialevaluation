#include "Scalar_Doub_Layer_Potential.hpp"

class Scalar_Doub_Layer_Potential_With_Subdivisions : public Scalar_Doub_Layer_Potential
{
protected:
    double zero_flag = 1e-7;
    std::vector<triangle_parameters> before_sectioning, after_sectioning;
public:
    Scalar_Doub_Layer_Potential_With_Subdivisions(GaussRule& gr):
        Scalar_Doub_Layer_Potential(gr){}
    std::complex<double> DoubAdditionalScalarFunction(ScalarBasisInterface *f, VectorXD<double,3> &y,
        VectorXD<double,3> &x, double Fy, VectorXD<double,2> &gradient);
    std::complex<double> NumericPartEvaluterDoubScalarPartial(ScalarBasisInterface *f,
        VectorXD<double,3> y, GaussRule gr);
    std::complex<double> EvalfScalarDoubPotPartial(GaussRule gr, ScalarBasisInterface *f,
        VectorXD<double,2> gauss_points, triangle_parameters &Ty, triangle_parameters &TxPartial);
    std::complex<double> ScalarDoubFunctPartial(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x);
    std::complex<double> ScalarDoubPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, ScalarBasisInterface *f,
        VectorXD<double,3> &y);
    std::complex<double> EvalfScalarPotWithSubdivisions(double epsilon, ScalarBasisInterface *f,
        VectorXD<double,2> gauss_points, triangle_parameters &Ty, int N, GaussRule grRude, GaussRule grFine);
    
    // these functions use the std:: name space and standard library function collections to work with basis functions
    // WARNING these new basis functions (obviously) should work only with the global coordinate system
    std::complex<double> EvalfScalarPotWithSubdivisions(double epsilon, 
        std::function<double(const VectorXD<double,3> &arg)> basisFunction,
        std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
        const triangle_parameters & localFunctionSupport, const VectorXD<double,3> & yArgument,
        int numberOfSublevels, GaussRule grRude, GaussRule grFine);
    complex<double> EvalfScalarDoubPotPartial(GaussRule gr, std::function<double(const VectorXD<double,3> &arg)> basisFunction,
        std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
        const VectorXD<double,3> & yGlobal, triangle_parameters &TxPartial);
    complex<double> NumericPartEvaluterDoubScalarPartial(std::function<double(const VectorXD<double,3> &arg)> basisFunction,
        std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
        const VectorXD<double,3> & y, GaussRule gr);
    complex<double> ScalarDoubPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, 
        std::function<double(const VectorXD<double,3> &arg)> basisFunction, 
        const VectorXD<double,3> &y);
};
