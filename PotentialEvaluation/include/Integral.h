#pragma once

#include <vector>
#include <math.h>
#include <functional>
#include <complex>
#include <iostream>

#include "VectorXD.hpp"

// пара коэфициент-корень многочлена Лежандра
struct Leg_pair{
	double coef, x;
};

struct interval
{
	double start, end;
};

class Functor{
public:
	virtual double operator()(double a)=0;
};

class Vector2DFunctor {
public:
        virtual VectorXD<double,2> operator()(double a)=0;
};
// по переданной функции возвращаем интеграл от неё.
class Integral {
	Integral(const Integral &);
	std::vector<Leg_pair> coefs;
	double (*func)(double arg);
public:
	void set_function(double (*f)(double a));
	double get_value(double a, double b, int n);
	double get_value(double a, double b, int n, Functor &f);
        double get_value(double a, double b, int n, const std::function<double(double)> &f);
        std::complex<double> get_value(double a, double b, int n, const std::function<std::complex<double>(double)> &f);
        VectorXD<double,2> get_value(double a, double b, int n, Vector2DFunctor &f);
	bool set_pair(double coef, double x);
	bool remove_last_coefs();
	Integral();
	~Integral();
};
