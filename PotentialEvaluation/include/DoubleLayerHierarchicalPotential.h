#ifndef DOUBLELAYERHIERARCHICALPOTENTIAL_H
#define DOUBLELAYERHIERARCHICALPOTENTIAL_H
#include "SingleLayerSurfacePotential.h"

class DoubleLayerHierarchicalPotential : SingleLayerSurfacePotential
{
    public:
        DoubleLayerHierarchicalPotential();
    protected:
    private:
};

#endif // DOUBLELAYERHIERARCHICALPOTENTIAL_H
