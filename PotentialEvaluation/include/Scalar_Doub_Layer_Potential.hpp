#pragma once
#include "Modyfied_Doub_Layer_Potential.hpp"
#include <vector>
#include "ScalarBasisInterface.hpp"
#include <cmath>
/*
Скалярный потенциал двойного слоя. Используется вместе с модифицированным
потенциалом для вычисления оператора B. Для его вычисления используется
разложение по базисным скалярным функциям скалярного произведения вектор-
ных базисных функций в двух точках.
*/
class Scalar_Doub_Layer_Potential : public Modyfied_Doub_Layer_Potential
{
public:
	//
	double Det(Block<double,3> &M);
	void Kramer(Block<double,3> &M, VectorXD<double,3> &right, VectorXD<double,3> &V);
	// вспомогательная функция, для вычисления гиперболического арктангенса
	double arctanh(double x);
	double sign(double x){return (x>0)? 1. : -1.;} // sign в смысле Соболева, при критическом значении параметра выбирается среднее между правым и левым предельным значением
	// Интегралы для вычисления компонент
	void find_roots(VectorXD<double,3> &loc_coords, double alpha, double &mu, double &nu);
	double F_r3_3(double s, double alpha, double mu, double nu, VectorXD<double,3> &local_coords); // компонента №3 с особенностью r^3
	// Функции вычисления аналитической части
	double r3_3_component(VectorXD<double,3> &arg); // нормальная компонента (№3) особенностью r^3
	double r3_3v2_component(VectorXD<double,3> &arg);// нормальная компонента (версия функции 2) (№3) особенностью r^3
	// Функция вычисления численной части скалярного потенциала двойного слоя
protected: // вспомогательные функции для вычисления потенциала двойного слоя
	complex< double > NumericScalarDoubLayer(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);		// прибавка к вычислению (получается численным интегрированием), y - точка, заданная в глобальный координатах (для удобства перевода ее в ЛСК данного треугольника)
	complex< double > AdditionalScalarDoubLayer(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // функция, которая используется выше (для получения численного интеграла)
	// Исчерпывают вычисление потенциала (полностью численный интеграл)
	complex< double > DoubPotentialEvalf(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);
	complex< double > DoubPotentFunct(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // интегрируемая фукнция
	// тестовая функция для проверки потенциала двойного слоя уравнения Лапласа для линейных базисных функций
	double F_linear(double s, double alpha, VectorXD<double,3> x);
	// вторая версия функции, которая считает интеграл от особенности третьего порядка, без вычисления корней полиномов, но с решением СЛАУ 3x3
	double F_r3_3v2(double s, double alpha, VectorXD<double,3> &local_coords);
	void GetCoefs(VectorXD<double,3> &coefs, double A1, double A2, double B1, double B2, double C1, double C2); // функция, для получения интегралов (используется как вспомогательная функцией выше)
public:
	complex< double > EvalfDoubPotential(bool is_singular, ScalarBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);
	double LinearDoubPot(VectorXD<double,2> GaussPoints_x, triangle_parameters &x_params);
	Scalar_Doub_Layer_Potential(GaussRule gr):Modyfied_Doub_Layer_Potential(gr){}
	Scalar_Doub_Layer_Potential():Modyfied_Doub_Layer_Potential(12){}
};
