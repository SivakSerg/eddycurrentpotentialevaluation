/*
Используется для вычисления оператора B.
B - это сумма потенциаллов: модифицированного
и скалярного.
*/
#pragma once
#include "TriangleParam.h"
#include "VectorXD.hpp"
#include "VectorBasisInterface.hpp"
#include "GaussRuleInfo.h"
#include <complex>
#include "Integral.h"
using namespace std;

// Функция, для вычисления модифицированного потенциалла двойного слоя
// Нормальные компоненты не нужны для вычисления скалярных произведений
// с базисными функциями т. к. базисные функции имеют нулевые компоненты
// для нормальных составляющих элементарного треугольника.
class Modyfied_Doub_Layer_Potential : public Functor
{
public:
        // функции, которыми проверяем интегрирование по u один из интегралов
        double ty, coefs[3];
        double operator()(double uy);
	// параметр, который используется для учета интегральных "нулей"
	double zero;
	triangle_parameters fix_params; // геометрические параметры треугольника, для которого проводятся вычисления
	// вспомогательные функции для вычисления потенциаллов (F - общие интеграллы)
	// для особенностей третьего порядка r
	double F_r2_3(double s, double alpha, double a, double b, double c);
	double F_r1_3(double t, double alpha, VectorXD<double,3> &local_coords);
	// для особенностей второго порядка r
	double F_r2_2(double s, double alpha, VectorXD<double,3> &local_coords);
	double F_r1_2(double t, double alpha, VectorXD<double,3> &local_coords);
	// Компоненты интегралла с особенностью r^3, r = sqrt(x*x + y*y + z*z)
	double r1_3_component(VectorXD<double,3> &local_coords);// координата на оси r1 для интеграла с особенностью порядка (r^3)
	double r2_3_component(VectorXD<double,3> &local_coords);// координата на оси r2 для интеграла с особенностью порядка (r^3)
	// Компоненты интегралла с особенностью r^2, r = sqrt(x*x + y*y + z*z)
	// содержат множителем комплексное волновое число
	double r1_2_component(VectorXD<double,3> &local_coords);// координата на оси r1 для интеграла с особенностью порядка (r^2)
	double r2_2_component(VectorXD<double,3> &local_coords);// координата на оси r2 для интеграла с особенностью порядка (r^2)
	// Переходим к получению аналитических выражений для потенциаллов, которые выражают градиентные составляющие
	double GradientValue(VectorXD<double,3> &local_coords, Block<double,2> &gradients); // local_coords - координаты точки, по которой производится численное интегрирование
	// вспомогательные функции, выражающие решение для функции выше
	double A11_Value(VectorXD<double,3> &local_coords, double A_const);
	double A22_Value(VectorXD<double,3> &local_coords, double A_const);
	double A21_A12_Value(VectorXD<double,3> &local_coords, double A_const, double B_const);
	// общий интеграл для квадрата в числителе
	double s2_t2_grad(double A , double B, double a, double b, double c, VectorXD<double,3> &local_coords, double s);
	double s0_t2_grad(VectorXD<double,3> &local_coords, double t); // t - меняется от t-t* до -t*
        double s2_t2_grad2(double alpha, double s, double t_y, double s_y, double u_y);
	// общий интегралл для смешанного произведения в числителе
	double st_grad(double a, double b, double c, VectorXD<double,3> &local_coords, double st);
	// вспомогательная функция для вычисления выражения арктангеса (гиперболического или тригонометрического)
	double F_arctan(double A1,double A2,double arg);
	complex< double > vave_num_k; // волновое число (выражается через циклическую частоту источника)
	// функции, вычисляющие часть потенциала, которая интегрируется численно
	TrigGaussRuleInfo tgr; // класс, реализующий численное интегрирование
	complex< double > NumericPartEvaluter(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);		// прибавка к вычислению (получается численным интегрированием), y - точка, заданная в глобальный координатах (для удобства перевода ее в ЛСК данного треугольника)
	complex< double > AdditionalFunction(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // функция, которая используется выше (для получения численного интеграла)
	// Функции, которые исчерпывают вычисление потенциалов для случая треугольников на больших расстояниях.
	// Фактически сводится к вычислению слагаемого потенциала "в лоб", без выделения частей с особенностями т. к.
	// при больших расстояниях, этих особенностей просто нет.
	complex< double > ModDoubPotentialEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);
	complex< double > ModDoubPotentFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // интегрируемая фукнция
	// ТЕСТОВАЯ ФУНКЦИЯ
	// используется для вычисления модифицированного потенциала, при нулевом волновом числе
	double TestFunc(double a, double b, double c, double x); // общий интеграл
	double arcsinh( double x );	// обратный гиперболический синус
        //
        double log_doub_area_int(double V, double L, double D, double C, double x); // общий интеграл для всех интегралов от логарифмической функции, подобной гиперболическому арксинусу.
        double F_r2_3_doub_area(double alpha_y, double s_y, double t_y, double u_y, double alpha_x, double s_x, triangle_parameters &y_params, triangle_parameters &x_params); // полный интеграл при взятии по двум треугольникам (у треугольника y берётся лишь одно направление)
        double r2_3_doub_area(double alpha_y, double s_y, triangle_parameters &y_params); // интегрирование производится по одному из направлений локальных коодинат треугольника y_params
public:
	Modyfied_Doub_Layer_Potential():zero(1e-8),tgr(12){} // Порядок метода Гаусса зафиксирован, хочешь повысить, меняй вручную
	Modyfied_Doub_Layer_Potential(GaussRule gr):zero(1e-8),tgr(gr){}
	void set_trg_params(triangle_parameters arg){this->fix_params = arg;}
        void set_gauss_rule(GaussRule gr){tgr = TrigGaussRuleInfo(gr);}
	void set_vave_num( complex< double > k ){vave_num_k = k;}
	virtual complex< double > EvalfPotential(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);// is_singular - есть или нет особенности в подынтегральном выражении
        virtual complex< double > EvalfPotential_no_r_int(VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params); // функция вычисляется без ключевого интеграла с особенностью, выражает только сингулярный случай
	double LinearPotential(VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);
        double F_arctan_2_mult(double A1,double A2,double arg);
};
