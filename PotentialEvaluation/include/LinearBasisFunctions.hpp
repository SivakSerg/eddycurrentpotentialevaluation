#pragma once
#include "VectorBasisInterface.hpp"
#include "FastVectorMult.hpp"
/*
	Данный класс по переданным параметрам треугольника
	строит линейные базисные функции как скалярные, так
	и векторные.
	//
	В принцыпе, эти базисные функции могут быть заданы
	и в глобальных координатах, однако при численном ин-
	тегрировании потенциаллов важно знать их выражение
	через локальные координаты.
	//
	Так же будут приведены выражения для образов этих
	функций граничных операторов (div_Г, curl_Г, gamma_D = n х (*) х n, gamma_N = curl (*) х n)

	ВНИМАНИЕ!!! НЕ РЕАГИРУЕТ НА НОМЕР БАЗИСНОЙ ФУНКЦИИ base_num.
	Номер базисной функции - это номер точки, которая выбрана вершиной треугольника
*/

class LinearVectorBasisFunctions : public VectorBasisInterface
{
public:
	VectorXD<double,3> n;
	static double Det(double M[][2], uint size); // для вычисления векторного произведения
	int sign; // знак граничного ротора
public:
	int get_local_base_count(){return 3;} // по числу ребер треугольника
	LinearVectorBasisFunctions();
	~LinearVectorBasisFunctions();
	// ВСЕ ФУНКЦИИ ВЫЧИСЛЯЮТСЯ ДЛЯ ТЕКУЩЕГО ПЕРЕДАННОГО ТРЕУГОЛЬНИКА
	// С УЧЕТОМ НОМЕРА ВЕРШИНЫ, КОТОРОЙ ПРОТИВОЛЕЖИТ РЕБРО
	void set_current_trig_parameter(triangle_parameters &arg); // base_num игнорируем
	void set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum); // основная функция передачи параметров треугольника в базисную функцию
	VectorXD<double,3> lambda(VectorXD<double,2> Gauss_points); // с непрерывными нормальными составляющими на ребре.
	VectorXD<double,3> u_tangen(VectorXD<double,2> Gauss_points); // с непрерывными касательными составляющими на ребре.
	// Граничная дивергенция
	double div_F_lam(VectorXD<double,2> Gauss_points);
	double div_u_tan(VectorXD<double,2> Gauss_points);
	// Граничный ротор
	double curl_F_lam(VectorXD<double,2> Gauss_points);
	double curl_u_tan(VectorXD<double,2> Gauss_points);
	// Вычисление граниентов (нужно для вычисления численных интеграллов)
	Block<double,2> gradient_u_tang(VectorXD<double,3> Local_arg);
	Block<double,2> gradient_lambda(VectorXD<double,3> Local_arg);
	// Матрицы - это набор векторов-строк градиентов каждой компоненты базисных функций
	// Операторы этих функций, только с аналитическим продолжением
	// Граничная дивергенция
	double div_F_lam(VectorXD<double,3> Local_arg);
	double div_u_tan(VectorXD<double,3> Local_arg);
	// Граничный ротор
	double curl_F_lam(VectorXD<double,3> Local_arg);
	double curl_u_tan(VectorXD<double,3> Local_arg);
	//
	//VectorXD<double,3> lambda(VectorXD<double,3> Local_arg);
	//VectorXD<double,3> u_tangen(VectorXD<double,3> Local_arg);
        void store_local_changes() {}; // здесь записывать изменения не следует
    virtual VectorBasisInterface* create_copy() const;
};
