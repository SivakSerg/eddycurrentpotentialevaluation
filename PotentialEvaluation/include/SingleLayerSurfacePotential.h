#ifndef SINGLELAYERSURFACEPOTENTIAL_H
#define SINGLELAYERSURFACEPOTENTIAL_H
#include "TriangleParam.h"
#include <complex>
#include <functional>
#include "VectorXD.hpp"
#include "GaussRuleInfo.h"
using namespace std;

class SingleLayerSurfacePotential
{
public:
    // параметры плоскости треугольника (координатная система, ассоциированная с плоскостью)
    VectorXD<double,3> r; // точка наблюдения
    VectorXD<double,3> p[3]; // вершины треугольника
    // координатные системы, ассоциированные с рёбрами (параметры треугольника)
    VectorXD<double,3> m[3]; // нормальные направления к рёбрам треугольника в плоскости треугольника
    VectorXD<double,3> s[3]; // тангенциальные направления в направлении рёбер
    double t_i_0[3];// координаты по нормали к ребру
    double s_i_minus[3]; // координаты по касательной к ребру (начала рёбер)
    double s_i_plus[3]; // координаты по касательной к ребру (концы рёбер)
    double R_plus[3]; // расстояние до концов рёбер
    double R_minus[3]; // расстояние до начал рёбер
    double R_i_0[3];
    double l_[3];
    double w_0;
    double u_0;
    double v_0;
    double zero;
    double area;
    // координатная система, ассоциированная с треугольником
    VectorXD<double,3> u, v, w;
    VectorXD<double,3> p_locals[3];
    void create_parameters(const VectorXD<double,3> &r,
                           const VectorXD<double,3> p[3]);
    void create_parameters_for_trinagle_only(const VectorXD<double,3> p[3]);
    double R_pow_n_edge_integral(uint edge_number, int n_order);
    double R_pow_n_times_s_pow_m_edge_integral(uint edge_number, int n_R_order, int m_s_order);
    double R_surface_integral(int n);
    void get_u_line_coefs(double &a, double &b, int edge);
    void get_v_line_coefs(double &a, double &b, int edge);
    double get_edge_integral(int n, int edge, unsigned int q, unsigned int p);
    double get_surface_integral(int n, uint q, uint p);
    complex<double> NumericalPart(int N, std::complex<double> k, double epsilon, GaussRule gr,
            const VectorXD<double,3> &x,
            std::function<double(const VectorXD<double,3> &arg)> basisFunction); // функция получения численной составляющей интеграла
    public:
        // возвращает отрицательное число при ошибке,
        // определяет сколько слагаемых требуется для представления экспоненты в степени k*x с заданной точностью epsilon
        int getTaylorEstimation(std::complex<double> k, double epsilon, double MaxDistance);
        SingleLayerSurfacePotential();
        virtual ~SingleLayerSurfacePotential();
        // вычисляем потенциал Гельмгольца простого слоя.
        std::complex<double> getScalarHelmholtzPotential(std::complex<double> k, double epsilon,
            GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r, int basisOrder,
            std::function<double(const VectorXD<double,3> &arg)> basisFunction);
        std::complex<double> getScalarHelmholtzPotentialCached(std::complex<double> k, double epsilon,
            GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r, int basisOrder,
            std::function<double(const VectorXD<double,3> &arg)> basisFunction,
            const std::vector<double> & decCoefs);
    protected:
        // интеграл от функции 1/r * basisFunction, экспонента будет учитываться позже.
        double getScalarPotential(GaussRule gr, VectorXD<double,3> p[],
            VectorXD<double,3> r, int basisOrder, std::function<double(const VectorXD<double,3> &arg)> basisFunction);
    private:
};

#endif // SINGLELAYERSURFACEPOTENTIAL_H
