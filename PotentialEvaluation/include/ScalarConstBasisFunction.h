#ifndef SCALARCONSTBASISFUNCTION_H
#define SCALARCONSTBASISFUNCTION_H
#include "ScalarBasisInterface.hpp"


#define CONST_BASIS_TYPE (1)
class ScalarConstBasisFunction : public ScalarBasisInterface
{
    public:
        virtual int get_local_base_count();	// возвращает количество базисных функций (динейно независимые)
        virtual void set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum);
        virtual double f_value(VectorXD<double,2> Gauss_point); // вычисляет значение функции в точке интегрирования
        virtual VectorXD<double,2> f_gradient(VectorXD<double,3> Local_arg); // нужна для аналитического продолжения функции за носитель (градиент в ЛСК треугольника)
        virtual ScalarBasisInterface *create_copy()const;     // функция создания копии функции (чисто виртуальная, подлежит определению для каждого класса отдельно)
        virtual int get_type(){return CONST_BASIS_TYPE;} // для соответствия классу-раскладывателю по базисным функциям этого типа
        ScalarConstBasisFunction();
        virtual ~ScalarConstBasisFunction();
    protected:
    private:
};

#endif // SCALARCONSTBASISFUNCTION_H
