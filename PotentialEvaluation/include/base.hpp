/*
Данный модуль включает:
А) типы данных
Б) интерфейс матрицы и предобусловливателя (для решателей)
В) интерфейс конечных подобластей (для построителей сетки)
Г) интерфейс вычислителей (итерационные и прямые методы)
Д) некоторые константы
*/
#pragma once
#ifndef SIWAKSERGBASEPACK
#define SIWAKSERGBASEPACK
//#include <types.h>
#include <exception>
#include <stdexcept>
#include <fstream>
#include <complex>
#include "//usr//include//x86_64-linux-gnu//sys//types.h"
//---------------------------------------------------
//КОНСТАНТЫ
//---------------------------------------------------
#ifndef PI
#define PI (3.141592653589793)
#endif // PI
#ifndef EXP
#define EXP (2.718281828)
#endif // EXP
#ifndef STR
typedef const char * STR;
#endif

#ifndef SGS
#define MU0 (4*PI*1e-7)
#define EPS0 (8.854187817e-12)
#define SIG0 (1.0)
#define GAMMA0 (1.0)
#else
#define MU0 (1.0)
#define EPS0 (1.0)
#define SIG0 (899000000000)
#define GAMMA0 (1.0)
#endif
//---------------------------------------------------
//ТИПЫ
//---------------------------------------------------
//#ifndef ulint
typedef uint ulint;
//#endif
//#ifndef uint
//typedef unsigned long int uint;
//#endif
//#define ulint uint

#ifndef transpose
template<class T> T transpose(T arg)
{
	return arg;
}
#endif

inline
double sign(double x){return (x > 0)?1. : -1.; }
//---------------------------------------------------
//КЛАСС МАТРИЦЫ И ПРЕДОБУСЛОВЛИВАТЕЛЯ
//---------------------------------------------------
/* обобщённый тип используемых предобуславливателей */
enum SYM_MATRIX_PREC_TYPE {
	LU,		// для всех типов матриц (для блочных не всегда сходится)
	LU_sq,	// для блочно симметричных матриц и некоторых других
	LLt,	// только для симметричных матриц (БЛОЧНО СИММЕТРИЧНЫЕ МАТРИЦЫ НЕ СИММЕТРИЧНЫ - ЗАКОН)
	Di		// диагональное предобусловливание (МОЖЕТ ОКАЗАТЬСЯ ТОЧНО БЫСТРЕЕ)
};
template<class Block, class Vector> class precond;
// Обобщённый класс матрицы (для различных форматов)
template<class Block, class Vector> class Matrix
{
	friend void *precond<Block, Vector>::get_matrix_buffer(Matrix<Block, Vector> *arg, uint buffer_num);
protected:
	virtual void *get_buff (uint n)=0;				   // возвращает форматные массивы
public:
	virtual bool multyply(Vector *arg, Vector *res)=0; // функция умножения матрицы
	virtual int show_type()=0;						   // определяет тип матрицы
	virtual ulint get_rowsize()=0;		// возвращает число строк матрицы
	virtual ulint get_colsize()=0;		// возвращает число столбцов.
	virtual int get_prec_type()=0;		// возвращает тип используемого предобуславливателя
};
// обобщённый класс предобуславливателя (для различных форматов, с различным типом разложения на 2 матрицы)
template<class Block, class Vector> class precond
{
	friend Matrix<Block, Vector>;
protected:
	void */*precond<Block, Vector>::*/get_matrix_buffer(Matrix<Block, Vector> *arg, uint buffer_num){return arg->get_buff(buffer_num);}
public:
	virtual bool forward_stroke(Vector *right, Vector *res)=0;					    //прямой ход
	virtual bool backward_stroke(Vector *right, Vector *res)=0;					    //обратный ход
	virtual bool Up_multiply(Vector *arg, Vector *res)=0;	// умножение верхнего треугольника
	virtual bool Low_multiply(Vector *arg, Vector *res)=0;  // умножение нижнего треугольника
	virtual bool PreconditionerFormation(Matrix<Block,Vector> *arg)=0;	//формирование предобуславливателя
	virtual int show_type()=0;											// определяет тип формата матрицы
};

//---------------------------------------------------
//БАЗОВЫЙ ЭЛЕМЕНТ ДЛЯ ПОСТРОИТЕЛЯ СЕТОК
//---------------------------------------------------
// структура, которая используется для передачи списка элементов
struct common_elem_struct
{
	uint *p, matr;
	common_elem_struct():p(0),matr(0){}
	virtual ulint build_face(ulint *face, ulint face_local_num)=0; //функция, которая строит набор номеров точек, определяющих грань, возвращает размер грани
	virtual ulint get_face_count()=0;		 //функция, возвращает количество граней
	virtual uint get_free_num(ulint fase_local_num)=0; // функция возвращает номер вершины, которая не лежит на заданной грани (для определения направления внешней нормали)
	virtual uint get_max_face_size()=0; // возвращает макисимальное количество точек, из которых может состоять грань элемента
	virtual bool is_adge_exist(uint pnum1, uint pnum2)const=0; // определяет, есть ли на элементе ребро с глобальными номерами pnum1 и pnum2
	uint& material(){return matr;}   // возвращает номер материалла текущего элемента
	uint& pnums(int n){return p[n];}	 // возвращает локальный номер точки
        uint pnums(int n)const{return p[n];} // для вызова из константных объектов
	virtual uint pcount()const=0;	  // возвращает количество точек элемента
	virtual ~common_elem_struct()
	{
		if(!p)delete[] p;
		p=0;
	}
};
//---------------------------------------------------
//БАЗОВЫЙ ИНТЕРФЕЙС РЕШАТЕЛЯ (итерационного, но и прямой в него впишется)
//---------------------------------------------------
template<class Block, class Vector, class ScalarType> class base_solver
{
public:
	virtual bool set_params(Vector *x0, Matrix<Block, Vector> *A, precond<Block, Vector> *Pr, Vector *right)=0;
	virtual int lin_solve(double eps, long critical, Vector *result)=0;
	virtual ScalarType ScalarMult(Vector *a, Vector *b)=0;
};
//---------------------------------------------------
//Класс вывода в файл различных структур данных
//---------------------------------------------------
class custom_output
{
public:
    inline bool array_output(double *values, int size, STR filename);
    inline bool array_output(std::complex<double> *values, int size, STR filename);
};
inline
bool custom_output::array_output(double* values, int size, STR filename)
{
    std::ofstream Output;
    Output.precision(10);
    Output.open(filename);
    Output << size << "\n";
    for(int i=0; i<size; ++i)
        Output << values[i] << "\n";
    Output << "\n";
    Output.close();
    return true;
}
inline
bool custom_output::array_output(std::complex<double> *values, int size, STR filename)
{
    std::ofstream Output;
    Output.precision(10);
    Output.open(filename);
    Output << (2*size) << "\n";
    for(int i=0; i<size; ++i)
        Output << values[i].real() << "\t" << values[i].imag() << "\n";
    Output << "\n";
    Output.close();
    return true;
}
#endif
