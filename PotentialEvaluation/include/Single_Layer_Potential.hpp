#pragma once
#include "VectorBasisInterface.hpp"
#include "ScalarBasisInterface.hpp"
#include "GaussRuleInfo.h"
#include <functional>
#include <complex>
using namespace std;
#define NON_SCALED_ZERO (1e-6)
/*
 Используется в операторе A и в его модификациях.
 Потенциал простого слоя для векторной, и для скалярной задачи.
 Вспомогательные функции класса с постфиксом Div - вычисляют
 потенциал от граничной дивергенции векторных базисных функций.
 Векторные функции возвращают результат в локальных координатах
 треугольника (ЛСК).
*/
struct  BemPotentialData {
    double SingleLayer;
    VectorXD<double,3> SingleLayerVec;
    double DoubleLayer;
    VectorXD<double,3> DoubleLayerVec;
};

class Single_Layer_Potential
{
public:
	mutable double zero_flag; // для относительного сравнения с нулём (при измельчении сетки)
	mutable triangle_parameters fix_params; // текущие параметры треугольника, по которому производим численное интегрирование
	complex< double > vave_num_k; // волновое число (выражается через циклическую частоту источника)
	// функции, вычисляющие часть потенциала, которая интегрируется численно
        mutable VectorXD<double,3> saved_curl;
        bool is_linear;
        mutable std::vector<triangle_parameters> before_sectioning, after_sectioning;
public:
	TrigGaussRuleInfo tgr; // класс, реализующий численное интегрирование
        virtual ~Single_Layer_Potential(){};
        void set_linear_function_usage(bool is_linear){this->is_linear = is_linear;}
protected:
	// вспомогательные функции получения потенциала
	double F_r_1(double s, double alpha, VectorXD<double,3> &local_coords); // общий интеграл для вычисления потенциала
public: // сделана публичной для тестирования
        BemPotentialData CalcPotentialNew(VectorXD<double,3> p[]) const;
	double r_1component(VectorXD<double,3> &local_coords); // вычисляет потенциал простого слоя оператора ЛАПЛАСА (Экспонента учитывается при численном интегрировании)
        double r_1componentV2(const VectorXD<double,3> &local_coords);
        double r_1componentV2global(const VectorXD<double,3> &global_coords) const;
private:
	// для разрешения сингулярностей при численном интегрировании
	VectorXD<complex< double >,3> NumericPartEvaluter(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);		// прибавка к вычислению (получается численным интегрированием), y - точка, заданная в глобальный координатах (для удобства перевода ее в ЛСК данного треугольника)
        VectorXD<complex< double >,3> NumericPartEvaluter2(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params); // добавка раскрывает градиентную часть
	complex< double > NumericPartEvaluterDiv(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);		// прибавка к вычислению (получается численным интегрированием), y - точка, заданная в глобальный координатах (для удобства перевода ее в ЛСК данного треугольника)
        complex< double > NumericPartEvaluterLambda(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> loc_nx);
	VectorXD<complex< double >,3> AdditionalFunction(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // функция, которая используется выше (для получения численного интеграла)
        VectorXD<complex<double>,3> AdditionalFunction2(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // учитывает градиентную составляющую
	complex< double > AdditionalFunctionDiv(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // функция, которая используется выше (для получения численного интеграла)
        complex< double > AdditionalFunctionLambda(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints, VectorXD<double,3> loc_nx);
        // Для того, чтобы исчерпать численную часть интегрирования при k=0,
        // вводим функции интегрирования векторного потенциала простого слоя
        // для интегрирования градиента функции
        double s_t_grad(double alpha, double s, double t_y, double s_y, double u_y) const;
        double A2_Value(VectorXD<double,3> &local_coords, double A_const) const; // первая компонента вектора
        double A1_Value(VectorXD<double,3> &local_coords, double A_const) const; // вторая компонента вектора
	// Функции, которые исчерпывают вычисление потенциалов для случая треугольников на больших расстояниях.
	// Фактически сводится к вычислению слагаемого потенциала "в лоб", без выделения частей с особенностями т. к.
	// при больших расстояниях, этих особенностей просто нет.
	VectorXD<complex< double >,3> SinglePotentialEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);
        complex<double> ScalarPotEvalfPartial(triangle_parameters &Tcurrent, GaussRule gr, ScalarBasisInterface *f, VectorXD<double,3> &y);
        complex<double> ScalarFunctPartial(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x);
	complex< double > SinglePotentialEvalfDiv(VectorBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &y_params);
        complex< double > SinglePotentialLambdaEvalf(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> loc_nx);
	VectorXD<complex< double >,3> SinglePotentFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // интегрируемая фукнция
	complex<double> SinglePotentFunctDiv(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints); // интегрируемая фукнция
        complex<double> LambdaPotFunct(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints, VectorXD<double,3> loc_nx); // Для интегрирования по частям модифицированного потенциала двойного слоя
        // Функции вычисления интегралов для скалярных потенциалов простого слоя
        complex<double> ScalarPotEvalf(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &Ty);
        complex<double> ScalarFunct(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints);
        complex<double> AdditionalScalarFunction(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> gauss_points);
        complex<double> AdditionalScalarFunction(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x);
        VectorXD<complex<double>,3> ScalarPotEvalfCurl(ScalarBasisInterface *f, VectorXD<double,3> &y, triangle_parameters &Ty);
        VectorXD<complex<double>,3> ScalarFunctCurl(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> GaussPoints);
        VectorXD<complex<double>,3> AdditionalScalarFunctionCurl(ScalarBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,2> gauss_points);
public:
	Single_Layer_Potential():tgr(21), is_linear(false){}
	Single_Layer_Potential(GaussRule gr):tgr(gr), is_linear(false){}
	void set_trg_params(triangle_parameters arg){this->fix_params = arg;}
	void set_vave_num( complex< double > k ){vave_num_k = k;}
        void set_gauss_rule(GaussRule gr){tgr = TrigGaussRuleInfo(gr);}
	VectorXD<complex<double>,3> EvalfSingleLayerVectorPot(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);// is_singular - есть или нет особенности в подынтегральном выражении
        VectorXD<complex<double>,3> EvalfSingleLayerVectorPot2(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);
        // sectioning for the calculation of the vector valued single layer potential
        VectorXD<complex<double>,3> EvalfSingleLayerVectorPotSections(double epsilon, VectorBasisInterface *f,
            VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params, int N, GaussRule grRude, GaussRule grFine);
        VectorXD<complex<double>,3> EvalfSingleLayerVectorPotPartial(GaussRule gr, VectorBasisInterface *f,
            VectorXD<double,2> gauss_points, triangle_parameters &Ty, triangle_parameters &TxPartial);
        VectorXD<complex<double>,3> NumericPartEvaluterPartial(VectorBasisInterface *f,
            VectorXD<double,3> y, GaussRule gr);
        VectorXD<complex<double>,3> AdditionalFunctionPartial(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x);
        VectorXD<complex<double>,3> VectorPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, VectorBasisInterface *f, VectorXD<double,3> &y);
        VectorXD<complex<double>,3> VectorFunctPartial(VectorBasisInterface *f, VectorXD<double,3> &y, VectorXD<double,3> &x);

	complex<double> EvalfSingleLayerPot(bool is_singular, VectorBasisInterface *f, VectorXD<double,2> GaussPoints_y, triangle_parameters &y_params);// is_singular - есть или нет особенности в подынтегральном выражении
        complex<double> EvalfLambdaPot(bool is_singular, VectorBasisInterface *fx, VectorXD<double,3> glob_y_arg, triangle_parameters &Sx); // Для интегрирования по частям модифицированного потенциала двойного слоя
        complex<double> EvalfScalarPotential(bool is_singular, VectorBasisInterface *fx, VectorXD<double,3> glob_y_arg, triangle_parameters &Sx);
        complex<double> EvalfScalarPotV(bool is_singular,ScalarBasisInterface *fx,VectorXD<double,2> gauss_points, triangle_parameters &Ty);
        complex<double> EvalfScalarPotVpartial(GaussRule gr, ScalarBasisInterface *f,
            VectorXD<double,2> gauss_points, triangle_parameters &Ty, triangle_parameters &TxPartial);
        complex<double> EvalfScalarPotVsubdivisions(double epsilon, ScalarBasisInterface *f,
            VectorXD<double,2> gauss_points, triangle_parameters &Ty, int N, GaussRule grRude, GaussRule grFine);
        complex<double> NumericPartEvaluterScalar(ScalarBasisInterface *f, VectorXD<double,3> y, triangle_parameters  &Ty);
        complex<double> NumericPartEvaluterScalarPartial(ScalarBasisInterface *f, VectorXD<double,3> y, GaussRule gr);
        VectorXD<complex<double>,3> EvalfScalarPotVCurl(bool is_singular,ScalarBasisInterface *f,VectorXD<double,2> gauss_points,triangle_parameters &Ty);
        VectorXD<complex<double>,3> NumericPartEvaluterScalarCurl(ScalarBasisInterface *f, VectorXD<double,3> y, triangle_parameters &Ty);
        
        // these functions use the std:: name space and standard library function collections to work with basis functions
        // WARNING these new basis functions (obviously) should work only with the global coordinate system
        VectorXD<complex<double>,3> EvalfSingleLayerVectorPotSections(double epsilon,
            std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> vectorBasisFunction,
            std::function<Block<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
            const triangle_parameters & localFunctionSupport, const VectorXD<double,3> & yArgument,
            int numberOfSubdivision, GaussRule grRude, GaussRule grFine) const;
        VectorXD<complex<double>,3> EvalfSingleLayerVectorPotPartial(GaussRule gr, 
            std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> basisFunction,
            std::function<Block<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
            const VectorXD<double,3> & yGlobal, triangle_parameters &TxPartial) const;
        VectorXD<complex<double>,3> NumericPartEvaluterPartial(
            std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> basisFunction,
            std::function<Block<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
            const VectorXD<double,3> & y, GaussRule gr) const;
        VectorXD<complex<double>,3> VectorPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, 
            std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> basisFunction,
            const VectorXD<double,3> &y) const;
        
        // these functions use the std:: name space and standard library function collections to work with basis functions
        // WARNING these new basis functions (obviously) should work only with the global coordinate system
        std::complex<double> EvalfSingleLayerScalarPotSections(double epsilon,
            std::function<double(const VectorXD<double,3> &arg)> vectorBasisFunction,
            std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
            const triangle_parameters & localFunctionSupport, const VectorXD<double,3> & yArgument,
            int numberOfSubdivision, GaussRule grRude, GaussRule grFine) const;
        std::complex<double> EvalfSingleLayerScalarPotPartial(GaussRule gr, 
            std::function<double(const VectorXD<double,3> &arg)> basisFunction,
            std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
            const VectorXD<double,3> & yGlobal, triangle_parameters &TxPartial) const;
        std::complex<double> NumericScalarPartEvaluterPartial(
            std::function<double(const VectorXD<double,3> &arg)> basisFunction,
            std::function<VectorXD<double,3>(const VectorXD<double,3> &arg)> gradBasisFunction,
            const VectorXD<double,3> & y, GaussRule gr) const;
        std::complex<double> ScalarPotEvalfPartial(triangle_parameters &Tk, GaussRule gr, 
            std::function<double(const VectorXD<double,3> &arg)> basisFunction,
            const VectorXD<double,3> &y) const;        
};
