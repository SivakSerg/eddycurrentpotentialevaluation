#pragma once

#include <vector>
#include <math.h>
#include <complex>
#include "Integral.h"
#include <functional>
using namespace std;


class complex_Functor{
public:
    virtual complex<double> operator()(double a) = 0;
};
// по переданной функции возвращаем интеграл от неё.
class complex_Integral {
    complex_Integral(const complex_Integral &);
    std::vector<Leg_pair> coefs;
    complex<double> (*func)(double arg);
public:
    void set_function(complex<double> (*f)(double a));
    complex<double>  get_value(double a, double b, int n);
    complex<double>  get_value(double a, double b, int n, complex_Functor &f);
    complex<double>  get_value(double a, double b, int n, function<complex<double>(double a)> f);
    bool set_pair(double coef, double x);
    bool remove_last_coefs();
    void set_6_points();
    complex_Integral();
    ~complex_Integral();
};
