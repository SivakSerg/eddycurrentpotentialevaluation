#pragma once
#include "ScalarBasisInterface.hpp"

#define LINEAR_BASIS_TYPE (1)
// линейная базисная функция на треугольнике - это обычная линейная L-координата
class LinearScalarBasis : public ScalarBasisInterface
{
	int base_num; // номер базисной функции, совпадает с номером L-координаты (соответствующей вершине)
public:
	LinearScalarBasis():base_num(0){}
	int get_local_base_count(){return 3;} // по числу вершин
	void set_base_num(uint base_num){this->base_num = base_num % 3;}
	void set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum); // основная функция передачи параметров треугольника в базисную функцию
	void set_current_trig_parameter(triangle_parameters &arg){this->current_parameter = arg;} // функция тестировщиков, для передачи парметров напрямую
	int get_type(){return LINEAR_BASIS_TYPE;}
	double f_value(VectorXD<double,2> Gauss_point);
	VectorXD<double,2> f_gradient(VectorXD<double,3> Local_arg)// вектор градиента - константный вектор
	{
		VectorXD<double,2> ret;
		for(int i=1; i<3; i++)
			ret.coords[i-1] = current_parameter.LMtr.block[0][i];
		return ret;
	}
    virtual ScalarBasisInterface* create_copy() const;

};


// Класс для разложения по скалярному базису проекции векторной базисной функции
class LinearVectorBasisDecomposer : public VectorBasisDecomposerInterface
{
	void build_preudo_buffs(triangle_parameters &params, triangle &trg, VectorXD<double,3> *points);
public:
	virtual int get_type(){return LINEAR_BASIS_TYPE;}
	bool Decompose_lambda
		(VectorBasisInterface *funct,
		 VectorXD<double,3> &n,
		 basis_storage &combination); // разложение скалярными базисными функциями
	bool Decompose_u
		(VectorBasisInterface *funct,
		 VectorXD<double,3> &n,
		 basis_storage &combination); // разложение скалярными базисными функциями
	bool CreateCombination
		(VectorBasisInterface *funct,
		 basis_storage &combination);
	double u_test(VectorBasisInterface *funct, VectorXD<double,3> n, basis_storage &combination, VectorXD<double,3> &glob_arg); // тестировочная функция, возвращающая норму разности между реальным значением функции и разлагаемым
};
