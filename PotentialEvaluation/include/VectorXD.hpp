#pragma once
#include <complex>
#include <math.h>
#include "base.hpp"
/*#ifndef ulint
typedef unsigned long ulint;
#endif
#ifndef uint
typedef unsigned long uint;
#endif*/
#ifndef abs
template<class T> T abs(const T &a) {return a;}
#endif // abs

template<class T, ulint size> class VectorXD;

template<class T, ulint size> class Block
{
protected:
	static T one;
	static T zero;
	static bool one_def;
	static bool zero_def;
public:
        Block()
        {
            Zerofied();
        }
	T block[size][size];	
	static void set_one(T one) {Block<T,size>::one=one; one_def=true;}
	static void set_zero(T zero) {Block<T,size>::zero=zero; zero_def=true;}
	static T get_one(){return one;}
	static T get_zero(){return zero;}
	uint get_dim() const {return size;}
	// ЖХОЛГЙС ОЕПВИПДЙНБ Ч ФПН ЮЙУМЕ Й ДМС УТБЧОЕОЙС БТЙЖНЕФЙЮЕУЛЙИ ФЙРПЧ Ч ТЕЫБФЕМЕ 
	T norma()
	{
            if (!zero_def) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }
            T accum = zero;		
            for(ulint i=0; i<size; i++)
                    for(ulint j=0; j<size; j++)
                            accum =accum + block[i][j]*block[i][j];
            return sqrt(accum);
	}
	Block<T,size> &operator=(const T arg)
	{
		for(int i=0; i<size; i++)
			for(int j=0; j<size; j++)
				block[i][j]=arg;
		return *this;
	}
	Block<T, size> &operator=(const Block<T, size> &arg)
	{
		for(unsigned int i=0; i<size; i++)
			for(unsigned int j=0; j<size; j++)
				block[i][j] = arg.block[i][j];
		return *this;
	}
        Block<T, size> &operator*=(const T arg)
        {
            for(int i=0; i<size; ++i)
                for(int j=0; j<size; ++j)
                    block[i][j] *= arg;
            return *this;
        }
	Block<T,size> operator*(const T arg) const
	{
		Block<T,size> res;
		for(uint i=0; i<size; i++)
			for(uint j=0; j<size; j++)
				res.block[i][j] = block[i][j] * arg;
		return res;
	}
	Block<T,size> operator+(const Block<T,size> & B)
	{
		for(int i=0; i<size; i++)
			for(int j=0; j<size; j++)
				B.block[i][j]=block[i][j]+B.block[i][j];
		return B;
	}	
	Block<T,size> operator-(const Block<T,size> & B) const
	{
                Block<T,size> newBlock;
		for(int i=0; i<size; i++)
			for(int j=0; j<size; j++)
				newBlock.block[i][j]=block[i][j]-B.block[i][j];
		return newBlock;
	}
	VectorXD<T, size> operator*(const VectorXD<T, size> &B) const
	{
            if (!zero_def) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }            
            VectorXD<T, size> result;
            result.common_init(zero);
            for(unsigned int i=0; i<size; i++)
                    for(unsigned int j=0; j<size; j++)
                            result.coords[i]=result.coords[i]+block[i][j]*B.coords[j];
            return result;
	}
        VectorXD<std::complex<T>, size> operator*(const VectorXD<std::complex<T>, size> & B) const
        {
            VectorXD<std::complex<T>, size> result;
            std::complex<T> czero(zero);
            result.common_init(czero);
            for(int i=0; i<size; i++)
		for(int j=0; j<size; j++)
                    result.coords[i]=result.coords[i]+block[i][j]*B.coords[j];
            return result;
        }
	Block<T,size> operator*(const Block<T,size> & B) const
	{
            if (!zero_def) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }            
            Block<T,size> result;
            result=zero;
            for(int i=0; i<size; i++)
                    for(int k=0; k<size; k++)
                            for(int j=0; j<size; j++)
                                    result.block[i][k]=result.block[i][k]+block[i][j]*B.block[j][k];
            return result;
	}
	Block<T,size> operator/(const Block<T,size> B)
	{
		return  B.inverse() * (*this);
	}
	// задание еденичной матрицы:
	bool Identified()
	{
            if (!one_def) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }
            if(one_def)
            {
                for(unsigned int i=0; i<size; i++)
                        for(unsigned int j=0; j<size; j++)
                                block[i][j]=(i==j)? one : zero;
                return true;
            }
            return false;
	}
	bool Zerofied()
	{
            if (!zero_def) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }            
            if(zero_def)
            {
                    for(int i=0; i<size; i++)
                            for(int j=0; j<size; j++)
                                    block[i][j]=zero;
                    return true;
            }
            return false;
	}
	// решение системы линейных уравнений методом Гаусса:
	bool linsolve(VectorXD<T,size> *F, uint F_count)
	{
		uint i, j, k, posmax, f, N, poses[size], swap;
		T tmp;
		Block<T,size> A;
		A = *this;
		N = size;
	
		for(k=0; k<N; k++)
			poses[k]=k;

		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (fabs(A.block[poses[i]][k])>fabs(A.block[poses[posmax]][k])) posmax=i;
			tmp = A.block[poses[posmax]][k];
			
			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			/*далее производим Гауссовское исключение*/
			for (j=k; j<N; j++) A.block[posmax][j]=A.block[posmax][j] / tmp;		// сокращаем на 
			for (f=0; f<F_count; f++) F[f].coords[posmax]=F[f].coords[posmax] / tmp; // ведущий элемент.

			for (i=k+1; i<size; i++) 
			{
				tmp=A.block[poses[i]][k];
				for (j=k; j<N; j++)
					A.block[poses[i]][j]=A.block[poses[i]][j] - A.block[posmax][j]*tmp;
				for (f=0; f<F_count; f++) 
					F[f].coords[poses[i]]=F[f].coords[poses[i]] - F[f].coords[posmax]*tmp;
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=N-1; k>0; k--)
				for (i=0; i<k; i++)
					for (f=0; f<F_count; f++)
						F[f].coords[poses[i]]=F[f].coords[poses[i]] - A.block[poses[i]][k]*F[f].coords[poses[k]];

			//Переупорядочиваем результат:
			VectorXD<T,size> reordered;
			for(f=0; f<F_count; f++)
			{
				for(i=0; i<size; i++)
					reordered.coords[i]=F[f].coords[poses[i]];
				F[f]=reordered;
			}
		return true;
	}
	// код точно такой же как и у решателя, на месте еденичной F должна оказаться обратная матрица
	Block<T,size> inverse()
	{
		uint i, j, k, posmax, f, N, F_count, poses[size], swap; // poses - массив индексов перестановок (хранит номера ведущих строк на k-ой итерации)
 		T tmp;
		Block<T,size> A;
		Block<T,size> F;
		F.Identified();		// в правой части - еденичная матрица, считаем, что one - была определена
		F_count = size;
		A = *this;
		N = size;

		for(k=0; k<N; k++)
			poses[k]=k;
	
		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
			{
                            if ( fabs(A.block[poses[i]][k])>fabs(A.block[poses[posmax]][k])) 
                            {
                                posmax=i;
                            }
                        }
			tmp = A.block[poses[posmax]][k];

			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			/*далее производим Гауссовское исключение*/
			for (j=k; j<N; j++) 
				A.block[posmax][j]=A.block[posmax][j]/tmp;
			for (f=0; f<F_count; f++)
				F.block[posmax][f]=F.block[posmax][f]/tmp;

			for (i=k+1; i<size; i++) 
			{
				tmp=A.block[poses[i]][k];
				for (j=k; j<N; j++)
					A.block[poses[i]][j]=A.block[poses[i]][j] - A.block[posmax][j]*tmp;
				for (f=0; f<F_count; f++) 
					F.block[poses[i]][f]=F.block[poses[i]][f] - F.block[posmax][f]*tmp;
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=N-1; k>0; k--)
				for (i=0; i<k; i++)
					for (f=0; f<F_count; f++)
						F.block[poses[i]][f]=F.block[poses[i]][f] - A.block[poses[i]][k]*F.block[poses[k]][f];

			//Переупорядочиваем результат:
			VectorXD<T,size> reordered;
			for(f=0; f<F_count; f++)
			{
				for(i=0; i<size; i++)
					reordered.coords[i]=F.block[poses[i]][f];
				for(i=0; i<size; i++)
					F.block[i][f]=reordered.coords[i];
			}
			return F;
	}
	T AbsDet()
	{
		uint i, j, k, posmax, N, poses[size], swap; // poses - массив индексов перестановок (хранит номера ведущих строк на k-ой итерации)
 		T maxelem, rowelem;
		Block<T,size> A;
		T result=one;
		A = *this;
		N = size;

		for(k=0; k<N; k++)
			poses[k]=k;
	
		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (fabs(A.block[poses[i]][k])>fabs(A.block[poses[posmax]][k])) posmax=i;
			maxelem = A.block[poses[posmax]][k];

			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			for (i=k+1; i<size; i++) 
			{
				rowelem=A.block[poses[i]][k];
				for (j=k; j<N; j++)
					A.block[poses[i]][j]=A.block[poses[i]][j] - A.block[posmax][j]* ( rowelem / maxelem );
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=0; k<N; k++)
				result = result * A.block[poses[k]][k];		
		return fabs(result);
	}
	T Det(T MinusOne)
	{
		uint i, j, k, posmax, N, poses[size], swap; // poses - массив индексов перестановок (хранит номера ведущих строк на k-ой итерации)
 		T maxelem, rowelem;
		Block<T,size> A;
		T result=one;
		A = *this;
		N = size;

		for(k=0; k<N; k++)
			poses[k]=k;
	
		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (fabs(A.block[poses[i]][k])>fabs(A.block[poses[posmax]][k])) posmax=i;
			maxelem = A.block[poses[posmax]][k];

			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			for (i=k+1; i<size; i++) 
			{
				rowelem=A.block[poses[i]][k];
				for (j=k; j<N; j++)
					A.block[poses[i]][j]=A.block[poses[i]][j] - A.block[posmax][j]* ( rowelem / maxelem );
			}			
		}
		bool reverse=false;
		ulint val = poses[0];
		for(int i=0; i<N; i++)
		{
			if(poses[i]<val)
				reverse = !reverse;
			val=poses[i];
		}
		
		// после превидения матрицы к треугольному виду, находим правую часть
		for (k=0; k<N; k++)
			result = result * A.block[poses[k]][k];
		if(reverse)
			result = result * MinusOne;

		return result;
	}
};
// Определение статических членов класса:
template<class T, ulint size> T Block<T,size>::one;
template<class T, ulint size> bool Block<T,size>::one_def = false;
template<class T, ulint size> T Block<T,size>::zero;
template<class T, ulint size> bool Block<T,size>::zero_def = false;

// Функция извлечения квадратного корня из матрицы, а на практике - это
// обычное LLt разложение матрицы: (возвращаем нижний треугольник)
template<class T, ulint size> static Block<T, size> sqrt(Block<T, size> A)
{
	Block<T, size> L;
	T sumdi, suml;
	L.Zerofied();
	for(ulint i=0; i<size; i++)
	{
		sumdi=Block<T, size>::get_zero();
		for(ulint j=0; j<i; j++)
		{
			suml=Block<T, size>::get_zero();
			for(ulint k=0; k<j; k++)
				suml=suml+L.block[i][k]*L.block[j][k]; // нижняя треугольная матрица
			L.block[i][j]=(A.block[i][j]-suml)/L.block[j][j];
			sumdi = sumdi + L.block[i][j] * L.block[i][j];
		}
		L.block[i][i]=sqrt(A.block[i][i]-sumdi);
	}
	return L;
}
/*LUsq - разложение матрицы (для предобуславливателя)*/
template<class T, uint size> bool lu_sq_decomp(Block<T,size> &L, Block<T,size> &U, Block<T,size> &M)
{
	L.Zerofied(); U.Zerofied();
	T sumdi, suml, sumu;
	for(ulint i=0; i<size; i++)
	{
		sumdi=Block<T, size>::get_zero();
		for(ulint j=0; j<i; j++)
		{
			suml=Block<T, size>::get_zero();
			sumu=Block<T, size>::get_zero();
			for(ulint k=0; k<j; k++)
			{
				suml=suml+L.block[i][k]*U.block[k][j]; // нижняя треугольная матрица
				sumu = sumu + L.block[j][k]*U.block[k][i]; // верхняя треугольная матрица
			}
			L.block[i][j]=(M.block[i][j]-suml)*(Block<T, size>::get_one()/U.block[j][j]);
			U.block[j][i]=(Block<T, size>::get_one()/L.block[j][j])*(M.block[j][i]-sumu);
			sumdi = sumdi + L.block[i][j] * U.block[j][i];
		}
		L.block[i][i]=sqrt(M.block[i][i]-sumdi);
		U.block[i][i]=L.block[i][i];
	}
	return true;
}
/*
Данный класс точки не зависит ни от типа-аргумента шаблона,
ни от размерности задачи. Предполагается, что все операции вычисляются с точками одной размерности.
*/
template<class T, ulint coords_count> class VectorXD
{
	static T zero;
        static bool isZeroSet;
public:
        VectorXD()
        {
            zerofied();
        }
	static void set_scalar_zero(T z)
        {
            zero=z; 
            isZeroSet = true;
        }
	static T get_zero()
        {
            if (!isZeroSet) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }
            return zero;
        }
	T coords[coords_count];
	ulint get_dim()
        {
            return coords_count;
        }
	//Скалярное произведение:
	T dotprod(VectorXD<T, coords_count> M) const
	{
            if (!isZeroSet) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }
            T accum = zero;
            for(unsigned int k=0; k<coords_count; k++)
                    accum=accum + coords[k]*M.coords[k];
            return accum;
	}
	void zerofied()
	{
            if (!isZeroSet) 
            {
                throw std::runtime_error("the zero value has not being set!");
            }
            for(unsigned int k=0; k<coords_count; k++)
                coords[k] = zero;
	}
	void common_init(T elem)
	{
		for(unsigned int k=0; k<coords_count; k++)
			coords[k]=elem;		
	}
	void common_init(T *elems)
	{
		for(int i=0; i<coords_count; i++)
			coords[i]=elems[i];
	}
	// Функция векторного произведения, обобщения на многомерный случай.
	static VectorXD<T, coords_count> vector_mult(VectorXD<T, coords_count> *args, T (*Det)(T matrix[coords_count-1][coords_count-1], ulint size)) // count - размерность задачи, count-1 - число агрументов, Det - функция, считающая определитель
	{
		T Matrix[coords_count-1][coords_count-1];
		ulint count=coords_count;		
		ulint size = coords_count-1;
		VectorXD<T, coords_count> result;
		int sign = 1;

			for(ulint i=0; i<size; i++)
				for(ulint j=0; j<size; j++)
					Matrix[i][j] = args[i].coords[j+1];			

			for(ulint k=0; k<size; k++)
			{
				result.coords[k]=Det(Matrix, size) * T(sign);
				sign *= -1;
				for(ulint i=0; i<size; i++)
					Matrix[i][k]=args[i].coords[k];
			}
			result.coords[count-1]=Det(Matrix, size) * T(sign);

		return result;
	}
	//Перегруженные операции:
	VectorXD<T, coords_count> &operator=(const T arg)
	{
		for(int k=0; k<coords_count; k++)
			coords[k] = arg;
		return *this;
	}
	template<class add_T> VectorXD<T, coords_count> &operator=(const VectorXD<add_T, coords_count> &M)
	{
		for(int k=0; k<coords_count; k++)
			coords[k]=M.coords[k];
		return *this;
	}
	VectorXD<T, coords_count> operator-(const VectorXD<T, coords_count> &M) const
	{
		VectorXD<T, coords_count> P;
		for(unsigned int k=0; k<coords_count; k++)
			P.coords[k]=coords[k]-M.coords[k];
		return P;
	}
	VectorXD<T, coords_count> operator+(const VectorXD<T, coords_count> &M) const
	{	
		VectorXD<T, coords_count> P;
		for(unsigned int k=0; k<coords_count; k++)
			P.coords[k]=coords[k]+M.coords[k];
		return P;
	}
	//произведение на число:	
	VectorXD<T, coords_count> operator*(const T arg) const
	{	
		VectorXD<T, coords_count> result;
		for(unsigned int k=0; k<coords_count; k++)
			result.coords[k]=coords[k]*arg;		
		return result;
	}
	// скалярное произведение
	T operator*(const VectorXD<T, coords_count> Arg) const
	{
		return dotprod(Arg);
	}
	VectorXD<T, coords_count> operator/(const T arg) const
	{
		VectorXD<T, coords_count> result;
		for(unsigned int k=0; k<coords_count; k++)
			result.coords[k]=coords[k]/arg;		
		return result;
	}
	
	T norma() const
	{ 
		return sqrt(dotprod(*this));
	}

	void normalize()
	{  
		T n = norma();
		for(unsigned int k=0; k<coords_count; k++)
			coords[k] = coords[k] / n;
	}

	bool operator>(const VectorXD<T, coords_count> &P)
	{
		bool res=true;
		for(int k=0; k<coords_count && res; k++)
			res = (coords[k]>P.coords[k]);
		return res;
	}

	bool operator>=(const VectorXD<T, coords_count> &P)
	{
		bool res=true;
		for(int k=0; k<coords_count && res; k++)
			res = (coords[k]>=P.coords[k]);
		return res;
	}

	bool operator<(const VectorXD<T, coords_count> &P)
	{
		bool res=true;
		for(int k=0; k<coords_count && res; k++)
			res = (coords[k]<P.coords[k]);
		return res;
	}

	bool operator<=(const VectorXD<T, coords_count> &P)
	{
		bool res=true;
		for(int k=0; k<coords_count && res; k++)
			res = (coords[k]<=P.coords[k]);
		return res;
	}
	// Операции над блоками:
	VectorXD<T,coords_count> operator*(const Block<T,coords_count> &Arg)
	{
		return Arg * (*this);
	}
	VectorXD<T,coords_count> operator/(const Block<T,coords_count> &Arg)
	{
		return Arg.inverse() * (*this);
	}
};
template<class T, ulint coords_count> T VectorXD<T, coords_count>::zero;
template<class T, ulint coords_count> bool VectorXD<T, coords_count>::isZeroSet = false;

inline double conjugation(double arg)
{
	return arg;
}

inline double norma(double arg)
{
	return fabs(arg);
}

template<class T, ulint size> inline Block<T, size> fabs(Block<T, size> arg)
{
	Block<T, size> res;
	for(ulint i=0; i<size; i++)
		for(ulint j=0; j<size; j++)
			res.block[i][j]=fabs(arg.block[i][j]);
	return res;
}

template<class T, ulint size> inline VectorXD<T,size> conjugation(VectorXD<T,size> Arg)
{
	VectorXD<T,size> result;
	for(ulint i=0; i<size; i++)
		result.coords[i]=conjugation(Arg.coords[i]);
	return result;
}
template<class T, ulint size> inline double norma(VectorXD<T,size> Arg)
{
	VectorXD<double,size> result;
	for(ulint i=0; i<size; i++)
		result.coords[i]=norma(Arg.coords[i]);
	return result.norma();
}

template<ulint size> inline double scalar_mult(VectorXD<double, size> Arg1, VectorXD<double, size> Arg2)
{
	return Arg1.dotprod(Arg2);
}

template<class T, ulint size> inline Block<T,size> transpose(Block<T,size> Arg)
{
	Block<T,size> TrArg;
	for(ulint i=0; i<size; i++)
		for(ulint j=0; j<size; j++)
			TrArg.block[i][j]=Arg.block[j][i];
	return TrArg;
}
