#pragma once
#include <complex>
#include "DinamicXD.hpp"
#include <vector>
/*
Этот код работает только при использовании процедур LAPACK и в архитектуре x64,
поскольку решается линейный массив (архитектура 32-х битной системы имеет ограничения на максимальный размер
адресного пространства).
*/
extern "C" void zgesv_(int *n, int *nrhs, std::complex<double> *a,
	int *lda, int *ipiv, std::complex<double> *b, int *ldb, int *info);

extern "C" void dgesv_(int* n, int* nrhs, double* a, int* lda, int* ipiv,
	double* b, int* ldb, int* info);

extern "C" {
    // LU decomoposition of a general matrix
    void dgetrf_(int* M, int *N, double* A, int* lda, int* IPIV, int* INFO);

    // generate inverse of a matrix given its LU decomposition
    void dgetri_(int* N, double* A, int* lda, int* IPIV, double* WORK, int* lwork, int* INFO);
}

void lapack_inverse(double* A, int N)
{
    int *IPIV = new int[N+1];
    int LWORK = N*N;
    double *WORK = new double[LWORK];
    int INFO;

    dgetrf_(&N,&N,A,&N,IPIV,&INFO);
    dgetri_(&N,A,&N,IPIV,WORK,&LWORK,&INFO);

    delete IPIV;
    delete WORK;
}

void lapack_inverse(DinamicBlock<double> &mtr)
{
    int N = mtr.get_size();
    std::vector<int> ipiv(N+1);
    int lwork = N*N;
    std::vector<double> work(lwork);
    int info;

    dgetrf_(&N, &N, mtr.get_buffer_ptr(), &N, &ipiv.front(), &info);
    dgetri_(&N, mtr.get_buffer_ptr(), &N, &ipiv.front(), &work.front(), &lwork, &info);
}

bool ZGESV(DinamicBlock<std::complex<double> > &A, std::complex<double> *rhs)
{
	int n, nrhs, lda, ldb, *ipiv, info;
	std::complex<double> im(0., 1.);
	std::complex<double> *a, *b;
	n = A.get_size();
	nrhs = 1;
	lda = n;
	ldb = n;
	ipiv = new int[n];
	A.TransposeInner(); // транспонируем матрицу, чтобы обход был по столбцам (как требуют процедуры фортрана).
	a = A.get_buffer_ptr();
	b = rhs;
	//
	zgesv_(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info);
	delete[] ipiv;
	//
	if (info == 0)
		return true;
	return false;
}

bool DGESV(DinamicBlock<double> &A, double *rhs)
{
	int n, nrhs, lda, ldb, *ipiv, info;
	double *a, *b;
	n = A.get_size();
	nrhs = 1;
	lda = n;
	ldb = n;
	ipiv = new int[n];
	A.TransposeInner(); // транспонируем матрицу, чтобы обход был по столбцам (как требуют процедуры фортрана).
	a = A.get_buffer_ptr();
	b = rhs;
	//
	dgesv_(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info);
	delete[] ipiv;
	//
	if (info == 0)
		return true;
	return false;
}
