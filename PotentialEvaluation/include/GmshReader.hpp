/*
    This class can be used to extract mesh data from 'Gmsh' file.
 */
 #include "base.hpp"
 #include <vector>
 #include <functional>
 #include <set>

 class GmshIO
 {
     enum TokenType {
        UNCLASSIFIED,
        COMMENT,
        FORMAT,
        PHYSICALNAMES,
        NODES,
        ELEMENTS,
        PERIODIC,
        NODEDATA,
        ELEMENTDATA,
        ELEMENTNODEDATA,
        INTERPOLATIONSCHEME
    };
    float versionNumber;
    int fileType, dataSize;
    int matrSize; // number of materials used in the file.
    struct materialDescription
    {
        int dimentions, materialNumber;
        std::string materialName;
        materialDescription(){};
        materialDescription(int dim, int matrNum, const std::string & name):
         dimentions(dim), materialNumber(matrNum), materialName(name){};
        bool operator<(const materialDescription & arg) const
        {
            if (dimentions < arg.dimentions)
                return true;
            else
            if (dimentions > arg.dimentions)
                return false;

            if (materialNumber < arg.materialNumber)
                return true;
            else
            if (materialNumber > arg.materialNumber)
                return false;

            if (materialName < arg.materialName)
                return true;
            else
            if (materialName > arg.materialName)
                return false;
            return false;
        }
    };
    struct nodeDescription
    {
        int nodeNumber;
        std::vector<double> point;
        bool operator<(const nodeDescription & arg) const
        {
            if (nodeNumber < arg.nodeNumber)
                return true;
            return false;
        }
    };
    struct elementDescription
    {
        int elemNumber;
        int materialNumber;
        std::vector<int> number;
        bool operator<(const elementDescription & arg) const
        {
            if (elemNumber < arg.elemNumber)
                return true;
            else
                if (elemNumber > arg.elemNumber)
                    return false;

            if (materialNumber < arg.elemNumber)
                return true;
            else
                if (materialNumber > arg.elemNumber)
                    return false;

            for (std::size_t i=0; i < std::min(number.size(), arg.number.size()); ++i)
            {
                if(number[i] < arg.number[i])
                {
                    return true;
                }
                else
                    if (number[i] > arg.number[i])
                    {
                        return false;
                    }
            }
            return false;
        }
    };
    std::set<materialDescription> materials;
    std::set<elementDescription> elements;
    std::set<nodeDescription> points;
    TokenType classifyToken(const std::string &token);
    bool readToken(std::stringstream & tokenStore, std::ifstream & file, std::string & outputToken);
    bool actionOnToken(std::stringstream & tokenStore, TokenType type, std::ifstream & file);
 public:
    bool readGmshFile(STR filename);
    bool outMesh(STR trg, STR xyz);
 };
