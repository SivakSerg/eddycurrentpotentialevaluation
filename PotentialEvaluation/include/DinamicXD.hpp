#pragma once

#ifndef _SIWAKS_WECTOR_DINAMIC_XD
#define _SIWAKS_WECTOR_DINAMIC_XD

//#ifndef ulint
//typedef unsigned long int ulint;
//#endif
//#ifndef uint
//typedef ulint uint;
//#endif
#include "base.hpp"
#include <math.h>
#include <stdio.h>
#include <stdexcept>

template<class T> class DinamicVector;

template<class T> class DinamicBlock
{
protected:
	bool is_sym;
	ulint size;
	static T one;
	static T zero;
	static bool one_def;
	static bool zero_def;
	T *Block;	
	friend DinamicVector<T>;
	//
	T **Massive; // для больших объемов памяти, не использует симметричность
public:
        static T get_zero() 
        {
            if(!zero_def)
                throw std::runtime_error("zero element is not defined!");
            return zero;
        }
        static T get_one() 
        {
            if(!one_def)
                throw std::runtime_error("unit element is not defined!");
            return one;
        }
	T *get_buffer_ptr(){ if ((Block != 0) && (is_sym == false)) return Block; return 0; } // Возвращается указатель на линейный буфер, который хранит данные непрерывно!
	void TransposeInner() // транспонирование можно применять для процедур FORTRAN
	{
		T swap;
		if (!is_sym)
		for (uint i = 0; i < size; ++i)
			for (uint j = 0; j < size; ++j)
			{
				swap = this->block(i, j);
				this->block(i,j) = this->block(j, i);
				this->block(j, i) = swap;
			}
	}
	DinamicBlock(const DinamicBlock<T>& arg)
	{
		if (arg.Massive == 0)
		{
			ulint S = arg.get_size();
			is_sym = arg.is_sym; // статическе аргументы присваивать бессмысленно!
			S = (is_sym) ? S*(S + 1) / 2 : S*S;
			Block = new T[S];
			for (ulint k = 0; k < S; k++)
				Block[k] = arg.Block[k];
			this->size = arg.get_size();
			Massive = 0;
		}
		else
		{	// для больших объемов памяти, не использует симметричность
			size = arg.size;
			Massive = new T*[arg.size];
			for (uint i = 0; i < arg.size; i++)
			{
				Massive[i] = new T[size];
				for (uint j = 0; j < size; j++)
					Massive[i][j] = arg.Massive[i][j];
			}
			Block = 0;
		}
	}
	ulint get_size() const {return size;}
	//DinamicBlock():Block(0),is_sym(false){}
	DinamicBlock(ulint size){ Block = new T[size * size]; this->size = size; is_sym = false; Massive = 0; }
	DinamicBlock(ulint size, bool is_sym)// выделяет в два раза меньше памяти если is_sym = true, block(i, j) = block(j, i)
	{
		this->is_sym = is_sym;
		this->size = size;
		if(is_sym) Block = new T[size * (size + 1) / 2];
		else Block = new T[size * size];
		Massive = 0;
	}
	DinamicBlock(bool use_massive, uint size)
	{
		this->size = size;
		is_sym = false;
		if (use_massive)
		{
			Massive = new T*[size];
			for (uint i = 0; i < size; i++)
				Massive[i] = new T[size];
			Block = 0;
		}
		else
		{
			Block = new T[size*size];			
			Massive = 0;
		}
	}
	T *operator[](uint k) // не определен для симметричного формата хранения
	{
		if (Massive)
			return Massive[k];
		if(!is_sym)
			return &Block[k*size];
		return 0;
	}
	void resize(uint size, bool is_sym) // Фактически дублирует конструктор 
	{
		if (Massive == 0)
		{
			this->is_sym = is_sym;
			this->size = size;
			if (Block != 0)delete[] Block;
			if (size == 0) Block = 0;
			else
			{
				if (is_sym) Block = new T[size * (size + 1) / 2];
				else Block = new T[size * size];
			}
		}
		else
		{
			for (uint i = 0; i < this->size; i++)
				delete[] Massive[i];
			delete[] Massive;
			// для больших объемов памяти, не использует симметричность
			this->size = size;			
			Massive = new T*[size];
			for (uint i = 0; i < size; i++)				
				Massive[i] = new T[size];
			Block = 0;
		}
	}
	void resize_massive(uint size)
	{
		if (Block)
		{
			delete[] Block;
			Block = 0;
		}
		if (Massive)
		{
			for (uint i = 0; i < this->size; i++)
				delete[] Massive[i];
			delete[] Massive;
		}
		// для больших объемов памяти, не использует симметричность
		this->size = size;
		is_sym = false;
		Massive = new T*[size];
		for (uint i = 0; i < size; i++)
			Massive[i] = new T[size];
		Block = 0;
	}
	void common_init(T arg)
	{
		for (uint i = 0; i < size; i++)
		for (uint j = 0; j < size; j++)
			block(i, j) = arg;
	}
	bool sym() const { return (Massive) ? false : is_sym; }
	virtual ~DinamicBlock()
	{
		//printf("Dinamic block dest started\n");
		if (Block) delete[] Block; Block = 0;
		// printf("Dinamic block dest ended\n");
		if (Massive)
		{
			for (uint i = 0; i < size; i++)
				delete[] Massive[i];
			delete[] Massive;
		}
	}
	T &block(ulint i, ulint j)
	{
		if (Massive == 0)
		{
			i = i%size; j = j%size;
			ulint mi, mj;
			if (is_sym)
			{
				mi = (i > j) ? i : j; // max -> row
				mj = (i > j) ? j : i; // min -> col
				return Block[mi*(mi + 1) / 2 + mj];
			}
			else
				return Block[i + j * size];
		}
		return Massive[i%size][j%size];
	}
	static void set_one(T one) {DinamicBlock<T>::one=one; one_def=true;}
	static void set_zero(T zero) {DinamicBlock<T>::zero=zero; zero_def=true;}
	uint get_dim() const {return size;}
	T norma()
	{
		T accum = zero;		
		for(ulint i=0; i<size; i++)
			for(ulint j=0; j<size; j++)
				accum =accum + block(i,j)*block(i,j);
		return sqrt(accum);
	}
	DinamicBlock<T> &operator=(const T arg)
	{
		for(int i=0; i<size; i++)
			for(int j=0; j<size; j++)
				block(i,j)=arg;
		return *this;
	}
	
	T get_const_value(uint i, uint j) const 
	{ 	
	  if (Massive == 0)
		{
			i = i%size; j = j%size;
			ulint mi, mj;
			if (is_sym)
			{
				mi = (i > j) ? i : j; // max -> row
				mj = (i > j) ? j : i; // min -> col
				return Block[mi*(mi + 1) / 2 + mj];
			}
			else
				return Block[i + j * size];
		}
		return Massive[i%size][j%size];	  
	}
	
	DinamicBlock<T> &operator=(const DinamicBlock<T> &arg)
	{
		ulint S = arg.get_size();
		S = (S > size)?size:S;
		for(uint i=0; i<S; i++)
			for(uint j=0; j<S; j++)
				block(i,j) = arg.get_const_value(i,j);
		return *this;
	}
	DinamicBlock<T> operator+(const DinamicBlock<T> &B)
	{
		ulint S = B.get_size();
		S = (S > size)?size:S;
		DinamicBlock<T> res(S);
		for(int i=0; i<S; i++)
			for(int j=0; j<S; j++)
				res.block(i,j)=block(i,j)+B.get_const_value(i,j);
		return res;
	}
	DinamicBlock<T> operator-(const DinamicBlock<T> &B)
	{
		ulint S = B.get_size();
		S = (S > size)?size:S;
		DinamicBlock<T> res(S);
		for(int i=0; i<S; i++)
			for(int j=0; j<S; j++)
				res.block(i,j)=block(i,j)-B.block(i,j);
		return res;
	}
	DinamicVector<T> operator*(const DinamicVector<T> &B)
	{
		ulint S = B.get_size();
		S = (S > size)?size:S;
		DinamicVector<T> result(S);
		result.common_init(zero);
		for(int i=0; i<S; i++)
			for(int j=0; j<S; j++)
				result.coords(i)=result.coords(i)+block(i,j)*B.coords(j);
		return result;
	}
	DinamicBlock<T> operator*(const DinamicBlock<T> &B)
	{
		ulint S = B.get_size();
		S = (S > size)?size:S;
		DinamicBlock<T> result(S);
		result=zero;
		for(int i=0; i<S; i++)
			for(int k=0; k<S; k++)
				for(int j=0; j<S; j++)
					result.block(i,k)=result.block(i,k)+block(i,j)*B.block(j,k);
		return result;
	}
	DinamicBlock<T> operator/(const DinamicBlock<T> &B)
	{
		return  B.inverse() * (*this);
	}
	DinamicBlock<T> operator/(const T &arg)
	{
		ulint S = get_size();
		DinamicBlock<T> Res(S);
		for(ulint i=0; i<S; i++)
			for(ulint j=0; j<S; j++)
				Res.block(i,j) = block(i,j) / arg;
		return Res;
	}
	// задание еденичной матрицы:
	bool Identified()
	{
		if(one_def)
		{
			for(int i=0; i<size; i++)
				for(int j=0; j<size; j++)
					block(i,j)=(i==j)? one : zero;
			return true;
		}
		else
			return false;
	}
	bool Zerofied()
	{
		if(zero_def)
		{
			if(!is_sym)// матрица не симметричная
			for(int i=0; i<size; i++)
				for(int j=0; j<size; j++)
					block(i,j)=zero;
			else // матрица симметричная
			for(int i=0; i<size; i++)
				for(int j=i; j<size; j++)
					block(i,j)=zero;
			return true;
		}
		else
			return false;
	}
	// решение системы линейных уравнений методом Гаусса с выбором ведущего
	bool linsolve(DinamicVector<T> *F, ulint f_count)
	{
		uint i, j, k, posmax, N, *poses, swap;
		T tmp;
		DinamicBlock<T> A(size);
		if(F == 0) return false;
		for(ulint f=0; f<f_count; f++)
			if(F[f].get_size()!=this->size)return false;
		poses = new uint[size];
		A = *this;
		N = size;
	
		for(k=0; k<N; k++)
			poses[k]=k;

		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (fabs(A.block(poses[i],k))>fabs(A.block(poses[posmax],k))) posmax=i;
			tmp = A.block(poses[posmax],k);
			
			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			/*далее производим Гауссовское исключение*/
			for (j=k; j<N; j++) A.block(posmax,j)=A.block(posmax,j) / tmp;		     // сокращаем на 
			for (i=0; i<f_count; i++) F[i].coords(posmax)=F[i].coords(posmax) / tmp; // ведущий элемент.

			for (i=k+1; i<size; i++) 
			{
				tmp=A.block(poses[i],k);
				for (j=k; j<N; j++)
					A.block(poses[i],j)=A.block(poses[i],j) - A.block(posmax,j)*tmp;
				for (ulint m=0; m<f_count; m++) F[m].coords(poses[i])=F[m].coords(poses[i]) - F[m].coords(posmax)*tmp;
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=N-1; k>0; k--)
				for (i=0; i<k; i++)
					for(ulint m=0; m<f_count; m++)
						F[m].coords(poses[i])=F[m].coords(poses[i]) - A.block(poses[i],k)*F[m].coords(poses[k]);

			//Переупорядочиваем результат:
			DinamicVector<T> reordered(F[0].get_size());
			for(ulint f = 0; f<f_count; f++)
			{
				for(i=0; i<size; i++)			
					reordered.coords(i)=F[f].coords(poses[i]);
				F[f] = reordered;
			}			
		delete[] poses;
		return true;
	}
	static bool LinSolveOverwrite(DinamicBlock<T> &A, T **F, ulint f_count) // функция переписывает исходную матрицу, не создавая новых экземпляров
	{
		uint i, j, k, posmax, N, *poses, swap;
		T tmp;		
		if (F == 0) return false;
		uint size = A.get_size();
		poses = new uint[size];		
		N = size;

		for (k = 0; k<N; k++)
			poses[k] = k;

		// down
		for (k = 0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax = k;				// здесь posmax - индекс в массиве poses
			for (i = k + 1; i<N; i++)
			if (abs(A.block(poses[i], k))>abs(A.block(poses[posmax], k))) posmax = i;
			tmp = A.block(poses[posmax], k);

			swap = poses[posmax];		// и здесь
			poses[posmax] = poses[k];
			poses[k] = swap;

			posmax = poses[k];		// а здесь это уже полноценный номер строки

			/*далее производим Гауссовское исключение*/
			for (j = k; j<N; j++) A.block(posmax, j) = A.block(posmax, j) / tmp;		     // сокращаем на 
			for (i = 0; i<f_count; i++) F[i][posmax] = F[i][posmax] / tmp; // ведущий элемент.

			for (i = k + 1; i<size; i++)
			{
				tmp = A.block(poses[i], k);
				for (j = k; j<N; j++)
					A.block(poses[i], j) = A.block(poses[i], j) - A.block(posmax, j)*tmp;
				for (ulint m = 0; m<f_count; m++) F[m][poses[i]] = F[m][poses[i]] - F[m][posmax] * tmp;
			}
		}
		// после превидения матрицы к треугольному виду, находим правую часть
		for (k = N - 1; k>0; k--)
		for (i = 0; i<k; i++)
		for (ulint m = 0; m<f_count; m++)
			F[m][poses[i]] = F[m][poses[i]] - A.block(poses[i], k)*F[m][poses[k]];

		//Переупорядочиваем результат:
		DinamicVector<T> reordered(size);
		for (ulint f = 0; f<f_count; f++)
		{
			for (i = 0; i<size; i++)
				reordered.coords(i) = F[f][poses[i]];
			for (i = 0; i<size; i++)
				F[f][i] = reordered.coords(i);
		}
		delete[] poses;
		return true;
	}
	// решение системы линейных уравнений методом Гаусса с выбором ведущего
	// для динамического массива векторов правых частей
	bool linsolve(T **F, ulint f_count)
	{
		uint i, j, k, posmax, N, *poses, swap;
		T tmp;
		DinamicBlock<T> A(size);
		if(F == 0) return false;
		poses = new uint[size];
		A = *this; // копируем матрицу, чтобы не затирать исходных данных
		N = size;
	
		for(k=0; k<N; k++)
			poses[k]=k;

		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (abs(A.block(poses[i],k))>abs(A.block(poses[posmax],k))) posmax=i;
			tmp = A.block(poses[posmax],k);
			
			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			/*далее производим Гауссовское исключение*/
			for (j=k; j<N; j++) A.block(posmax,j)=A.block(posmax,j) / tmp;		     // сокращаем на 
			for (i=0; i<f_count; i++) F[i][posmax]=F[i][posmax] / tmp; // ведущий элемент.

			for (i=k+1; i<size; i++) 
			{
				tmp=A.block(poses[i],k);
				for (j=k; j<N; j++)
					A.block(poses[i],j)=A.block(poses[i],j) - A.block(posmax,j)*tmp;
				for (ulint m=0; m<f_count; m++) F[m][poses[i]]=F[m][poses[i]] - F[m][posmax]*tmp;
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=N-1; k>0; k--)
				for (i=0; i<k; i++)
					for(ulint m=0; m<f_count; m++)
						F[m][poses[i]]=F[m][poses[i]] - A.block(poses[i],k)*F[m][poses[k]];

			//Переупорядочиваем результат:
			DinamicVector<T> reordered(size);
			for(ulint f = 0; f<f_count; f++)
			{
				for(i=0; i<size; i++)			
					reordered.coords(i)=F[f][poses[i]];
				for(i=0; i<size; i++)
					F[f][i] = reordered.coords(i);
			}			
		delete[] poses;
		return true;
	}
	bool linsolve(T *result, T *right)
	{
		if(result!=right)
		for(uint i=0; i<size; i++)
			result[i] = right[i];
		return linsolve(&result,1);
	}
	bool linsolve(DinamicVector<T> &result, DinamicVector<T> &right)
	{
		result = right;
		return linsolve(&result,1);
	}
	// код точно такой же как и у решателя, на месте еденичной F должна оказаться обратная матрица
	DinamicBlock<T> inverse()
	{
		uint i, j, k, posmax, f, N, F_count, *poses, swap; // poses - массив индексов перестановок (хранит номера ведущих строк на k-ой итерации)
 		T tmp;
		DinamicBlock<T> A(size);
		DinamicBlock<T> F(size);
		poses = new uint[size];
		F.Identified();		// в правой части - еденичная матрица, считаем, что one - была определена
		F_count = size;
		A = *this;
		N = size;

		for(k=0; k<N; k++)
			poses[k]=k;
	
		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (fabs(A.block(poses[i],k))>fabs(A.block(poses[posmax],k))) posmax=i;
			tmp = A.block(poses[posmax],k);

			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			/*далее производим Гауссовское исключение*/
			for (j=k; j<N; j++) 
				A.block(posmax,j)=A.block(posmax,j)/tmp;
			for (f=0; f<F_count; f++)
				F.block(posmax,f)=F.block(posmax,f)/tmp;

			for (i=k+1; i<size; i++) 
			{
				tmp=A.block(poses[i],k);
				for (j=k; j<N; j++)
					A.block(poses[i],j)=A.block(poses[i],j) - A.block(posmax,j)*tmp;
				for (f=0; f<F_count; f++) 
					F.block(poses[i],f)=F.block(poses[i],f) - F.block(posmax,f)*tmp;
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=N-1; k>0; k--)
				for (i=0; i<k; i++)
					for (f=0; f<F_count; f++)
						F.block(poses[i],f)=F.block(poses[i],f) - A.block(poses[i],k)*F.block(poses[k],f);

			//Переупорядочиваем результат:
			DinamicVector<T> reordered(size);
			for(f=0; f<F_count; f++)
			{
				for(i=0; i<size; i++)
					reordered.coords(i)=F.block(poses[i],f);
				for(i=0; i<size; i++)
					F.block(i,f)=reordered.coords(i);
			}
			return F;
	}
	// перегруженая процедура для избежания дублирования памяти.
	bool inverse(DinamicBlock<T> &F)
	{
		uint i, j, k, posmax, f, N, F_count, *poses, swap; // poses - массив индексов перестановок (хранит номера ведущих строк на k-ой итерации)
 		T tmp;
		if(F.get_size()!=this->size) return false;
		DinamicBlock<T> A(size);
		poses = new uint[size];
		F.Identified();		// в правой части - еденичная матрица, считаем, что one - была определена
		F_count = size;
		A = *this;
		N = size;

		for(k=0; k<N; k++)
			poses[k]=k;
	
		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (fabs(A.block(poses[i],k))>fabs(A.block(poses[posmax],k))) posmax=i;
			tmp = A.block(poses[posmax],k);

			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			/*далее производим Гауссовское исключение*/
			for (j=k; j<N; j++) 
				A.block(posmax,j)=A.block(posmax,j)/tmp;
			for (f=0; f<F_count; f++)
				F.block(posmax,f)=F.block(posmax,f)/tmp;

			for (i=k+1; i<size; i++) 
			{
				tmp=A.block(poses[i],k);
				for (j=k; j<N; j++)
					A.block(poses[i],j)=A.block(poses[i],j) - A.block(posmax,j)*tmp;
				for (f=0; f<F_count; f++) 
					F.block(poses[i],f)=F.block(poses[i],f) - F.block(posmax,f)*tmp;
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=N-1; k>0; k--)
				for (i=0; i<k; i++)
					for (f=0; f<F_count; f++)
						F.block(poses[i],f)=F.block(poses[i],f) - A.block(poses[i],k)*F.block(poses[k],f);

			//Переупорядочиваем результат:
			DinamicVector<T> reordered(size);
			for(f=0; f<F_count; f++)
			{
				for(i=0; i<size; i++)
					reordered.coords(i)=F.block(poses[i],f);
				for(i=0; i<size; i++)
					F.block(i,f)=reordered.coords(i);
			}
			return true;
	}
	T AbsDet()
	{
		uint i, j, k, posmax, N, *poses, swap; // poses - массив индексов перестановок (хранит номера ведущих строк на k-ой итерации)
 		T maxelem, rowelem;
		DinamicBlock<T> A(size);
		T result=one;
		A = *this;
		N = size;
		poses = new uint[size];
		for(k=0; k<N; k++)
			poses[k]=k;
	
		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (abs(A.block(poses[i],k))>abs(A.block(poses[posmax],k))) posmax=i;
			maxelem = A.block(poses[posmax],k);

			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			for (i=k+1; i<size; i++) 
			{
				rowelem=A.block(poses[i],k);
				for (j=k; j<N; j++)
					A.block(poses[i],j)=A.block(poses[i],j) - A.block(posmax,j)* ( rowelem / maxelem );
			}			
		}
			// после превидения матрицы к треугольному виду, находим правую часть
			for (k=0; k<N; k++)
				result = result * A.block(poses[k],k);		
		return abs(result);
	}
	T Det(T MinusOne, T *eigenvalues = 0)
	{
		uint i, j, k, posmax, N, *poses, swap; // poses - массив индексов перестановок (хранит номера ведущих строк на k-ой итерации)
 		T maxelem, rowelem;
		DinamicBlock<T> A(size);
		T result=one;
		A = *this;
		N = size;
		poses = new uint[size];
		for(k=0; k<N; k++)
			poses[k]=k;
	
		// down
		for (k=0; k<N; k++)
		{
			//находим ведущий элемент (на который делим)
			posmax=k;				// здесь posmax - индекс в массиве poses
			for (i=k+1; i<N; i++) 
				if (abs(A.block(poses[i],k))>abs(A.block(poses[posmax],k))) posmax=i;
			maxelem = A.block(poses[posmax],k);

			swap=poses[posmax];		// и здесь
			poses[posmax]=poses[k];
			poses[k]=swap;

			posmax=poses[k];		// а здесь это уже полноценный номер строки

			for (i=k+1; i<size; i++) 
			{
				rowelem=A.block(poses[i],k);
				for (j=k; j<N; j++)
					A.block(poses[i],j)=A.block(poses[i],j) - A.block(posmax,j)* ( rowelem / maxelem );
			}			
		}
		bool reverse=false;		// считаем число перестановок
		ulint val = poses[0];
		for(int i=0; i<N; i++)
		{
			if(poses[i]<val)
				reverse = !reverse;
			val=poses[i];
		}
		
		// после превидения матрицы к треугольному виду, находим правую часть
		for (k=0; k<N; k++)
			result = result * A.block(poses[k],k);
		if(reverse)
			result = result * MinusOne;
		//////////////////////////////////////////////////////////////////////////
		if(eigenvalues != 0) // тогда выводим собственные значения матрицы
			for(uint i=0; i<N; i++)
				eigenvalues[ i ] = A.block(poses[i],i);
		return result;
	}
	bool multiply(DinamicBlock<T> &B, DinamicBlock<T> &result)
	{
		ulint S = size;	
		if(S != size || result.get_size() != size)return false;
		for(int i=0; i<S; i++)
			for(int k=0; k<S; k++)
			{
				result.block(i,k) = this->zero;
				for(int j=0; j<S; j++)
					result.block(i,k)=result.block(i,k)+block(i,j)*B.block(j,k);		
			}
		return true;
	}
	bool multiply(DinamicVector<T> &B, DinamicVector<T> &result)
	{
		ulint S = size;
		if(B.get_size()!=size || result.get_size() != size) return false;
		for(int i=0; i<S; i++)
		{
			result.coords(i) = this->zero;
			for(int j=0; j<S; j++)
				result.coords(i)=result.coords(i)+block(i,j)*B.coords(j);
		}
		return true;
	}
	bool multiply(T *arg, T *res)
	{
		for(uint i=0; i<size; i++)
		{
			res[ i ] = block(i,0) * arg[0];
			for(uint j=1; j<size; j++)
				res[ i ] = res[ i ] + block(i,j) * arg[j];
		}
		return true;
	}
};
// Определение статических членов класса:
template<class T> T DinamicBlock<T>::one;
template<class T> bool DinamicBlock<T>::one_def;
template<class T> T DinamicBlock<T>::zero;
template<class T> bool DinamicBlock<T>::zero_def;

// Функция извлечения квадратного корня из матрицы, а на практике - это
// обычное LLt разложение матрицы: (возвращаем нижний треугольник)
template<class T> static DinamicBlock<T> sqrt(DinamicBlock<T> A)
{
	DinamicBlock<T> L(A.get_size());
	T sumdi, suml;
	uint size = A.get_size();
	L.Zerofied();
	for(ulint i=0; i<size; i++)
	{
		sumdi=DinamicBlock<T>::get_zero();
		for(ulint j=0; j<i; j++)
		{
			suml=DinamicBlock<T>::get_zero();
			for(ulint k=0; k<j; k++)
				suml=suml+L.block(i,k)*L.block(j,k); // нижняя треугольная матрица
			L.block(i,j)=(A.block(i,j)-suml)/L.block[j][j];
			sumdi = sumdi + L.block(i,j) * L.block(i,j);
		}
		L.block(i,i)=sqrt(A.block(i,i)-sumdi);
	}
	return L;
}
/*
Данный класс точки не зависит ни от типа-аргумента шаблона,
ни от размерности задачи. Предполагается, что все операции вычисляются с точками одной размерности.
*/
template<class T> class DinamicVector
{
	static T zero;
	T *Coords;
	ulint coords_count;
public:
        T &operator[](int k){return Coords[k];}
	T *get_pointer(){return Coords;}
	DinamicVector(const DinamicVector<T>& arg)
	{
		ulint S = arg.get_size();		
		Coords = new T[S];
		for(ulint k=0; k<S; k++)
			Coords[k] = arg.Coords[k];
		this->coords_count = arg.get_size();
	}
	ulint get_size() const {return coords_count;}
	DinamicVector(ulint size){coords_count = size; Coords = new T[coords_count];}
	virtual ~DinamicVector()
	{
		//printf("Dinamic vect dest started\n");
		if (Coords)delete[] Coords; Coords = 0;
		//printf("Dinamic vect dest ended\n");
	}
	T &coords(ulint i){return Coords[i];}
	static void set_scalar_zero(T z){zero=z;}
	static T get_zero(){return zero;}
	ulint get_dim(){return coords_count;}
	//Скалярное произведение:
	T dotprod(DinamicVector<T> &M)
	{
		T accum = zero;
		for(int k=0; k<coords_count; k++)
			accum=accum + coords(k)*M.coords(k);
		return accum;
	}
	void common_init(T elem)
	{
		for(int k=0; k<coords_count; k++)
			coords(k)=elem;		
	}
	void common_init(T *elems)
	{
		for(int i=0; i<coords_count; i++)
			coords(i)=elems[i];
	}
	// Функция векторного произведения, обобщения на многомерный случай.
	DinamicVector<T> vector_mult(DinamicVector<T> *args, T (*Det)(T *matrix, ulint size)) // size - размерность задачи, count-1 - число агрументов, Det - функция, считающая определитель
	{
		DinamicBlock<T> Matrix(coords_count-1);
		ulint count=coords_count;		
		ulint size = coords_count-1;
		DinamicVector<T> result(coords_count);
		int sign = 1;

			for(ulint i=0; i<size; i++)
				for(ulint j=0; j<size; j++)
					Matrix.block(i,j) = args[i].coords(j+1);

			for(ulint k=0; k<size; k++)
			{
				result.coords(k)=Det(Matrix.Block, size) * sign;
				sign *= -1;
				for(ulint i=0; i<size; i++)
					Matrix.block(i,k)=args[i].coords(k);
			}
			result.coords(count-1)=Det(Matrix, size) * sign;

		return result;
	}
	//Перегруженные операции:
	DinamicVector<T> &operator=(const T arg)
	{
		for(int k=0; k<coords_count; k++)
			coords(k) = arg;
		return *this;
	}
	DinamicVector<T> &operator=(const DinamicVector<T> &M)
	{
		ulint S = M.get_size();
		S = (coords_count > S)?S:coords_count;
		for(int k=0; k<S; k++)
			coords(k)=M.coords(k);
		return *this;
	}
	DinamicVector<T> operator-(const DinamicVector<T> &M)
	{
		ulint S = M.get_size();
		S = (coords_count > S)?S:coords_count;
		DinamicVector<T> P;
		for(int k=0; k<S; k++)
			P.coords(k)=coords(k)-M.coords(k);
		return P;
	}
	DinamicVector<T> operator+(const DinamicVector<T> &M)
	{	
		ulint S = M.get_size();
		S = (coords_count > S)?S:coords_count;
		DinamicVector<T> P(S);
		for(int k=0; k<S; k++)
			P.coords(k)=coords(k)+M.coords(k);
		return P;
	}

	//произведение на число:	
	DinamicVector<T> operator*(const T &arg)
	{	
		DinamicVector<T> result(coords_count);
		for(int k=0; k<coords_count; k++)
			result.coords(k)=coords(k)*arg;		
		return result;
	}
	// покомпонентное произведение
	DinamicVector<T> operator*(const DinamicVector<T> Arg)
	{
		ulint S = Arg.get_size();
		S = (coords_count > S)?S:coords_count;
		DinamicVector<T> result;
		for(int k=0; k<S; k++)
			result.coords(k)=coords(k)*Arg.coords(k);		
		return result;
	}
	DinamicVector<T> operator/(const T arg)
	{
		DinamicVector<T> result;
		for(int k=0; k<coords_count; k++)
			result.coords(k)=coords(k)/arg;		
		return result;
	}
	
	T norma()
	{ 
		return sqrt(dotprod(*this));
	}

	void normalize()
	{  
		T n = norma();
		for(int k=0; k<coords_count; k++)
			coords(k) = coords(k) / n;
	}

	bool operator>(const DinamicVector<T> &P)
	{
		ulint S = P.get_size();
		S = (coords_count > S)?S:coords_count;
		bool res=true;
		for(int k=0; k<S && res; k++)
			res = coords(k)>P.coords(k);		
		return res;
	}

	bool operator>=(const DinamicVector<T> &P)
	{
		ulint S = P.get_size();
		S = (coords_count > S)?S:coords_count;
		bool res=true;
		for(int k=0; k<S && res; k++)
			res = coords(k)>=P.coords(k);
		return res;
	}

	bool operator<(const DinamicVector<T> &P)
	{
		ulint S = P.get_size();
		S = (coords_count > S)?S:coords_count;
		bool res=true;
		for(int k=0; k<S && res; k++)
			res = coords(k)<P.coords(k);		
		return res;
	}

	bool operator<=(const DinamicVector<T> &P)
	{
		ulint S = P.get_size();
		S = (coords_count > S)?S:coords_count;
		bool res=true;
		for(int k=0; k<S && res; k++)
			res = coords(k)<=P.coords(k);
		return res;
	}
	// Операции над блоками:
	DinamicVector<T> operator*(const DinamicBlock<T> &Arg)
	{
		return Arg * (*this);
	}
	DinamicVector<T> operator/(const DinamicBlock<T> &Arg)
	{
		return Arg.inverse() * (*this);
	}
};
template<class T> T DinamicVector<T>::zero;

template<class T> DinamicBlock<T> fabs(DinamicBlock<T> arg)
{
        uint size = arg.get_size();
	DinamicBlock<T> res(size);
	for(ulint i=0; i<size; i++)
		for(ulint j=0; j<size; j++)
			res.block(i,j)=fabs(arg.block(i,j));
	return res;
}

template<class T> DinamicVector<T> conjugation(DinamicVector<T> Arg)
{
        uint size = Arg.get_size();
	DinamicVector<T> result(size);
	for(ulint i=0; i<size; i++)
		result.coords(i)=conjugation(Arg.coords(i));
	return result;
}
template<class T> double norma(DinamicVector<T> Arg)
{
        uint size = Arg.get_size();
	DinamicVector<double> result(size);	
	for(ulint i=0; i<size; i++)
		result.coords(i)=norma(Arg.coords(i));
	return result.norma();
}

template<class T> T scalar_mult(DinamicVector<T> Arg1, DinamicVector<T> Arg2)
{
	return Arg1.dotprod(Arg2);
}

template<class T> DinamicBlock<T> transpose(DinamicBlock<T> Arg)
{
        uint size = Arg.get_size();
	DinamicBlock<T> TrArg(size);
	for(ulint i=0; i<size; i++)
		for(ulint j=0; j<size; j++)
			TrArg.block(i,j)=Arg.block(j,i);
	return TrArg;
}
#endif
