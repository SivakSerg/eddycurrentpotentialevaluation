#include "SingleLayerSurfacePotential.h"

class DoubleLayerPotential : public SingleLayerSurfacePotential
{
public:
std::complex<double> getScalarDoubleLayerHelmholtzPotential(std::complex<double> k, double epsilon,
    GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r,
    int basisOrder, std::function<double(const VectorXD<double,3> &arg)> basisFunction);

std::complex<double> getScalarDoubleLayerHelmholtzPotential(std::complex<double> k, double epsilon,
    GaussRule gr, VectorXD<double,3> p[], VectorXD<double,3> r,
    int basisOrder, std::function<double(const VectorXD<double,3> &arg)> basisFunction,
    const std::vector<double> & decCoeffs);

    std::complex<double> DoubleLayerNumericalPart(int N, std::complex<double> k, double epsilon, GaussRule gr,
    const VectorXD<double,3> &x,
    std::function<double(const VectorXD<double,3> &arg)> basisFunction);
    // these functions can be used for the Modyfied Double Layer Potential
    double get_surface_grad_integral_u(int n, uint q, uint p);
    double get_surface_grad_integral_v(int n, uint q, uint p);
    public:
};
