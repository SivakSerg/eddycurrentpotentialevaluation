#pragma once
#include "VectorXD.hpp"
#include "base_elements.hpp"
#include <stdio.h>
#include <complex>
#include <math.h>
#include "FastVectorMult.hpp"
#include <algorithm>
#include <vector>
#include "TrigInternalPoints.hpp"
#include <algorithm>

// функция считает площадь треугольника
double trig_area(triangle & trig, std::vector<VectorXD<double,3> > points);

// Функция проверяет, принадлежит ли точка треугольнику или нет.
bool is_inside(triangle & trig, const std::vector<VectorXD<double,3> > & points, const VectorXD<double,3> & argToCheck, double tolerance);

// параметры треугольника, необходимые для вычисления функции (для одной выбранной вершины)
struct triangle_parameters
{	uint elnum;
	static double Det(double M[][2], uint size) {return M[0][0]*M[1][1] - M[0][1]*M[1][0];}
	double t_k, t_star;					// Длина ребра, противолежащего точке, для которой строились характеристики
	VectorXD<double,3> x_star;			// точка, на которую опущена высота
	VectorXD<double,3> x0, r1, r2, n;	// система координат для заданной точки (нормированная)
	VectorXD<double,3> x1, x2;			// оставшиеся точки треугольника
	double tg_a1, tg_a2, s_t; // параметры, используемые в аналитическом решении (тангенсы углов с высотой и длина высоты)
	Block<double,3> S;	      // матрица перехода от глобальной системы координат XYZ к локальной системе R1_R2_n, заданной на треугольнике (потребуется при численном интегрировании)
	uint trg_num;			  // глобальный номер треугольника, для которого построены параметры (чтобы не перестраивать для имеющихся)
	// функции построения преобразования координат
	VectorXD<double,3> get_local_coordinate_point(VectorXD<double,3> global_point) const; // возвращает точку в локальной системе координат треугольника с центром в вершине с номером local_vert_num
	VectorXD<double,3> get_global_coordinate_point(VectorXD<double,3> local_point) const; // возвращает точку в глобальной системе координат
        VectorXD<double,3> get_local_coordinate_vector(VectorXD<double,3> global_point) const; // вектор не центрирован в начале координат
        VectorXD<complex<double>,3> get_local_coordinate_vector(VectorXD<complex<double>,3> global_point) const;
	VectorXD<std::complex<double>,3> get_global_coordinate_point(VectorXD<std::complex<double>,3> local_point) const; // возвращает точку в глобальной системе координат
	VectorXD<double,2> get_Gauss_point( VectorXD<double,3> Local_point ) const; // по проекции на плоскость треугольника (первые две компоненты вектора) определяем какая точка Гаусса ей соответствует для этого треугольника
	Block<double,3> GaussMtr; // матрица для получения координат точек Гаусса
	Block<double,3> LMtr;	  // матрица для получения L-координат
	bool reversed;	 // Дополнительный параметр, передаваемый для определения "кручения" базисных функций
	bool lam_negate; // Изменение направления lambda-функций
	// Функция-построитель параметров треугольника
	bool build(uint trg_num, uint p_loc_num, const triangle *buff, const VectorXD<double,3> *points);
	// Функция-построитель параметров треугольника №2 (нужна для согласования скалярных базисных функций)
	bool build2(uint trg_num, uint p_loc_num, const triangle *buff, const VectorXD<double,3> *points);
	void reverse_normal(); // изменяет напарвление нормали на треугольнике на противоположное
	// функция нужна для численного интегрирования
	VectorXD<double,2> find_projection_point( VectorXD<double,3> Local_arg ) const; // находит проекцию точки на треугольник и возвращает ее точку Гаусса
	VectorXD<double,3> get_Global_point(VectorXD<double,2> Gauss_point) const; // возвращает точку в глобальной системе координат
	void create_section(std::vector< std::vector<VectorXD<double, 3> > > &lines, uint edge_pnum, uint section_pnum, uint pnum, double height) const; // строит отрезки лучей, исходящих из заданной вершины на высоте height над треугольником
        double Dist(const VectorXD<double,3> & arg) const;
        double Diam() const;

        void get_circumcircle_params(double &R, VectorXD<double,3> &point_glob) const; // возвращает радиус и центр описанной окружности в глобальных координатах переданных вершин
        bool GetInternalPoints(vector<VectorXD<double,3> > &points, int n);
        bool is_point_inside(const VectorXD<double,3> & argToCheck, double tolerance);
        // разбиение на 4 треугольника
        bool Build_Sections( std::vector< triangle_parameters > &params );
        double Build_Sections( std::vector< triangle_parameters > &params, double k);
        VectorXD<double,3> get_global_coordinate_vector(VectorXD<double,3> local_vector) const;
        VectorXD<complex<double>,3> get_global_coordinate_vector(VectorXD<complex<double>,3> local_vector) const;
        double get_Radius(); // the radius of the circumscribed circle
        double get_internal_radius();
        double get_max_edge();
        triangle_parameters():elnum(0), t_k(0.0), t_star(0.0), tg_a1(0.0), tg_a2(0.0), s_t(0.0), trg_num(0)
        {
            VectorXD<double,3>::set_scalar_zero(0.0);
            x_star.zerofied();
            x0.zerofied();
            r1.zerofied();
            r2.zerofied();
            n.zerofied();
            x1.zerofied();
            x2.zerofied();
            // для глобальных координат
            VectorXD<double,3>::set_scalar_zero(0.0);
            Block<double,3>::set_one(1.0);
            Block<double,3>::set_zero(0.0);
            // для локальных координат
            VectorXD<double,2>::set_scalar_zero(0.0);
            Block<double,2>::set_one(1.0);
            Block<double,2>::set_zero(0.0);
            lam_negate = false;
            reversed = false;
        }
        triangle_parameters(const triangle_parameters & arg):elnum(arg.elnum), t_k(arg.t_k),
        t_star(arg.t_star), tg_a1(arg.tg_a1), tg_a2(arg.tg_a2), s_t(arg.s_t), trg_num(arg.trg_num)
        {
            VectorXD<double,3>::set_scalar_zero(0.0);
            x_star = arg.x_star;
            x0 = arg.x0;
            r1 = arg.r1;
            r2 = arg.r2;
            n = arg.n;
            x1 = arg.x1;
            x2 = arg.x2;
        }

        virtual ~triangle_parameters(){};
        const triangle_parameters &operator=(const triangle_parameters & arg)
        {
            tg_a2 = arg.tg_a2;
            s_t = arg.s_t;
            trg_num = arg.trg_num;
            t_star = arg.t_star;
            tg_a1 = arg.tg_a1;
            elnum = arg.elnum;
            t_k = arg.t_k;
            x_star = arg.x_star;
            x0 = arg.x0;
            r1 = arg.r1;
            r2 = arg.r2;
            n = arg.n;
            x1 = arg.x1;
            x2 = arg.x2;
            return *this;
        }
};
