/*
Интерфейс скалярных базисных функций нужен для
разложения по басису тех скалярных произведений
векторных функций, которые используются при
вычислении оператора B. В частности его использует
Scalar_Doub_Layer_Potential.

ВНИМАНИЕ!!! Данные функции используются сами по себе.
Никаких операторов от них вычислять не нужно в вариационной
постановке. Однако

ВНИМАНИЕ!!!
К данному классу должен быть приложен объект, который
отвечает за разложение по базису векторной функции,
умноженой на другой скалярный вектор.
*/
#pragma once
#include "TriangleParam.h"
#include "VectorXD.hpp"
#include "VectorBasisInterface.hpp"
#include "GaussRuleInfo.h"
#include <vector>
#include "base.hpp"

using namespace std;

struct scalCache_elem
{
	VectorXD<double,3> loc_arg; // значения даны в локальных координатах используемого треугольника
        VectorXD<double,3> glob_curl; // значение граничного ротора скалярной функции, равное векторному произведению градиента на нормаль
	double value;
};
// структура данных, упрявляющая содержимым кэш-данных функции
class scalCache_rule
{
public:
	uint next_value; // счетчик очередного значения
	vector< scalCache_elem > cache;
	bool use_cache;
public:
	scalCache_rule():use_cache(false),next_value(0){};
	void set_size(GaussRule &gr); // задает размер вектору
};

class ScalarBasisInterface
{
public:
	triangle_parameters current_parameter;
	scalCache_rule Cache_values;
	double InterpValue;
	VectorXD<double,2> InterpGrad;
	// set-функция определения параметров области
	virtual int get_local_base_count()=0;	// возвращает количество базисных функций (динейно независимые)
	virtual void set_current_trig_parameter(int base_num, const triangle *area, const VectorXD<double,3> *points, uint elnum)=0;
	virtual double f_value(VectorXD<double,2> Gauss_point)=0; // вычисляет значение функции в точке интегрирования
	virtual VectorXD<double,2> f_gradient(VectorXD<double,3> Local_arg)=0; // нужна для аналитического продолжения функции за носитель (градиент в ЛСК треугольника)
	// Эта функция нужна для вычисления численной части векторных потенциаллов С и B
	VectorXD<double,3> get_Global_point(VectorXD<double,2> Gauss_point); // точка Гаусса в глобальных системах координат
	VectorXD<double,3> get_Local_point(VectorXD<double,2> Gauss_point); // точка Гаусса в локальных системах координат текущего треугольника
	// аналитическое продолжение функции за носитель
	virtual double f_value( VectorXD<double,3> local_arg );
	// для соответствия классу-раскладывателю по базисным функциям этого типа
	virtual int get_type()=0;
	// функция, оптимизирующая вычисление (для численного интегрирования)
	scalCache_elem &get_cache_value(VectorXD<double,2> &GaussPoint);
    // функция создания копии функции (чисто виртуальная, подлежит определению для каждого класса отдельно)
    virtual ScalarBasisInterface *create_copy()const=0;
    virtual ~ScalarBasisInterface();
};
struct ScalarBasisAnsamble
{
	uint vert_basis, edge_basis; // Количество локальных реберных и вершинных функций должно быть кратно 3-м
	vector<ScalarBasisInterface *> basis_functions; // size - edge_basis - vert_basis = количество элементов, ассоциированных с элементом
	ScalarBasisAnsamble():vert_basis(0),edge_basis(0){}
        void create_copy(vector<ScalarBasisInterface*> &copy);
	virtual ~ScalarBasisAnsamble();
};
VectorXD<double,3> B_curl(ScalarBasisInterface *f, VectorXD<double,2> gauss_points);

VectorXD<double,3> B_curl(ScalarBasisInterface *f, VectorXD<double,3> loc_point);

// структура для хранения разложения по базису
struct basis_storage
{
	std::vector<ScalarBasisInterface *> basis_functions;
	std::vector< double > weights;
	// функция, которая выражает значение в виде линейной комбинации
	double get_value(VectorXD<double,3> Glob_arg);
	VectorXD<double,3> get_gradient(VectorXD<double,3> Glob_arg); // проверим так же разложение градиента (в глобальных координатах)
	void clear();
	virtual ~basis_storage();
};
// класс, который разлагает проекцию векторной функции на линейную комбинацию скалярных функций
class VectorBasisDecomposerInterface
{
public:
	triangle_parameters current_parameter; // параметры треугольника, на котором раскладываются базисные функции
	virtual int get_type()=0; // для соответствия функциям, указанным выше
	virtual bool Decompose_lambda
		(VectorBasisInterface *funct,
		 VectorXD<double,3> &n,
		 basis_storage &combination)=0; // разложение скалярными базисными функциями
	virtual bool Decompose_u
		(VectorBasisInterface *funct,
		 VectorXD<double,3> &n,
		 basis_storage &combination)=0; // разложение скалярными базисными функциями
	virtual bool CreateCombination
		(VectorBasisInterface *funct,
		 basis_storage &combination)=0; // отвечает за заполнения контейнера скалярных базисных функций и определение нормали
	virtual double u_test(VectorBasisInterface *funct, VectorXD<double,3> n, basis_storage &combination, VectorXD<double,3> &glob_arg){return -1e+8;}
};
