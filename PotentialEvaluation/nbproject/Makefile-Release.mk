#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/ComplexIntegral.o \
	${OBJECTDIR}/src/DoubleLayerHierarchicalPotential.o \
	${OBJECTDIR}/src/DoubleLayerPotential.o \
	${OBJECTDIR}/src/GaussRuleInfo.o \
	${OBJECTDIR}/src/GmshReader.o \
	${OBJECTDIR}/src/Integral.o \
	${OBJECTDIR}/src/LinearBasisFunction.o \
	${OBJECTDIR}/src/LinearScalarFunctions.o \
	${OBJECTDIR}/src/Mod_Doub_Pot_by_parts.o \
	${OBJECTDIR}/src/Modyfied_Doub_Layer_Potential.o \
	${OBJECTDIR}/src/ScalarBasisInterface.o \
	${OBJECTDIR}/src/ScalarConstBasisFunction.o \
	${OBJECTDIR}/src/Scalar_Doub_Layer_Potential.o \
	${OBJECTDIR}/src/Scalar_Doub_Layer_Potential_With_Subdivisions.o \
	${OBJECTDIR}/src/SingleLayerPotential.o \
	${OBJECTDIR}/src/SingleLayerSurfacePotential.o \
	${OBJECTDIR}/src/TriangleParam.o \
	${OBJECTDIR}/src/TrigInternalPoints.o \
	${OBJECTDIR}/src/UVdecomposer.o \
	${OBJECTDIR}/src/Vect_Doub_Layer_Potential.o \
	${OBJECTDIR}/src/VectorBasisInterface.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libPotentialEvaluation.${CND_DLIB_EXT}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libPotentialEvaluation.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libPotentialEvaluation.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} -shared -fPIC

${OBJECTDIR}/src/ComplexIntegral.o: src/ComplexIntegral.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ComplexIntegral.o src/ComplexIntegral.cpp

${OBJECTDIR}/src/DoubleLayerHierarchicalPotential.o: src/DoubleLayerHierarchicalPotential.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/DoubleLayerHierarchicalPotential.o src/DoubleLayerHierarchicalPotential.cpp

${OBJECTDIR}/src/DoubleLayerPotential.o: src/DoubleLayerPotential.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/DoubleLayerPotential.o src/DoubleLayerPotential.cpp

${OBJECTDIR}/src/GaussRuleInfo.o: src/GaussRuleInfo.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/GaussRuleInfo.o src/GaussRuleInfo.cpp

${OBJECTDIR}/src/GmshReader.o: src/GmshReader.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/GmshReader.o src/GmshReader.cpp

${OBJECTDIR}/src/Integral.o: src/Integral.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Integral.o src/Integral.cpp

${OBJECTDIR}/src/LinearBasisFunction.o: src/LinearBasisFunction.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/LinearBasisFunction.o src/LinearBasisFunction.cpp

${OBJECTDIR}/src/LinearScalarFunctions.o: src/LinearScalarFunctions.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/LinearScalarFunctions.o src/LinearScalarFunctions.cpp

${OBJECTDIR}/src/Mod_Doub_Pot_by_parts.o: src/Mod_Doub_Pot_by_parts.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Mod_Doub_Pot_by_parts.o src/Mod_Doub_Pot_by_parts.cpp

${OBJECTDIR}/src/Modyfied_Doub_Layer_Potential.o: src/Modyfied_Doub_Layer_Potential.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Modyfied_Doub_Layer_Potential.o src/Modyfied_Doub_Layer_Potential.cpp

${OBJECTDIR}/src/ScalarBasisInterface.o: src/ScalarBasisInterface.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ScalarBasisInterface.o src/ScalarBasisInterface.cpp

${OBJECTDIR}/src/ScalarConstBasisFunction.o: src/ScalarConstBasisFunction.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ScalarConstBasisFunction.o src/ScalarConstBasisFunction.cpp

${OBJECTDIR}/src/Scalar_Doub_Layer_Potential.o: src/Scalar_Doub_Layer_Potential.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Scalar_Doub_Layer_Potential.o src/Scalar_Doub_Layer_Potential.cpp

${OBJECTDIR}/src/Scalar_Doub_Layer_Potential_With_Subdivisions.o: src/Scalar_Doub_Layer_Potential_With_Subdivisions.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Scalar_Doub_Layer_Potential_With_Subdivisions.o src/Scalar_Doub_Layer_Potential_With_Subdivisions.cpp

${OBJECTDIR}/src/SingleLayerPotential.o: src/SingleLayerPotential.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/SingleLayerPotential.o src/SingleLayerPotential.cpp

${OBJECTDIR}/src/SingleLayerSurfacePotential.o: src/SingleLayerSurfacePotential.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/SingleLayerSurfacePotential.o src/SingleLayerSurfacePotential.cpp

${OBJECTDIR}/src/TriangleParam.o: src/TriangleParam.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/TriangleParam.o src/TriangleParam.cpp

${OBJECTDIR}/src/TrigInternalPoints.o: src/TrigInternalPoints.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/TrigInternalPoints.o src/TrigInternalPoints.cpp

${OBJECTDIR}/src/UVdecomposer.o: src/UVdecomposer.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/UVdecomposer.o src/UVdecomposer.cpp

${OBJECTDIR}/src/Vect_Doub_Layer_Potential.o: src/Vect_Doub_Layer_Potential.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Vect_Doub_Layer_Potential.o src/Vect_Doub_Layer_Potential.cpp

${OBJECTDIR}/src/VectorBasisInterface.o: src/VectorBasisInterface.cpp
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/VectorBasisInterface.o src/VectorBasisInterface.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
